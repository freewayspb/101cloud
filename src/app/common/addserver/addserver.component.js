import controller from './addserver.controller';
import template from './addserver.tpl.html';

const AddServerComponent = {
  controller,
  template
};

export default AddServerComponent;
