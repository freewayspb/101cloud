export default {
  select: {
    search: {
      placeholder: 'Search',
      data: '',
      options: {
        isSearch: true
      }
    },
    filter: {
      data: [{displayName: 'All'}],
      placeholder: 'Filter by'
    },
    radiogroup: {
      data: [{title: 'Select All', value: true},
        {title: 'Reset All', value: false}],
      options: {
        classNameItem: 'components-dc-service__radio'
      },
      model: false
    }
  },
  license: {
    licenseKey: {
      title: 'License Key',
      data: '',
      options: {
        classNameWrapper: ''
      }
    },
    haveLicense: {
      data: [{title: 'I already own a License', value: true},
        {title: 'I need the License from the Provider', value: false}],
      options: {
        classNameItem: 'step-license__radio'
      },
      model: true
    }
  },
  general: {
    cloud: {
      title: 'Cloud',
      data: [{displayName: 'Select'}]
    },
    iProvider: {
      title: 'Infrastructure Provider',
      data: [{displayName: 'Select'}]
    },
    subscription: {
      title: 'Subscription',
      data: [{displayName: 'Select'}]
    },
    datacenter: {
      title: 'Datacenter',
      data: [{displayName: 'Select'}]
    },
    serverName: {
      title: 'Server Name',
      placeholder: 'Enter Server Name',
      data: ''
    },
    domainName: {
      title: 'Domain Name',
      placeholder: 'Enter Domain Name',
      data: ''
    },
    operatingSystemVersion: {
      title: 'Operating System Version',
      data: [
        {displayName: 'Windows Server 2016 Standard Edition'},
        {displayName: 'Windows Server 2016 Datacenter Edition'},
        {displayName: 'Windows Server 2012 R2 Standard Edition'},
        {displayName: 'Windows Server 2012 R2 Datacenter Edition'}
      ]
    },
    description: {
      title: 'Description*',
      placeholder: 'Enter Description'
    },
    businessOwner: {
      data: [],
      title: 'Business Owner'
    },
    businessDeputyOwner: {
      data: [],
      title: 'Business Deputy Owner'
    },
    technicalOwner: {
      data: [],
      title: 'Technical Owner'
    },
    technicalDeputyOwner: {
      data: [],
      title: 'Technical Deputy Owner'
    },
    ram: {
      title: 'RAM',
      data: [{displayName: 'Select amount of RAM'}]
    },
    cores: {
      title: 'CPU Cores',
      data: [{displayName: 'Select amount of Cores'}]
    },
    diskspaces: {
      title: 'Diskspace for OS',
      data: [{displayName: '128 GB'}, {displayName: '256 GB'}, {displayName: '512 GB'}]
    },
    disk: {
      data: {
        title: 'Disk',
        model: true,
        valueTrue: true,
        valueFalse: false
      },
      options: {
        className: 'add-server__step-general_toggleDisks'
      }
    }
  },
  maintenance: {
    schedule: {
      title: 'From - To',
      data: {},
      range: true
    },
    availability: {
      title: 'Patching',
      placeholder: 'Select timeslot',
      data: [
        {
          displayName: 'Every Day'
        }, {
          displayName: 'Every Week'
        }, {
          displayName: 'Every Month'
        }, {
          displayName: 'Never'
        }
      ],
      active: {}
    },
    maintenanceWindow: {
      title: 'Maintenace Window',
      data: [
        {
          displayName: 'Window1'
        },
        {
          displayName: 'Window2'
        }
      ]
    },
    maintenanceType: {
      title: 'Maintenance Type',
      data: [
        {
          displayName: 'Save State'
        },
        {
          displayName: 'Shutdown'
        },
        {
          displayName: 'Pause'
        }
      ]
    },
    backupServer: {
      title: 'Backup Server',
      data: [
        {
          displayName: 'Yes'
        },
        {
          displayName: 'No'
        }
      ]
    },
    backupInterval: {
      title: 'Backup Interval',
      data: [
        { displayName: 'Hourly' },
        { displayName: 'Daily' },
        { displayName: 'Weekly' },
        { displayName: 'Monthly' }
      ]
    },
    dataRetentionTime: {
      title: 'Data Retention Time',
      data: [
        { displayName: '1 Week' },
        { displayName: '2 Weeks' },
        { displayName: '1 Month' },
        { displayName: '1 Year' }
      ]
    },
    monitoringService: {
      title: 'Monitoring Server',
      data: [
        { displayName: 'Yes' },
        { displayName: 'No' }
      ]
    },
    serviceManagementContracts: {
      title: 'Service Management Contracts',
      data: [
        { displayName: 'Support' },
        { displayName: 'Consolidated Billing' },
        { displayName: 'Operations' }
      ]
    }
  }
};
