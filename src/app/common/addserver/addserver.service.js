class AddServerService {

  /*@ngInject*/

  constructor($http, dataService) {
    this.$http = $http;
    this.dataService = dataService;
    this.steps = [
      {
        title: 'general',
        name: 'general'
      }, {
        title: 'license',
        name: 'license'
      }, {
        title: 'maintenance',
        name: 'maintenance'
      }
    ];
    this.informationPanelInfo = {
      type: 'price',
      delivery: '2 Days'
    };
  }

  getItems(item) {
    return this.dataService.getItems(item)
      .then(result => result, error => {
        console.error(':::: ERROR IN DEPCONF -> ADDSERVER SERVICE GETITEMS ::::', error);
      });
  }

  proceed() {
    this.classService.isWrapperWide = true;
    this.informationPanelInfo.priceList = _.map(this.getSelectedItems(), element => {
      return {
        name: element.name,
        price: element.price,
        inputs: {
          qty: {
            data: 1,
            placeholder: 1,
            options: {
              classNameInput: 'information-panel-qty-no-arrows',
              type: 'number',
              min: 1
            }
          }
        }
      };
    });
    this.state = 'configure';
    this.notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: true});
    this.notificationService.broadcast('onInformationChange', this.informationPanelInfo);
  }
}

export default AddServerService;
