import controller from './iz-add-server-general.controller';
import template from './iz-add-server-general.tpl.html';

const AddServerGeneralComponent = {
  controller,
  template
};

export default AddServerGeneralComponent;
