import inputs from '../../addserver.inputs.model';
import HardwareItem from './HardwareItem.class';

function AddServerGeneralController() {
  class Letter {
    constructor(letter) {
      this.displayName = letter;
      this.isLetter = true;
    }
  }
  const vm = this;
  const letters = 'EFGHIJKLMNOPQRSTUVWXYZ';
  this.availableLetters = [];
  for (const letter of letters) {
    this.availableLetters.push(new Letter(letter));
  }
  this.adapters = [];
  this.disks = [];

  function removeLetter(letter) {
    // TODO. It seems that letter is an object, not string. Check if indexOf always can compare objects (works in Chrome)
    const letterIndex = vm.availableLetters.indexOf(letter);
    vm.availableLetters.splice(letterIndex, 1);
  }
  function addLetter(letter) {
    if (vm.availableLetters.length === 0 || vm.availableLetters[0].displayName > letter) {
      vm.availableLetters.splice(0, 0, new Letter(letter));
      return;
    }
    let prevLetterIndex = -1;
    // seek for place to insert it in alphabetical order
    vm.availableLetters.forEach((element, index) => {
      if (element.displayName < letter) {
        prevLetterIndex = index;
      } else if (element.displayName === letter) {
        prevLetterIndex = -1;
      }
    });
    if (prevLetterIndex + 1) {
      vm.availableLetters.splice(prevLetterIndex, 0, new Letter(letter));
    }
  }
  this.$onInit = () => {
    this.inputs = inputs;
  };

  this.driveLetterCallbacks = {
    onChange: selected => {
      _.each(letters, letter => {
        let hasLetter = false;
        _.each(this.disks, disk => {
          if (disk.driveLetterOption.selected.value && disk.driveLetter.selected.displayName === letter) {
            hasLetter = true;
          } else if (!disk.driveLetterOption.selected.value) {
            disk.driveLetter.selected = {};
          }
        });
        if (!hasLetter) {
          addLetter(letter);
        }
      });
      if (selected.isLetter) {
        removeLetter(selected);
      }
      checkAvailableLetters();
    }
  };

  /**
   * @name addItems
   * @desc Method for adding items on components server -> hardware step (Network Adapters and Disks)
   * @returns Void
   * @param {string} entity shall be 'adapters' or 'disks'
   */
  this.addItems = entity => {
    if (this.availableLetters.length > 0) {
      const Item = new HardwareItem(entity, vm.availableLetters);
      if (entity === 'disks') {
        Item.driveLetter.data = vm.availableLetters;
        removeLetter(vm.availableLetters[0]);
        checkAvailableLetters();
      }
      this[entity].push(Item);
    }
  };
  this.addItems('disks');
  this.addItems('disks');
  this.addItems('disks');
  this.addItems('adapters');

  /**
   * @name removeItems
   * @desc Method for removing items on components-server -> hardware step (Network Adapters and Disks)
   * @returns Void
   * @param {string} entity shall be 'adapters' or 'disks'
   * @param {string} index shall be an index of 'adapters' or 'disks' array
   */
  this.removeItems = (entity, index) => {
    if (entity === 'disks') {
      addLetter(this.disks[index].driveLetter.selected.displayName);
      checkAvailableLetters();
    }
    this[entity].splice(index, 1);
  };

  function checkAvailableLetters() {
    vm.isAddingDisabled = (vm.availableLetters.length === 0)
  }
}

export default AddServerGeneralController;
