/**
 * @name HardwareItem
 * @desc Class which describes Network Adapter or Disk
 * @returns {Object}
 * @param {string} entity shall be 'adapters' or 'disks'
 */
export default class HardwareItem {
  constructor(entity, letterArray) {
    if (entity === 'adapters') {
      this.name = {
        title: 'Adapter Name',
        placeholder: 'Enter Name'
      };
      this.zone = {
        data: [
          {displayName: 'Green Zone'},
          {displayName: 'Red Zone'},
          {displayName: 'DMZ'},
          {displayName: 'Lab'},
          {displayName: 'Production'}
        ],
        title: 'Zone',
        options: {
          classNameWrapper: 'no-margin'
        }
      };
      this.subnet = {
        title: 'Subnet',
        placeholder: 'Enter Subnet'
      };
      this.ipAddressAllocation = {
        data: [{displayName: 'DHCP Reservation'}, {displayName: 'Fixed'}],
        title: 'IP Address Allocation',
        options: {
          classNameWrapper: 'no-margin'
        }
      };
    } else if (entity === 'disks') {
      this.amount = {
        data: [{displayName: 'Select Amount of Disks'}],
        title: 'Amount of Disks'
      };
      this.size = {
        data: [
          {displayName: '100 GB'},
          {displayName: '200 GB'},
          {displayName: '500 GB'},
          {displayName: '1 TB'},
          {displayName: '1.5 TB'},
          {displayName: '2 TB'}
          ],
        title: 'Size',
        options: {
          classNameWrapper: 'no-margin'
        }
      };
      this.fileSystem = {
        data: [{displayName: 'NTFS'}, {displayName: 'ReFS'}, {displayName: 'FAT32'}],
        title: 'File System',
        options: {
          classNameWrapper: 'no-margin'
        }
      };
      this.partitionType = {
        data: [
          {displayName: 'GPT'},
          {displayName: 'MBR'}
        ],
        title: 'Partition Type',
        options: {
          classNameWrapper: 'no-margin'
        }
      };
      this.label = {
        title: 'Disk Label',
        placeholder: 'Disk Label'
      };
      this.driveLetterOption = {
        data: [
          {displayName: 'Drive Letter', value: true, isLetterOption: true},
          {displayName: 'Mount Point', value: false, isLetterOption: true}
        ],
        title: ' ',
        selected: {displayName: 'Drive Letter', value: true, isLetterOption: true},
        options: {
          classNameWrapper: 'no-margin'
        }
      };
      this.driveLetter = {
        data: [],
        title: ' ',
        options: {
          classNameWrapper: 'no-margin'
        },
        selected: {displayName: letterArray[0].displayName, isLetter: true}
      };
    }
  }
}
