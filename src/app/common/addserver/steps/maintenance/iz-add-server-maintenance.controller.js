import inputs from '../../addserver.inputs.model';

function AddServerMaintenanceController() {

  this.$onInit = () => {
    this.inputs = inputs;
  };
}

export default AddServerMaintenanceController;
