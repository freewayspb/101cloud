import controller from './iz-add-server-maintenance.controller';
import template from './iz-add-server-maintenance.tpl.html';

const AddServerMaintenanceComponent = {
  controller,
  template
};

export default AddServerMaintenanceComponent;
