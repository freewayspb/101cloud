import controller from './iz-add-server-license.controller';
import template from './iz-add-server-license.tpl.html';

const AddServerLicenseComponent = {
  controller,
  template
};

export default AddServerLicenseComponent;
