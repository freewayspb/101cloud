import inputs from '../../addserver.inputs.model';

function AddServerLicenseController(currencyService) {

  const vm = this;

  this.$onInit = () => {
    this.inputs = inputs;
  };

  /**
   * @name toggleLicense
   * @desc Method for adding/removing class for License input regarding of entered text and HaveLicense toggler.
   * @returns Void
   */
  function toggleLicense() {
    vm.inputs.license.licenseKey.options.classNameWrapper =
      (vm.inputs.license.licenseKey.data.length > 0 && vm.inputs.license.haveLicense.model)
        ? 'step-license_license-key'
        : '';
  }
  this.callbacks = {
    onChange: toggleLicense
  };

  this.getPriceStr = price => currencyService.getPriceString(price);
}

export default AddServerLicenseController;
