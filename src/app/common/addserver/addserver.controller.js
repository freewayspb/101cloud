import _ from 'lodash';
import inputs from './addserver.inputs.model';

function AddServerController($state, notificationService, addServerService, classService) {

  const vm = this;

  this.state = 'select';
  this.inputs = inputs;
  this.screenStates = addServerService.screenStates;
  this.informationPanelInfo = addServerService.informationPanelInfo;
  this.$state = $state;
  this.classService = classService;
  this.notificationService = notificationService;

  this.$onInit = () => {
    vm.progressbar = {
      steps: addServerService.steps
    };
    vm.inputs.select.radiogroup.model = false;
    addServerService.getItems('server-new-server.json')
      .then(serverList => {
        this.serverList = serverList;
        changeAll();
        addServerService.getItems('maintenance.json')
          .then(maintenanceInputs => {
            this.maintenance = maintenanceInputs;
          });
      });
  };

  this.selectCallbacks = {
    onChange: changeAll
  };

  function changeAll() {
    const isSelected = vm.inputs.select.radiogroup.model === 'true';
    _.each(vm.serverList, element => {
      element.isSelected = isSelected;
    });
  }

  this.updateRadios = () => {
    const selected = this.getSelectedItems().length;
    this.inputs.select.radiogroup.model =
      (selected < this.serverList.length && selected > 0) ? 'undefined' : Boolean(selected);
  };

  this.getSelectedItems = () => _.filter(this.serverList, element => element.isSelected);

  this.continue = addServerService.proceed;

  /**
   * @name $onDestroy
   * @desc When we leave this component we need to broadcast an event for side information panel
   * @returns Void
   */
  this.$onDestroy = () => {
    notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: false});
    classService.isWrapperWide = false;
  };
}

export default AddServerController;
