import controller from './loader.controller';
import template from './loader.tpl.html';

const LoaderComponent = {
  controller,
  template
};

export default LoaderComponent;
