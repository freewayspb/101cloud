class LoaderService {
  constructor() {
    this.isLoading = {
      value: false
    };
    this.sessions = [];
  }

  setLoad(value, ...sessionKey) {

    const currentSession = _.map(sessionKey, sessionObj => {
      let sessionStringValue;
      if (typeof sessionObj === 'object') {
        sessionStringValue = JSON.stringify(sessionObj);
      } else {
        sessionStringValue = sessionObj;
      }
      return sessionStringValue;
    }).join(';');

    const $body = $('body');
    if (value) {
      this.sessions.push(currentSession);
      this.isLoading.value = true;
      $body.css({'overflow': 'hidden'});
    } else {
      const sessionIndex = _.findIndex(this.sessions, session => session === session);
      this.sessions.splice(sessionIndex, 1);
      if (this.sessions.length === 0) {
        $body.css({'overflow': 'scroll'});
        this.isLoading.value = false;
      }
    }
  }
}

export default LoaderService;
