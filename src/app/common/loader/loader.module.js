import component from './loader.component';
import service from './loader.service';

const loader = angular
  .module('loader', [])
  .component('izLoader', component)
  .service('loaderService', service)
  .name;

export default loader;
