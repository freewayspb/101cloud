function LoaderController(loaderService) {
  this.isLoading = loaderService.isLoading;
}

export default LoaderController;
