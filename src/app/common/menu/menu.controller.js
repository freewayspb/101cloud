function MenuController($state, dataService, authService) {
  /* ngInject */
  const vm = this;
  vm.$state = $state;
  dataService.getItems('leftMenu.json').then(
    result => {vm.menuList = result;},
    err => {
      console.error(err);
    });
  vm.logout = () => {
    authService.logout();
  };
}

export default MenuController;
