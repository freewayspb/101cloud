import MenuComponent from './menu.component';

const menu = angular
  .module('leftMenu', [])
  .component('leftMenu', MenuComponent)
  .name;

export default menu;
