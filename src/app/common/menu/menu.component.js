import controller from './menu.controller';
import template from './menu.tpl.html';

const MenuComponent = {
  controller,
  template
};

export default MenuComponent;
