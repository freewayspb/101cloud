class AddServiceService {

  /*@ngInject*/

  constructor($http, dataService) {
    this.$http = $http;
    this.dataService = dataService;
    this.inputs = {
      search: {
        data: '',
        placeholder: 'Search',
        options: {
          isSearch: true
        }
      },
      filter: {
        data: [{displayName: 'All'}],
        placeholder: 'Filter'
      },
      radiogroup: {
        data: [{title: 'Select All', value: true},
          {title: 'Reset All', value: false}],
        options: {
          classNameItem: 'components-dc-service__radio'
        },
        model: false
      }
    };
  }

  getItems(item) {
    return this.dataService.getItems(item)
      .then(result => result, error => {
        console.error(':::: ERROR IN OPERATIONS -> ADDSERVICE SERVICE GETITEMS ::::', error);
      });
  }
}

export default AddServiceService;
