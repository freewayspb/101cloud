function AddServiceController(addServiceService, notificationService, $state, classService) {
  const vm = this;
  this.isAllSelected = true;
  this.inputs = addServiceService.inputs;

  this.$onInit = () => {
    this.currentState = $state.current.name.split('.')[1];
    classService.isWrapperWide = false;
    addServiceService.getItems('serviceTypes.json')
      .then(result => {
        this.serviceTypes = result;
        changeAll();
      });
  };

  this.selectCallbacks = {
    onChange: changeAll
  };
  function changeAll() {
    const isSelected = vm.inputs.radiogroup.model === 'true';
    _.each(vm.serviceTypes, element => {
      element.isSelected = isSelected;
    });
  }

  this.updateRadios = () => {
    const seleced = this.getSelectedItems().length;
    this.inputs.radiogroup.model = (seleced < this.serviceTypes.length && seleced > 0) ? 'undefined' : Boolean(seleced);
  };

  this.getSelectedItems = () => _.filter(this.serviceTypes, element => element.isSelected);

  this.proceed = () => {
    classService.isWrapperWide = true;
    $state.go(`app.${this.currentState}.services.addservice.configure`);
    notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: true});
    notificationService.broadcast('onInformationChange', {
      type: 'price',
      priceList: _.map(this.getSelectedItems(), element => {
          return {
            name: element.name,
            price: element.price,
            inputs: {
              qty: {
                data: 1,
                placeholder: 1,
                options: {
                  classNameInput: 'information-panel-qty-no-arrows',
                  type: 'number',
                  min: 1
                }
              }
            }
          };
        }),
      delivery: '2 Days',
      message: 'Please enter your payment information for verification. ' +
      'Your email and phone may be used to contact you regarding information for components subscribers, ' +
      'billing and your subscription.'
    });
  };

  this.$onDestroy = () => {
    classService.isWrapperWide = false;
    notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: false});
  };
}

export default AddServiceController;
