import controller from './addservice.controller';
import template from './addservice.tpl.html';

const AddServiceComponent = {
  controller,
  template
};

export default AddServiceComponent;
