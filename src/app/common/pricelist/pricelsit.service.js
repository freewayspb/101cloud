class SPLAService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Product Name',
        dataProperty: 'Name',
        enableSorting: true
      },
      {
        name: 'Product ID',
        dataProperty: 'Pid',
        enableSorting: true
      },
      {
        name: 'Price',
        dataProperty: 'Price',
        enableSorting: true,
        specials: {
          type: 'price',
          currency: 'CHF'
        }
      },
      {
        name: 'Subscription Type',
        dataProperty: 'Type',
        enableSorting: true
      }
    ];
    this.inputs = {
      country: {
        placeholder: 'Country',
        selected: {
          Capital: "Bern",
          Currency: "CHF",
          Id: 43,
          Name: "Switzerland",
          Native: "Schweiz",
          Phone: "41",
          Region: {Id: 4, Short: "EU", Name: "Europe"},
          Short: "CH"
        }
      },
      reseller: {
        placeholder: 'Reseller'
      },
      currency: {
        placeholder: 'Currency',
        data: [
          {displayName: 'CHF', value: 'chf', symbol: 'CHF'},
          {displayName: 'USD', value: 'usd', symbol: '$'},
          {displayName: 'EUR', value: 'eur', symbol: '€'}
        ]
      },
      period: {
        placeholder: 'Period',
        data: [{displayName: 'from X to Y', value: 'all'}]
      },
      search: {
        placeholder: 'Search',
        options: {
          isSearch: true
        }
      }
    }
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> SPLA SERVICE ::::', error);
    });
  }
}

export default SPLAService;
