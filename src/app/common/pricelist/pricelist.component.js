import controller from './pricelist.controller';
import template from './pricelist.tpl.html';

const SPLAPricelistComponent = {
  controller,
  template
};

export default SPLAPricelistComponent;
