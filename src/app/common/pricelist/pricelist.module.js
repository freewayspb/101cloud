import angular from 'angular';

import component from './pricelist.component';
import service from './pricelsit.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.pricelist', {
      url: '/pricelist/:entity',
      template: '<pricelist />'
    })
};

config.$inject = ['$stateProvider'];

const spla = angular
  .module('app.pricelist', [])
  .component('pricelist', component)
  .service('pricelistService', service)
  .config(config)
  .name;

export default spla;
