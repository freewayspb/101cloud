function PricelistController(pricelistService, $stateParams, countryService, $q, headerService) {
  this.isLoading = true;
  this.$onInit = () => {
    this.inputs = pricelistService.inputs;

    countryService.countriesQ
      .then(countries => {
        this.inputs.country.data = countries;
      }, error => {
        console.error('asd', error);
      });

    headerService.getTenantsPromise()
      .then(tenants => {
        this.inputs.reseller.data = tenants;
      }, error => {
        console.error('sad', error);
      });
    $q.all([countryService.countriesQ, headerService.getTenantsPromise()])
      .then(() => {
        this.isLoading = false;
      })
  };

  this.canGetPricelist = () => {
    return this.inputs.country.selected &&
        this.inputs.reseller.selected &&
        this.inputs.currency.selected &&
        this.inputs.period.selected;
  };

  this.getPricelist = () => {
    this.isLoading = true;
    this.isPriceListLoaded = false;
    
    const country = this.inputs.country.selected.Short.toLowerCase(),
      reseller = this.inputs.reseller.selected.Name.toLowerCase(),
      currency = this.inputs.currency.selected.value,
      period = this.inputs.period.selected.value,
      entity = $stateParams.entity,
      queryString = `pricelist-${entity}-${country}-${reseller}-${currency}-${period}.json`;

    pricelistService.getItems(queryString)
      .then(result => {

        pricelistService.headers[2].specials.currency = this.inputs.currency.selected.symbol;

        this.tableOptions = {
          data: result,
          headers: pricelistService.headers,
          isToolbarHidden: true,
          hideHeader: true
        };
        this.isLoading = false;
        this.isPriceListLoaded = true;
      });
  };
}

export default PricelistController;
