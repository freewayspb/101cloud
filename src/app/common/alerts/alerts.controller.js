/* ngInject */
function NotificationController(alertsService) {
  const vm = this;
  vm.$onInit= () => {
    vm.alerts = alertsService.alerts = [];
  };

  vm.closeAlert = function(index) {
    vm.alerts.splice(index, 1);
  };
}

export default NotificationController;
