import NotificationComponent from './alerts.component';
import NotificationService from './alerts.service';

const menu = angular
  .module('notification', [])
  .component('notification', NotificationComponent)
  .service('alertsService', NotificationService)
  .name;

export default menu;
