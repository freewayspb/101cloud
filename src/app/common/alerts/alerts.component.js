import controller from './alerts.controller';
import template from './alerts.tpl.html';

const MenuComponent = {
  controller,
  transclude: true,
  template
};

export default MenuComponent;
