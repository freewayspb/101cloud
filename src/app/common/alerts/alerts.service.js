class AlertsService {

  /*@ngInject*/

  constructor() {
    this.alerts = [];
  }
  getAlert() {
    return this.alerts;
  }
  setAlert(alert) {
    if (typeof alert === 'string') {
      alert = {msg: alert, type: 'danger'};
    }

    this.alerts.push({msg: alert.msg || '', type: alert.type, showTime: alert.showTime});
  }
}

export default AlertsService;

