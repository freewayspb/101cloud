import template from './radio.tpl.html';
import controller from './radio.controller';

const RadioComponent = {
  template,
  controller,
  bindings: {
    model: '=',
    rawCallbacks: '=',
    item: '='
  }
};

export default RadioComponent;
