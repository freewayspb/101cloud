function IzTableRadioController() {
  this.$onInit = () => {
    this.value = (this.value === 'true' || this.value === true);
    this.callbacks = {
      onChange: this.rawCallbacks && this.rawCallbacks.onChange.bind(this)
    };
  };
}

export default IzTableRadioController;
