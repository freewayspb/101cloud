import component from "./cloud-type-integrations.component";
import service from "./cloud-type-integrations.service";

const CloudTypeIntegrations = angular
  .module('app.cloud-type-integrations', [])
  .component('cloudTypeIntegrations', component)
  .service('cloudTypeIntegrationsService', service)
  .name;

export default CloudTypeIntegrations;
