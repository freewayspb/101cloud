import template from "./cloud-type-integrations.tpl.html";
import controller from "./cloud-type-integrations.controller";

const ActionsComponent = {
  template,
  controller,
  bindings: {
    value: '<'
  }
};

export default ActionsComponent;
