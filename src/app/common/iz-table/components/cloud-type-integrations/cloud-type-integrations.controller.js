function ActionButtonController($state) {
  this.$onInit = () => {
    this.isVisible = ((this.item.entity.status !== -1 && this.context === true) || !$state.is('app.depconf.clouds.list'));
  };
}

export default ActionButtonController;
