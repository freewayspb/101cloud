function PriceDisplayController(currencyService) {
  this.$onInit = () => {
    if (this.item.entity.Subscriptions && this.item.entity.Subscriptions[2] === 'down') {
      this.isEntityBad = true;
    }

    this.getPrice = price => {
      if (this.currency === 'undefined') {
        this.currency = this.item.entity.Currency || '';
      }
      return currencyService.getPriceString(price, '');
    };
  };

}

export default PriceDisplayController;
