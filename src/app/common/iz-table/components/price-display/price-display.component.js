import template from './price-display.tpl.html';
import controller from './price-display.controller';

const PriceDisplayComponent = {
  template,
  controller,
  bindings: {
    value: '<',
    currency: '@',
    suffix: '@',
    item: '='
  }
};

export default PriceDisplayComponent;
