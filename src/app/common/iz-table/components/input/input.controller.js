function TableInputController() {
  const vm = this;
  vm.$onInit = () => {
    vm.callbacks = {
      onChange: vm.rawCallbacks.onChange.bind(this)
    };
  };
}

export default TableInputController;
