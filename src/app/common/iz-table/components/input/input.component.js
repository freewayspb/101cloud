import template from './input.tpl.html';
import controller from './input.controller';

const TableInputComponent = {
  template,
  controller,
  bindings: {
    inputModel: '=',
    rawCallbacks: '=',
    item: '='
  }
};

export default TableInputComponent;
