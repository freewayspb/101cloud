import component from './input.component';

const InputModule = angular
  .module('app.table-input', [])
  .component('izTableInput', component)
  .name;

export default InputModule;
