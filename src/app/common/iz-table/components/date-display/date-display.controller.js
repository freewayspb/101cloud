function DateDisplayController($filter) {
  const vm = this;
  vm.$onInit=() => {
    if(vm.value === null) {
      vm.value = '';
    } else {
      vm.value = new Date(vm.value);
      if(!vm.dateFormat) {
        vm.dateFormat = 'dd.MM.yyyy HH:mm';
      }
    }
  };
}

export default DateDisplayController;
