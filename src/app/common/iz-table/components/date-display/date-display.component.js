import template from './date-display.tpl.html';
import controller from './date-display.controller';

const DateDisplayComponent = {
  template,
  controller,
  bindings: {
    value: '<',
    dateFormat: '<'
  }
};

export default DateDisplayComponent;
