function IzTableCheckboxController() {
  this.$onInit = () => {
    this.value = (this.value === 'true' || this.value === true);
    this.callbacks = {
      onChange: this.rawCallbacks.onChange.bind(this)
    };
  };
}

export default IzTableCheckboxController;
