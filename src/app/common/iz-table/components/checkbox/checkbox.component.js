import template from './checkbox.tpl.html';
import controller from './checkbox.controller';

const CheckboxComponent = {
  template,
  controller,
  bindings: {
    value: '=',
    rawCallbacks: '=',
    item: '='
  }
};

export default CheckboxComponent;
