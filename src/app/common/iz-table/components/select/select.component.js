import template from './select.tpl.html';
import controller from './select.controller';

const ActionsComponent = {
  template,
  controller,
  bindings: {
    selectModel: '='
  }
};

export default ActionsComponent;
