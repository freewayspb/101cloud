import component from './select.component';

const SelectModule = angular
  .module('app.table-select', [])
  .component('izTableSelect', component)
  .name;

export default SelectModule;
