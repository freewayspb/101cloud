import template from './general-discount.tpl.html';
import controller from './general-discount.controller';

const ActionsComponent = {
  template,
  controller,
  bindings: {
    data: '='
  }
};

export default ActionsComponent;
