function ActionButtonController() {
  this.inputs = {
    notApply: {
      data: {
        title: 'Not apply if the margin is less than, %',
        model: true,
        valueTrue: true,
        valueFalse: false
      }
    },
    notApplyValue: {
      data: '',
      options: {
        type: 'number',
        min: 0,
        max: 100
      }
    }
  };
}

export default ActionButtonController;
