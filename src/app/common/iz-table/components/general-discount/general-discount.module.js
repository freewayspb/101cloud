import component from './general-discount.component';

const GeneralDiscountModule = angular
  .module('app.table-generalDiscount', [])
  .component('izTableGeneralDiscount', component)
  .name;

export default GeneralDiscountModule;
