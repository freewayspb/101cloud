import template from './percent-display.tpl.html';
import controller from './percent-display.controller';

const PercentDisplayComponent = {
  template,
  controller,
  bindings: {
    value: '<',
    item: '='
  }
};

export default PercentDisplayComponent;
