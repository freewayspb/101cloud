import template from './iconed-name-display.tpl.html';
import controller from './iconed-name-display.controller';

const IconedNameDisplayComponent = {
  template,
  controller,
  bindings: {
    value: '<',
    item: '<'
  }
};

export default IconedNameDisplayComponent;
