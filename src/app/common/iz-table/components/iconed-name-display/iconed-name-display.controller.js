function IconedNameDisplayController(apiService, $window) {
  const vm = this;
  vm.rdpServer = () => {
    const url = apiService.izTenantApi = '/101cloud/Servers/' + vm.item.entity.Id + '/Connections';
    $window.open(url, '_blank');
  };
}

export default IconedNameDisplayController;
