import controller from './power-menu.controller';
import template from './power-menu.tpl.html';

const IzTable = {
  controller,
  template,
  bindings: {
    state: '='
  }
};

export default IzTable;
