import IzTableComponent from './power-menu.component';
import IzTableService from './power-menu.service';

const powerMenu = angular
  .module('app.power-menu', [])
  .component('powerMenu', IzTableComponent)
  .service('powerMenuService', IzTableService)
  .name;

export default powerMenu;
