/* ngInject */
function PowerMenu() {
  const map = new Map([
    [true, 'icon-power_icon-3'],
    [false, 'icon-power_icon-4']
  ]);

  this.$onInit = () => {
    this.state = map.get(this.state);
  };

  this.setState = state => {
    this.state = state;
  };
}

export default PowerMenu;
