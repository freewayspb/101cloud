import template from './payment.tpl.html';
import controller from './payment.controller';

const PriceDisplayComponent = {
  template,
  controller,
  bindings: {
    value: '<',
    suffix: '@'
  }
};

export default PriceDisplayComponent;
