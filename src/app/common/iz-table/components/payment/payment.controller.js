function PriceDisplayController() {
  this.$onInit=() => {
  };

  this.getPayment = value => {
    value = value === true ? 'by contract' : 'by credit cart';
    return value;
  }
}

export default PriceDisplayController;
