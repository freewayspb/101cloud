function DashboardSubscriptionController(currencyService) {
  this.$onInit = () => {
    this.price = currencyService.getPriceString(+this.value[0]);
    this.percent = this.value[1];
    this.direction = this.value[2];
  };
}

export default DashboardSubscriptionController;
