import template from './dashboard-subscription.tpl.html';
import controller from './dashboard-subscription.controller';

const DashboardSubscriptionComponent = {
  template,
  controller,
  bindings: {
    value: '<'
  }
};

export default DashboardSubscriptionComponent;
