import template from './dashboard-extremum.tpl.html';
import controller from './dashboard-extremum.controller';

const DashboardExtremumComponent = {
  template,
  controller,
  bindings: {
    value: '<'
  }
};

export default DashboardExtremumComponent;
