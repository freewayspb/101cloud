function DashboardExtremumController(currencyService) {
  this.$onInit=() => {
    this.price = currencyService.getPriceString(+this.value[0]);
    this.month = this.value[1];
  };
}

export default DashboardExtremumController;
