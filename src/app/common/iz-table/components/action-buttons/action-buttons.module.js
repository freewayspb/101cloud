import component from './action-buttons.component';
import service from './action-buttons.service';

const ActionButtons = angular
  .module('app.action-buttons', [])
  .component('actionbuttons', component)
  .service('actionButtonsService', service)
  .name;

export default ActionButtons;
