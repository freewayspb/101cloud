import template from './action-buttons.tpl.html';
import controller from './action-buttons.controller';

const ActionsComponent = {
  template,
  controller,
  bindings: {
    context: '<',
    edit: '<',
    delete: '<',
    archive: '<',
    view: '<',
    gear: '<',
    download: '<',
    editLicense: '<',
    moreLicense: '<',
    qty: '<',
    item: '<',
    rawCallbacks: '<'
  }
};

export default ActionsComponent;
