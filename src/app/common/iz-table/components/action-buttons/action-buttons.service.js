class ActionButtonsClass {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  getInvoice(id) {
    return this.dataService.getItems(id, './app/assets/invoice-objects/');
  }
}

export default ActionButtonsClass;
