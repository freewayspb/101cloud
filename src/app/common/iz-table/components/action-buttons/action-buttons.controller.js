function ActionButtonController($state, $window) {
  this.$onInit = () => {
    this.isVisible = ((this.item.entity.status !== -1 && this.context === true) ||
      !$state.is('app.depconf.clouds.list'));
  };

  this.viewItem = () => {
    $state.go('app.billing.invoices.view-invoice', {invoiceId: this.item.entity.InvoiceId});
  };

  this.downloadItem = () => {
    $window.open(this.item.entity.InvoiceFileUrl, '_blank');
  };

  this.goToDetails = () => {
    $state.go('app.softlic.orderlic.details', {id: this.item.entity.Id});
  };

  this.unassignLicense = () => {
    this.item.entity.assignment = 'Unassigned';
  };

  this.assignLicense = e => {
    this.item.entity.assignment = e.target.getAttribute('name');
  };

  this.getMoreLicense = () => {
    $state.go('app.softlic.office365.addoffice365plan', {ProductId: this.item.entity.Id});
  };

  this.incrementQty = item => {
    item.qty++;
    this.rawCallbacks.qty.onChange(item, item.qty);
  };

  this.decrementQty = item => {
    if (item.qty > 0) {
      item.qty--;
      this.rawCallbacks.qty.onChange(item, item.qty);
    }
  };
}

export default ActionButtonController;
