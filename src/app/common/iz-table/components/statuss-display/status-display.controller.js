function StatusDisplayController() {
  this.$onInit = () => {
    if (this.icons) {
      if (this.status === 0) {
        this.model = 'stop';
      } else if (this.status < 10 && this.status !== 1 && this.state === 'undefined') {
        this.model = 'warn';
      } else if (this.status === 100 || this.status === 1) {
        this.model = 'ok';
      } else {
        this.model = this.state;
      }
    } else if (this.license) {
      this.model = 'license';
    } else if (this.userStatus) {
      this.model = 'userStatus';
    }
  };
}

export default StatusDisplayController;
