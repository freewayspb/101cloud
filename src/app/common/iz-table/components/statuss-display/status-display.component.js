import template from './status-display.tpl.html';
import controller from './status-display.controller';

const StatusDisplayComponent = {
  template,
  controller,
  bindings: {
    status: '<',
    icons: '<',
    state: '@',
    license: '<',
    userStatus: '<'
  }
};

export default StatusDisplayComponent;
