function DashboardGraphController(currencyService, $scope) {
  this.$onInit = () => {
    $scope.labels = ["Q1", "Q2", "Q3", "Q4"];
    $scope.data = [
      [5, 5, 10, 0]
    ];
    $scope.options = {
      responsive: false,
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false
        }]
      },
      elements: {
        line: {
          tension: 0
        }
      },
      tooltips: {
        enabled: false
      }
    };
  };
}

export default DashboardGraphController;
