import template from './dashboard-graph.tpl.html';
import controller from './dashboard-graph.controller';

const DashboardGraphComponent = {
  template,
  controller,
  bindings: {
    graphData: '='
  }
};

export default DashboardGraphComponent;
