import template from './pretty-checkbox.tpl.html';
import controller from './pretty-checkbox.controller';

const PrettyCheckboxComponent = {
  template,
  controller,
  bindings: {
    value: '='
  }
};

export default PrettyCheckboxComponent;
