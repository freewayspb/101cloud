function PrettyCheckboxController() {
  this.$onInit = () => {
    this.value = (this.value === 'true' || this.value === true);
  };
}

export default PrettyCheckboxController;
