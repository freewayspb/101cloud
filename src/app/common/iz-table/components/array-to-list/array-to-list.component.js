import template from './array-to-list.tpl.html';
import controller from './array-to-list.controller';

const ArrayToListComponent = {
  template,
  controller,
  bindings: {
    value: '<'
  }
};

export default ArrayToListComponent;
