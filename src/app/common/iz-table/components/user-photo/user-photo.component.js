import template from './user-photo.tpl.html';
import controller from './user-photo.controller';

const UserPhotoComponent = {
  template,
  controller,
  bindings: {
    value: '='
  }
};

export default UserPhotoComponent;
