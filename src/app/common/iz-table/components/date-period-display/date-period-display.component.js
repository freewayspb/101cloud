import template from './date-period-display.tpl.html';
import controller from './date-period-display.controller';

const DatePeriodDisplayComponent = {
  template,
  controller,
  bindings: {
    startDate: '<',
    endDate: '<'
  }
};

export default DatePeriodDisplayComponent;
