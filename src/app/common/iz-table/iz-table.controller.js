import _each from 'lodash/each';

function IzTable($filter, izTableService) {
  const vm = this;
  vm.$onInit = () => {
    izTableService.$ctrl = vm;

    vm.data = angular.copy(vm.tableOptions.data);
    if (this.tableOptions.tableGridToggler) {
      this.view = 'grid';
    }
    if(vm.tableOptions.tables) {
      _each(vm.tableOptions.tables, table => {
        table.gridOptions = {
          data: table.data,
          columnDefs: izTableService.getHeaders(table.headers),
          enableColumnResizing: true,
          onRegisterApi: gridApi => {
            vm.gridApi = gridApi;
          },
          enableRowSelection: table.enableRowSelection || false,
          enableSelectAll: table.enableSelectAll || false,
          multipleSorting: true,
          enableHorizontalScrollbar: false,
          enableVerticalScrollbar: false
        };
      });
    } else {
      vm.gridOptions = {
        data: vm.tableOptions.data,
        columnDefs: izTableService.getHeaders(vm.tableOptions.headers),
        enableColumnResizing: true,
        enableRowHeaderSelection: true,
        enableCellEditOnFocus: vm.tableOptions.enableCellEditOnFocus || false,
        onRegisterApi: gridApi => {
          vm.gridApi = gridApi;
        },
        enableRowSelection: vm.tableOptions.enableRowSelection || false,
        enableSelectAll: vm.tableOptions.enableSelectAll || false,
        rowHeight: vm.tableOptions.rowHeight,
        multipleSorting: true,
        enableHorizontalScrollbar: false,
        enableVerticalScrollbar: false
      };
    }

    vm.searchText = {
      placeholder: 'Search',
      options: {
        isSearch: true,
        classNameWrapper: 'iz-table-inputs'
      }
    };

    if (vm.tableOptions.dropdownOptions) {
      if (vm.tableOptions.dropdownOptions.options) {
        vm.tableOptions.dropdownOptions.options.classNameWrapper = 'iz-table-inputs';
      } else {
        vm.tableOptions.dropdownOptions.options = {
          classNameWrapper: 'iz-table-inputs'
        };
      }
    }
  };

  vm.callbacks = {
    onChange: refreshData
  };

  vm.tableCallbacks = {};

  vm.export = () => {
    const myElement = angular.element(document.querySelectorAll('.custom-csv-link-location'));
    vm.gridApi.exporter.csvExport( 'all', 'all', myElement );
  };

  function refreshData() {
    if (vm.tableOptions.tables) {
      _each(vm.tableOptions.tables, table => {
        table.gridOptions.data = $filter('filter')(table.data, vm.searchText.data);
      });
    } else {
      vm.gridOptions.data = $filter('filter')(vm.data, vm.searchText.data);
    }

  }
}

export default IzTable;
