import _ from 'lodash';

class IzTable {

  /*@ngInject*/

  constructor() {
  }

  /**
   * @name getHeaders
   * @desc Translates headers into smart-table acceptable ones
   * @returns {Object}
   * @param {Object} rawHeaders are the headers which go from outside
   */
  getHeaders(rawHeaders) {
    return _.map(rawHeaders, rawHeader => {
      rawHeader = this.prepare(rawHeader);
      this.$ctrl[rawHeader.dataProperty + 'Callbacks'] = rawHeader.specials.callbacks;
      switch (rawHeader.specials.type) {
        case 'actions':
          rawHeader.cellClass = 'iz-table-actions';
          rawHeader.cellTemplate = `<actionbuttons context="${rawHeader.specials.params.context}"
                                    edit="${rawHeader.specials.params.edit}"
                                    delete="${rawHeader.specials.params.delete}"
                                    archive="${rawHeader.specials.params.archive}"
                                    view="${rawHeader.specials.params.view}"
                                    gear="${rawHeader.specials.params.gear}"
                                    download="${rawHeader.specials.params.download}"
                                    clip="${rawHeader.specials.params.clip}"
                                    pdf="${rawHeader.specials.params.pdf}"
                                    edit-license="${rawHeader.specials.params.editLicense}"
                                    more-license="${rawHeader.specials.params.moreLicense}"
                                    qty="${rawHeader.specials.params.qty}"
                                    raw-callbacks="grid.appScope.$ctrl['${rawHeader.dataProperty + 'Callbacks'}']"
                                    item="row"></actionbuttons>`;
          break;
        case 'switch':
          rawHeader.cellClass = rawHeader.headerCellClass = 'iz-table-status-header';
          rawHeader.cellTemplate = '<prettycheckbox value="MODEL_COL_FIELD"></prettycheckbox>';
          break;
        case 'status':
          rawHeader.cellClass = rawHeader.headerCellClass = 'iz-table-status-header';
          rawHeader.cellTemplate = `<statusdisplay status="grid.getCellValue(row, col)"
                                    icons="${rawHeader.specials.params.icons}"
                                    state="${rawHeader.specials.params.state}"
                                    license="${rawHeader.specials.params.license}"
                                    user-status="${rawHeader.specials.params.userStatus}"></statusdisplay>`;
          break;
        case 'power':
          rawHeader.cellClass = rawHeader.headerCellClass = 'iz-table-status-header';
          rawHeader.cellTemplate = '<power-menu state="MODEL_COL_FIELD"></power-menu>';
          break;
        case 'date':
          rawHeader.cellTemplate = `<datedisplay value="grid.getCellValue(row, col)"
                                               date-format="${rawHeader.specials.dateFormat}"></datedisplay>`;
          break;
        case 'period':
          rawHeader.cellTemplate = `<date-period-display start-date="grid.getCellValue(row, col)[0]"
          end-date="grid.getCellValue(row, col)[1]"></date-period-display>`;
          break;
        case 'price':
          rawHeader.cellTemplate = `<pricedisplay value="grid.getCellValue(row, col)"
                                                currency="${rawHeader.specials.currency}"
                                                item="row"></pricedisplay>`;
          break;
        case 'payment':
          rawHeader.cellTemplate = `<payment value="grid.getCellValue(row, col)"></payment>`;
          break;
        case 'iconed':
          rawHeader.cellTemplate = `<iconednamedisplay value="grid.getCellValue(row, col)"
                                                     item="row"></iconednamedisplay>`;
          break;
        case 'index':
          rawHeader.cellClass = rawHeader.headerCellClass = 'iz-table-status-header';
          break;
        case 'checkbox':
          rawHeader.headerTemplate = ``;
          rawHeader.cellTemplate = `<iz-table-checkbox value="MODEL_COL_FIELD" item="row"
          raw-callbacks="grid.appScope.$ctrl['${rawHeader.dataProperty + 'Callbacks'}']"></iz-table-checkbox>`;
          break;
        case 'radio':
          rawHeader.headerTemplate = ``;
          rawHeader.cellTemplate = '<iz-table-radio model="grid.appScope.radioModel" item="row"></iz-table-radio>';
          break;
        case 'dashboard-extremum':
          rawHeader.cellTemplate = `<iz-dashboard-extremum value="grid.getCellValue(row, col)">
</iz-dashboard-extremum>`;
          break;
        case 'dashboard-subscription':
          rawHeader.cellTemplate = `<iz-dashboard-subscription value="grid.getCellValue(row, col)">
</iz-dashboard-subscription>`;
          break;
        case 'dashboard-graph':
          rawHeader.cellTemplate = `<iz-dashboard-graph graphData="grid.getCellValue(row, col)"></iz-dashboard-graph>`;
          break;
        case 'user-photo':
          rawHeader.cellTemplate = `<iz-user-photo value="grid.getCellValue(row, col)"></iz-user-photo>`;
          break;
        case 'array':
          rawHeader.cellTemplate = `<array-to-list value="grid.getCellValue(row, col)"></array-to-list>`;
          break;
        case 'select':
          const selectModel = JSON.stringify(rawHeader.specials.params);
          rawHeader.cellTemplate = `<iz-table-select select-model='${selectModel}'></iz-table-select>`;
          break;
        case 'input':
          rawHeader.cellTemplate = `<iz-table-input input-model="MODEL_COL_FIELD"
                                        raw-callbacks="grid.appScope.$ctrl['${rawHeader.dataProperty + 'Callbacks'}']"
                                        item="row">
                                    </iz-table-input>`;
          break;
        case 'general-discount':
          rawHeader.cellTemplate = `<iz-table-general-discount data="grid.getCellValue(row, col)">
</iz-table-general-discount>`;
          break;
        case 'ct-integration':
          rawHeader.cellTemplate = `<cloud-type-integrations value="grid.getCellValue(row, col)">
</cloud-type-integrations>`;
          break;
        case 'percent':
          rawHeader.cellTemplate = `<iz-percent-display value="MODEL_COL_FIELD"></iz-percent-display>`;
          break;
        default:
          break;
      }
      return rawHeader;
    });
  }

  /**
   * @name prepare
   * @desc Bug fix for Sonar's "too complicated function getHeaders"
   * @returns {Object}
   * @param {Object} rawHeader
   */
  prepare(rawHeader) {
    rawHeader.sortField = rawHeader.canSort && rawHeader.dataProperty;
    rawHeader.name = rawHeader.name || ' ';
    rawHeader.field = rawHeader.dataProperty || ' ';
    rawHeader.specials = rawHeader.specials || {};
    rawHeader.headerCellClass = rawHeader.enableSorting && 'ui-grid-sortable';
    return rawHeader;
  }
}

export default IzTable;
