import controller from './iz-table.controller';
import template from './iz-table.tpl.html';

const IzTable = {
  controller,
  template,
  bindings: {
    tableOptions: '=',
    externalScope: '='
  }
};

export default IzTable;
