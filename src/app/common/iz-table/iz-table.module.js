import IzTableComponent from './iz-table.component';
import IzTableService from './iz-table.service';
import PowerMenuModule from './components/power-menu/power-menu.module';
import ActionButtonsModule from './components/action-buttons/action-buttons.module';
import SelectModule from './components/select/select.module';
import InputComponent from './components/input/input.component';
import GeneralDiscountModule from './components/general-discount/general-discount.module';
import CloudTypeIntegrations from './components/cloud-type-integrations/cloud-type-integrations.module';
import PrettyCheckboxComponent from './components/pretty-checkbox/pretty-checkbox.component';
import StatusDisplayComponent from './components/statuss-display/status-display.component';
import DateDisplayComponent from './components/date-display/date-display.component';
import PriceDisplayComponent from './components/price-display/price-display.component';
import IconedNameDisplay from './components/iconed-name-display/iconed-name-display.component';
import DatePeriodDisplayComponent from './components/date-period-display/date-period-display.component';
import Payment from './components/payment/payment.component';
import izTableCheckbox from './components/checkbox/checkbox.component';
import izTableRadio from './components/radio/radio.component';
import dashboardExtremum from './components/dashboard-extremum/dashboard-extremum.component';
import dashboardSubscription from './components/dashboard-subscription/dashboard-subscription.component';
import dashboardGraph from './components/dashboard-graph/dashboard-graph.component';
import userPhoto from './components/user-photo/user-photo.component';
import arrayToList from './components/array-to-list/array-to-list.component';
import percentDisplay from './components/percent-display/percent-display.component';

const izTable = angular
  .module('app.iz-table', [PowerMenuModule, ActionButtonsModule, SelectModule, GeneralDiscountModule,
    CloudTypeIntegrations,
    'ui.grid', 'ui.grid.resizeColumns', 'ui.grid.autoResize', 'ui.grid.selection', 'ui.grid.exporter',
    'ui.grid.edit', 'ui.grid.cellNav'])
  .component('izTable', IzTableComponent)
  .component('prettycheckbox', PrettyCheckboxComponent)
  .component('statusdisplay', StatusDisplayComponent)
  .component('datedisplay', DateDisplayComponent)
  .component('pricedisplay', PriceDisplayComponent)
  .component('iconednamedisplay', IconedNameDisplay)
  .component('datePeriodDisplay', DatePeriodDisplayComponent)
  .component('payment', Payment)
  .component('izTableInput', InputComponent)
  .component('izTableCheckbox', izTableCheckbox)
  .component('izTableRadio', izTableRadio)
  .component('izDashboardExtremum', dashboardExtremum)
  .component('izDashboardSubscription', dashboardSubscription)
  .component('izDashboardGraph', dashboardGraph)
  .component('izUserPhoto', userPhoto)
  .component('arrayToList', arrayToList)
  .component('izPercentDisplay', percentDisplay)
  .service('izTableService', IzTableService)
  .name;

export default izTable;
