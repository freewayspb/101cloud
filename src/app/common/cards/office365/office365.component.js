import template from './office365.tpl.html';
import controller from './office365.controller';

const Office365Component = {
  template,
  controller,
  bindings: {
    item: '<'
  }
};

export default Office365Component;
