function Office365Controller(currencyService) {
  let price;
  this.$onInit = () => {
    price = currencyService.getPriceString(this.item.price);
  };

  this.getPrice = () => price.substr(0, price.length - 1) + ' ' + (this.item.suffix || 'user/month');
}

export default Office365Controller;
