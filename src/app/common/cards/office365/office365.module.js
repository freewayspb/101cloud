import component from './office365.component';

const Office365Module = angular
  .module('app.office365.service', [])
  .component('izOffice365Card', component)
  .name;

export default Office365Module;
