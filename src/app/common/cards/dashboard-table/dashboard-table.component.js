import template from './dashboard-table.tpl.html';
import controller from './dashboard-table.controller';

const DashboardTableComponent = {
  template,
  controller,
  bindings: {
    data: '=',
    onSelect: '&'
  }
};

export default DashboardTableComponent;
