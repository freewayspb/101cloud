const DashboardTableInputs = {
  entities: {
    data: [
      {displayName: 'Managed Services', entity: 'managed-services'},
      {displayName: 'Operations', entity: 'operations'},
      {displayName: 'Licenses', entity: 'licenses'},
      {displayName: 'Services', entity: 'services'},
      {displayName: 'Infrastructure Providers', entity: 'i-providers'}
    ],
    selected: {displayName: 'Licenses', entity: 'licenses'},
    options: {
      classNameWrapper: 'no-margin'
    }
  },
  period: {
    data: [
      {displayName: 'Month View', period: 'month'},
      {displayName: 'Quarter View', period: 'quarter'},
      {displayName: 'Year View', period: 'year'}
    ],
    selected: {displayName: 'Year View', period: 'year'},
    options: {
      classNameWrapper: 'no-margin'
    }
  }
};

export default DashboardTableInputs;