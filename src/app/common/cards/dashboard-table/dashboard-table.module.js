import component from './dashboard-table.component';

const DashboardTableModule = angular
  .module('app.dashboardtable', [])
  .component('izDashboardTableCard', component)
  .name;

export default DashboardTableModule;
