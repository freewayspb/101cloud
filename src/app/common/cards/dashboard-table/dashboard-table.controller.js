import inputs from './dashboard-table.inputs'

function DashboardTableController() {
  this.$onInit = () => {
    this.inputs = _.cloneDeep(inputs);
    this.tableData = this.data.tableOptions;
  };
}

export default DashboardTableController;
