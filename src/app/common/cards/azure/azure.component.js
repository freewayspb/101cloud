import template from './azure.tpl.html';
import controller from './azure.controller';

const AzureComponent = {
  template,
  controller,
  bindings: {
    item: '<'
  }
};

export default AzureComponent;
