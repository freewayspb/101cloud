import component from './azure.component';

const AzureModule = angular
  .module('app.azure.service', [])
  .component('izAzureCard', component)
  .name;

export default AzureModule;
