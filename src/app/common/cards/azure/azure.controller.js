function AzureController(currencyService) {
  this.$onInit = () => {
    this.price = currencyService.getPriceString(this.item.price) + ' ' + (this.item.suffix || 'user/month');
  };
}

export default AzureController;
