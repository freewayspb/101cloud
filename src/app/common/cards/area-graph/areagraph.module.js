import component from './areagraph.component';

const DonutWidgetModule = angular
  .module('app.areagraph', [])
  .component('izAreaGraphCard', component)
  .name;

export default DonutWidgetModule;
