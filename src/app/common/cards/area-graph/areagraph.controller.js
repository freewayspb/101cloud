import inputs from './areagraph.inputs';

function AreaGraphController() {
  class Button {
    constructor(color, title) {
      this.color = getRGBA(color);
      this.nativeColor = '#' + color;
      this.title = title;
      this.model = 1;

      function getRGBA(hexCode) {
        const a = 0.75;
        const bytes = [
          hexCode[0] + hexCode[1],
          hexCode[2] + hexCode[3],
          hexCode[4] + hexCode[5]
        ];
        const r = getColorItemValue(bytes[0]);
        const g = getColorItemValue(bytes[1]);
        const b = getColorItemValue(bytes[2]);

        return `rgba(${r}, ${g}, ${b}, ${a})`;

        function getColorItemValue(hex) {
          hex = [getByte(hex[0]), getByte(hex[1])];
          return +hex[1] + (+hex[0] * 16);
        }

        function getByte(h) {
          return h.replace('a', '10')
            .replace('b', '11')
            .replace('c', '12')
            .replace('d', '13')
            .replace('e', '14')
            .replace('f', '15');
        }
      }
    }
  }
  this.$onInit = () => {
    this.inputs = _.cloneDeep(inputs);
    this.colors = ['77cfdd', '97abf1', 'fee0a7', 'fcbdb3', 'afb6c8'];
    this.buttons = [];
    const data = _.map(this.graph.datasets, (dataset, index) => {
      this.buttons.push(new Button(this.colors[index], dataset.label));
      dataset.backgroundColor = this.buttons[index].color;
      return dataset.data;
    });
    if (this.graph.options && (this.graph.options.entity || this.graph.options.period)) {
      this.inputs.entities.selected = _.find(this.inputs.entities.data,
        item => item.entity === this.graph.options.entity);
      this.inputs.period.selected = _.find(this.inputs.period.data, item => item.period === this.graph.options.period);
    }
    this.data = {
      labels: this.graph.labels,
      data: data,
      datasets: this.graph.datasets,
      options: {
        scales: {
          yAxes: [{
            stacked: true
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        },
        tooltips: {
          enabled: false
        }
      }
    };
  };

  this.selectCallbacks = {
    onChange: () => {
      const params = {
        period: this.inputs.period.selected.period,
        entity: this.graph.options && this.graph.options.isDetails && this.inputs.entities.selected.entity
      };
      this.onSelect({params: params});
    }
  };

  this.toggleGraphs = item => {
    _.each(this.graph.datasets, (dataset, index) => {
      dataset.hidden = !Boolean(this.buttons[index].model);
    });
  };
}

export default AreaGraphController;
