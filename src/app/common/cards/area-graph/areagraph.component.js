import template from './areagraph.tpl.html';
import controller from './areagraph.controller';

const AreaGraphComponent = {
  template,
  controller,
  bindings: {
    graph: '=',
    onSelect: '&'
  }
};

export default AreaGraphComponent;
