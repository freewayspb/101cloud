import component from './spla.component';

const SplaModule = angular
  .module('app.spla', [])
  .component('izSplaCard', component)
  .name;

export default SplaModule;
