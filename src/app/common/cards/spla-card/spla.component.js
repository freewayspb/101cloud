import template from './spla.tpl.html';
import controller from './spla.controller';

const SplaComponent = {
  template,
  controller,
  bindings: {
    item: '<',
    callbacks: '<'
  }
};

export default SplaComponent;
