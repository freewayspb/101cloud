function SplaController(currencyService) {
  this.$onInit = () => {
    this.price = currencyService.getPriceString(this.item.price) + ' ' + (this.item.suffix || 'user/month');
    this.item.qty = 0;
  };

  this.getPriceString = price => currencyService.getPriceString(price);

  this.incrementQty = item => {
    item.qty++;
    this.callbacks.qty.onChange(item, item.qty);
  };

  this.decrementQty = item => {
    if (item.qty > 0) {
      item.qty--;
      this.callbacks.qty.onChange(item, item.qty);
    }
  };
}

export default SplaController;
