import template from './server.component.tpl.html';
import controller from './server.controller';

const ServerComponent = {
  template,
  controller,
  bindings: {
    item: '<'
  }
};

export default ServerComponent;
