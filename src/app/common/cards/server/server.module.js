import component from './server.component';

const ServerCard = angular
  .module('app.cards.server', [])
  .component('izServerCard', component)
  .name;

export default ServerCard;
