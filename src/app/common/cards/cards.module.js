import serviceCard from './service/service.module';
import serverCard from './server/server.module';
import azureCard from './azure/azure.module';
import office365Card from './office365/office365.module';
import spla from './spla-card/spla.module';

const SplaModule = angular
  .module('app.spla.service', [serviceCard, serverCard, azureCard, office365Card, spla])
  .name;

export default SplaModule;
