function CloudTypeModalController($uibModalInstance, item) {
  const topoMap = {
    Starter: '101_Starter.jpg',
    Standard: '101_Standard.jpg',
    Expert: '101_Professional.jpg',
    Corporate: '101_Corporate.jpg',
    Enterprise: ''
  };

  this.$onInit = () => {
    this.topology = item.TypeImagePath || '/app/assets/img/' + topoMap[item.Type];
    this.item = item;
  };

  this.close = () => {
    $uibModalInstance.dismiss();
  };
}

export default CloudTypeModalController;
