import template from './cloud-type.tpl.html';
import controller from './cloud-type.controller';

const CloudTypeComponent = {
  template,
  controller,
  bindings: {
    item: '<',
    showCompare: '<',
    showDescription: '<',
    indexBoldStart: '<'
  }
};

export default CloudTypeComponent;
