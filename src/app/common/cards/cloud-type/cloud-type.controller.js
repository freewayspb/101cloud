import modalController from './modal/modal.controller';
import modalTemplate from './modal/modal.tpl.html';

function CloudTypeController(currencyService, $uibModal) {
  const vm = this;

  this.$onInit = () => {
    this.price = currencyService.getPriceString(this.item.Price, JSON.parse(localStorage.Currency).Symbol);
  };

  this.open = () => {
    $uibModal.open({
      animation: true,
      template: modalTemplate,
      controller: modalController,
      controllerAs: '$ctrl',
      size: 'lg',
      resolve: {
        item: function () {
          return vm.item;
        }
      }
    });
  };
}

export default CloudTypeController;
