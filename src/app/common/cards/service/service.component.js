import template from './service.component.tpl.html';
import controller from './service.controller';

const ServiceComponent = {
  template,
  controller,
  bindings: {
    item: '<'
  }
};

export default ServiceComponent;
