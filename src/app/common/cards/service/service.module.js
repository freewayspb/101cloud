import component from './service.component';

const ServiceCard = angular
  .module('app.cards.service', [])
  .component('izServiceCard', component)
  .name;

export default ServiceCard;
