class SubscriptionItem {
  constructor(rawSubscription, tenantId) {
    if (rawSubscription.entity === 'azure' &&
        rawSubscription.fields.general.switcher.model === 'existing') {
      const existingFields = rawSubscription.fields.existing;
      this.tenantId = tenantId;
      this.TenantGuid = existingFields.tenantId.data;
      this.ClientId = existingFields.login.data;
      this.ClientSecret = existingFields.password.data;
    } else {
      throw ':::: ENTITY OF EXISTING SUBSCRIPTION MUST BE AZURE AND IT MUST BE AS EXISTING ::::';
    }
  }
}

export default SubscriptionItem;
