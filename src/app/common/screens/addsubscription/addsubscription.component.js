import controller from './addsubscription.controller';
import template from './addsubscription.tpl.html';

const AddSubscriptionsComponent = {
  bindings: {
    validate: '&',
    datacenter: '=',
    hideIProvider: '=',
    type: '='
  },
  controller,
  template
};

export default AddSubscriptionsComponent;
