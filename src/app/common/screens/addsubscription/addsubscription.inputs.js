const AddSubscriptionInputs = () => {
  const NewSubscriptionInputs = {
    name: {
      title: 'Subscription Name',
      data: '',
      placeholder: 'Enter name'
    },
    description: {
      title: 'Description',
      data: '',
      placeholder: 'Enter description'
    },
    type: {
      title: 'Subscription Type',
      data: [
        {displayName: '1 Year', period: 7, suffix: 'per year'},
        {displayName: '3 Months', period: 2, suffix: 'per quarter'},
        {displayName: '1 Month', period: 4, suffix: 'per month'},
        {displayName: '1 Day', period: 5, suffix: 'per day'}
      ],
      selected: {displayName: '1 Month', period: 4}
    },
    cloudType: {
      title: 'Cloud Type',
      data: [{displayName: 'Starter'}],
      selected: {displayName: 'Starter'}
    }
  };

  const ExistingSubscriptionInputs = {
    radiogroup: {
      data: [{
        title: 'Select Subscription',
        value: 'subscription'
      }, {
        title: 'Enter Tenant ID',
        value: 'id'
      }],
      options: {
        classNameWrapper: 'add-subscriptions__existing-list-item_radiogroup',
        classNameItem: 'add-subscriptions__existing-list-item_radioitem'
      },
      model: 'subscription'
    },
    subscription: {
      data: [],
      placeholder: 'Select Subscription'
    },
    tenantId: {
      data: '',
      placeholder: 'Enter Tenant Id'
    },
    login: {
      data: '',
      title: 'Login',
      placeholder: 'Enter Login',
      options: {
        required: true
      }
    },
    password: {
      data: '',
      title: 'Password',
      placeholder: 'Enter Password',
      options: {
        type: 'password',
        isPassword: true,
        required: true
      }
    }
  };

  const GeneralSubscriptionInputs = {
    switcher: {
      data: [{
        title: 'Add New Subscription',
        value: 'new'
      }, {
        title: 'Add Existing Subscription',
        value: 'existing'
      }],
      options: {
        classNameWrapper: 'add-subscriptions__existing-list-item_radiogroup',
        classNameItem: 'add-subscriptions__existing-list-item_radioitem'
      },
      model: 'new'
    },
    termsConditions: {
      EN: '<p>EN Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p> <p>Please read <a href=\'#\'>Terms&Conditions of the Service Provider</a></p>',
      DE: '<p>DE Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p> <p>Please read <a href=\'#\'>Terms&Conditions of the Service Provider</a></p>'
    },
    confirm: {
      data: {
        valueTrue: true,
        valueFalse: false,
        model: false,
        title: 'I accept the Terms & Conditions'
      },
      options: {
        required: true
      }
    },
    language: {
      data: [ {displayName: 'EN'}, {displayName: 'DE'} ],
      selected: {displayName: 'EN'}
    }
  };

  const subscriptionTypes = [
    {
      Name: 'OFFICE 365 Subscription',
      id: 5,
      entity: 'office-365',
      type: 'virtual-datacenter',
      Price: 1
    },
    {
      Name: 'AZURE Subscription',
      id: 2,
      entity: 'azure',
      type: 'virtual-datacenter',
      Price: 2
    },
    {
      Name: 'INFRASTRUCTURE PROVIDER Subscription',
      id: 3,
      entity: 'infrastructure-provider',
      type: 'virtual-datacenter',
      Price: 4,
      isIProvider: true
    },
    {
      Name: 'ADDITIONAL SERVICES Subscription',
      id: null,
      entity: 'additional-services',
      type: 'virtual-datacenter',
      Price: 8,
      isAddServices: true
    },
    {
      Name: '101 CLOUD Subscription',
      id: 4,
      entity: '101-cloud',
      type: 'virtual-datacenter',
      Price: 16,
      is101Cloud: true
    }
  ];

  _.each(subscriptionTypes, type => {
    const newSubscription = _.cloneDeep(NewSubscriptionInputs);
    const existing = _.cloneDeep(ExistingSubscriptionInputs);
    const general = _.cloneDeep(GeneralSubscriptionInputs);
    if (type.entity === 'azure') {
      existing.radiogroup.data[1].title = 'Enter Tenant GUID';
      existing.login.title = 'Enter Application Id';
      existing.password.title = 'Enter Secret Key';
    }
    type.fields = {
      new: newSubscription,
      existing,
      general
    };
  });

  return subscriptionTypes;
};
export default AddSubscriptionInputs();
