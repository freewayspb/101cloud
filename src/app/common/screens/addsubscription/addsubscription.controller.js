import inputs from './addsubscription.inputs';
import globalInputs from '../../../components/depconf/clouds/addcloud/addcloud.inputs.model';
import SubscriptionItem from './SubscriptionItem.class';

function AddSubscriptionController(currencyService, countryService, apiService, addSubscriptionService,
                                   datacenterService, notificationService, $scope, headerService, alertsService) {

  const vm = this;

  vm.$onInit = () => {
    this.inputs = addSubscriptionService.inputs = inputs;

    this.periodCallbacks = _.map(this.inputs, input => {
      const callback = {};
      if (input.entity === 'infrastructure-provider') {
        callback.onChange = () => {
          reloadVds();
        };
      }
      return callback;
    });

    reloadDcs();
    reloadVds();
    reloadAddServ();
    validate();

    globalInputs._Subscriptions = inputs;

    if (this.type === 'individual') {
      _.each(inputs, input => {
        input.fields.existing.tenantId.title = input.fields.existing.radiogroup.data[1].title;
      });
    }

    notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: true});
  };

  this.regionCallbacks = {
    onChange: selected => {
      if (selected.isAll) {
        this.inputs.region.selected = undefined;
      }
      const Name = selected && selected.Name;
      this.inputs.country.data = datacenterService.getCountriesByRegionName(Name);
      inputs.iProvider.data = datacenterService.getTenantsByRegionAndCountryAndCityNames(Name);
      validate();
    }
  };

  this.countryCallbacks = {
    onChange: selected => {
      this.inputs.region.selected = countryService.getRegionByCountry(selected);
      inputs.iProvider.data =
        datacenterService.getTenantsByRegionAndCountryAndCityNames(this.inputs.region.selected.Name, selected.Name);
      validate();
    }
  };


  function reloadDcs() {
    vm.isLoading = true;
    if (!vm.datacenter) {
      datacenterService.getDatacenters()
        .then(() => {
          vm.location = datacenterService.getRegionsCountriesAndCitiesOfDatacenters();
          vm.isLoading = false;
        });
    }
  }

  function reloadVds() {
    vm.isLoading = true;
    headerService.getTenantPromise()
      .then(tenantid => {
        const infrastructureProviderPeriod = _.find(vm.inputs, input => input.entity === 'infrastructure-provider')
          .fields.new.type.selected;

        const currency = JSON.parse(localStorage.Currency).Name.toUpperCase();
        const fabricid = globalInputs.DataCenter.Id;
        const periodtype = infrastructureProviderPeriod.period;
        const params = {currency, fabricid, periodtype, tenantid, producttype: 2, typeofbusiness: 2};
        addSubscriptionService.get(apiService.basePaths.products, {params})
          .then(vds => {
            _.each(vds, vd => {
              vd.suffix = infrastructureProviderPeriod.suffix;
            });
            vm.virtualDatacenters = vds;
            vm.isLoading = false;
          }, error => {
            console.error(':::: AN ERROR OCCURRED IN ADD SUBSCRIPTION CONTROLLER ::::', error);
            alertsService.setAlert({msg: 'Cannot load virtual datacenter for current tenant', type: 'danger'});
            vm.isLoading = false;
          });
      });
  }

  function reloadAddServ() {
    addSubscriptionService.getItems('add-subscriptions-add-services.json')
      .then(result => {
        vm.addServices = result;
        inputs._additionalServices = result;
      });
  }

  /**
   * @name getPrice
   * @desc Gets formatted price as on design
   * @returns {string}
   * @param {number} price actual price
   */
  vm.getPrice = price => currencyService.getPriceString(price);
  vm.getIntPrice = price => vm.getPrice(price).substr(0, currencyService.getPriceString(price).length - 3);
  vm.callbacks = {
    onChange: validate
  };

  vm.saveIndividual = item => {
    headerService.getTenantPromise()
      .then(tenantId => addSubscriptionService.post({data: new SubscriptionItem(item, tenantId)}))
      .then(() => {
        alertsService.setAlert({msg: 'Subscription successfully "created"', type: 'success'});
      })
      .catch(error => {
        alertsService.setAlert('Failed to create subscription');
        console.error(':::: FAILED TO CREATE SUBSCRIPTION ::::', error);
      });
  };

  function validate() {
    const allTermsAreAccepted = !Boolean(_.findIndex(inputs, input => !input.fields.general.confirm.data.model) + 1);
    $scope.subscriptionForm && $scope.subscriptionForm.$setValidity('required', allTermsAreAccepted);
    vm.validate({isValid: allTermsAreAccepted});
  }

  this.$onDestroy = () => {
    globalInputs.Subscriptions = addSubscriptionService.getSubscriptionInfo();
  };
}

export default AddSubscriptionController;
