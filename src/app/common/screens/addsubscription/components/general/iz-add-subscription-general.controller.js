import globalInputs from '../../../../../components/depconf/clouds/addcloud/addcloud.inputs.model'
import subscriptionInputs from '../../addsubscription.inputs';

function IzAddSubscriptionGeneralController(addSubscriptionService, currencyService, notificationService) {
  this.$onInit = () => {
    if (this.virtualDatacenters.length > 0) {
      this.virtualDatacenterId = this.virtualDatacenters[0].Id;
    }
    const activeVdc = _.find(this.virtualDatacenters, vdc => vdc.Id === this.virtualDatacenterId);
    this.setVirtualDatacenter(activeVdc);
    this.inputs = addSubscriptionService.inputs;
  };

  this.getPrice = price => currencyService.getPriceString(price);
  
  this.setVirtualDatacenter = item => {
    subscriptionInputs._virtualDataCenter = globalInputs._virtualDataCenter = item || {};
  };

  this.onAddingItemComplete = (uid) => {
    addSubscriptionService.virtualDataCenterInfoId = uid;
  };
  
  this.$onDestroy = () => {
    const vdc = _.find(this.virtualDatacenters, vdc => vdc.Id === this.virtualDatacenterId);
    notificationService.broadcast('onInformationPriceListItemChange', {
      id: addSubscriptionService.virtualDataCenterInfoId,
      name: vdc.Name,
      price: vdc.Price,
      inputs: {
        qty: {
          data: 1,
          placeholder: 1,
          options: {
            classNameInput: 'information-panel-qty-no-arrows',
            type: 'number',
            min: 1
          }
        }
      },
      cb: this.onAddingItemComplete
    });
  }
}

export default IzAddSubscriptionGeneralController;
