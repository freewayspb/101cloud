import controller from './iz-add-subscription-general.controller';
import template from './iz-add-subscription-general.tpl.html';

const AddSubscriptionsGeneralComponent = {
  bindings: {
    virtualDatacenters: '='
  },
  controller,
  template
};

export default AddSubscriptionsGeneralComponent;
