import controller from './iz-add-subscription-checkout.controller';
import template from './iz-add-subscription-checkout.tpl.html';

const AddSubscriptionsCheckoutComponent = {
  controller,
  template
};

export default AddSubscriptionsCheckoutComponent;
