import inputs from './iz-add-subscription-checkout.inputs';

function IzAddSubscriptionCheckoutController() {
  this.$onInit = () => {
    this.inputs = inputs;
  };
}

export default IzAddSubscriptionCheckoutController;
