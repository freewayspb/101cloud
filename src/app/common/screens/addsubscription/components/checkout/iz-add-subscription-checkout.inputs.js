const AddSubscriptionCheckoutInputs = {
  payment: {
    title: 'Payment method',
    data: [
      { displayName: 'Credit/Debit card' },
      { displayName: 'Billing Consolidator' }
    ]
  }
};

export default AddSubscriptionCheckoutInputs;