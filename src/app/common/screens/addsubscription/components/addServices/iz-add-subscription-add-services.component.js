import controller from './iz-add-subscription-add-services.controller';
import template from './iz-add-subscription-add-services.tpl.html';

const AddSubscriptionsAddServicesComponent = {
  bindings: {
    addServices: '='
  },
  controller,
  template
};

export default AddSubscriptionsAddServicesComponent;
