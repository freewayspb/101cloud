import subscriptionInputs from "../../addsubscription.inputs";

function IzAddSubscriptionAddServicesController(currencyService) {
  const vm = this;
  this.getPriceString = price => currencyService.getPriceString(price);
  updatePrice();
  this.incrementQty = item => {
    item.qty = item.qty || 0;
    item.qty++;
    updatePrice();
  };
  this.decrementQty = item => {
    item.qty = item.qty || 0;
    if (item.qty > 0) {
      item.qty--;
    }
    updatePrice();
  };
  function updatePrice() {
    const index = _.findIndex(subscriptionInputs, input => input.entity === 'additional-services');
    let sum = 0;
    _.each(vm.addServices, element => {
      sum += element.Price * (element.qty || 0);
    });
    subscriptionInputs[index].Price = sum;
  }
}

export default IzAddSubscriptionAddServicesController;
