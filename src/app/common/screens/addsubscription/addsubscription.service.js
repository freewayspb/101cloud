class SubscriptionsService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.isLegalInputsValid = [];
  }

  getItems(param) {
    return this.dataService.getItems(param).then(
      result => result,
      error => {
        console.error(':::: ERROR OCCURRED IN SUBSCRIPTION -> GET ITEMS ::::', error);
        throw error;
      });
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN ADD SUBSCRIPTION SERVICE GET ::::', error);
        throw error;
      });
  }

  post(param) {
    return this.dataService.post(this.apiService.singeSubscriptionItem, param.data)
      .then(result => result, error => {
          console.error(':::: ERROR IN ADDSUBSCRIPTION SERVICE POST ::::', error, param);
          throw error;
        });
  }

  getSubscriptionInfo() {
    return _.map(this.inputs, input => {
      const singleSubscriptionObject = {
        SubscriptionName: input.fields.new.name.data,
        SubscriptionDescription: input.fields.new.description.data
      };
      if (input.entity !== 'additional-services') {
        singleSubscriptionObject.SubscriptionType = input.id;
      }
      if (input.entity === 'azure' && input.fields.general.switcher.model === 'existing') {
        singleSubscriptionObject.TenantGUIDId = input.fields.existing.tenantId.data;
        singleSubscriptionObject.ClientId = input.fields.existing.login.data;
        singleSubscriptionObject.ClientSecret = input.fields.existing.password.data;
      }
      return singleSubscriptionObject;
    });
  }
}

export default SubscriptionsService;
