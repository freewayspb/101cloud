import controller from './addlocation.controller';
import template from './addlocation.tpl.html';

const AddLocationComponent = {
  bindings: {
    validate: '&'
  },
  controller,
  template
};

export default AddLocationComponent;
