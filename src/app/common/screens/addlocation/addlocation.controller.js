import inputs from './addlocation.inputs';
import globalInputs from '../../../components/depconf/clouds/addcloud/addcloud.inputs.model';

/*@ngInject*/
function AddLocationController(subnetService, countryService, $q, $scope, addLocationService) {
  const vm = this;
  vm.isLoading = true;
  vm.inputs = addLocationService.inputs = inputs;
  vm.inputs.subnets = {
    networks: [
      {name: 'My Network'}
    ]
  };

  vm.$onInit = () => {
    countryService.regionsQ
      .then(result => {
        vm.inputs.region.data = _.concat(countryService.allRegions, result);
        vm.inputs.region.selected = undefined;
        return result;
      }, error => {
        console.error(':::: ERROR OCCURRED WHILE FETCHING REGIONS IN ADD LOCATION CONTROLLER ::::', error);
        throw error;
      });

    vm.inputs.mapSource = {
      locations: {}
    };

    countryService.countriesQ
      .then(result => {
        vm.inputs.country.data = result;
        vm.inputs.country.selected = undefined;
        return result;
      }, error => {
        console.error(':::: ERROR OCCURRED WHILE FETCHING COUNTRIES IN ADD LOCATION CONTROLLER ::::', error);
        throw error;
      });

    $q.all([countryService.regionsQ, countryService.countriesQ]).then(() => {
      vm.isLoading = false;

      if (globalInputs.Offices) {
        const country = countryService.getcountryById(globalInputs.Offices[0].Address.Country.Id);

        this.inputs.country.selected = country;
        this.inputs.region.selected = country.Region;
      }

      validate();
    });
  };

  vm.updateMap = () => {
    vm.inputs.mapSource.locations = {
      countryRegion: vm.inputs.country.selected && vm.inputs.country.selected.displayName || '',
      adminDistrict: '',
      locality: vm.inputs.city.selected && vm.inputs.city.selected.displayName || '',
      postalCode: '',
      addressLine: '',
      userLocation: ''
    };
  };

  vm.countryCallbacks = {
    onChange: selected => {
      vm.inputs.prefix.data = selected.Short + '01';
      vm.inputs.region.selected = countryService.getRegionByCountry(selected);
      $scope.$apply();
      vm.updateMap();
      validate();
    }
  };

  vm.regionCallbacks = {
    onChange: selected => {
      vm.inputs.country.data = countryService.getCountriesByRegion(selected);
      vm.inputs.country.selected = undefined;
      if (selected.isAll) {
        vm.inputs.region.selected = undefined;
        vm.inputs.country.data = countryService.countries;
      }
      validate();
    }
  };

  vm.genericCallbacks = {
    onChange: validate
  };

  this.validateSubnet = validate;

  function validate() {
    vm.validate({isValid: $scope.locationForm.$valid});
  }

  this.$onDestroy = () => {
    const Offices = addLocationService.getLocationPostModel();
    globalInputs.Offices = Offices;
  };
}

export default AddLocationController;
