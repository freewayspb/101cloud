const addLocationInputs = {
  name: {
    title: 'Name',
    placeholder: 'Enter',
    options: {
      required: true
    }
  },
  country: {
    title: 'Country',
    data: [],
    options: {
      required: true
    }
  },
  prefix: {
    title: 'Location Prefix',
    placeholder: ''
  },
  description: {
    data: '',
    title: 'Description'
  },
  region: {
    title: 'Region',
    data: [],
    // placeholder: 'All Regions',
    options: {
      required: true
    }
  },
  city: {
    title: 'City',
    placeholder: 'Enter City',
    data: ''
  },
  zip: {
    title: 'ZIP',
    placeholder: 'Enter ZIP'
  },
  street: {
    title: 'Street',
    placeholder: 'Enter street'
  },
  streetNumber: {
    title: 'Street Number',
    placeholder: 'Enter street'
  },
  subnets: []
};

export default addLocationInputs;
