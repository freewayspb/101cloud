import Country from'../Country.class';

class AddLocationService {

  /*@ngInject*/

  constructor($http, dataService, subnetService) {
    this.$http = $http;
    this.dataService = dataService;
    this.subnetService = subnetService;
  }

  getLocationPostModel() {
    const country = new Country(this.inputs.country.selected);
    const address = {
      Id: 1,
      Country: country,
      Street: this.inputs.street.selected,
      StreetNumber: this.inputs.streetNumber.selected,
      ZipCode: this.inputs.zip.selected,
      City: this.inputs.city.selected,
      Longitude: 4.4,
      Latitude: 3.3,
      IsDefault: true,
      IsBilling: true
    };
    const networks = this.subnetService.getNetworks(address);
    return [
      {
        'Networks': networks,
        'Id': 0,
        'Name': this.inputs.name.data,
        'Description': this.inputs.description.data,
        'SiteCode': this.inputs.prefix.data,
        'Address': address
      }
    ];
  }
}

export default AddLocationService;
