import angular from 'angular';

import AddCustomer from './addcustomer/addcustomer.component';
import AddDatacenter from './adddatacenter/adddatacenter.component';
import AddLocation from './addlocation/addlocation.component';
import AddSubscription from './addsubscription/addsubscription.component';
  import AddSubscriptionGeneral from './addsubscription/components/general/iz-add-subscription-general.component';
  import AddSubscriptionCheckout from './addsubscription/components/checkout/iz-add-subscription-checkout.component';
  import AddSubscriptionAddServices from
    './addsubscription/components/addServices/iz-add-subscription-add-services.component';

import AddCustomerService from './addcustomer/addcustomer.service';
import AddDatacenterService from './adddatacenter/adddatacenter.service';
import AddLocationService from './addlocation/addlocation.service';
import AddSubscriptionService from './addsubscription/addsubscription.service';

const screens = angular
  .module('app.screens', [])
  .component('addCustomer', AddCustomer)
  .component('addDatacenter', AddDatacenter)
  .component('addLocation', AddLocation)
  .component('addSubscription', AddSubscription)
    .component('addSubscriptionGeneral', AddSubscriptionGeneral)
    .component('addSubscriptionCheckout', AddSubscriptionCheckout)
    .component('addSubscriptionAddServices', AddSubscriptionAddServices)
  .service('addCustomerService', AddCustomerService)
  .service('addDatacenterService', AddDatacenterService)
  .service('addLocationService', AddLocationService)
  .service('addSubscriptionService', AddSubscriptionService)
  .name;

export default screens;
