import globalInputs from '../../../components/depconf/clouds/addcloud/addcloud.inputs.model';
import inputs from './adddatacenter.inputs';

function AddDatacenterController(addDatacenterService, alertsService, subnetService, $scope, datacenterService,
                                 countryService) {
  const vm = this;
  this.isLoading = !addDatacenterService.wasLoaded;
  this.inputs = inputs;

  vm.$onInit = () => {
    inputs.mapSource = {
      locations: {}
    };
    datacenterService.getDatacenters()
      .then(datacenters => {
        addDatacenterService.wasLoaded = true;
        vm.fullDatacenters = datacenters;

        const locations = datacenterService.getRegionsCountriesAndCitiesOfDatacenters();

        inputs.dataCenterCountry.data = locations.countries;
        inputs.dataCenterRegion.data = _.concat(countryService.allRegions, locations.regions);

        getAvailableTenants();

        validate();
        this.isLoading = false;
      }, error => {
        alertsService.setAlert({msg: 'Cannot GET datacenters', type: 'danger'});
        console.error(':::: CANNOT GET DATACENTERS ::::', error);
        this.isLoading = false;
      });
    vm.updateMap();
  };

  vm.updateMap = () => {
    inputs.mapSource.locations = {
      countryRegion: inputs.dataCenterCountry.selected && inputs.dataCenterCountry.selected.Name || '',
      adminDistrict: '',
      locality: inputs.dataCenterCity.selected && inputs.dataCenterCity.selected.Name || '',
      postalCode: '',
      addressLine: '',
      userLocation: ''
    };
  };

  vm.cityCallbacks = {
    onChange: getAvailableTenants
  };

  vm.countryCallbacks = {
    onChange: selected => {
      vm.updateMap();
      inputs.dataCenterRegion.selected =
        countryService.getRegionByCountry(selected);

      inputs.dataCenterCity.data =
        addDatacenterService.getCitiesByCountryName(vm.fullDatacenters, selected.Name);
      inputs.dataCenterCity.selected = undefined;

      inputs.dataCenterCity.placeholder = '';
      inputs.locationPrefix.data = selected.Short + '01';
      getAvailableTenants();
      validate();
    }
  };

  vm.regionCallbacks = {
    onChange: selected => {
      if (selected.isAll) {
        inputs.dataCenterRegion.selected = undefined;
        inputs.dataCenterCountry.data = addDatacenterService.getUniqCountries(vm.fullDatacenters);
        inputs.dataCenterCountry.placeholder = '';
        inputs.dataCenterCity.placeholder = '';
      } else {
        inputs.dataCenterCountry.data =
          datacenterService.getCountriesByRegionName(selected.Name);
      }
      inputs.dataCenterCity.selected = undefined;
      inputs.dataCenterCountry.selected = undefined;
      getAvailableTenants();
      validate();
    }
  };

  vm.fabricCallbacks = {
    onChange: selected => {
      inputs.dataCenterTechnology.data = selected.OsTemplates;
      setRegion();
      validate();
      datacenterService.datacenter = addDatacenterService.currentDatacenter = selected;
      vm.networks = selected.Networks;
    }
  };

  vm.osTemplateCallbacks = {
    onChange: validate
  };

  vm.iProviderCallbacks = {
    onChange: selected => {
      inputs.dataCenterFabric.data =
        addDatacenterService.getFabricsByTenantName(vm.fullDatacenters, selected.Name);
      inputs.dataCenterFabric.selected = undefined;
      inputs.dataCenterTechnology.selected = undefined;
    }
  };

  vm.genericCallbacks = {
    onChange: validate
  };

  this.validateSubnet = validate;

  function setRegion() {
    const tenant = inputs.dataInfrastructureProvider.selected.Name;
    const fabric = inputs.dataCenterFabric.selected.Name;
    const address = addDatacenterService.getLocationInfoByTenantAndDatacenterName(vm.fullDatacenters, tenant, fabric);
    if (!inputs.dataCenterCountry.selected) {
      inputs.dataCenterCountry.selectedHidden = address.Country;
      inputs.dataCenterCountry.placeholder = address.Country.Name;
      inputs.locationPrefix.data = inputs.dataCenterCountry.selectedHidden.Short + '01';
    }
    if (!inputs.dataCenterRegion.selected) {
      inputs.dataCenterRegion.selectedHidden = address.Country.Region;
      inputs.dataCenterRegion.placeholder = address.Country.Region.Name;
    }
    if (!inputs.dataCenterCity.selected) {
      inputs.dataCenterCity.placeholder = address.City;
    }
  }

  function getAvailableTenants() {
    const region = (inputs.dataCenterRegion.selected && !inputs.dataCenterRegion.selected.isAll &&
      inputs.dataCenterRegion.selected.Name) || null;

    const country = (inputs.dataCenterCountry.selected && !inputs.dataCenterCountry.selected.isAll &&
      inputs.dataCenterCountry.selected.Name) || null;

    const city = (inputs.dataCenterCity.selected && !inputs.dataCenterCity.selected.isAll &&
      inputs.dataCenterCity.selected.Name) || null;

    inputs.dataInfrastructureProvider.data =
      addDatacenterService.getTenantsByRegionAndCountryAndCityNames(vm.fullDatacenters, region, country, city);

    if (inputs.dataInfrastructureProvider.selected &&
      !_.find(inputs.dataInfrastructureProvider.data,
        tenant => tenant.Id === inputs.dataInfrastructureProvider.selected.Id)) {
      inputs.dataInfrastructureProvider.selected = undefined;
      inputs.dataCenterFabric.selected = undefined;
      inputs.dataCenterTechnology.selected = undefined;
    }
  }

  function validate() {
    vm.validate({isValid: $scope.datacenterForm.$valid});
  }

  this.$onDestroy = () => {
    const Networks = subnetService.getNetworks();
    const PostModel = addDatacenterService.getDatacenterPostModel(inputs, Networks);
    globalInputs.DataCenter = PostModel;
  };
}

export default AddDatacenterController;
