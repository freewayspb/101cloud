import controller from './adddatacenter.controller';
import template from './adddatacenter.tpl.html';

const AddDatacenterComponent = {
  bindings: {
    ngModel: '=',
    validate: '&'
  },
  controller,
  template
};

export default AddDatacenterComponent;
