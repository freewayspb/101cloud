const newDatacenter = {
  name: {
    title: 'Datacenter name',
    placeholder: 'Enter Name',
    data: '',
    options: {
      required: true
    }
  },
  locationPrefix: {
    title: 'Location Prefix',
    placeholder: '',
    data: ''
  },
  internalNetwork: {
    title: 'Internal Name',
    placeholder: 'Enter Internal Name',
    data: ''
  },
  privateDmzNetwork: {
    title: 'Private DMZ Network',
    placeholder: 'Enter Private DMZ Network',
    data: ''
  },
  publicDmzNetwork: {
    title: 'Public DMZ Network',
    placeholder: 'Enter Public DMZ Network',
    data: ''
  },
  dataCenterFabric: {
    title: 'Fabric',
    data: [],
    selected: undefined,
    options: {
      required: true
    }
  },
  dataCenterRegion: {
    title: 'Region',
    placeholder: 'All Regions',
    data: [],
    selected: undefined,
    selectedHidden: undefined,
    options: {
      classNameWrapper: 'no-margin'
    }
  },
  dataCenterCountry: {
    title: 'Country',
    placeholder: '',
    data: [],
    selected: undefined,
    selectedHidden: undefined,
    options: {
      classNameWrapper: 'no-margin'
    }
  },
  dataCenterCity: {
    title: 'City',
    placeholder: '',
    data: [],
    selected: undefined,
    selectedHidden: null,
    options: {
      classNameWrapper: 'no-margin'
    }
  },
  dataInfrastructureProvider: {
    title: 'Infrastructure Provider',
    placeholder: '',
    data: [],
    selected: undefined,
    options: {
      required: true
    }
  },
  dataCenterTechnology: {
    title: 'Technology',
    data: [],
    selected: undefined,
    options: {
      required: true,
      classNameWrapper: 'no-margin'
    }
  },
  dataCenterSubscription: {
    title: 'Subscription',
    data: [
      {'displayName': 'Subscription 1'},
      {'displayName': 'Subscription 2'},
      {'displayName': 'Subscription 3'}
    ],
    selected: {}
  },
  hardwaresInformation: [
    {
      'name': 'Standard 1',
      'mainFeatures': [
        {'type': 'processor', 'description': '2 Cores'},
        {'type': 'storage', 'description': '7 GB'}
      ],
      'features': [
        {'type': 'disc', 'description': '4 Data Disks'},
        {'type': 'speed', 'description': '6400 Max IOPS'},
        {'type': 'database', 'description': '14 GB Local SSD'},
        {'type': 'scale', 'description': 'Load Balancing'},
        {'type': 'i', 'description': 'Premium Disk Support'}
      ],
      'price': 90.71
    },
    {
      'name': 'Standard 2',
      'mainFeatures': [
        {'type': 'processor', 'description': '2 Cores'},
        {'type': 'storage', 'description': '7 GB'}
      ],
      'features': [
        {'type': 'disc', 'description': '4 Data Disks'},
        {'type': 'speed', 'description': '6400 Max IOPS'},
        {'type': 'database', 'description': '14 GB Local SSD'},
        {'type': 'scale', 'description': 'Load Balancing'},
        {'type': 'i', 'description': 'Premium Disk Support'}
      ],
      'price': 180.01
    },
    {
      'name': 'Standard 3',
      'mainFeatures': [
        {'type': 'processor', 'description': '4 Cores'},
        {'type': 'storage', 'description': '14 GB'}
      ],
      'features': [
        {'type': 'disc', 'description': '4 Data Disks'},
        {'type': 'speed', 'description': '6400 Max IOPS'},
        {'type': 'database', 'description': '14 GB Local SSD'},
        {'type': 'scale', 'description': 'Load Balancing'},
        {'type': 'i', 'description': 'Premium Disk Support'}
      ],
      'price': 200.52
    }
  ],
  subnets: [],
  subnetTemplate: {},
  networks: []
};

export default newDatacenter;
