import Country from '../Country.class';

class AddDatacenterService {

  /*@ngInject*/

  constructor($http, dataService, countryService) {
    this.$http = $http;
    this.dataService = dataService;
    this.countryService = countryService;
    this.location = {
      countryRegion: '',
      adminDistrict: '',
      locality: '',
      postalCode: '',
      addressLine: '',
      userLocation: ''
    };
  }

  saveDatacenter(data) {
    this.dataService.post('/DataService', data).then(
      result => {
        console.warn(result);
        return result;
      },
      error => {
        console.error(':::: ERROR IN DEPCONF -> ADD DATACENTER SERVICE SAVE DATACENTER ::::', error);
        throw error;
      }
    );
  }

  getItems(item) {
    return this.dataService.getItems(item)
      .then(result => result, error => {
        console.error(':::: ERROR IN DEPCONF -> ADD DATACENTER SERVICE GETITEMS ::::', error);
        throw error;
      });
  }

  getAvailableDatacenters(datacenterList) {
    return _.filter(datacenterList, datacenter => datacenter.Networks[0].Subnets.length > 0);
  }

  getListWithAllEntities(entity, entityList) {
    entity = entity || 'All Entities';
    return _.concat({displayName: entity, isAll: true}, entityList);
  }

  getUniqRegions(fullDatacenters) {
    const uniqDatacentersByRegions = _.uniqBy(fullDatacenters, fullDatacenter =>
      fullDatacenter.Addresses[0].Country.Region.Id);
    const uniqRegions = _.map(uniqDatacentersByRegions, fullDatacenter => fullDatacenter.Addresses[0].Country.Region);
    return this.getListWithAllEntities('All Regions', uniqRegions);
  }

  getUniqCountries(fullDatacenters) {
    const uniqDatacentersByCountry = _.uniqBy(fullDatacenters, fullDatacenter =>
      fullDatacenter.Addresses[0].Country.Id);
    const dirtyCounries = _.map(uniqDatacentersByCountry, datacenter => datacenter.Addresses[0].Country);
    return _.sortBy(dirtyCounries, 'Name');
  }

  getCountriesByRegionName(fullDatacenters, region) {
    const uniqCountries = this.getUniqCountries(fullDatacenters);
    const uniqueCountriesByRegion = _.filter(uniqCountries,
      country => country.Region && country.Region.Name === region);
    return this.getListWithAllEntities('All Countries', uniqueCountriesByRegion);
  }

  getRegionByCountryName(fullDatacenters, country) {
    const datacenterByCountry = _.find(fullDatacenters, datacenter => datacenter.Addresses[0].Country.Name === country);
    return datacenterByCountry.Addresses[0].Country.Region;
  }

  getCitiesByCountryName(fullDatacenters, country) {
    const datacentersByCity = _.filter(fullDatacenters, datacenter => datacenter.Addresses[0].Country.Name === country);
    const dirtyCities = _.map(datacentersByCity, datacenter => datacenter.Addresses[0].City);
    const uniqCities = _.uniq(dirtyCities);
    const sortedUniqCities = _.sortBy(uniqCities, city => city);
    return this.getListWithAllEntities('All Cities', _.map(sortedUniqCities, city => {
      return { displayName: city };
    }));
  }

  getTenantsByRegionAndCountryAndCityNames(fullDatacenters, region, country, city) {
    let datacenters = _.cloneDeep(fullDatacenters);
    if (region) {
      datacenters = _.filter(datacenters, datacenter => datacenter.Addresses[0].Country.Region.Name === region);
    }
    if (country) {
      datacenters = _.filter(datacenters, datacenter => datacenter.Addresses[0].Country.Name === country);
    }
    if (city) {
      datacenters = _.filter(datacenters, datacenter => datacenter.Addresses[0].City === city);
    }
    const dirtyTenants = _.map(datacenters, datacenter => datacenter.Tenant);

    return _.uniqBy(dirtyTenants, 'Id');
  }

  getFabricsByTenantName(fullDatacenters, tenant) {
    return _.filter(fullDatacenters, datacenter => datacenter.Tenant.Name === tenant);
  }

  getNetworksByTenantAndDatacenterName(fullDatacenters, tenantName, datacenterName) {
    const datacentersByTenant = _.filter(fullDatacenters, datacenter => datacenter.Tenant.Name === tenantName);
    const datacenterByDatacenterName = _.find(datacentersByTenant, datacenter => datacenter.Name === datacenterName);
    return datacenterByDatacenterName.Networks;
  }

  getDatacenterByTenantAndDatacenterName(fullDatacenters, tenantName, datacenterName) {
    const datacentersByTenant = _.filter(fullDatacenters, datacenter => datacenter.Tenant.Name === tenantName);
    return _.find(datacentersByTenant, datacenter => datacenter.Name === datacenterName);
  }

  getLocationInfoByTenantAndDatacenterName(fullDatacenters, tenant, datacenterName) {
    const datacenter = this.getDatacenterByTenantAndDatacenterName(fullDatacenters, tenant, datacenterName);
    return datacenter.Addresses[0];
  }

  getDatacenterPostModel(inputs, networks) {
    this.currentDatacenter.Addresses[0].Country = new Country(this.currentDatacenter.Addresses[0].Country);
    this.currentDatacenter.Addresses[0].isDefault = true;
    this.currentDatacenter.Addresses[0].isBilling = true;

    return {
      'Tenant': {
        'Id': inputs.dataInfrastructureProvider.selected.Id,
        'Name': inputs.dataInfrastructureProvider.selected.Name,
        'Description': inputs.dataInfrastructureProvider.selected.Description,
      },
      'Networks': networks,
      'Id': this.currentDatacenter.Id,
      'Name': inputs.name.data,
      'Description': this.currentDatacenter.Description,
      'Environment': this.currentDatacenter.Environment,
      'Domain': this.currentDatacenter.Environment,
      'Addresses': this.currentDatacenter.Addresses,
      'Account': this.currentDatacenter.Account,
      'Guid': '{CF706214-010F-4438-B363-D36A137197B4}',
      'OsTemplates': this.currentDatacenter.OsTemplates,
      'DataSource': this.currentDatacenter.DataSource,
      'IsDeleted': false,
      'IsPrivate': false
    };
  }
}

export default AddDatacenterService;
