import globalInputs from '../../../components/depconf/clouds/addcloud/addcloud.inputs.model';
import inputs from './addcustomer.inputs';

function IzAddCloudCustomerController(addCustomerService, countryService, $scope) {
  const vm = this;
  this.inputs = addCustomerService.inputs = inputs;
  this.local = localStorage;
  this.isLoading = true;
  this.$onInit = () => {
    countryService.countriesQ
      .then(countries => {
        inputs.companyContactAddress.billingCountry.data = inputs.companyContactAddress.country.data = countries;
        validateForm();
        if (this.init) {
          setInitData();
        }
        this.isLoading = false;
      });
  };

  this.toggleExemption = () => {
    this.exempt = !this.exempt;
  };

  this.customerLocation = {
    locations: {
      countryRegion: '',
      adminDistrict: '',
      locality: '',
      postalCode: '',
      addressLine: '',
      userLocation: ''
    }
  };
  this.customerBillingLocation = {
    locations: {
      countryRegion: '',
      adminDistrict: '',
      locality: '',
      postalCode: '',
      addressLine: '',
      userLocation: ''
    }
  };

  const updateMap = () => {
    validateForm();
    this.customerLocation.locations = {
      countryRegion: inputs.companyContactAddress.country.selected &&
      inputs.companyContactAddress.country.selected.displayName || '',

      adminDistrict: inputs.companyContactAddress.state.selected &&
      inputs.companyContactAddress.state.selected.displayName || '',

      locality: inputs.companyContactAddress.city.data || '',
      postalCode: inputs.companyContactAddress.zip.data || '',
      addressLine: (inputs.companyContactAddress.address.data || '') + (inputs.companyContactAddress.number.data || ''),
      userLocation: ''
    };
  };
  const updateBillingMap = () => {
    validateForm();
    this.customerBillingLocation.locations = {
      countryRegion: inputs.companyContactAddress.billingCountry.selected &&
        inputs.companyContactAddress.billingCountry.selected.displayName || '',

      adminDistrict: inputs.companyContactAddress.billingState.selected &&
        inputs.companyContactAddress.billingState.selected.displayName || '',

      locality: inputs.companyContactAddress.billingCity.data || '',
      postalCode: inputs.companyContactAddress.billingZip.data || '',
      addressLine: (inputs.companyContactAddress.billingAddress.data || '') +
        (inputs.companyContactAddress.billingNumber.data || ''),
      userLocation: ''
    };
  };

  this.countryCallbacks = {
    onChange: () => {
      updateMap();
      validateForm();
    }
  };
  this.countryBillingCallbacks = {
    onChange: updateBillingMap
  };
  this.inputCallbacks = {
    onBlur: updateMap
  };
  this.inputBillingCallbacks = {
    onBlur: updateBillingMap
  };

  this.genericCallbacks = {
    onChange: validateForm
  };

  this.validateOwner = validateForm;

  function validateForm() {
    const isVatValidByNonEu = vm.inputs.companyContactAddress.country.selected
      && vm.inputs.companyContactAddress.country.selected.Region.Short !== 'EU';
    const isVatValidByVatExemption = vm.exempt && vm.inputs.companyVatIdSelect.selected;
    const isVatValidByVatInupt = !vm.exempt && vm.inputs.companyVatIdInput.data !== '';

    $scope.tenantForm.$setValidity('required', isVatValidByNonEu || isVatValidByVatExemption || isVatValidByVatInupt);

    vm.validate({isValid: $scope.tenantForm.$valid});
  }

  function setInitData() {
    inputs.companyInformation.companyName.data = vm.init.CompanyName;
    inputs.companyInformation.typeOfBusiness.selected = {displayName: vm.init.CompanyBusinessType};
    setAddresses();
    vm.initData = vm.init.Accounts;
  }

  function setAddresses() {
    _.each(vm.init.Addresses, address => {
      if (address.isBilling) {
        inputs.companyContactAddress.billingAddress.data = address.AddressFirst;
        inputs.companyContactAddress.billingAddress2.data = address.AddressSecond;
        inputs.companyContactAddress.billingNumber.data = address.Number;
        inputs.companyContactAddress.billingZip.data = address.Zip;
        inputs.companyContactAddress.billingCity.data = address.City;
        inputs.companyContactAddress.billingCountry.selected = setCountry(address.CountryShort);
      } else {
        inputs.companyContactAddress.address.data = address.AddressFirst;
        inputs.companyContactAddress.address2.data = address.AddressSecond;
        inputs.companyContactAddress.number.data = address.Number;
        inputs.companyContactAddress.zip.data = address.Zip;
        inputs.companyContactAddress.city.data = address.City;
        inputs.companyContactAddress.billingCountry.selected = setCountry(address.CountryShort);
      }
    });

    function setCountry(Short) {
      return _.find(countryService.countries, country => country.Short === Short);
    }
  }

  this.$onDestroy = () => {
    globalInputs.Tenant = addCustomerService.getTenantPostModel(this.exempt);
  };
}

export default IzAddCloudCustomerController;
