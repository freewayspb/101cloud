import controller from './addcustomer.controller';
import template from './addcustomer.tpl.html';

const IzAddCloudCustomerComponent = {
  bindings: {
    validate: '&',
    init: '=',
    isNoInit: '='
  },
  controller,
  template
};

export default IzAddCloudCustomerComponent;
