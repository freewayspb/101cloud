import Country from '../Country.class';

class AddCloudCustomerService {

  /*@ngInject*/

  constructor(ownerService, headerService) {
    this.ownerService = ownerService;
    this.headerService = headerService;
  }

  getCustomerAddresses(inputs) {
    const addresses = [{
      Id: 2,
      Country: new Country(inputs.companyContactAddress.country.selected),
      Street: inputs.companyContactAddress.address.data,
      StreetNumber: inputs.companyContactAddress.number.data,
      ZipCode: inputs.companyContactAddress.zip.data,
      City: inputs.companyContactAddress.city.data,
      Longitude: null,
      Latitude: null,
      IsDefault: true,
      IsBilling: false
    }];
    if (!inputs.companyContactAddress.receiveBillings.data.model) {
      const billing = {
        Id: 1,
        Country: new Country(inputs.companyContactAddress.billingCountry.selected),
        Street: inputs.companyContactAddress.billingAddress.data,
        StreetNumber: inputs.companyContactAddress.billingNumber.data,
        ZipCode: inputs.companyContactAddress.billingZip.data,
        City: inputs.companyContactAddress.billingCity.data,
        Longitude: null,
        Latitude: null,
        IsDefault: true,
        IsBilling: true
      };
      addresses.push(billing);
    }
    return addresses;
  }

  getTenantPostModel(exempt) {
    let VAT;
    if (exempt && this.inputs.companyVatIdSelect.selected) {
      VAT = this.inputs.companyVatIdSelect.selected.displayName;
    } else {
      VAT = this.inputs.companyVatIdInput.data;
    }
    return {
      ContactPerson: this.ownerService.getContactPersonPostModel(),
      Owners: this.ownerService.getOwnersPostModel(),
      MinimalUser: this.inputs.companyInformation.quantityOfUsers.selected.min,
      MaximalUser: this.inputs.companyInformation.quantityOfUsers.selected.max,
      Addresses: this.getCustomerAddresses(this.inputs),
      Id: this.headerService.getTenantId(),
      Name: this.inputs.companyInformation.companyName.data,
      Description: '',
      ValueAddedTaxID: this.inputs.companyContactAddress.country.selected.Region.Short === 'EU'
        ? VAT
        : '',
      Type: {
        Id: this.inputs.companyInformation.typeOfBusiness.selected.id,
        Type: this.inputs.companyInformation.typeOfBusiness.selected.displayName
      },
      MicrosoftCustomer: null
    };
  }

  getCompanyPutModel(CompanyId) {
    const Accounts = this.ownerService.getOwnersPostModel();
    Accounts.push(this.ownerService.getContactPersonPostModel());
    const Addresses = this.getCustomerAddresses(this.inputs);
    return {
      Accounts,
      CompanyId,
      Addresses,
      CompanyBusinessType: this.inputs.companyInformation.typeOfBusiness.selected.displayName,
      CompanyName: this.inputs.companyInformation.companyName.data,
      UsersRange: null
    };
  }
}

export default AddCloudCustomerService;
