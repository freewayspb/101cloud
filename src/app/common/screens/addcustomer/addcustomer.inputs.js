const getCustomerFields = () => {
  return {
    companyInformation: {
      companyName: {
        title: 'Company Name',
        placeholder: 'Enter Company Name',
        data: '',
        options: {
          required: true
        }
      },
      quantityOfUsers: {
        title: 'Number of Users that will use the cloud',
        data: [
          {displayName: '1-100', min: 1, max: 100},
          {displayName: '101-200', min: 101, max: 200},
          {displayName: '201-500', min: 201, max: 500},
          {displayName: '501-1000', min: 501, max: 1000},
          {displayName: 'More than 1000', min: 1000, max: ''}
        ],
        selected: {displayName: '1-100', min: 1, max: 100}
      },
      typeOfBusiness: {
        title: 'Type of Business',
        data: [
          {
            id: 1,
            displayName: 'Private Person'
          }, {
            id: 2,
            displayName: 'Corporate General'
          }, {
            id: 3,
            displayName: 'Education'
          }, {
            id: 4,
            displayName: 'Government'
          }, {
            id: 5,
            displayName: 'Non-Profit'
          }
        ],
        selected: { id: 2, displayName: 'Corporate General' }
      }
    },
    companyContactAddress: {
      receiveBillings: {
        data: {
          title: 'Use for Contact & Billing',
          model: true,
          valueTrue: true,
          valueFalse: false
        }
      },
      address: {
        title: 'Street',
        placeholder: 'Enter Street Name',
        data: ''
      },
      number: {
        title: 'House Number',
        placeholder: 'Enter House Number',
        data: ''
      },
      address2: {
        title: 'Address 2',
        placeholder: 'Enter Address 2',
        data: ''
      },
      zip: {
        title: 'ZIP',
        placeholder: 'Enter ZIP',
        data: ''
      },
      city: {
        title: 'City',
        placeholder: 'Enter City',
        data: ''
      },
      state: {
        title: 'State',
        data: [
          {displayName: 'Generic State'}
        ],
        selected: {}
      },
      country: {
        title: 'Country',
        data: [],
        selected: undefined,
        options: {
          required: true
        }
      },
      billingAddress: {
        title: 'Street',
        placeholder: 'Enter Street',
        data: ''
      },
      billingNumber: {
        title: 'House Number',
        placeholder: 'Enter House Number',
        data: ''
      },
      billingAddress2: {
        title: 'Address 2',
        placeholder: 'Enter Address 2',
        data: ''
      },
      billingZip: {
        title: 'ZIP',
        placeholder: 'Enter ZIP',
        data: ''
      },
      billingCity: {
        title: 'City',
        placeholder: 'Enter Billing City',
        data: ''
      },
      billingState: {
        title: 'State',
        data: [
          {displayName: 'Generic State'}
        ],
        selected: {}
      },
      billingCountry: {
        title: 'Country',
        data: [],
        selected: {}
      }
    },
    companyVatIdInput: {
      title: 'Company VAT ID *',
      placeholder: 'Enter Company VAT ID',
      data: ''
    },
    companyVatIdSelect: {
      title: 'Company VAT ID *',
      placeholder: 'Select the Reason for Exemption',
      data: [
        {displayName: 'Charity'},
        {displayName: 'Non-EU Customer'},
        {displayName: 'EU Non-VAT Registreted Customer'},
        {displayName: 'VAT unavailable'}
      ],
      options: {
        classNameWrapper: 'no-margin'
      }
    },
    ownerInformation: []
  };
};

export default getCustomerFields();
