import template from './checkout-name.tpl.html';
import controller from './checkout-name.controller';

const CheckoutNameComponent = {
  template,
  controller,
  bindings: {
    data: '<'
  }
};

export default CheckoutNameComponent;
