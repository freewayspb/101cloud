function CheckoutDescriptionController($timeout) {
  this.$onInit = () => {
    $timeout(() => {
      this.realHeight = $('#checkout-description__description').height();
      this.theoreticalHeight = $('#checkout-description__description-hidden').height();
    }, 100);

  }
}

export default CheckoutDescriptionController;
