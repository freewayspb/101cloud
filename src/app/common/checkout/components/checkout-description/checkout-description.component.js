import template from './checkout-description.tpl.html';
import controller from './checkout-description.controller';

const CheckoutDescriptionComponent = {
  template,
  controller,
  bindings: {
    data: '<'
  }
};

export default CheckoutDescriptionComponent;
