import controller from './iz-add-cloud-checkout.controller';
import template from './iz-add-cloud-checkout.tpl.html';

const IzAddCloudCheckoutComponent = {
  controller,
  template,
  bindings: {
    data: '='
  }
};

export default IzAddCloudCheckoutComponent;
