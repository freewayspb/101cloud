function IzAddCloudCheckoutController(currencyService, headerService, $scope, alertsService) {
  const vm = this;
  this.isLoading = false;
  this.vat = currencyService.vat;
  this.license = {
    data: {
      valueFalse: false,
      valueTrue: true,
      model: false,
      title: 'I accept the'
    },
    options: {
      link: {
        href: '#',
        title: 'terms and conditions'
      }
    }
  };
  this.periodSelect = {
    data: [
      {displayName: 'Per Day'},
      {displayName: 'Per Month'},
      {displayName: 'Per Quarter'},
      {displayName: 'Per Year'}
    ]
  };
  $scope.$on('onCurrencyChange', (e, newVal, oldVal) => {
    alertsService.setAlert({msg: 'Cannot change currency right now', type: 'warning'});
    headerService.setCurrency(oldVal, {skipNext: true});
  });

  class CheckoutItem {
    constructor(item) {
      let vdcList;
      if (item.type === 'virtual-datacenter' && item.id === 2) {
        vdcList = this.createVDCList(item.entity);
      }
      this.product = {
        name: item.Name,
        type: item.Type
      };
      this.description = {
        benefitList: item.BenefitsList,
        integration: item.IntegrationList,
        description: item.Description,
        list: vdcList
      };
      vm.periodSelect.selected = item.period || {displayName: 'n/a'};
      this.period = _.cloneDeep(vm.periodSelect);
      this.price = item.Price;
      this.isAddServices = item.isAddServices;
      this.id = item.id;
    }

    createVDCList(item) {
      return [
        `${item.DownloadSpeed} Mbps up- & download`,
        `${item.PublicIp4} IPv4 incl. /${item.PublicIp6} IPv6 network`,
        `${item.Ram} GB RAM and ${item.Cpu} vCPU (1 vCPU min ${item.MinCpuFrequency} GHz)`,
        `${item.Storage} GB Performance Storage`,
        `Support 24/7/365`
      ];
    }
  }

  this.$onInit = () => {
    if (vm.cloudData) {
      this.items = [new CheckoutItem(vm.cloudData.Cloud)];
      _.each(vm.cloudData.Subscriptions, subscription => {
        if (subscription.id === 2) {
          subscription.Type = vm.cloudData.Subscriptions._virtualDataCenter.Name;
          subscription.entity = vm.cloudData.Subscriptions._virtualDataCenter;
        }
        if (subscription.id !== 4) {
          this.items.push(new CheckoutItem(subscription));
        }
      });
    }
    if (vm.generalData) {
      _.each(vm.generalData.data, item => {
        item.price = item.Price;
      });
    }
  };

  this.incrementQty = item => {
    item.qty++;
  };

  this.decrementQty = item => {
    if (item.qty) {
      item.qty--;
    }
  };

  this.getItemsLength = () => (this.items && this.items.length) || (this.generalData && this.generalData.data.length);

  this.getPriceString = (price, currency) => currencyService.getPriceString(price, currency);

  this.getSubTotal = () => {
    const items = this.items && this.items.length
      ? this.items
      : this.generalData.data;

    return _.reduce(items, (sum, element) => {
      const realprice = element.price > -1 ? element.price : 0;
      return sum + realprice;
    }, 0);
  };
  this.getPriceStr = currencyService.getPriceString.bind(currencyService);
}

export default IzAddCloudCheckoutController;
