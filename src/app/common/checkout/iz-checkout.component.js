import controller from './iz-checkout.controller';
import template from './iz-checkout.tpl.html';

const IzAddCloudCheckoutComponent = {
  controller,
  template,
  bindings: {
    cloudData: '=',
    generalData: '=',
    callbacks: '=',
    isLoading: '=',
    tableOptions: '='
  }
};

export default IzAddCloudCheckoutComponent;
