import angular from 'angular';

import AuthService from './auth.service';

const auth = angular
  .module('app.auth', ['AdalAngular'])
  .service('authService', AuthService)
  .name;

export default auth;
