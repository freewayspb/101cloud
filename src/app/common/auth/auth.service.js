class AuthService {
  /*ngInject*/
  constructor(headerService, adalAuthenticationService) {
    this.adalAuthenticationService = adalAuthenticationService;
    this.headerService = headerService;
    this.user = {};
    this.userInfo = headerService.user = adalAuthenticationService.userInfo;
  }
  login() {
    this.adalAuthenticationService.login();
  };
  logout() {
    this.user = this.headerService.user = {};
    this.adalAuthenticationService.logOut();
  };

  loginInProgress() {this.adalAuthenticationService.loginInProgress();};
}

export default AuthService;
