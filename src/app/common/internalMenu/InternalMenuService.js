import find from 'lodash/find';
class InternalMenuService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  /**
   * @name getList
   * @desc Inner Method, which provides current internal menu list.
   * @returns {Object[]}
   * @memberOf Services.InternalMenuService
   */

  getList(param) {
    return this.dataService.getItems('internalmenu.json').then(result => {
      const section = find(result, function (o) {
        return o.section === param;
      });
      return section.list;
    }, error => {
      console.error(':::: ERROR OCCURRED IN MENU SERVICE ::::', error);
    });
  }
}

export default InternalMenuService;
