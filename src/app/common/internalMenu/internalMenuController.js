/* ngInject */
function InternalMenuController(menuService, $state) {
  const vm = this;
  vm.$state = $state;
  vm.$onInit = () => {
    menuService.getList(vm.section).then(list => {
      vm.menuList = list;
    });
  };
}

export default InternalMenuController;
