import controller from './internalMenuController';
import template from './internalMenu.tpl.html';

const InternalMenuComponent = {
  controller,
  template,
  bindings: {
    section: '='
  }
};

export default InternalMenuComponent;
