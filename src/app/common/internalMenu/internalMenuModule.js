import InternalMenuComponent from './InternalMenuComponent';

const internalMenu = angular
  .module('app.internalmenu', [])
  .component('internalMenu', InternalMenuComponent)
  .name;

export default internalMenu;
