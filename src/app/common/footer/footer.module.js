import FooterComponent from './footer.component';

const footer = angular
  .module('app.common.footer', [])
  .component('pageFooter', FooterComponent)
  .name;

export default footer;
