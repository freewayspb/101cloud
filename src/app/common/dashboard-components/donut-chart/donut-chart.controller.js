function izDonutChartController() {
  const vm = this;
  vm.$onInit = () => {
    vm.labels = vm.chartOptions.labels;
    vm.data = vm.chartOptions.data;
    vm.options = vm.chartOptions.options;
  };
}

export default izDonutChartController;
