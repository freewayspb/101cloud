import controller from './donut-chart.controller';
import template from './donut-chart.tpl.html';

const IzDonutChart = {
  controller,
  template,
  bindings: {
    chartOptions: '<'
  }
};

export default IzDonutChart;
