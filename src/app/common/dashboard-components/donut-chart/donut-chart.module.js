import DonutChart from './donut-chart.component';

const izDonutChart = angular
  .module('app.donut-chart', [])
  .component('izDonutChart', DonutChart)
  .name;

export default izDonutChart;
