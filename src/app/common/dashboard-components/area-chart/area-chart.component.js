import controller from './area-chart.controller';
import template from './area-chart.tpl.html';

const IzAreaChart = {
  controller,
  template,
  bindings: {
    chartOptions: '='
  }
};

export default IzAreaChart;
