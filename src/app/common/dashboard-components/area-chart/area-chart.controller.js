function izAreaChartController() {
  const vm = this;
  this.$onInit = () => {
    vm.labels = vm.chartOptions.labels;
    vm.data = vm.chartOptions.data;
    vm.options = vm.chartOptions.options;
  };
}

export default izAreaChartController;
