import AreaChart from './area-chart.component';

const izAreaChart = angular
  .module('app.area-chart', [])
  .component('izAreaChart', AreaChart)
  .name;

export default izAreaChart;
