import area from './area-chart/area-chart.module';
import donutWidget from './donut-widget/donut-widget.module';

const izChart = angular
  .module('app.dashboard-components', ['chart.js', area, donutWidget])
  .name;

export default izChart;
