import template from './donut-widget.tpl.html';
import controller from './donut-widget.controller';

const DonutWidgetComponent = {
  template,
  controller,
  bindings: {
    chartOptions: '=',
    onSelect: '&'
  }
};

export default DonutWidgetComponent;
