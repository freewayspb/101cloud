function DonutWidgetController() {
  const vm = this;
  vm.$onInit = () => {
    vm.donut = vm.chartOptions;
  };
}

export default DonutWidgetController;
