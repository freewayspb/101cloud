import component from './donut-widget.component';

const DonutWidgetModule = angular
  .module('app.donutwidget', [])
  .component('izDonutWidget', component)
  .name;

export default DonutWidgetModule;
