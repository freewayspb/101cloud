import angular from 'angular';

import auth from './auth/auth.module';
import footer from './footer/footer.module';
import menu from './menu/menu.module';
import header from './header/header.module';
import forms from './forms/forms.module';
import internalMenu from './internalMenu/internalMenuModule';
import informationPanel from './information-panel/information-panel.module';
import izTable from './iz-table/iz-table.module';
import screens from './screens/screens.module';
import areaGraph from './cards/area-graph/areagraph.module';
import dashboardTable from './cards/dashboard-table/dashboard-table.module';
import loader from './loader/loader.module';

import cards from './cards/cards.module';

import addServer from './addserver/addserver.component';
    import addServerGeneral from './addserver/steps/general/iz-add-server-general.component';
    import addServerLicense from './addserver/steps/license/iz-add-server-license.component';
    import addServerMaintenance from './addserver/steps/maintenance/iz-add-server-maintenance.component';

import addService from './addservice/addservice.component';

import checkout from './checkout/iz-checkout.component';
    import CheckoutDescription from './checkout/components/checkout-description/checkout-description.component';
    import CheckoutName from './checkout/components/checkout-name/checkout-name.component';

import checkoutService from './checkout/iz-checkout.service';

import addServerService from './addserver/addserver.service';
import addServiceService from './addservice/addservice.service';

import izChart from './dashboard-components/dashboard-components.module';

import notification from './alerts/alerts.module';

import pricelist from './pricelist/pricelist.module';

const common = angular
  .module('app.common', [auth, footer,
    menu,
    header,
    forms,
    internalMenu,
    informationPanel,
    izTable,
    screens,
    izChart,
    notification,
    areaGraph,
    dashboardTable,
    cards,
    pricelist,
    loader])
  .component('addServer', addServer)
      .component('izAddServerGeneral', addServerGeneral)
      .component('izAddServerLicense', addServerLicense)
      .component('izAddServerMaintenance', addServerMaintenance)
  .component('addService', addService)
  .component('izCheckoutScreen', checkout)
  .component('checkoutDescription', CheckoutDescription)
  .component('checkoutName', CheckoutName)
  .service('addServerService', addServerService)
  .service('addServiceService', addServiceService)
  .service('checkoutService', checkoutService)
  .name;

export default common;
