export default class HeaderService {

  /*@ngInject*/

  constructor(dataService, apiService, $q, notificationService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.notificationService = notificationService;
    this.currentTenantId = '';
    this.tenants = [];
    this.tenantPromise = $q.defer();
    this.user = {};
    this.currency = {};
  }

  getTenants(params) {
    return this.dataService.get(this.apiService.basePaths.tenants, params).then(
      result => result,
      error => {
        console.error(':::: ERROR OCCURRED IN HEADER SERVICE ::::', error);
      });
  }

  setTenant(tenant) {
    this.notificationService.broadcast('onTenantChange', tenant, this.tenant);
    this.tenant = tenant;
    this.currentTenantId = tenant.Id;
    localStorage.Tenant = JSON.stringify(tenant);
    this.tenantPromise.resolve();
  }

  setCurrency(currency) {
    if (this.currency.Name !== currency.Name) {
      this.notificationService.broadcast('onCurrencyChange', currency, this.currency);
    }
    this.currency = currency;
    this.currencies.selected = _.find(this.currencies.data, item => item.Name === currency.Name);
    localStorage.Currency = JSON.stringify(currency);
  }

  getCurrency() {
    return this.currency;
  }

  setTenants(data) {
    this.tenants = data;
  }

  getTenantId() {
    return this.currentTenantId;
  }

  getTenantsPromise() {
    return this.tenantPromise.promise.then(() => this.tenants);
  }

  getTenantPromise() {
    return this.tenantPromise.promise
      .then(() => this.currentTenantId,
        error => {
          console.error(':::: ERROR OCCURRED IN HEADER SERVICE GET TENANT PROMISE ::::', error);
          throw error;
        });
  }
}
