import HeaderComponent from './header.component';
import HeaderService from './header.service';

const header = angular
  .module('app.common.header', [])
  .component('topHeader', HeaderComponent)
  .service('headerService', HeaderService)
  .name;

export default header;
