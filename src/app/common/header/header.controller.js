function HeaderController(headerService, adalAuthenticationService) {
  const vm = this;
  vm.tenants = {};
  vm.adalAuthenticationService = adalAuthenticationService;

  vm.userInfo = headerService.user = vm.adalAuthenticationService.userInfo;

  vm.$onInit = function () {
    vm.adalAuthenticationService.getUser().then(
      response => vm.user = response,
      error => console.error(error)
    );

    headerService.getTenants()
      .then(response => {
          headerService.setTenants(response);
          vm.tenants.data = response;
          if (response && response[0] && !vm.tenants.selected) {
            const index = getStoredTenantIndex(response) === -1
              ? 0
              : getStoredTenantIndex(response);
            vm.tenants.selected = response[index];
            vm.setTenant();
          }
        },
        error => console.error('error: ' + error));

    vm.languages = {
      data: [
        {
          Name: 'English',
          icon: {
            class: 'en',
            img: '/app/assets/img/en.svg'
          }
        },
        {
          Name: 'Deutsch',
          icon: {
            class: 'de',
            img: '/app/assets/img/de.svg'
          }
        },
        {
          Name: 'Francais',
          icon: {
            class: 'fr',
            img: '/app/assets/img/fr.svg'
          }
        },
        {
          Name: 'Espanol',
          icon: {
            class: 'es',
            img: '/app/assets/img/es.svg'
          }
        }
      ],
      selected: {
        Name: 'English',
        icon: {
          class: 'en',
          img: '/app/assets/img/en.svg'
        }
      }
    };
    vm.currency = headerService.currencies = {
      data: [
        {Name: 'CHF', Symbol: 'CHF'},
        {Name: 'EUR', Symbol: '€'},
        {Name: 'USD', Symbol: '$'}
      ]
    };

    if (localStorage.Currency) {
      vm.currency.selected = JSON.parse(localStorage.Currency);
    } else {
      vm.currency.selected = {Name: 'CHF', Symbol: 'CHF'};
    }
    headerService.setCurrency(vm.currency.selected);

    function getStoredTenantIndex(tenants) {
      const Tenant = JSON.parse(localStorage.Tenant || '{}');
      let index = -1;
      if (Tenant.Id) {
        index = _.findIndex(tenants, tenant => tenant.Id === Tenant.Id);
      }
      return index;
    }
  };

  vm.login = () => {
    vm.adalAuthenticationService.login();
  };
  vm.logout = () => {
    vm.user = headerService.user = {};
    vm.adalAuthenticationService.logOut();
  };
  vm.loginInProgress = () => adalAuthenticationService.loginInProgress();
  vm.setTenant = () => {
    headerService.setTenant(vm.tenants.selected);
  };
  vm.setCurrency = () => {
    headerService.setCurrency(vm.currency.selected);
  };
  vm.tenantCallbacks = {
    onChange: vm.setTenant
  };
  vm.currencyCallbacks = {
    onChange: vm.setCurrency
  };
}

export default HeaderController;
