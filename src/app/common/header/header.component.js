import controller from './header.controller';
import template from './header.tpl.html';

const HeaderComponent = {
  controller,
  template: template
};

export default HeaderComponent;
