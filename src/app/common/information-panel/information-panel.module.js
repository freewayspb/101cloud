import InformationPanelComponent from './information-panel.component';

const menu = angular
  .module('informationPanel', [])
  .component('izInformationPanel', InformationPanelComponent)
  .name;

export default menu;
