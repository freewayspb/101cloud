import controller from './information-panel.controller';
import template from './information-panel.tpl.html';

const InformationPanelComponent = {
  transclude: true,
  controller,
  template
};

export default InformationPanelComponent;
