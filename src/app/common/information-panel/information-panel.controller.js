import _ from 'lodash';
function InformationPanelController($scope, currencyService, informationPanelService) {

  const vm = this;

  this.model = {};
  this.vat = currencyService.vat;
  function setButtons() {
    if (informationPanelService.buttons && informationPanelService.buttons.length > 0) {
      vm.buttons = informationPanelService.buttons;
    }
  }

  $scope.$on('onInformationToggle', (event, params) => {
    this.setVisibility(params.setShown);
    this.setOpen(params.setOpen);
    setButtons();
  });

  $scope.$on('onInformationChange', (event, params) => {
    this.model = params;
    if (this.model.cb) {
      this.model.cb(new Date().getTime(), this.model);
    }
    setButtons();
    this.inputs.qty.data = this.model.qty;
  });

  $scope.$on('onInformationPriceListItemChange', (event, params) => {
    const id = new Date().getTime();
    if (this.model && this.model.type === 'price') {
      if (!params.id) {
        params.id = id;
        this.model.priceList.push(params);
      } else {
        const itemIndex = _.findIndex(this.model.priceList, element => element.id === params.id);
        params.id = id;
        this.model.priceList[itemIndex] = params;
      }
      if (params.cb) {
        params.cb(id, this.model);
      }
    }
    setButtons();
  });

  let isOpen = false,
    isShown = false;

  /**
   * @name setOpen
   * @desc Sets toolbar's expansion or toggles expansion/collapsion of side information panel
   * @returns {undefined}
   * @param {boolean} isExpanded sets toolbar expansion to this value
   */
  this.setOpen = isExpanded => {
    if (isExpanded === undefined) {
      isOpen = !isOpen;
    } else {
      isOpen = isExpanded;
    }
  };

  /**
   * @name setVisibility
   * @desc Sets tooldar's visibility or toggles visibility of side information panel
   * @returns {undefined}
   * @param {boolean} isVisible sets toolbar visibility to this value
   */
  this.setVisibility = isVisible => {
    if (isVisible === undefined) {
      isShown = !isShown;
    } else {
      isShown = isVisible;
    }
  };

  /**
   * @name isOpen
   * @desc Tells to view whether we should expand the toolbar
   * @returns {boolean}
   */
  this.isOpen = () => isOpen;

  /**
   * @name isShown
   * @desc Tells to view whether we should show the toolbar
   * @returns {boolean}
   */
  this.isShown = () => isShown;

  /**
   * @name getSum
   * @desc Tells to view whether we should show the toolbar
   * @returns {number}
   */
  this.getPriceListSubtotal = () =>
    _.reduce(this.model.priceList, (sum, newVal) => sum + newVal.price * (newVal.inputs.qty.data || 1), 0);

  this.getItemSubtotal = () => this.model.price * this.model.qty * (1 - this.model.discount / 100);

  this.getPriceStr = price => currencyService.getPriceString(price);

  function refreshData() {
    if (vm.inputs.qty.data === null) {
      vm.inputs.qty.placeholder = vm.model.qty = 1;
    } else {
      vm.model.qty = +vm.inputs.qty.data;
    }
  }
  this.callbacks = {
    onChange: refreshData
  };
  this.inputs = {
    qty: {
      data: 1,
      placeholder: 1,
      options: {
        classNameWrapper: 'information-panel-qty-select',
        classNameInput: 'information-panel-qty-no-arrows',
        type: 'number',
        min: 1
      }
    }
  };
}

export default InformationPanelController;
