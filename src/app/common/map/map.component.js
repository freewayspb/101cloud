import controller from './map.controller';
import template from './map.tpl.html';

const mapComponent = {
  bindings: {
    mapName: '@',
    zoom: '@',
    ngModel: '=',
    hideLocation: '<'
  },
  controller,
  template
};

export default mapComponent;
