function MapController($scope, mapService) {
  const vm = this;

  vm.$onInit = () => {
    if(!vm.ngModel.pushpins) {
      vm.ngModel.pushpins = [];
    }
    vm.zoom = vm.zoom ? _.toNumber(vm.zoom) : 15;
    vm.mapOptions = {
      center: { latitude: 47.428199768066406, longitude: 8.5729398727417},
      zoom: vm.zoom,
      mapType: 'road',
      options: {
        disableScrollWheelZoom: true,
        disableTouchInput: true,
        enableClickableLogo: false,
        disableZooming: true,
        enableSearchLogo: false,
        showDashboard: true,
        disableBirdseye: true,
        showMapTypeSelector: false
      }
    };
    $scope.$watchCollection(() => vm.ngModel.locations, vm.getLocation);
  };

  vm.getLocation = data => {
    if (data && (data.countryRegion || data.locality || data.addressLine)) {
      mapService.getLocation(mapService.getBingRestString(data)).then(
        coordinates => {
          if(coordinates) {
            const locations = {
              latitude: coordinates[0],
              longitude: coordinates[1]
            };
            if(!vm.ngModel.pushpins[0]) {
              vm.ngModel.pushpins.push({location: locations});
            } else {
              vm.ngModel.pushpins[0].location = locations;
            }
            vm.setCenter(locations);
          }
        }
      );
    }
  };

  vm.setCenter = locations => {
    vm.map.setView({
      center: {
        latitude: locations.latitude,
        longitude: locations.longitude
      },
      zoom: locations.zoom || vm.zoom
    });
  };

  vm.getUserPosition = () => {
    mapService.getUserPosition(vm)
      .then(response => {
        const data = {
          latitude: response.coords.latitude,
          longitude: response.coords.longitude,
          zoom: vm.zoom || 13
        };
        vm.setCenter(data);
    },
      error => console.error(error));
  };

  vm.onMapReady = function (map) {
    vm.map = map;
    if (vm.ngModel) {
      if (!vm.hideLocation) {
        vm.getUserPosition();
      }
      vm.getLocation(vm.ngModel.locations);
    }
  };
}

export default MapController;
