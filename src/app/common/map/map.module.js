import MapComponent from './map.component';
import MapService from './map.service';

const mapModule = angular
  .module('mapModule', ['angularBingMaps'])
  .component('izMap', MapComponent)
  .service('mapService', MapService)
  .name;

export default mapModule;
