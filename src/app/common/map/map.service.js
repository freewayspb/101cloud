export default class MapService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.bingApiKey = 'AjgPLsKBC6t_mm19IDn1-fsg_ZXu7y129dHOxCcwa0QFcGw0gcRH4SLIUwEzVw-X';
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getUserPosition(options) {
    if (navigator.geolocation){
      return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(pos => {
          resolve(pos);
        }, err => {
          reject(err);
        }, options);
      });
    } else {
      return Promise.resolve({
        pos: {
          coords: {
            latitude: 0,
            longitude: 0
          }
        }
      });
    }
  }

  getBingRestString (data) {
    return '?countryRegion=' + (data.countryRegion || '') +
      '&adminDistrict=' + (data.adminDistrict || '') +
      '&locality=' + (data.locality || '') +
      '&postalCode=' + (data.postalCode || '') +
      '&addressLine=' + (data.addressLine || '') +
      '&userLocation=' + (data.userLocation || '') +
      '&includeNeighborhood=' + (data.includeNeighborhood || true) +
      '&maxResults=' + (data.maxResults || 10) +
      '&jsonp=JSON_CALLBACK' +
      '&key=' + this.bingApiKey;
  }

  getLocation(params) {
    return this.dataService.CallBingRestService(this.apiService.basePaths.location + params).then(response => {
        if(response.resourceSets && response.resourceSets[0].resources[0]) {
          return response.resourceSets[0].resources[0].point.coordinates;
        } else {
          return false;
        }
      },
      error => console.error(':::: ERROR OCCURRED IN MAP CONTROLLER ::::', error)
    );
  }
}

