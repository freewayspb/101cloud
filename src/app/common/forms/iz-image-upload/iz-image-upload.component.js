import controller from './iz-image-upload.controller';
import template from './iz-image-upload.tpl.html';

const component = {
  bindings: {
    ngModel: '='
  },
  transclude: true,
  controller,
  template
};

export default component;
