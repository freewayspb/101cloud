import modalTemplate from './modal/modal.tpl.html';
import modalController from './modal/modal.controller';

function izImageUploadController(FileUploader, $uibModal) {
  this.seed = new Date().getTime();
  this.imageUploader = new FileUploader(
    {
      onAfterAddingFile: () => {
        const preview = document.querySelector(`.iz-image-upload__attacher_preview.iu-${this.seed}`);
        const file = document.querySelector(`.iz-image-upload__attacher_select.iu-${this.seed}`).files[0];
        const reader = new FileReader();

        reader.onloadend = () => {
          preview.src = this.imageBase64Original = reader.result;
        };

        if (file) {
          reader.readAsDataURL(file);
        } else {
          preview.src = '';
        }
      }
    }
  );

  this.setCropParams = params => {
    const preview = document.querySelector(`.iz-image-upload__attacher_preview.iu-${this.seed}`);
    preview.src = params.ImageBase64;
  };

  this.open = () => {
    const modal = $uibModal.open({
      animation: true,
      template: modalTemplate,
      controller: modalController,
      controllerAs: '$ctrl',
      size: 'lg',
      resolve: {
        imageBase64: () => {
          return this.imageBase64Original;
        }
      }
    });

    modal.result
      .then(this.setCropParams);
  };
}

export default izImageUploadController;
