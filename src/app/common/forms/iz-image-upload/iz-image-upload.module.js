import component from './iz-image-upload.component';
import 'angular-img-cropper';

const DraggableModule = angular
  .module('forms.izDraggableModule', ['angular-img-cropper'])
  .component('izImageUpload', component)
  .name;

export default DraggableModule;
