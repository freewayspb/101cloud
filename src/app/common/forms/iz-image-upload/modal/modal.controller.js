function CloudTypeModalController($uibModalInstance, imageBase64) {

  this.$onInit = () => {
    this.imageBase64 = imageBase64;
    this.cropper = {
      sourceImage: imageBase64,
      croppedImage: null
    };
    this.bounds = {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0
    };
  };


  this.close = () => {
    $uibModalInstance.dismiss();
  };

  this.confirm = () => {
    $uibModalInstance.close({ImageBase64: this.cropper.croppedImage, ImageBounds: this.bounds});
  };
}

export default CloudTypeModalController;
