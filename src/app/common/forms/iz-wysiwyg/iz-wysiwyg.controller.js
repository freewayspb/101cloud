function WysiwygController() {
  this.tinymceOptions = {
    plugins: 'link image code lists advlist',
    toolbar: 'bold italic underline | bullist numlist | indent outdent | alignleft aligncenter alignright | ' +
    'code | importpdf',
    menubar: '',
    setup: function (editor) {
      editor.addButton('importpdf', {
        icon: 'icon iz-icon icon-pdf-download'
      });
    }
  };
}

export default WysiwygController;
