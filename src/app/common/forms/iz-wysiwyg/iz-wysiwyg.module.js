import WysiwygComponent from './iz-wysiwyg.component';
import 'angular-ui-tinymce';

const WysiwygModule = angular
  .module('forms.izWysiwygModule', ['ui.tinymce'])
  .component('izWysiwyg', WysiwygComponent)
  .name;

export default WysiwygModule;
