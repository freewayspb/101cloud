import controller from './iz-wysiwyg.controller';
import template from './iz-wysiwyg.tpl.html';

const wysiwygComponent = {
  bindings: {
    ngModel: '='
  },
  transclude: true,
  controller,
  template
};

export default wysiwygComponent;
