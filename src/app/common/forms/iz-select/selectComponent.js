import controller from './SelectController';
import template from './select.tpl.html';

const selectComponent = {
  bindings: {
    name: '@',
    data: '=',
    ngModel: '=',
    callbacks: '=',
    callbackOptions: '=',
    autocomplete: '@',
    ngDisabled: '='
  },
  transclude: true,
  controller,
  template
};

export default selectComponent;
