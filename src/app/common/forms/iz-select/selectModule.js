import SelectComponent from './selectComponent';
import SelectService from './selectService';
import uiSelect from 'ui-select'

const selectModule = angular
  .module('forms.selectModule', [uiSelect])
  .component('izSelect', SelectComponent)
  .service('izSelectService', SelectService)
  .name;

export default selectModule;
