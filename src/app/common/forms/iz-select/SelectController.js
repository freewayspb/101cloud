function SelectController(izSelectService) {
  const vm = this;

  this.selectId = 'select-' + new Date().getTime();

  vm.$onInit = () => {
    izSelectService.ngModel = this.ngModel;
  };

  vm.disabled = undefined;

  vm.enable = function() {
    vm.disabled = false;
  };

  vm.disable = function() {
    vm.disabled = true;
  };

  vm.clear = function() {
    vm.ngModel.selected = undefined;
  };

  vm.setOption = izSelectService.setOption = value => {
    vm.ngModel.selected = value;
    vm.ngModel.placeholder = undefined;
    if (vm.callbacks && vm.callbacks.onSelect) {
      vm.callbacks.onSelect();
    }
  };
}

export default SelectController;
