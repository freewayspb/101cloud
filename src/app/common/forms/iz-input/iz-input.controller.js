function InputController() {
  const vm = this;
  this.$onInit = () => {
    if (vm.ngModel && !vm.ngModel.options) {
      vm.ngModel.options = {};
    }
    if (vm.callbackOptions && vm.callbackOptions.onChange && !vm.callbackOptions.onChange.params) {
      vm.callbackOptions.onChange.params = {};
    }
  };
}

export default InputController;
