import InputComponent from './iz-input.component';

const InputModule = angular
  .module('forms.izInputModule', [])
  .component('izInput', InputComponent)
  .name;

export default InputModule;
