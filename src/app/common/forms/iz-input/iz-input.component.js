import controller from './iz-input.controller';
import template from './iz-input.tpl.html';

const selectComponent = {
  bindings: {
    ngModel: '=',
    callbacks: '=',
    callbackOptions: '=',
    ngDisabled: '<',
    uiMask: '@',
    uiMaskPlaceholderChar: '@',
    name: '@'
  },
  controller,
  template
};

export default selectComponent;
