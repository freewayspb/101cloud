import template from './owner-select.tpl.html';
import controller from './owner-select.controller';

const ownerSelectComponent = {
  bindings: {
    validate: '&',
    init: '='
  },
  controller,
  template
};

export default ownerSelectComponent;
