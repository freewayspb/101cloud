import OwnerSelectComponent from './owner-select.component';
import OwnerSelectService from './owner-select.service';

const OwnerSelectModule = angular
  .module('forms.ownerSelectModule', [])
  .component('izOwnerSelect', OwnerSelectComponent)
  .service('ownerSelectService', OwnerSelectService)
  .name;

export default OwnerSelectModule;
