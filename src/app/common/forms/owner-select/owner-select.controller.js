function ownerSelectController(ownerSelectService, apiService, $scope) {
  const vm = this;

  this.$onInit = () => {
    const accountRolesMap = {
      'user': 1,
      'business-owner': 2,
      'technical-owner': 3,
      'business-owner-deputy': 4,
      'technical-owner-deputy': 5,
      'contact-person': 6
    };
    ownerSelectService.get(apiService.tenantAccountRoles)
      .then(result => {
        const accountRoles = _.filter(result,
          role => role.Id !== accountRolesMap['user'] && role.Id !== accountRolesMap['contact-person']);
        const accountRoleObjects = _.map(accountRoles, role => {
          return {
            role,
            inputs: {
              data: [],
              title: role.RoleName,
              placeholder: 'Select'
            }
          };
        });
        this.accountRoles = _.chunk(accountRoleObjects, 2);
      });
  };

  this.genericCallbacks = {
    onChange: validate
  };

  function validate() {
    vm.validate({isValid: $scope.ownersSubForm.$valid});
  }
}

export default ownerSelectController;
