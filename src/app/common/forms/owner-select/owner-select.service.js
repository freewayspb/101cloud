class ownerSelectService {

  constructor(dataService) {
    this.dataService = dataService;
  }

  getOwnersPostModel() {
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN OWNER SELECT SERVICE ::::', error);
      throw error;
    });
  }
}

export default ownerSelectService;
