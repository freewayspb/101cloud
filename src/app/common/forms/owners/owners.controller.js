import inputs from './owners.inputs';

function ownersController(ownerService, countryService, $scope, apiService) {
  const vm = this;
  this.$onInit = () => {
    countryService.countriesQ
      .then(countries => {
        let phones = _.map(countries, country => {
          return +country.Phone
        });
        phones = _.filter(phones, phone => {
          return !_.isNaN(phone)
        });
        phones.push(1809);
        phones.push(1829);
        phones = _.sortBy(phones);
        phones = _.uniq(phones);
        phones = _.map(phones, phone => {
          return {
            displayName: '+' + phone
          };
        });
        ownerService.get(apiService.tenantAccountRoles)
          .then(result => {
            result = _.filter(result, item => item.Id !== 1);
            if (!ownerService.owners || (this.init && this.init.length > 0)) {
              this.inputs = ownerService.owners = _.map(result, role => {
                if (this.init && this.init.length > 0) {
                  const currentInput = _.find(this.init, item => item.AccountType.Id === role.Id);
                  inputs.title.selected = {displayName: currentInput.title};
                  inputs.givenName.data = currentInput.FirstName;
                  inputs.lastName.data = currentInput.LastName;
                  inputs.middleName.data = currentInput.MiddleName;
                  inputs.suffix.data = currentInput.Suffix;
                  inputs.password.data = currentInput.Password;
                  inputs.email.data = currentInput.Address;
                  inputs.alternativeEmail.data = currentInput.AddressAlternative;
                }
                return {
                  displayName: role.RoleName + ' Information',
                  roleName: role.RoleName,
                  formName: role.RoleName.split(' ').join(''),
                  fields: _.cloneDeep(inputs)
                };
              });
            } else {
              this.inputs = ownerService.owners;
            }
            _.each(this.inputs, owner => {
              owner.fields.phone1CountryCode.data = owner.fields.phone2CountryCode.data = phones;
            });
            validate();
          });
      });
  };

  this.genericCallbacks = {
    onChange: validate
  };

  function validate() {
    vm.validate({isValid: $scope.ownersSubForm.$valid});
  }
}

export default ownersController;
