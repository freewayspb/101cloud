import template from './owners.tpl.html';
import controller from './owners.controller';

const ownersComponent = {
  bindings: {
    validate: '&',
    init: '='
  },
  controller,
  template
};

export default ownersComponent;
