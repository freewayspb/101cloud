import Owner from './Owner.class';

class ownerService {

  constructor(dataService) {
    this.dataService = dataService;
  }

  getOwnersPostModel() {
    return _.map(this.owners.slice(0, 4), rawOwner => new Owner(rawOwner));
  }

  getContactPersonPostModel() {
    return new Owner(this.owners[4])
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN OWNER SERVICE ::::', error);
      throw error;
    });
  }
}

export default ownerService;