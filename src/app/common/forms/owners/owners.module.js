import OwnersComponent from './owners.component';
import OwnerService from './owner.service';

const OwnersModule = angular
  .module('forms.ownersModule', [])
  .component('izOwners', OwnersComponent)
  .service('ownerService', OwnerService)
  .name;

export default OwnersModule;
