import _ from 'lodash';
function RadiogroupController() {
  this.$onInit = () => {
    if(this.ngModel.model !== undefined && this.ngModel.data) {
      this.index = _.findIndex(this.ngModel.data, (o) => { return o.value === this.ngModel.model; });
      this.onClick(null, this.index);
    } else {
      this.index = 0;
    }
    this.ngModel.active = this.ngModel.data[this.index];
  };

  this.onClick = (e, index) => {
    this.callbacks && this.callbacks.onClick && this.callbacks.onClick(e, this.callbackOptions && this.callbackOptions.onClick && this.callbackOptions.onClick.params);
    if (this.index !== index) {
      this.ngModel.active = this.ngModel.data[index];
      this.index = index;
      this.callbacks && this.callbacks.onChange && this.callbacks.onChange(e, this.callbackOptions && this.callbackOptions.onChange && this.callbackOptions.onChange.params);
    }
  };
}

export default RadiogroupController;
