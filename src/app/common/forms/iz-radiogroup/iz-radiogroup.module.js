import RadiogroupComponent from './iz-radiogroup.component';

const radiogroupModule = angular
  .module('forms.radioModule', [])
  .component('izRadiogroup', RadiogroupComponent)
  .name;

export default radiogroupModule;
