import controller from './iz-radiogroup.controller';
import template from './iz-radiogroup.tpl.html';

const radiogroupComponent = {
  bindings: {
    ngModel: '=',
    callbacks: '=',
    callbackOptions: '='
  },
  controller,
  template
};

export default radiogroupComponent;
