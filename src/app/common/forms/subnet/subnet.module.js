import SubnetComponent from './subnet.component';
import SubnetService from './subnet.service';

const SubnetModule = angular
  .module('forms.subnet', [])
  .component('subnet', SubnetComponent)
  .service('subnetService', SubnetService)
  .name;

export default SubnetModule;
