const SubnetInputs = {
  NetworkInputs: {
    internalNetwork: {
      title: 'Internal Network',
      placeholder: 'Enter Internal Network',
      data: '',
      options: {
        required: true
      }
    },
    privateDmzNetwork: {
      title: 'Private DMZ Network',
      placeholder: 'Enter Private DMZ Network',
      data: '',
      options: {
        required: true
      }
    },
    publicDmzNetwork: {
      title: 'Public DMZ Network',
      placeholder: 'Enter Public DMZ Network',
      data: '',
      options: {
        required: true
      }
    }
  },
  SubnetInputs: {
    class: {
      data: [
        {
          title: 'A',
          value: 1,
          StartIpv4: [10, 0, 0, 1],
          EndIpv4: [10, 255, 255, 254]
        },
        {
          title: 'B',
          value: 2,
          StartIpv4: [172, 16, 0, 1],
          EndIpv4: [172, 16, 255, 254]
        },
        {
          title: 'C',
          value: 3,
          StartIpv4: [192, 168, 0, 1],
          EndIpv4: [192, 168, 255, 254]
        }
      ],
      model: 1
    },
    ip4: {
      ip: {
        title: 'IP Address',
        data: ''
      },
      mask: {
        title: 'Subnet Mask',
        placeholder: '255.255.255.0',
        data: [
          { displayName: '255.255.255.0', value: 24 }
        ],
        selected: { displayName: '255.255.255.0', value: 24 }
      },
      maskBits: {
        title: 'Mask Bits',
        placeholder: '24',
        data: [
          { displayName: '24', value: 24
          }
        ],
        selected: {displayName: '24', value: 24}
      },
      gateway: {
        title: 'Gateway',
        data: ''
      }
    },
    ip6: {
      prefixMask: {
        title: 'Prefix Mask',
        placeholder: '2001:AA11:E000:1B00::/64'
      },
      subnet: {
        title: 'Subnet the /48 network into',
        placeholder: '/48',
        data: [
          {
            'displayName': '/48'
          }
        ]
      },
      gateway: {
        title: 'Gateway',
        placeholder: '2001:AA11:E000:1B00::1'
      }
    },
    Vlan: {
      title: 'VLAN',
      placeholder: 'Range from 1 - 4095',
      data: ''
    },
    Description: {
      title: 'Description',
      placeholder: 'Enter',
      data: ''
    }
  },
  DmzSelect: {
    options: {
      required: true
    }
  }
};

export default SubnetInputs;
