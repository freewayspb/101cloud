import inputs from './subnet.inputs';

class SubnetworkViewModel {
  constructor(dbSubnetwork, SubnetRole) {
    dbSubnetwork = dbSubnetwork || this.getSubnetworkMock(SubnetRole);
    this.SubnetRole = dbSubnetwork.SubnetRole;
    this.fields = _.cloneDeep(inputs.SubnetInputs);
    this._original = {
      Id: dbSubnetwork.Id,
      Name: dbSubnetwork.Name
    };
    this.fields.ip4.ip.data = dbSubnetwork.StartIpv4;
    this.fields.ip4.gateway.data = dbSubnetwork.DefaultGateway;
    this.fields.Vlan.data = dbSubnetwork.Vlan;
    this.fields.Description.data = dbSubnetwork.VlanName;
  }

  getSubnetworkMock(SubnetRole) {
    return {
      SubnetRole,
      isFake: true
    };
  }
}

export default SubnetworkViewModel;
