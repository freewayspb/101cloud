import inputs from './subnet.inputs';
import SubnetworkPostModel from './SubnetworkPostModel.class';

class SubnetServices {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.dmzTemplates = [
      {
        displayName: 'No DMZ',
        type: 0,
        imageURL: '/app/assets/img/no-dmz.png'
      }, {
        displayName: '1-Tier DMZ',
        type: 1,
        imageURL: '/app/assets/img/1-tier-dmz.png'
      }, {
        displayName: '2-Tier DMZ',
        type: 2,
        imageURL: '/app/assets/img/2-tier-dmz.png'
      }
    ];
    this.allNetworksTpl = [
      {
        'Create': true,
        'Subnets': [],
        'Name': '',
        'Description': '',
        'Vlan': null,
        'VlanName': '',
        'StartIpv4': '255.255.255.255',
        'SubnetMaskIpv4': null,
        'IsDeleted': false
      },
      {
        'Create': true,
        'Subnets': [],
        'Name': '',
        'Description': '',
        'Vlan': null,
        'VlanName': '',
        'StartIpv4': '255.255.255.255',
        'SubnetMaskIpv4': null,
        'IsDeleted': false
      },
      {
        'Create': true,
        'Subnets': [],
        'Name': '',
        'Description': '',
        'Vlan': null,
        'VlanName': '',
        'StartIpv4': '255.255.255.255',
        'SubnetMaskIpv4': null,
        'IsDeleted': false
      }
    ];
    this.isInputsDisabled = {value: true};
  }

  getSubnetRoles() {
    return this.dataService.get(this.apiService.subnetRoles)
      .then(result => result);
  }

  getSubnetworks(address) {
    return _.map(this.selectedSubnetworks, element => new SubnetworkPostModel(element, address));
  }

  getNetworks(address) {
    const readyNetworks = _.map(this.networks, (network, index) => {
      network.Subnets = _.filter(this.getSubnetworks(address),
        subnet => subnet.SubnetRole.Id === this.subnetRoles[index].Id);
      network.Name = network.Name || '';
      network.Description = network.Description || '';
      network.Vlan = network.Vlan || 3691;
      network.VlanName = network.VlanName || '';
      network.StartIpv4 = network.StartIpv4 || '';
      network.SubnetMaskIpv4 = network.SubnetMaskIpv4 || 0;
      network.IsDeleted = false;
      return network;
    });
    return _.filter(readyNetworks, net => net.Subnets.length > 0);
  }
}

export default SubnetServices;
