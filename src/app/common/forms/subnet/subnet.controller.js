import inputs from './subnet.inputs';
import SubnetworkViewModel from './SubnetworkViewModel.class';

/*@ngInject*/
function SubnetController(subnetService, $scope) {
  const vm = this;
  this.inputs = _.cloneDeep(inputs);
  this.isInputsDisabled = subnetService.isInputsDisabled;

  vm.$onInit = () => {
    subnetService.networks = this.ngModel;
    subnetService.getSubnetRoles()
      .then(subnetRoles => {
        _.each(subnetRoles, (role, index) => {
          subnetService.dmzTemplates[index].SubnetRole = role;
        });
        vm.subnetRoles = subnetService.subnetRoles = subnetRoles;
        if (this.ngModel) {
          setAvailableNetworks();
        } else {
          setAllNetworks();
        }
      });
  };

  this.genericCallbacks = {
    onChange: validate
  };

  this.addSubnetwork = role => {
    if (this.mode === 'predefined') {
      const availableSubnetworks = _.filter(this.availableSubnetworks,
        subnetwork => subnetwork.SubnetRole.Id === role.Id);
      if (this.availableSubnetworks.length > 0) {
        this.selectedSubnetworks.push(new SubnetworkViewModel(availableSubnetworks[0]));
      }
      this.availableSubnetworks = _.filter(this.availableSubnetworks,
        subnet => subnet.Id !== availableSubnetworks[0].Id);
    } else if (this.mode === 'infinite') {
      this.selectedSubnetworks.push(new SubnetworkViewModel(null, role));
    }
  };

  this.canAddSubnetwork = role => (_.filter(this.availableSubnetworks,
    subnetwork => (subnetwork.SubnetRole.Id === role.Id)).length > 0) || this.mode === 'infinite';

  this.infrastructureCallbacks = {
    onChange: selected => {
      if (_.filter(vm.selectedSubnetworks,
          subnet => subnet.SubnetRole.Id === selected.SubnetRole.Id).length === 0) {
        vm.addSubnetwork(selected.SubnetRole);
      }
    }
  };

  function setAvailableNetworks() {
    vm.mode = 'predefined';
    vm.availableSubnetworks = [];
    vm.selectedSubnetworks = subnetService.selectedSubnetworks = [];

    //TODO: think on awaken head if we need this
    _.each(vm.ngModel, network => {
      _.each(network.Subnets, subnet => {
        subnet._Network = network;
      });
      vm.availableSubnetworks = _.concat(vm.availableSubnetworks, network.Subnets);
    });

    let availableRoles = _.uniqBy(vm.availableSubnetworks, subnetwork => subnetwork.SubnetRole.Id);
    availableRoles = _.map(availableRoles, subnet => subnet.SubnetRole);
    vm.inputs.DmzSelect.data = [];

    _.each(subnetService.dmzTemplates, template => {
      _.each(availableRoles, role => {
        if (template.SubnetRole.Id === role.Id) {
          vm.inputs.DmzSelect.data.push(template);
        }
      });
    });

    vm.inputs.DmzSelect.selected = vm.inputs.DmzSelect.data[0];
    if (_.filter(vm.selectedSubnetworks,
        subnet => subnet.SubnetRole.Id === vm.inputs.DmzSelect.selected.SubnetRole.Id).length === 0) {
      vm.addSubnetwork(vm.inputs.DmzSelect.selected.SubnetRole);
    }
  }

  function setAllNetworks() {
    vm.mode = 'infinite';
    subnetService.networks = subnetService.allNetworksTpl;
    subnetService.isInputsDisabled.value = false;
    vm.inputs.DmzSelect.data = [];
    vm.selectedSubnetworks = subnetService.selectedSubnetworks = [];
    _.each(subnetService.dmzTemplates, template => {
      vm.inputs.DmzSelect.data.push(template);
    });
    vm.inputs.DmzSelect.selected = vm.inputs.DmzSelect.data[0];
    if (_.filter(vm.selectedSubnetworks,
        subnet => subnet.SubnetRole.Id === vm.inputs.DmzSelect.selected.SubnetRole.Id).length === 0) {
      vm.addSubnetwork(vm.inputs.DmzSelect.selected.SubnetRole);
    }
  }

  function validate() {
    vm.validate({isValid: $scope.networksForm.$valid});
  }
}

export default SubnetController;
