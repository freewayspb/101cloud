import controller from './subnet.controller';
import template from './subnet.tpl.html';

const subnetComponent = {
  bindings: {
    ngModel: '=',
    validate: '&'
  },
  controller,
  template
};

export default subnetComponent;
