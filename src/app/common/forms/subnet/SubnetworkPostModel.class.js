class SubnetworkPostModel {
  constructor(SubnetworkVm, address) {
    this.Create = false;
    this.Id = SubnetworkVm._original.Id;
    this.Name = SubnetworkVm._original.Name || '';
    this.Description = SubnetworkVm.fields.Description.data;
    this.Vlan = +SubnetworkVm.fields.Vlan.data;
    this.VlanName = SubnetworkVm.fields.Vlan.data;
    this.StartIpv4 = SubnetworkVm.fields.ip4.ip.data;
    this.EndIpv4 = SubnetworkVm.fields.ip4.ip.data;
    this.SubnetMaskIpv4 = SubnetworkVm.fields.ip4.maskBits.selected.value;
    this.DefaultGateway = SubnetworkVm.fields.ip4.gateway.data;
    this.SubnetRole = {
      Id: SubnetworkVm.SubnetRole.Id,
      Name: SubnetworkVm.SubnetRole.Name
    };
    this.IsDeleted = false;
    this.Address = address || SubnetworkVm.Address || {
        // Id: 1,
        Country: {Id: 61}
        // Street: '',
        // StreetNumber: '',
        // ZipCode: '',
        // City: '',
        // Region: '',
        // Longitude: null,
        // Latitude: null,
        // IsDefault: true,
        // IsBilling: true
      };
  }
}

export default SubnetworkPostModel;
