import controller from './user-role.controller';
import template from './user-role.tpl.html';

const userRoleComponent = {
  bindings: {
    ngModel: '=',
    className: '@',
    callbacks: '='
  },
  transclude: true,
  controller,
  template
};

export default userRoleComponent;
