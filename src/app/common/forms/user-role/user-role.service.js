class UserRoleServices {
  /*@ngInject*/
  constructor() {
    this.userRoleTemplate = {
      'userRole': {
        'title': '101Cloud Role',
          'data': [
          {'displayName': 'Administrator'},
          {'displayName': 'User'},
          {'displayName': 'Accountant'}
        ],
          'placeholder': 'Select Role',
          'options': {
          'required': true
        }
      },
      'userGroup': {
        'title': 'User Group',
          'data': [
          {'displayName': 'Group1'},
          {'displayName': 'Group2'}
        ],
          'placeholder': 'Select Group',
          'options': {
          'required': true
        }
      },
      'customer': {
        'title': 'Customer',
          'data': [
          {'displayName': 'Customer1'},
          {'displayName': 'Customer2'}
        ],
          'placeholder': 'Select Customer',
          'options': {
          'required': true
        }
      },
      'x': {
        'title': 'X',
          'data': [
          {'displayName': 'In Cloud'},
          {'displayName': 'On Premise'},
          {'displayName': 'Federated'}
        ],
          'placeholder': 'Select Title',
          'options': {
          'required': true
        }
      }
    };
    this.userRoles = {
      data: []
    };
  }
  addUserRole() {

  }
}

export default UserRoleServices;
