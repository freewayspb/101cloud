import UserRoleComponent from './user-role.component';
import UserRoleService from './user-role.service';

const UserRoleModule = angular
  .module('forms.userRole', [])
  .component('userRole', UserRoleComponent)
  .service('userRoleService', UserRoleService)
  .name;

export default UserRoleModule;
