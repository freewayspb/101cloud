/*@ngInject*/
function UserRoleController(userRoleService) {
  const vm = this;

  vm.$onInit = () => {
    vm.dataDMZ = subnetService.DMZ;
    vm.dataDMZ.selected = subnetService.DMZ.data[0];
    vm.ngModel.subnets = {
      options: subnetService.options,
      subnets: []
    };
    subnetService.networks = vm.ngModel.networks;
    this.ngModel.subnets.options.internalNetwork.data = vm.ngModel.networks[0].Name;

    vm.add = type => {
      type = type || 'Internal Network';
      const subnetTemplate = angular.copy(subnetService.subnet);
      subnetTemplate.SubnetRole.Name = type;
      subnetTemplate.SubnetRole.id = vm.ngModel.subnets.subnets.length;
      vm.ngModel.subnets.subnets.push(subnetTemplate);
      subnetService.subnetList = vm.ngModel.subnets.subnets;
    };
    vm.add();
    if (vm.ngModel.subnetTemplate) {
      subnetService.setFirstSubnetwork(vm.ngModel.subnets.subnets, vm.ngModel.subnetTemplate)
    }

    this.isInputsDisabled = subnetService.isInputsDisabled;
  };
}

export default UserRoleController;
