import angular from 'angular';

import izSelect from './iz-select/selectModule';
import izInput from './iz-input/iz-input.module';
import izTextarea from './iz-textarea/iz-textarea.module';
import izSubnet from './subnet/subnet.module';
import izRadioGroup from './iz-radiogroup/iz-radiogroup.module';
import izCheckbox from './iz-checkbox/iz-checkbox.module';
import izScheduler from './iz-scheduler/iz-scheduler.module';
import izDatepicker from './iz-datepicker/iz-datepicker.module';
import progressBar from './progressbar/progressbar.module';
import owners from './owners/owners.module';
import owner from './owner/owner.module';
import draggable from './iz-draggable/iz-draggable.module';
import ownerSelect from './owner-select/owner-select.module';
import wysiwyg from './iz-wysiwyg/iz-wysiwyg.module';
import izImageUpload from './iz-image-upload/iz-image-upload.module';

const forms = angular
  .module('forms', [
    izSelect,
    izInput,
    izTextarea,
    izSubnet,
    izRadioGroup,
    izCheckbox,
    izScheduler,
    izDatepicker,
    progressBar,
    owners,
    owner,
    draggable,
    ownerSelect,
    wysiwyg,
    izImageUpload
  ])
  .name;

export default forms;
