const getOwnerInput = init => {
  let defdata, defselection, countryCode, regionCode, defemail;
  if (localStorage.isDevMode) {
    defdata = 'jaaaaaamMMM2017';
    defselection = {displayName: '+Mr Postman'};
    defemail = 'jaaaaaamMMM2025@101cloud.net';
    countryCode = {displayName: '+41'};
    regionCode = '044';
  } else {
    defdata = '';
  }
  return init || {
    roleName: null,
    title: {
      title: 'Title',
      data: [
        {displayName: 'Mr'},
        {displayName: 'Mrs'}
      ],
      placeholder: 'Select title',
      selected: defselection,
      options: {
        required: true
      }
    },
    givenName: {
      title: 'Given Name',
      data: defdata,
      placeholder: 'Enter Given Name',
      options: {
        required: true
      }
    },
    lastName: {
      title: 'Last Name',
      data: defdata,
      placeholder: 'Enter Last Name',
      options: {
        required: true
      }
    },
    middleName: {
      title: 'Middle Name',
      placeholder: 'Enter Middle Name',
      data: defdata
    },
    userName: {
      title: 'User Name',
      placeholder: 'Enter User Name',
      data: defdata
    },
    suffix: {
      title: 'Suffix',
      placeholder: 'Enter Suffix',
      data: defdata
    },
    email: {
      title: 'Email Address',
      placeholder: 'Enter Email Address',
      data: defemail,
      options: {
        required: true,
        type: 'email'
      }
    },
    alternativeEmail: {
      title: 'Alternative Email Address',
      placeholder: 'Enter Alternative Email Address',
      data: defemail,
      options: {
        type: 'email'
      }
    },
    phone1: {
      title: 'Work Phone Number',
      placeholder: 'Enter Phone Number',
      data: defdata,
      options: {
        required: true
      }
    },
    phone1CountryCode: {
      title: 'Country Code',
      placeholder: 'Enter Code',
      data: [],
      selected: countryCode,
      options: {
        required: true
      }
    },
    phone1RegionCode: {
      title: 'Area Code',
      placeholder: 'Enter Code',
      data: regionCode,
      options: {
        required: true
      }
    },
    phone2: {
      title: 'Work Phone Number',
      placeholder: 'Enter Phone Number',
      data: defdata,
      options: {
        required: true
      }
    },
    phone2CountryCode: {
      title: 'Country Code',
      placeholder: 'Enter Code',
      data: [],
      selected: countryCode,
      options: {
        required: true
      }
    },
    phone2RegionCode: {
      title: 'Area Code',
      placeholder: 'Enter Code',
      data: regionCode,
      options: {
        required: true
      }
    },
    password: {
      title: 'Password',
      placeholder: 'Enter Password',
      data: defdata,
      options: {
        required: true,
        pattern: `(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}`,
        isPassword: true,
        type: 'password',
        popover: {
          msg: 'Password must contain at least one uppercase, one lowercase standard latin letter and a digit',
          trigger: 'mouseenter'
        }
      }
    }
  };
};

export default getOwnerInput();
