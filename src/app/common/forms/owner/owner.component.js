import template from './owner.tpl.html';
import controller from './owner.controller';

const ownersComponent = {
  bindings: {
    validate: '&',
    init: '='
  },
  controller,
  template
};

export default ownersComponent;
