import OwnersComponent from './owner.component';
import OwnerService from './owner.service';

const OwnersModule = angular
  .module('forms.ownerModule', [])
  .component('izOwner', OwnersComponent)
  .service('ownerService', OwnerService)
  .name;

export default OwnersModule;
