import inputs from './owner.inputs';

function ownersController(ownerService, countryService, $scope, apiService) {
  const vm = this;
  this.inputs = inputs;

  this.$onInit = () => {
    countryService.countriesQ
      .then(countries => {
        let phones = _.map(countries, country => +country.Phone);
        phones = _.filter(phones, phone => !_.isNaN(phone));
        phones.push(1809);
        phones.push(1829);
        phones = _.sortBy(phones);
        phones = _.uniq(phones);
        phones = _.map(phones, phone => {
          return {
            displayName: '+' + phone
          };
        });
        this.inputs.phone1CountryCode.data = this.inputs.phone2CountryCode.data = phones;
      });
  };

  this.genericCallbacks = {
    onChange: validate
  };

  function validate() {
    vm.validate({isValid: $scope.ownersSubForm.$valid});
  }
}

export default ownersController;
