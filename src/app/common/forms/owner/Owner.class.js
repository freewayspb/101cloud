class Owner {
  constructor(rawOwner) {
    this.Id = 1;
    this.Jobposition = null;
    this.ReceiveBillings = false;
    this.Firstname = rawOwner.fields.givenName.data;
    this.Lastname = rawOwner.fields.lastName.data;
    this.Middlename = rawOwner.fields.middleName.data || null;
    this.Password = rawOwner.fields.password.data;
    this.Title = rawOwner.fields.title.selected.displayName || null;
    this.Suffix = rawOwner.fields.suffix.data || null;
    this.Emails = [
      {
        Id: 1,
        EmailAddress: rawOwner.fields.email.data,
        EmailType: {
          Id: 1,
          Name: 'Business'
        }
      },
      {
        Id: 1,
        EmailAddress: rawOwner.fields.alternativeEmail.data,
        EmailType: {
          Id: 2,
          Name: 'Private'
        }
      }
    ];
    this.Phones = [
      {
        Id: 1,
        CountryCode: rawOwner.fields.phone1CountryCode.selected.displayName,
        RegionCode: rawOwner.fields.phone1RegionCode.data,
        Number: rawOwner.fields.phone1.data,
        PhoneType: {
          Id: 2,
          Name: 'Phone'
        }
      },
      {
        Id: 1,
        CountryCode: rawOwner.fields.phone2CountryCode.selected.displayName,
        RegionCode: rawOwner.fields.phone2RegionCode.data,
        Number: rawOwner.fields.phone2.data,
        PhoneType: {
          Id: 1,
          Name: 'Mobile'
        }
      }
    ];
    this.Role = {
      Id: 1,
      Name: rawOwner.roleName
    };
  }
}

export default Owner;
