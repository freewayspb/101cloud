import DraggableComponent from './iz-draggable.component';

const DraggableModule = angular
  .module('forms.izDraggableModule', [])
  .component('izDraggable', DraggableComponent)
  .name;

export default DraggableModule;
