import controller from './iz-draggable.controller';
import template from './iz-draggable.tpl.html';

const selectComponent = {
  bindings: {
    ngModel: '='
  },
  controller,
  template
};

export default selectComponent;
