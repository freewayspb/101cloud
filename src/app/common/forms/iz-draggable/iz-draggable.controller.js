function DraggableController($element) {
  this.$onInit = () => {
    this.ngModel.data = this.ngModel.data || 0;
  };

  this.setProgress = (event, qty) => {
    if (qty || qty === 0) {
      this.ngModel.data = qty;
      return;
    }
    if (event.buttons === 1) {
      this.ngModel.data = event.offsetX / $element.width() * 100;
      this.carretProgress = event.offsetX;
    }
  };

  this.getLabel = () => {
    let label;
    _.each(this.ngModel.values, (value, key) => {
      const range = key.split('-');
      let singleValue;
      if (range.length === 1) {
        singleValue = range[0];
      }
      if ((this.ngModel.data >= +range[0] && this.ngModel.data <= +range[1]) || (this.ngModel.data === +singleValue)) {
        label = value.val + value.suffix;
      }
    });
    return label;
  };
}

export default DraggableController;
