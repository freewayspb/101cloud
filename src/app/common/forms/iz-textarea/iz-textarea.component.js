import controller from './iz-textarea.controller';
import template from './iz-textarea.tpl.html';

const textareaComponent = {
  bindings: {
    ngModel: '=',
    ngDisabled: '<',
    className: '@',
    callbacks: '='
  },
  transclude: true,
  controller,
  template
};

export default textareaComponent;
