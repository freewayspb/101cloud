import TextareaComponent from './iz-textarea.component';

const TextareaModule = angular
  .module('forms.izTextareaModule', [])
  .component('izTextarea', TextareaComponent)
  .name;

export default TextareaModule;
