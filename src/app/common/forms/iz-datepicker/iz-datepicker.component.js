import controller from './iz-datepicker.controller';
import template from './iz-datepicker.tpl.html';

const DatepickerComponent = {
  bindings: {
    ngModel: '='
  },
  transclude: true,
  controller,
  template
};

export default DatepickerComponent;
