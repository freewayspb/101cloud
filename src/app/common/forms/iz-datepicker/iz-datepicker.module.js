import DatepickerComponent from './iz-datepicker.component';

const DatepickerModule = angular
  .module('forms.izDatepickerModule', [])
  .component('izDatepicker', DatepickerComponent)
  .name;

export default DatepickerModule;
