import CheckboxComponent from './iz-checkbox.component';

const checkboxModule = angular
  .module('forms.checkboxModule', [])
  .component('izCheckbox', CheckboxComponent)
  .name;

export default checkboxModule;
