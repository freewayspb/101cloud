function CheckboxController() {
  this.$onInit = function () {
    this.hasLink = this.ngModel && this.ngModel.options && this.ngModel.options.link;
  };

  this.preventModelChange = () => {
    if (this.ngModel.data.model === this.ngModel.data.valueTrue) {
      this.ngModel.data.model = this.ngModel.data.valueFalse;
    } else {
      this.ngModel.data.model = this.ngModel.data.valueTrue;
    }
  };
}

export default CheckboxController;
