import controller from './iz-checkbox.controller';
import template from './iz-checkbox.tpl.html';

const checkboxComponent = {
  bindings: {
    ngModel: '=',
    callbacks: '='
  },
  controller,
  template
};

export default checkboxComponent;
