import ProgressBarComponent from './progressbar.component';
import ProgressBarService from './progressbar.service';

const ProgressBarModule = angular
  .module('forms.progressBarModule', [])
  .component('progressBar', ProgressBarComponent)
  .service('progressBarService', ProgressBarService)
  .name;

export default ProgressBarModule;
