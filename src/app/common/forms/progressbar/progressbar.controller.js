/*@ngInject*/
function ProgressBarController(progressBarService, notificationService) {

  this.notificationService = notificationService;

  this.$onInit = () => {
    const vm = this;
    vm.stepsCount = vm.ngModel.steps.length - 1;

    vm.goToStep = progressBarService.goToStep;
    vm.hasNextStep = progressBarService.hasNextStep;
    vm.hasPreviousStep = progressBarService.hasPreviousStep;
    vm.incrementStep = progressBarService.incrementStep;
    vm.decrementStep = progressBarService.decrementStep;
    vm.getProgress = progressBarService.getProgress;

    this.getCurrentIndex = () => _.findIndex(this.ngModel.steps, element =>
      element.name === this.ngModel.currentStep.name);

    this.isNextDisabled = () => vm.hasNextStep && vm.ngModel.steps[vm.getCurrentIndex() + 1].isDisabled;

    this.isCheckoutDisabled = () => _.findIndex(vm.ngModel.steps, step => step.isValid === false) + 1;

    vm.goToStep(0);
  };
}

export default ProgressBarController;
