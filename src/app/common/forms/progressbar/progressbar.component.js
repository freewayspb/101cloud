import controller from './progressbar.controller';
import template from './progressbar.tpl.html';

const progressbarComponent = {
  bindings: {
    ngModel: '=',
    progressType: '@',
    callbacks: '='
  },
  controller,
  template
};

export default progressbarComponent;
