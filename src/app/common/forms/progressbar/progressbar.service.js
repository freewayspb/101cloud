import _ from 'lodash';

export default class ProgressBarService {

  /*@ngInject*/

  constructor() {
  }

  goToStep(index) {
    if (!_.isUndefined(this.ngModel.steps[index]) && !this.ngModel.steps[index].isDisabled) {
      this.ngModel.previousStep = _.cloneDeep(this.ngModel.currentStep);
      this.ngModel.currentStep = this.ngModel.steps[index];
      this.ngModel.currentStep.isLast = !this.hasNextStep();
    }
    if (this.callbacks && this.callbacks.onStepClick) {
      this.callbacks.onStepClick(this.ngModel.currentStep, this.ngModel.previousStep);
    }
  }

  hasNextStep() {
    const stepIndex = this.getCurrentIndex();
    const nextStep = stepIndex + 1;
    return !_.isUndefined(this.ngModel.steps[nextStep]);
  }

  hasPreviousStep() {
    const stepIndex = this.getCurrentIndex();
    const previousStep = stepIndex - 1;
    return !_.isUndefined(this.ngModel.steps[previousStep]);
  }

  incrementStep() {
    if ( this.hasNextStep() )
    {
      const stepIndex = this.getCurrentIndex();
      const nextStep = stepIndex + 1;
      this.goToStep(nextStep);
    }
  }

  decrementStep() {
    if ( this.hasPreviousStep() )
    {
      const stepIndex = this.getCurrentIndex();
      const previousStep = stepIndex - 1;
      this.goToStep(previousStep);
    }
  }

  getProgress() {
    return  (this.getCurrentIndex() / this.stepsCount) * 100;
  }
}

