function SchedulerController() {
  const vm = this;

  this.days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
  this.hours = [];
  this.schedule = [];

  for (let i = 0; i < 24; i++) {
    const prefix = i < 10
      ? '0'
      : '';

    this.hours.push(prefix + i.toString() + ':00');
  }

  for (let i = 0; i < 7; i++) {
    this.schedule[i] = [];
    for (let j = 0; j < 24; j++) {
      this.schedule[i][j] = false;
    }
  }

  this.setData = () => {
    this.ngModel.data = this.schedule;
    this.isOpened = false;
    this.ngModel.display = getDataStr();
  };

  function getDataStr() {
    const dataObj = {};
    let isDayActive;
    let times;

    _.each(vm.schedule, (day, index) => {
      times = [];

      isDayActive = false;
      _.each(day, findActiveHours);
      if (isDayActive) {
        dataObj[vm.days[index]] = times.join(', ');
      }
    });
    return stringifyDataObj(dataObj);

    function findActiveHours(time, timeIndex) {
      if (time) {
        isDayActive = true;
        const prefix = timeIndex < 10 ? '0' : '';

        times.push(prefix + timeIndex.toString() + ':00');
      }
    }
  }

  function stringifyDataObj(dataObj) {
    let result = '';
    _.each(dataObj, (element, index) => {
      result += index + ': ' + element + '; ';
    });
    return result;
  }
}

export default SchedulerController;
