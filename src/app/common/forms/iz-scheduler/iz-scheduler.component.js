import controller from './iz-scheduler.controller';
import template from './iz-scheduler.tpl.html';

const SchedulerComponent = {
  bindings: {
    ngModel: '='
  },
  transclude: true,
  controller,
  template
};

export default SchedulerComponent;
