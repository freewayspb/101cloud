import SchedulerComponent from './iz-scheduler.component';

const SchedulerModule = angular
  .module('forms.izSchedulerModule', [])
  .component('izScheduler', SchedulerComponent)
  .name;

export default SchedulerModule;
