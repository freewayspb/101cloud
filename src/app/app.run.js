const run = ($rootScope, $state, $stateParams, $transitions, $location, $trace) => {
  /*ngInject*/
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $trace.enable('TRANSITION');

  $transitions.onStart( { to: 'app.**' }, trans => {
    $state = trans.router.stateService;
    const adalAuthenticationService = trans.injector().get('adalAuthenticationService');
    // If the user is not authenticated
    if (!adalAuthenticationService.userInfo.isAuthenticated && $location.$$path !== '/invite') {
      console.log($location.$$path);
      if ($location.$$path !== '/login'){
        localStorage.setItem('global.beforeLoginPath', $location.$$path);
        console.log('1');
        $state.go('login');
      }
      else if ($location.$$path === '/invite') {
        console.log('2');
        localStorage.setItem('global.beforeLoginPath', '/');
      }
      else {
        localStorage.setItem('global.beforeLoginPath', '/');
        console.log('3');
        $state.go('login');
      }
    }
  });
};

export default run;
