import notFoundTemplate from './components/errors/404.tpl.html';

const routing = ($urlRouterProvider,
                 $stateProvider,
                 $locationProvider,
                 angularBingMapsProvider,
                 adalAuthenticationServiceProvider,
                 $httpProvider,
                 $sceDelegateProvider,
                 $qProvider) => {
  /*ngInject*/
  $qProvider.errorOnUnhandledRejections(false);
  $locationProvider.html5Mode(true).hashPrefix('!');

  $stateProvider
    .state('app', {
      abstract: true,
      template: '<app />'
    })
    .state('app.dashboard', {
      url: '/',
      template: '<dashboard />',
      requireADLogin: true
    })
    .state('register', {
      url: '/register',
      template: '<register />'
    })
    .state('404', {
      url: '/404',
      template: notFoundTemplate
    });

  $urlRouterProvider.otherwise($injector => {
    const $state = $injector.get('$state');
    if (localStorage.getItem('global.beforeLoginPath')) {
      $locationProvider.path(localStorage.getItem('global.beforeLoginPath'));
    }
    else {
      $state.go('app.dashboard');
    }

  });


  angularBingMapsProvider.setDefaultMapOptions({
    credentials: 'AjgPLsKBC6t_mm19IDn1-fsg_ZXu7y129dHOxCcwa0QFcGw0gcRH4SLIUwEzVw-X',
    enableClickableLogo: false
  });
  angularBingMapsProvider.bindCenterRealtime(false);

  const endpoints = {
    'https://graph.microsoft.com/': 'https://graph.microsoft.com',
    'https://localhost:8080/': 'https://101cloudnet.onmicrosoft.com/',
    'https://101cloud-portal-dev-auth.azurewebsites.net/': 'https://iz000.onmicrosoft.com/',
    'https://portal.101cloud.net/': 'https://101cloudnet.onmicrosoft.com/',
    'https://52.174.194.236/': 'https://101cloudnet.onmicrosoft.com/',
    'https://101cloudtenants.azurewebsites.net/': 'a63283dd-661d-455b-bd08-f30cb9ad6177',
    'https://101cloudclouds.azurewebsites.net/': 'a63283dd-661d-455b-bd08-f30cb9ad6177',
    'https://101cloudlicenses.azurewebsites.net/': 'a63283dd-661d-455b-bd08-f30cb9ad6177',
    'https://101cloudbilling.azurewebsites.net/': 'a63283dd-661d-455b-bd08-f30cb9ad6177',
    'https://portalbillings.azurewebsites.net/': '96e56774-bc14-48be-b00c-4df83d61d215',
    'https://portaltenants.azurewebsites.net/': '96e56774-bc14-48be-b00c-4df83d61d215',
    'https://portalclouds.azurewebsites.net/': '96e56774-bc14-48be-b00c-4df83d61d215',
    'https://portallicenses.azurewebsites.net/': '96e56774-bc14-48be-b00c-4df83d61d215',
    'https://portaltemplates.azurewebsites.net/': '96e56774-bc14-48be-b00c-4df83d61d215',
    'https://portalautomation.azurewebsites.net/': '96e56774-bc14-48be-b00c-4df83d61d215'
  };

  let location;
  let clientId;
  let tenantAD;
  if (window.location.href.indexOf('localhost:8080') + 1) {
    location = 'https://localhost:8080/';
    clientId = 'a63283dd-661d-455b-bd08-f30cb9ad6177';
    tenantAD = 'iz000.onmicrosoft.com';
  }
  else if (window.location.href.indexOf('portal.101cloud.net') + 1) {
    location = 'https://portal.101cloud.net/';
    clientId = '96e56774-bc14-48be-b00c-4df83d61d215';
    tenantAD = '101cloudnet.onmicrosoft.com';
  }
  else if (window.location.href.indexOf('52.174.194.236') + 1) {
    location = 'https://52.174.194.236/';
    clientId = '96e56774-bc14-48be-b00c-4df83d61d215';
    tenantAD = '101cloudnet.onmicrosoft.com';
  }
  else {
    location = 'https://101cloud-portal-dev-auth.azurewebsites.net/';
    clientId = 'a63283dd-661d-455b-bd08-f30cb9ad6177';
    tenantAD = 'iz000.onmicrosoft.com';
  }

  adalAuthenticationServiceProvider.init(
    {
      tenant: tenantAD,
      clientId: clientId,
      extraQueryParameter: 'nux=1',
      requireADLogin: true,
      redirectUri: location,
      localLoginUrl: '/login',
      cacheLocation: 'localStorage',
      anonymousEndpoints: [
        '/login',
        '/invite',
        '/app/assets/',
        '/bootstrap/general-discount.tpl.html',
        'https://101cloudtenants.azurewebsites.net/101Cloud/Invite',
        'https://portaltenants.azurewebsites.net/101Cloud/Invite'
      ],
      endpoints: endpoints,
      postLogoutRedirectUri: location + 'login'
    },
    $httpProvider
  );

  $sceDelegateProvider.resourceUrlWhitelist([
    location,
    'https://dev.virtualearth.net/REST/**',
    'http://ecn.dev.virtualearth.net/**'
  ]);
};

export default routing;
