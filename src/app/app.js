import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import satellizer from 'satellizer';
import ngSanitize from 'angular-sanitize';

import ngAnimate from 'angular-animate';
import 'angular-aria';
import 'angular-ui-grid';

// styles
import './style/app.scss';

// modules
import run from './app.run';
import routing from './app.config';
import AppComponent from './app.component';
import components from './components/components.module';
import common from './common/common.module';
import services from './services/services.module';
import menu from './common/menu/menu.module';
import mapModule from './common/map/map.module';

// 3rd party modules
import uiMask from 'angular-ui-mask';
import 'angular-file-upload';
require('../../node_modules/angular-bing-maps/dist/angular-bing-maps.js');
import adalAngular from 'modular-adal-angular';
require('../../node_modules/angular-local-storage');
require('../../node_modules/chart.js/dist/Chart.min.js');
require('../../node_modules/angular-chart.js/dist/angular-chart.min.js');
require('../../node_modules/moment/moment');
require('../../node_modules/angular-moment/angular-moment.min');

// JSON imports
require('file-loader!./assets/json/addlocation.json');
require('file-loader!./assets/json/internalmenu.json');
require('file-loader!./assets/json/iprovider.json');
require('file-loader!./assets/json/maintenance.json');
require('file-loader!./assets/json/operations.json');
require('file-loader!./assets/json/test_clouds.json');
require('file-loader!./assets/json/test_locations.json');
require('file-loader!./assets/json/operations.json');
require('file-loader!./assets/json/operations-servers.json');
require('file-loader!./assets/json/operations-services.json');
require('file-loader!./assets/json/subscriptions.json');
require('file-loader!./assets/json/server-new-server.json');
require('file-loader!./assets/json/datacenters.json');
require('file-loader!./assets/json/serviceTypes.json');
require('file-loader!./assets/json/billing-invoices.json');
require('file-loader!./assets/json/subnet.json');
require('file-loader!./assets/json/leftMenu.json');
require('file-loader!./assets/json/sl-azure.json');
require('file-loader!./assets/json/sl-spla.json');
require('file-loader!./assets/json/sl-azure_azure-active-directory.json');
require('file-loader!./assets/json/sl-ordersoft.json');
require('file-loader!./assets/json/sl-ordersoft-details.json');
require('file-loader!./assets/json/billing-dashboard-year.json');
require('file-loader!./assets/json/billing-dashboard-month.json');
require('file-loader!./assets/json/billing-dashboard-quarter.json');
require('file-loader!./assets/json/billing-dashboard-licenses-year.json');
require('file-loader!./assets/json/billing-dashboard-licenses-month.json');
require('file-loader!./assets/json/billing-dashboard-licenses-quarter.json');
require('file-loader!./assets/json/billing-dashboard-operations-year.json');
require('file-loader!./assets/json/billing-dashboard-operations-month.json');
require('file-loader!./assets/json/billing-dashboard-operations-quarter.json');
require('file-loader!./assets/json/billing-dashboard-services-year.json');
require('file-loader!./assets/json/billing-dashboard-services-month.json');
require('file-loader!./assets/json/billing-dashboard-services-quarter.json');
require('file-loader!./assets/json/billing-drill-down-licenses-year.json');
require('file-loader!./assets/json/add-subscriptions-add-services.json');
require('file-loader!./assets/json/acthis-appusage.json');
require('file-loader!./assets/json/pricelist-spla-ch-nexellent-chf-all.json');
require('file-loader!./assets/json/pricelist-spla-ch-nexellent-eur-all.json');
require('file-loader!./assets/json/pricelist-spla-ch-nexellent-usd-all.json');
require('file-loader!./assets/json/test_users.json');
require('file-loader!./assets/json/settings-partners-customers.json');
require('file-loader!./assets/json/settings-partners-partners.json');
require('file-loader!./assets/json/settings_price-settings.json');
require('file-loader!./assets/json/ps-cs-security.json');
require('file-loader!./assets/json/test_groups.json');
require('file-loader!./assets/json/settings-risk-management.json');
require('file-loader!./assets/json/settings-additional-services.json');
require('file-loader!./assets/json/settings-cloud-types.json');
require('file-loader!./assets/json/settings-fabric-settings.json');
require('file-loader!./assets/json/settings-ht.json');
require('file-loader!./assets/json/settings-legal-settings.json');
require('file-loader!./assets/json/settings-subscriptions.json');
require('file-loader!./assets/json/settings-vdc.json');
require('file-loader?name=[name].[ext]!./assets/img/favicon.ico');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/en.svg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/es.svg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/fr.svg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/de.svg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/Banner.jpg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/101_Starter.jpg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/101_Standard.jpg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/101_Professional.jpg');
require('file-loader?name=/app/assets/img/[name].[ext]!./assets/img/101_Corporate.jpg');

// azure IIS config
require('file-loader!./assets/web.config');

angular
  .module('app', [
    uiRouter,
    uiBootstrap,
    uiMask,
    menu,
    satellizer,
    mapModule,
    common,
    components,
    services,
    ngSanitize,
    ngAnimate,
    adalAngular,
    'LocalStorageModule',
    'ngCookies',
    'angularMoment',
    'angularFileUpload'
  ])
  .component('app', AppComponent)
  .config(routing)
  .run(run);
