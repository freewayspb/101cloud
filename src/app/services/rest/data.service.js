class DataService {

  /*@ngInject*/

  constructor($http, apiService, loaderService) {
    this.$http = $http;
    this.apiService = apiService;
    this.loaderService = loaderService;
  }

  getItems($url, jsonUrl) {
    const apiUrl = jsonUrl || this.apiService.basePaths.json;
    return this.$http
      .get(apiUrl + $url)
      .then(result => result.data, error => {
        console.error(':::: ERROR IN DATASERVICE GETITEMS ::::', error);
        throw error;
      });
  }

  get($path, $params) {
    this.loaderService.setLoad(true, $path, $params);
    return this.$http
      .get($path, $params)
      .then(result => {
          this.loaderService.setLoad(false, $path, $params);
          return result.data;
        },
        error => {
          this.loaderService.setLoad(false, $path, $params);
          console.error(':::: ERROR IN DATASERVICE GET ::::', error);
          throw error;
        });
  }

  post($path, data, $params) {
    this.loaderService.setLoad(true, $path, data, $params);
    console.log('something');
    return this.$http
      .post($path, data, $params)
      .then(result => {
        this.loaderService.setLoad(false, $path, data, $params);
        return result.data;
      }, error => {
        this.loaderService.setLoad(false, $path, data, $params);
        console.error(':::: ERROR IN DATASERVICE POST ::::', error);
        throw error;
      });
  }

  put($path, data, $params) {
    this.loaderService.setLoad(true, $path, data, $params);
    return this.$http
      .put($path, data, $params)
      .then(result => {
        this.loaderService.setLoad(false, $path, data, $params);
        return result.data;
      }, error => {
        this.loaderService.setLoad(false, $path, data, $params);
        console.error(':::: ERROR IN DATASERVICE PUT ::::', error);
        throw error;
      });
  }

  CallBingRestService(request) {
    return this.$http
      .jsonp(request, {jsonpCallbackParam: 'jsonp'})
      .then(
        response => response.data,
        error => console.error(error)
      );
  }
}

export default DataService;
