export default class ApiService {

  /*@ngInject*/

  constructor() {
    this.protocol = 'https://';

    /* dev api config */
    this.izBilling = this.protocol + '101cloudlicenses.azurewebsites.net';
    this.izInvoices = this.protocol + '101cloudbilling.azurewebsites.net';
    this.izTenantApi = this.protocol + '101cloudtenants.azurewebsites.net';
    this.izTemplateApi = this.protocol + '101cloudtemplates.azurewebsites.net';
    this.izClouds = this.protocol + '101cloudclouds.azurewebsites.net';

    /* prod api config */
    // this.izInvoices = 'https://portalbillings.azurewebsites.net';
    // this.izTenantApi = 'https://portaltenants.azurewebsites.net';
    // this.izClouds = 'https://portalclouds.azurewebsites.net';
    // this.izBilling =  'https://portallicenses.azurewebsites.net';
    // this.izTemplateApi = 'https://portaltemplates.azurewebsites.net';

    this.basePaths = {
      json: './app/assets/json/',
      icons: './app/assets/img/icons',
      tenantApi: 'https://microsoft-apiappa18d48147c544590807271e4a1ef3dd7.azurewebsites.net/101Cloud',
      api: this.izClouds + '/101Cloud',
      fullDataCenterEndPoint: this.izTenantApi + '/101cloud/FullFabrics',
      tenants: this.izTenantApi + '/101Cloud/Tenants',
      products: this.izBilling + '/101Cloud/Products',
      orders: this.izBilling + '/101Cloud/Order',
      makeOrder: this.izBilling + '/101Cloud/Order',
      location: this.protocol + 'dev.virtualearth.net/REST/v1/Locations',
      licenses: this.izBilling + '/101Cloud/Licenses',
      fullDatacenters: this.izTenantApi + '/101Cloud/FullFabrics',
      cloudDatacenters: this.izTenantApi + '/101Cloud/CloudDataCenters',
      hardwareTemplates: this.izTemplateApi + '/101Cloud/HardwareTemplate?tenantId=',
      billingInvoices: this.izInvoices + '/101Cloud/Invoices',
      priceFilters: this.izInvoices + '/101Cloud/PriceFilter',
      priceSettings: this.izInvoices + '/101Cloud/RateCard',
      servers: this.izTenantApi + '/101Cloud/Servers',
      actions: this.izTenantApi + '/101Cloud/Activities/Tenant',
      users: this.izTenantApi + '/101Cloud/AzureUsers',
      groups: this.izTenantApi + '/101Cloud/AzureGroups',
      invite: this.izTenantApi + '/101Cloud/Invite'
    };
    this.countryListEndPoint = this.izTenantApi + '/101Cloud/Countries';
    this.regionListEndPoint = this.izTenantApi + '/101Cloud/Regions';
    this.FullCloud = this.basePaths.api + '/FullClouds';
    this.serverIconsPath = this.basePaths.icons + '/servers/';
    this.Clouds = this.basePaths.api + '/Clouds';
    this.subnetRoles = this.basePaths.api + '/SubnetRoles';
    this.companyInfo = this.izTenantApi + '/101Cloud/Company/';
    this.tenantAccountRoles = this.izTenantApi + '/101Cloud/TenantAccountRoles/';
    this.singeSubscriptionItem = this.izTenantApi + '/101Cloud/AzureSubscriptions/';
    this.subscriptions = this.izTenantApi + '/101Cloud/Subscriptions/';
    this.spla = this.izBilling + '/101Cloud/Products';
  }
}
