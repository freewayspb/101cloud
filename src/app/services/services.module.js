import angular from 'angular';

import DataService from './rest/data.service';
import ApiService from './rest/api.service';

import NotificationService from './controls/notification.service';
import CountryService from './controls/country.service';
import DatacenterService from './controls/datacenter.service';

import CurrencyService from './views/currency.service';
import ClassService from './views/class.service';
import InformationPanelService from './views/information-panel.service';

import AddCloudService from '../components/depconf/clouds/addcloud/addcloud.service';
import CloudsService from '../components/depconf/clouds/clouds.service';
import LocationsService from '../components/depconf/locations/locations.service';
import CompareCloudService from '../components/depconf/clouds/comparecloud/comparecloud.service';
import InternalMenuService from '../common/internalMenu/InternalMenuService';
import OperationsServersService from '../components/operations/servers/servers.service';
import OperationsServicesService from '../components/operations/services/services.service';

const services = angular
  .module('app.services', [])
  .service('dataService', DataService)
  .service('apiService', ApiService)
  .service('notificationService', NotificationService)
  .service('currencyService', CurrencyService)
  .service('classService', ClassService)
  .service('cloudsService', CloudsService)
  .service('locationsService', LocationsService)
  .service('addCloudService', AddCloudService)
  .service('compareCloudService', CompareCloudService)
  .service('menuService', InternalMenuService)
  .service('operationsServersService', OperationsServersService)
  .service('operationsServicesService', OperationsServicesService)
  .service('countryService', CountryService)
  .service('datacenterService', DatacenterService)
  .service('informationPanelService', InformationPanelService)
  .name;

export default services;
