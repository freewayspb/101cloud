class CurrencyService {

  /*@ngInject*/

  constructor($filter) {
    this.$filter = $filter;
    this.vat = 0.08;
  }

  /**
   * @name getPriceString
   * @desc Gets price string from number
   * @returns {string}
   * @param {number} price is price in number format
   * @param {string} symbol is symbol of currency
   */
  getPriceString(price, symbol) {
    const decimalSeparator = '.';
    const thousandsSeparator = '\'';
    let currency = symbol || 'CHF';
    if (symbol === '') {
      currency = '';
    }

    if (typeof price === 'number') {
      if (price < 0) {
        price = 'included in your package';
      } else {
        price = this.$filter('currency')(price, `${currency} `);
        price = price.split(',');
        price[price.length - 1] = price[price.length - 1].split('.');
        price[price.length - 1] = price[price.length - 1].join(decimalSeparator);
        price = price.join(thousandsSeparator);
      }
    }
    return price;
  }

  /**
   * @name formatPrice
   * @desc Convert price string from number to country currency format
   * @returns {string} price is price in currency format
   * @param {price} price is price in number format
   * @param {currency} currency is currency for country format
   */
  formatPrice(price, currency) {
    if(!currency) {
      currency = 'CHF';
    }
    price = this.$filter('currency')(price, `${currency} `);
    return price;
  }
}

export default CurrencyService;
