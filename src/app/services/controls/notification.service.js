class NotificationService {

  /*@ngInject*/

  constructor($rootScope) {
    this.rootScope = $rootScope;
  }

  /**
   * @name broadcast
   * @desc Method to broadcast notifications through all the scopes
   * @returns void
   * @param {string} name is event's name
   * @param {Object} eventData is an object to push through events
   */
  broadcast(name, ...eventData) {
    this.rootScope.$broadcast(name, ...eventData);
  }
}

export default NotificationService;
