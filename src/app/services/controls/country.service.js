class CountryService {

  /*@ngInject*/
  constructor(dataService, apiService) {
    this.countriesQ = dataService.get(apiService.countryListEndPoint)
      .then(result => {
        this.countries = result;
        return result;
      }, error => {
        console.error(':::: ERROR OCCURRED IN COUNTRY SERVICE COUNTRIESQ ::::', error);
        throw error;
      });

    this.regionsQ = dataService.get(apiService.regionListEndPoint)
      .then(result => {
        this.regions = result;
        return result;
      }, error => {
        console.error(':::: ERROR OCCURRED IN COUNTRY SERVICE REGIONSQ ::::', error);
        throw error;
      });

    this.allRegions = {displayName: 'All Regions', isAll: true};
    this.allCountries = {displayName: 'All Countries', isAll: true};
  }

  getCountriesByRegion(regionObj) {
    if (!regionObj) {
      return _.concat(this.allCountries, this.countries);
    } else {
      return _.concat(this.allCountries, _.filter(this.countries, country => country.Region.Id === regionObj.Id));
    }
  }

  getRegionByCountry(countryObj) {
    return _.find(this.regions, region => countryObj.Region.Id === region.Id);
  }

  getcountryById(Id) {
    return _.find(this.countries, country => country.Id === Id);
  }
}

export default CountryService;
