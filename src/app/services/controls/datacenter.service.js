class DatacenterService {

  /*@ngInject*/
  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.getDatacenters();
  }

  getDatacenters() {
    return this.dataService.get(this.apiService.basePaths.fullDataCenterEndPoint)
      .then(datacenters => {
        const dcNoNetworks = _.filter(datacenters, datacenter => datacenter.Networks.length > 0);
        this.datacenters = _.filter(dcNoNetworks,
          datacenter => Boolean(_.findIndex(datacenter.Networks, network => network.Subnets.length > 0) + 1));
        this.getRegionsCountriesAndCitiesOfDatacenters();
        return this.datacenters;
      }, error => {
        console.error(':::: ERROR OCCURRED IN DATACENTER SERVICE GET DATACENTERS::::', error);
        throw error;
      });
  }

  getRegionsCountriesAndCitiesOfDatacenters() {
    let regions, countries, cities;

    regions = _.map(this.datacenters, datacenter => datacenter.Addresses[0].Country.Region);
    regions = _.uniqBy(regions, 'Id');
    regions = _.sortBy(regions, 'Name');

    countries = _.map(this.datacenters, datacenter => datacenter.Addresses[0].Country);
    countries = _.uniqBy(countries, 'Id');
    countries = _.sortBy(countries, 'Name');

    cities = _.map(this.datacenters, datacenter => datacenter.Addresses[0].City);
    cities = _.uniq(cities);
    cities = _.sortBy(cities);

    this.regions = regions;
    this.countries = countries;
    this.cities = cities;

    return {
      regions,
      countries,
      cities
    };
  }

  getTenantsByRegionAndCountryAndCityNames(region, country, city) {
    let datacenters = _.cloneDeep(this.datacenters);
    if (region) {
      datacenters = _.filter(datacenters, datacenter => datacenter.Addresses[0].Country.Region.Name === region);
    }
    if (country) {
      datacenters = _.filter(datacenters, datacenter => datacenter.Addresses[0].Country.Name === country);
    }
    if (city) {
      datacenters = _.filter(datacenters, datacenter => datacenter.Addresses[0].City === city);
    }
    return _.map(datacenters, datacenter => datacenter.Tenant);
  }

  getCountriesByRegionName(region) {
    const countries = this.getRegionsCountriesAndCitiesOfDatacenters().countries;
    if (region) {
      return _.filter(countries, country => country.Region.Name === region);
    } else {
      return countries;
    }
  }
}

export default DatacenterService;
