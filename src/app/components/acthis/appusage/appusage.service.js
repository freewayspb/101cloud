class AppusageService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getActions(tenantId, days) {
    return this.dataService.get(this.apiService.basePaths.actions + '/' + tenantId + '/Days/' + days).then(
      response => response,
      error => console.error(error));
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN ACTIVITIES & HISTORY -> APPLICATION USAGE SERVICE ::::', error);
      throw error;
    });
  }
}

export default AppusageService;
