function AppusageController(appusageService, headerService, $filter, $scope) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    headerService.getTenantPromise()
      .then(
        tenantId => {
          vm.isLoading = false;
          appusageService.getActions(tenantId, 30)
            .then(result => {
              vm.actions = result;
              vm.tableOptions = {
                data: vm.actions,
                headers: [
                  // {
                  //   name: 'Application',
                  //   dataProperty: 'Application',
                  //   enableSorting: true
                  // }, {
                  //   name: 'Unique Users',
                  //   dataProperty: 'Users',
                  //   enableSorting: true
                  // }, {
                  //   name: 'Total Sign Ins',
                  //   dataProperty: 'SignIns',
                  //   enableSorting: true
                  // }
                  {
                    name: 'Account',
                    dataProperty: 'Account',
                    enableSorting: true
                  }, {
                    name: 'Action',
                    dataProperty: 'Action',
                    enableSorting: true
                  }, {
                    name: 'Type',
                    dataProperty: 'Object',
                    enableSorting: true
                  }, {
                    name: 'Details',
                    dataProperty: 'Details',
                    enableSorting: true
                  }, {
                    name: 'Date',
                    dataProperty: 'DateTime',
                    enableSorting: true,
                    specials: {
                      type: 'date'
                    }
                  }
                ],
                dropdownOptions: {
                  data: [{displayName: '24 Hours'}, {displayName: '7 Days'}, {displayName: '30 Days'}],
                  placeholder: 'Select Period',
                  selected: {displayName: '7 Days'}
                }
              };
            });
        },
        error => {
          console.error(error);
          vm.isLoading = false;
        });
  };
  $scope.$on('onTenantChange', (event, tenant) => {
    vm.isLoading = true;
    appusageService.getActions(tenant.Id, 30).then(
      response => {
        vm.isLoading = false;
        vm.actions = response;
        vm.refreshData();
      }
    );
  });

  vm.refreshData = () => {
    if (vm.tableOptions) {
      vm.tableOptions.data = $filter('filter')(vm.actions, vm.searchText);
    }
  };
}

export default AppusageController;
