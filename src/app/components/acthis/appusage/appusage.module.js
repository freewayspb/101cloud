import angular from 'angular';

import component from './appusage.component';
import service from './appusage.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.acthis.appusage', {
      url: '/appusage',
      template: '<appusage></appusage>'
    });
};

const appusage = angular
  .module('app.acthis.appusage', [])
  .component('appusage', component)
  .service('appusageService', service)
  .config(config)
  .name;

export default appusage;
