import controller from './appusage.controller';
import template from './appusage.tpl.html';

const AppusageComponent = {
  controller,
  template
};

export default AppusageComponent;
