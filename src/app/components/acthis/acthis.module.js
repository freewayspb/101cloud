import angular from 'angular';

import component from './acthis.component';

import appusage from './appusage/appusage.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.acthis', {
      url: '/acthis',
      redirectTo: 'app.acthis.appusage',
      template: '<acthis></acthis>'
    });
};

const acthis = angular
  .module('app.acthis', [appusage])
  .component('acthis', component)
  .config(config)
  .name;

export default acthis;
