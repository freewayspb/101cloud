import controller from './acthis.controller';
import template from './acthis.tpl.html';

const ActhisComponent = {
  controller,
  template
};

export default ActhisComponent;
