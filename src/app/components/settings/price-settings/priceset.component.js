import controller from './priceset.controller';
import template from './priceset.tpl.html';

const PriceSettingsComponent = {
  controller,
  template
};

export default PriceSettingsComponent;
