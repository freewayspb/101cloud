import angular from 'angular';

import component from './priceset.component';
import service from './priceset.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.pricesettings', {
      url: '/price-settings',
      template: '<price-settings></price-settings>'
    });
};

const priceset = angular
  .module('app.settings.pricesettings', [])
  .component('priceSettings', component)
  .service('priceSettingsService', service)
  .config(config)
  .name;

export default priceset;
