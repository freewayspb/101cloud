import _ from 'lodash';
function PriceSettingsController(priceSettingsService, headerService, $scope) {
  const vm = this;

  class PriceList {
    constructor(rawItem, index) {
      const changedPricesKeyMap = _.uniq(priceSettingsService.changedPricesKeyMap);
      const keys = _.uniq(changedPricesKeyMap[index]);

      this.RateCardId = rawItem.RateCardId;
      _.each(keys, key => {
        switch (key) {
          case 'Price':
            this.Price = rawItem.FinalPrice;
            break;
          case 'Public':
            this.Public = rawItem.IsPublic;
            break;
          default:
            this[key] = rawItem[key];
            break;
        }
      });
    }
  }

  vm.isLoading = true;
  vm.filters = priceSettingsService.filters;

  $scope.$on('onTenantChange', (event, tenant) => {
    priceSettingsService.tenantId = tenant.Id;
    priceSettingsService.getFiltersData(tenant.Id);
  });

  vm.getPricelist = () => {
    vm.isPriceListLoaded = false;
    priceSettingsService.getPriceSettings()
      .then(prices => {
        _.each(prices, item => {
          vm.countPercent(item, item.FinalPrice);
        });
        priceSettingsService.tableOptions.data = prices;
        priceSettingsService.settingsPrices = angular.copy(prices);
        vm.isPriceListLoaded = true;
      });
  };

  vm.countPercent = priceSettingsService.countPercent;

  vm.publishPricelist = () => {
    priceSettingsService.changedPrices = _.map(priceSettingsService.changedPrices,
      (priceList, index) => new PriceList(priceList, index));
    priceSettingsService.publishPricelist()
      .then(() => {
        priceSettingsService.changedPrices = [];
          priceSettingsService.changedPricesKeyMap = [];
      },
      error => console.error(error)
    );
  };

  //TODO: remove second request when page reloads
  vm.$onInit = () => {
    vm.tableOptions = priceSettingsService.tableOptions;
    headerService.getTenantPromise().then(tenantId => priceSettingsService.getFiltersData(tenantId));
  };
}

export default PriceSettingsController;
