import _ from 'lodash';

class priceSettingsService {
  /*ngInject*/
  constructor(dataService, apiService) {
    const vm = this;
    vm.dataService = dataService;
    vm.apiService = apiService;
    vm.settingsPrices = [];
    vm.tenantId = '';
    vm.changedPrices = [];
    vm.changedPricesKeyMap = [];
    const gridCallbacks = key => {
      return {
        onChange: function (finalPriceCrunch) {
          delete this.item.entity.$$hashKey;
          const currentPriceId = _.findIndex(vm.settingsPrices, ['RateCardId', this.item.entity.RateCardId]);
          const preparedPriceId = _.findIndex(vm.changedPrices, ['RateCardId', this.item.entity.RateCardId]);

          if (finalPriceCrunch) {
            vm.tableOptions.data[currentPriceId] = vm.countPercent(this.item.entity, finalPriceCrunch);
          }

          if (preparedPriceId === -1) {
            vm.changedPrices.push(this.item.entity);
            vm.changedPricesKeyMap.push([key]);
          } else {
            vm.changedPrices[preparedPriceId] = this.item.entity;
            vm.changedPricesKeyMap[preparedPriceId].push(key);
          }
        }
      };
    };
    vm.tableOptions = {
      data: [],
      search: 'right',
      enableCellEditOnFocus: true,
      headers: [
        {
          name: 'Product name',
          dataProperty: 'ProductName',
          enableSorting: true,
          enableCellEdit: true
        }, {
          name: 'Product ID',
          dataProperty: 'ProductId',
          enableSorting: true
        }, {
          name: 'Type Of Business',
          dataProperty: 'TypeOfBusiness',
          enableSorting: true
        }, {
          name: 'Subscription Type',
          dataProperty: 'Subscription',
          enableSorting: true
        }, {
          name: 'Price',
          dataProperty: 'Price',
          enableSorting: true,
          specials: {
            type: 'price'
          }
        }, {
          name: 'Erp Price',
          dataProperty: 'ErpPrice',
          enableSorting: true,
          specials: {
            type: 'price'
          }
        }, {
          name: 'Final Price',
          dataProperty: 'FinalPrice',
          enableSorting: true,
          specials: {
            type: 'input',
            options: {
              currency: true
            },
            callbacks: gridCallbacks('Price')
          }
        }, {
          name: 'Effective margin',
          dataProperty: 'EffectiveMargin',
          enableSorting: true,
          specials: {
            type: 'percent'
          }
        },{
          name: 'Gap to erp',
          dataProperty: 'GapToErp',
          enableSorting: true,
          specials: {
            type: 'percent'
          }
        },{
          name: 'Public',
          dataProperty: 'IsPublic',
          enableSorting: true,
          type: 'boolean',
          width: '50',
          specials: {
            type: 'checkbox',
            callbacks: gridCallbacks('Public')
          }
        }, {
          name: 'Reselling By Others',
          dataProperty: 'ResellingByOther',
          enableSorting: true,
          type: 'boolean',
          width: '50',
          specials: {
            type: 'checkbox',
            callbacks: gridCallbacks('ResellingByOthers')
          }
        }
      ]
    };
    vm.filters = {
      Countries: {
        placeholder: 'Country',
        data: []
      },
      Types: {
        placeholder: 'Type of Business',
        data: []
      },
      Currencies: {
        placeholder: 'Currency',
        data: []
      },
      Periods: {
        placeholder: 'Subscription type',
        data: []
      },
      Vendors: {
        placeholder: 'Vendor',
        data: []
      },
      Products: {
        placeholder: 'Product',
        data: []
      },
      Search: {
        options: {
          isSearch: true
        }
      },
      callback: {
        onChange: () => vm.getFiltersData(vm.tenantId)
      }
    };

    vm.setFiltersParams = filters => {
      return {
        countryname: filters.Countries.selected && filters.Countries.selected.value || '',
        typeofbusiness: filters.Types.selected && filters.Types.selected.value || '',
        currency: filters.Currencies.selected && filters.Currencies.selected.value || '',
        chargeperiodtype: filters.Periods.selected && filters.Periods.selected.value || '',
        vendor: filters.Vendors.selected && filters.Vendors.selected.value || '',
        product: filters.Products.selected && filters.Products.selected.value || '',
        resellerid: vm.tenantId
      };
    };

    vm.filterDataFillin = (filter, data) => {
      filter.data = [];
      filter.data.push({Name: 'All', value: null});
      _.each(data, item => {
        filter.data.push({Name: item, value: item});
      });
      _.sortedUniq(filter.data);
    };

    vm.getFilters = filters => {
      return vm.dataService.get(vm.apiService.basePaths.priceFilters, filters).then(
        result => result
      );
    };

    vm.getPriceSettings = () => {
      const filters = vm.setFiltersParams(vm.filters);
      return vm.dataService.get(vm.apiService.basePaths.priceSettings, {params: filters}).then(
        result => result
      );
    };

    vm.getItems = param => {
      return vm.dataService.getItems(param).then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN PARTNERS&SETTINGS -> PRICE SETTINGS SERVICE ::::', error);
      });
    };

    vm.countPercent = (item, finalPriceCrunch) => {
      const FinalPrice = parseFloat(finalPriceCrunch);
      const Price = parseFloat(item.Price);
      const erpPrice = parseFloat(item.ErpPrice);

      if (Price === 0 && FinalPrice === 0) {
        item.EffectiveMargin = 0;
        item.GapToErp = 0;
      } else if (FinalPrice === 0) {
        item.EffectiveMargin = null;
        item.GapToErp = 100;
      } else if (erpPrice !== null) {
        item.EffectiveMargin = (FinalPrice - Price) / Price * 100;
        item.GapToErp = (FinalPrice - erpPrice) / erpPrice * 100;
      }

      if (erpPrice === null) {
        item.GapToErp = null;
      }

      return item;
    };

    vm.publishPricelist = () => {
      const data = {TenantId: vm.tenantId, Items: vm.changedPrices};
      return this.dataService.post(this.apiService.basePaths.priceSettings, data).then(
        result => result
      );
    };
  }

  getFiltersData(tenantId) {
    this.tenantId = tenantId;
    const filtersParams = this.setFiltersParams(this.filters);
    this.getFilters({params: filtersParams}).then(
      filters => {
        this.filterDataFillin(this.filters.Countries, filters.Countries);
        this.filterDataFillin(this.filters.Vendors, filters.Vendor);
        this.filterDataFillin(this.filters.Types, filters.TypeOfBusiness);
        this.filterDataFillin(this.filters.Products, filters.Product);
        this.filterDataFillin(this.filters.Periods, filters.ChargeType);
        this.filterDataFillin(this.filters.Currencies, filters.Currencies);
      }
    );
  }
}

export default priceSettingsService;
