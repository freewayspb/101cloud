import controller from './legal-settings.controller';
import template from './legal-settings.tpl.html';

const LegalComponent = {
  controller,
  template
};

export default LegalComponent;
