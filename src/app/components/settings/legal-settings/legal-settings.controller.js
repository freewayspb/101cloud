function LegalController(settingsLegalService) {
  this.isLoading = true;
  settingsLegalService.getItems('settings-legal-settings.json')
    .then(result => {
      this.tableOptions = {
        data: result,
        headers: settingsLegalService.headers,
        sref: 'app.settings.legal.add'
      };
      this.isLoading = false;
    });
}

export default LegalController;
