import angular from 'angular';

import component from './legal-settings-add.component';
import service from './legal-settings-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.legal.add', {
      url: '/add',
      template: '<settings-add-legal></settings-add-legal>'
    });
};

const sAddLegalSettings = angular
  .module('app.settings.legal.add', [])
  .component('settingsAddLegal', component)
  .service('settingsAddLegalSettingsService', service)
  .config(config)
  .name;

export default sAddLegalSettings;
