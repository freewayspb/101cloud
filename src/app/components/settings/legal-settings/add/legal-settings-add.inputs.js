const SettingsAddLegalSettingsInputs = {
  name: {
    data: '',
    title: 'Terms & Conditions Name',
    placeholder: 'Enter'
  },
  type: {
    data: [
      {displayName: 'Genral'},
      {displayName: 'For Purchasing Licenses'}
    ],
    title: 'Terms & Conditions Type',
    placeholder: 'Select'
  },
  fromDate: {
    data: null
  },
  toDate: {
    data: null
  },
  description: {
    data: ''
  },
  status: {
    date: 'active'
  },
  retried: {
    data: {
      title: 'Retried',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  }
};

export default SettingsAddLegalSettingsInputs;
