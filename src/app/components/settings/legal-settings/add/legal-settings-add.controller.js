import inputs from './legal-settings-add.inputs';

function AddLegalSettingsController(FileUploader) {
  this.inputs = inputs;

  this.documentUploader = new FileUploader();
}

export default AddLegalSettingsController;
