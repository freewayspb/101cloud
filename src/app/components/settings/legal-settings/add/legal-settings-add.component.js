import controller from './legal-settings-add.controller';
import template from './legal-settings-add.tpl.html';

const AddLegalSettingsComponent = {
  controller,
  template
};

export default AddLegalSettingsComponent;
