class BillingLegalService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Terms & Conditions Name',
        dataProperty: 'TermsConditionsName',
        enableSorting: true
      },
      {
        name: 'Terms & Conditions Type',
        dataProperty: 'TermsConditionsType',
        enableSorting: true
      },
      {
        name: 'Description',
        dataProperty: 'Description',
        enableSorting: true
      },
      {
        name: 'From',
        dataProperty: 'From',
        enableSorting: true,
        specials: {
          type: 'date'
        }
      },
      {
        name: 'To',
        dataProperty: 'To',
        enableSorting: true,
        specials: {
          type: 'date'
        }
      },
      {
        name: 'Status',
        dataProperty: 'Status',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'status',
          params: {
            icons: true
          }
        }
      },
      {
        name: 'Attachment',
        dataProperty: 'Attachment',
        enableSorting: true
      },
      {
        name: 'Retry',
        dataProperty: 'Retry',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> LEGAL SETTINGS -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingLegalService;
