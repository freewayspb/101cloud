import angular from 'angular';

import component from './legal-settings.component';
import service from './legal-settings.service';

import AddLegalSettings from './add/legal-settings-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.legal', {
      url: '/legal',
      template: '<ui-view />',
      redirectTo: 'app.settings.legal.list'
    })
    .state('app.settings.legal.list', {
      url: '/list',
      template: '<settings-legal></settings-legal>'
  });
};

const sLegal = angular
  .module('app.settings.legal', [AddLegalSettings])
  .component('settingsLegal', component)
  .service('settingsLegalService', service)
  .config(config)
  .name;

export default sLegal;
