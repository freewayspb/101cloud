import controller from './settings.controller';
import template from './settings.tpl.html';

const PartsetComponent = {
  controller,
  template
};

export default PartsetComponent;
