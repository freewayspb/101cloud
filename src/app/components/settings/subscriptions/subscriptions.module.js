import angular from 'angular';

import component from './subscriptions.component';
import service from './subscriptions.service';

import AddSubscriptions from './add/subscriptions-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.subscriptions', {
      url: '/subscriptions',
      template: '<ui-view />',
      redirectTo: 'app.settings.subscriptions.list'
    })
    .state('app.settings.subscriptions.list', {
      url: '/list',
      template: '<settings-subscriptions></settings-subscriptions>'
    });
};

const sSubscriptions = angular
  .module('app.settings.subscriptions', [AddSubscriptions])
  .component('settingsSubscriptions', component)
  .service('settingsSubscriptionsService', service)
  .config(config)
  .name;

export default sSubscriptions;
