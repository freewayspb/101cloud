function SubscriptionsController(settingsSubscriptionsService) {
  this.isLoading = true;
  settingsSubscriptionsService.getItems('settings-subscriptions.json')
    .then(result => {
      this.tableOptions = {
        data: result,
        headers: settingsSubscriptionsService.headers,
        sref: 'app.settings.subscriptions.add'
      };
      this.isLoading = false;
    });
}

export default SubscriptionsController;
