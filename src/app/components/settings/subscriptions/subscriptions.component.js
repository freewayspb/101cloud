import controller from './subscriptions.controller';
import template from './subscriptions.tpl.html';

const SubscriptionsComponent = {
  controller,
  template
};

export default SubscriptionsComponent;
