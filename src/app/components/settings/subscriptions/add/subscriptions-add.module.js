import angular from 'angular';

import component from './subscriptions-add.component';
import service from './subscriptions-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.subscriptions.add', {
      url: '/add',
      template: '<settings-add-subscriptions></settings-add-subscriptions>'
    });
};

const sAddSubscriptions = angular
  .module('app.settings.subscriptions.add', [])
  .component('settingsAddSubscriptions', component)
  .service('settingsAddSubscriptionsService', service)
  .config(config)
  .name;

export default sAddSubscriptions;
