class BillingAddSubscriptionsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> ADD HT -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingAddSubscriptionsService;
