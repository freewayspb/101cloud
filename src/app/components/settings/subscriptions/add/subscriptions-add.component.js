import controller from './subscriptions-add.controller';
import template from './subscriptions-add.tpl.html';

const AddSubscriptionsComponent = {
  controller,
  template
};

export default AddSubscriptionsComponent;
