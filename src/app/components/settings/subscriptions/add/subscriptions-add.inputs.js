const SettingsAddSubscriptionsInputs = {
  name: {
    title: 'Name',
    placeholder: 'Enter'
  },
  subscriptionGroup: {
    title: 'Subscription Group'
  },
  partnerCenterVersion: {
    title: 'Partner Center Version'
  },
  partnerId: {
    title: 'Partner Id',
    placeholder: 'Enter'
  },
  mpnId: {
    title: 'MPN ID',
    placeholder: 'Enter Microsoft Partner Network ID'
  },
  registeredPartnerDomain: {
    title: 'Registered Partner Domain',
    placeholder: 'partnerdomain.onmicrosoft.com'
  },
  login: {
    title: 'Login',
    placeholder: 'user@partnerdomain.onmicrosoft.com'
  },
  password: {
    title: 'Password',
    placeholder: 'set password',
    options: {
      isPassword: true
    }
  },
  passwordConfirm: {
    title: 'Confirm Password',
    placeholder: 'set password',
    options: {
      isPassword: true
    }
  },
  regions: {
    title: 'Region',
    options: {
      multiselect: true
    }
  },
  countries: {
    title: 'Country',
    options: {
      multiselect: true
    }
  }
};

export default SettingsAddSubscriptionsInputs;
