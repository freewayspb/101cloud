import inputs from './subscriptions-add.inputs';

function AddSubscriptionsController() {
  this.inputs = inputs;
}

export default AddSubscriptionsController;
