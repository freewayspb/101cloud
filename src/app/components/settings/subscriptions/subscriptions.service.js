class BillingSubscriptionsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true
      },
      {
        name: 'Status',
        dataProperty: 'Status',
        enableSorting: true
      },
      {
        name: 'Partner Center',
        dataProperty: 'PartnerCenter',
        enableSorting: true
      },
      {
        name: 'Partner Id',
        dataProperty: 'PartnerId',
        enableSorting: true
      },
      {
        name: 'Mpn Id',
        dataProperty: 'MpnId',
        enableSorting: true
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> SUBSCRIPTIONS SETTINGS -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingSubscriptionsService;
