import controller from './own-subscriptions.controller';
import template from './own-subscriptions.tpl.html';

const OwnSubscriptionsComponent = {
  controller,
  template
};

export default OwnSubscriptionsComponent;
