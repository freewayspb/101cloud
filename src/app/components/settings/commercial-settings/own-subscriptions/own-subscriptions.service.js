class OwnSubscriptionsService {

  /*@ngInject*/

  constructor() {
    this.headers = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true
      },
      {
        name: 'Subscription Id',
        dataProperty: 'SubscriptionId',
        enableSorting: true
      },
      {
        name: 'Type',
        dataProperty: 'Type',
        enableSorting: true
      },
      {
        name: 'Business Owner',
        dataProperty: 'BusinessOwner',
        enableSorting: true
      },
      {
        name: 'Technical Owner',
        dataProperty: 'TechnicalOwner',
        enableSorting: true,
      },
      {
        name: 'Status',
        dataProperty: 'Status',
        enableSorting: true,
      },
      {
        name: 'Billing Period',
        dataProperty: 'BillingPeriod',
        enableSorting: true,
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
    this.data = [
      {
        Name: 'Subscription 1',
        SubscriptionId: '12',
        Type: 'Infrastructure Provider',
        BusinessOwner: 'Dan Brown',
        TechnicalOwner: 'Dan Brown',
        Status: 'Active',
        BillingPeriod: '01/01/2016 - 31/12/2016',
      },
      {
        Name: 'Subscription 1',
        SubscriptionId: '123',
        Type: 'Infrastructure Provider',
        BusinessOwner: 'Dan Brown',
        TechnicalOwner: 'Dan Brown',
        Status: 'Active',
        BillingPeriod: '01/01/2015 - 31/12/2015',
      },
      {
        Name: 'Subscription 1',
        SubscriptionId: '1234',
        Type: 'Office 365',
        BusinessOwner: 'Dan Brown',
        TechnicalOwner: 'Dan Brown',
        Status: 'Active',
        BillingPeriod: '01/01/2015 - 31/12/2015',
      }
    ];
  }

}

export default OwnSubscriptionsService;
