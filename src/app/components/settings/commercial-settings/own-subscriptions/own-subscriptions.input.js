const SCsCurrencyInputs = {
  regions: {
    data: [],
    title: 'Regions',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  },
  countries: {
    data: [],
    title: 'Countries',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  },
  serviceProvider: {
    data: [],
    title: 'Service Provider'
  },
  businessOwner: {
    data: [],
    title: 'Business Owner'
  },
  businessOwnerDeputy: {
    data: [],
    title: 'Business Owner Deputy'
  },
  technicalOwner: {
    data: [],
    title: 'Technical Owner'
  },
  technicalOwnerDeputy: {
    data: [],
    title: 'Technical Owner Deputy'
  }
};

export default SCsCurrencyInputs;
