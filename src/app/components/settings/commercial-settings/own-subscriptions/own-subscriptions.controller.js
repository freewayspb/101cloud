import inputs from './own-subscriptions.input';

function OwnSubscriptionsController(settingsComsetOwnSubscriptionsService, countryService) {
  
  this.$onInit = () => {
      this.ownSubscriptionVisible = true;
      
      this.type = 'individual';
      this.inputs = inputs;

      countryService.countriesQ
          .then(countries => {
              this.inputs.countries.data = countries;
          });

      countryService.regionsQ
          .then(regions => {
              this.inputs.regions.data = regions;
          });

    this.tableOptions = {
      data: settingsComsetOwnSubscriptionsService.data,
      headers: settingsComsetOwnSubscriptionsService.headers
    };

    this.add = () => {
      this.ownSubscriptionVisible = false;
    }

  };
}

export default OwnSubscriptionsController;
