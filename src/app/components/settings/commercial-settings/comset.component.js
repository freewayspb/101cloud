import controller from './comset.controller';
import template from './comset.tpl.html';

const ComsetComponent = {
  controller,
  template
};

export default ComsetComponent;
