const psCsCurrencyInputs = {
  groupDealProgress: {
    data: 20,
    min: 0,
    max: Infinity,
    values: {
      '0-2': {val: '0'},
      '2-20': {val: '1k'},
      '20-30': {val: '2k'},
      '30-40': {val: '3k'},
      '40-50': {val: '5k'},
      '50-60': {val: '10k'},
      '60-70': {val: '15k'},
      '70-80': {val: '20k'},
      '80-98': {val: '50k'},
      '98-102': {val: '∞'}
    }
  },
  groupMonthProgress: {
    data: 20,
    min: 0,
    max: Infinity,
    values: {
      '0-2': {val: '0'},
      '2-20': {val: '1k'},
      '20-30': {val: '2k'},
      '30-40': {val: '3k'},
      '40-50': {val: '5k'},
      '50-60': {val: '10k'},
      '60-70': {val: '15k'},
      '70-80': {val: '20k'},
      '80-98': {val: '50k'},
      '98-102': {val: '∞'}
    }
  },
  groupDealIsExceeded: {
    data: {
      model: 1,
      valueTrue: 1,
      valueFalse: 0
    }
  },
  groupMonthIsExceeded: {
    data: {
      model: 1,
      valueTrue: 1,
      valueFalse: 0
    }
  }
};

export default psCsCurrencyInputs;
