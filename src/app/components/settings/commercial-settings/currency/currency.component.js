import controller from './currency.controller';
import template from './currency.tpl.html';

const CurrencyComponent = {
  controller,
  template
};

export default CurrencyComponent;
