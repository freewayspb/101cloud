import inputs from './currency.inputs';
import modalController from './modal/modal.controller';
import modalTemplate from './modal/modal.tpl.html';

function CurrencyController($uibModal) {
  this.$onInit = () => {
    this.inputs = inputs;

    class Currency {
      constructor(currency, countries) {
        this.id = new Date().getTime();
        this.currency = currency;
        this.availability = {countries};
      }
    }

    this.currencies = [];

    this.editCurrency = result => {
      if (result.id) {
        const currencyIndex = _.findIndex(this.currencies, currency => currency.id === result.id);
        this.currencies[currencyIndex] = new Currency(result.currency, result.countries);
      } else {
        this.currencies.push(new Currency(result.currency, result.countries));
      }
    };

    this.add = () => {
      const addNewCurrency = $uibModal.open({
        animation: true,
        template: modalTemplate,
        controller: modalController,
        controllerAs: '$ctrl',
        size: 'sm',
        resolve: {currency: undefined}
      });

      addNewCurrency.result
        .then(this.editCurrency);
    };

    this.edit = currency => {
      const editCurrency = $uibModal.open({
        animation: true,
        template: modalTemplate,
        controller: modalController,
        controllerAs: '$ctrl',
        size: 'sm',
        resolve: {currency}
      });

      editCurrency.result
        .then(this.editCurrency);
    };

    this.delete = index => {
      this.currencies.splice(index, 1);
    };

    this.getCountriesString = countries => {
      const names = _.map(countries, country => country.Name);
      return names.join(', ');
    };
  };
}


export default CurrencyController;
