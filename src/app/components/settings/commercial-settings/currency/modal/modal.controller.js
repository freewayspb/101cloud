import inputs from './modal.inputs';

function CloudTypeModalController($uibModalInstance, currency, countryService) {
  this.$onInit = () => {
    this.inputs = inputs;
    this.currency = currency;

    countryService.countriesQ
      .then(countries => {
        this.inputs.countries.data = countries;
      });

    countryService.regionsQ
      .then(regions => {
        this.inputs.regions.data = regions;
      });

    if (currency) {
      this.inputs.currency.selected = currency.currency;
      this.inputs.countries.selected = currency.availability.countries;
      this.inputs.regions.selected = currency.availability.regions;
    } else {
      this.inputs.currency.selected = undefined;
      this.inputs.countries.selected = undefined;
      this.inputs.regions.selected = undefined;
    }
  };

  this.confirm = () => {
    $uibModalInstance.close({
      currency: this.inputs.currency.selected,
      countries: this.inputs.countries.selected,
      regions: this.inputs.regions.selected,
      id: currency && currency.id
    });
  };
}

export default CloudTypeModalController;
