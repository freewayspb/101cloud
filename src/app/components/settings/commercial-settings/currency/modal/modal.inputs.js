const SCsCurrencyInputs = {
  currency: {
    title: 'Currency',
    data: [
      {displayName: 'USD', Symbol: '$'},
      {displayName: 'EUR', Symbol: '€'},
      {displayName: 'CHF', Symbol: 'CHF'},
      {displayName: 'CNY', Symbol: '¥'},
      {displayName: 'GBP', Symbol: '£'}
    ]
  },
  regions: {
    data: [],
    title: 'Regions',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  },
  countries: {
    data: [],
    title: 'Countries',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  }
};

export default SCsCurrencyInputs;
