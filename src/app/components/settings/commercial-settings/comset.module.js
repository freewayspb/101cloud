import angular from 'angular';

import component from './comset.component';
import service from './comset.service';

import billingLimits from './billing-limits/billing-limits.component';
import currency from './currency/currency.component';
import invoiceTemplates from './invoice-templates/invoice-templates.component';
import paymentTerms from './payment-terms/payment-terms.component';
import security from './security/security.component';
import restrictions from './restrictions/restrictions.component';
import ownSubscriptions from './own-subscriptions/own-subscriptions.component';

import paymentTermsService from './payment-terms/payment-terms.service';
import securityService from './security/security.service';
import OwnSubscriptionsService from './own-subscriptions/own-subscriptions.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.comset', {
      url: '/comset',
      template: '<settings-comset />'
    });
};

const bComset = angular
  .module('app.settings.comset', [])
  .component('settingsComset', component)
  .component('psCsBillingLimits', billingLimits)
  .component('psCsCurrency', currency)
  .component('psCsInvoiceTemplates', invoiceTemplates)
  .component('psCsPaymentTerms', paymentTerms)
  .component('psCsSecurity', security)
  .component('psCsRestrictions', restrictions)
  .component('psCsOwnSubscriptions', ownSubscriptions)
  .service('settingsComsetService', service)
    .service('settingsComsetPaymentTermsService', paymentTermsService)
    .service('settingsComsetSecurityService', securityService)
    .service('settingsComsetOwnSubscriptionsService', OwnSubscriptionsService)
  .config(config)
  .name;

export default bComset;
