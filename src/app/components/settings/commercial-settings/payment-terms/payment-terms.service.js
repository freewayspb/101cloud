class BillingBillingLimitsService {

  /*@ngInject*/

  constructor() {
    this.headers = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true
      },
      {
        name: 'Payment Interval',
        dataProperty: 'PaymentInterval',
        enableSorting: true
      },
      {
        name: 'Disount %',
        dataProperty: 'Discount',
        enableSorting: true
      },
      {
        name: 'Type',
        dataProperty: 'Type',
        enableSorting: true
      },
      {
        name: 'From',
        dataProperty: 'From',
        enableSorting: true,
        specials: {
          type: 'date'
        }
      },
      {
        name: 'To',
        dataProperty: 'To',
        enableSorting: true,
        specials: {
          type: 'date'
        }
      },
      {
        name: 'Status',
        dataProperty: 'Status',
        enableSorting: false,
        specials: {
          type: 'switch'
        }
      },
      {
        name: 'Is Default',
        dataProperty: 'IsDefault',
        enableSorting: true,
        specials: {
          type: 'radio'
        }
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
    this.data = [
      {
        Id: 1,
        Name: 'Payment in advance',
        PaymentInterval: 0,
        Discount: 15,
        Type: 'Regular',
        From: '2017-03-06T07:20:04Z',
        To: '2017-04-06T07:20:04Z',
        Status: true
      },
      {
        Id: 2,
        Name: 'Payment in 10 days',
        PaymentInterval: 10,
        Discount: 12,
        Type: 'Regular',
        From: '2017-03-06T07:20:04Z',
        To: '2017-04-06T07:20:04Z',
        Status: true
      },
      {
        Id: 3,
        Name: 'Payment in 30 days',
        PaymentInterval: 30,
        Discount: 3,
        Type: 'Regular',
        From: '2017-03-06T07:20:04Z',
        To: '2017-04-06T07:20:04Z',
        Status: true
      },
      {
        Id: 4,
        Name: 'Newcomer proposal',
        PaymentInterval: 10,
        Discount: 20,
        Type: 'Temporary',
        From: '2017-03-06T07:20:04Z',
        To: '2017-04-06T07:20:04Z',
        Status: true
      }
    ];
  }

}

export default BillingBillingLimitsService;
