function BillingLimitsController(settingsComsetPaymentTermsService) {
  this.$onInit = () => {
    this.tableOptions = {
      data: settingsComsetPaymentTermsService.data,
      headers: settingsComsetPaymentTermsService.headers,
      hideHeader: true,
      isToolbarHidden: true
    };
  };
}

export default BillingLimitsController;
