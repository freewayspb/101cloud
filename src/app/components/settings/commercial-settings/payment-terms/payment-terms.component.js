import controller from './payment-terms.controller';
import template from './payment-terms.tpl.html';

const BillingLimitsComponent = {
  controller,
  template
};

export default BillingLimitsComponent;
