import controller from './billing-limits.controller';
import template from './billing-limits.tpl.html';

const BillingLimitsComponent = {
  controller,
  template
};

export default BillingLimitsComponent;
