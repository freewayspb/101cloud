import inputs from './billing-limits.inputs';

function BillingLimitsController() {
  this.$onInit = () => {
    this.inputs = inputs;
    const draggableSuffix = String.fromCharCode(160) + 'CHF';

    _.each(inputs.groupDealProgress.values, value => {
      if (value.val !== '0' && value.val !== '∞') {
        value.suffix = draggableSuffix;
      } else {
        value.suffix = '';
      }
    });

    _.each(inputs.groupMonthProgress.values, value => {
      if (value.val !== '0' && value.val !== '∞') {
        value.suffix = draggableSuffix;
      } else {
        value.suffix = '';
      }
    });

    class Group {
      constructor(name, description) {
        this.Name = name;
        this.Description = description;
        this.fields = _.cloneDeep(inputs);
      }
    }
    this.groups = [
      new Group('Group A', 'Default settings of limits'),
      new Group('Group B', 'Low risk occurrence customers'),
      new Group('Group C', 'Medium risk occurrence customers')
    ];
    this.defaultGroup = this.groups[0].Name;


    this.addGroup = () => {
      this.groups.push(new Group('Undeletable Group', 'Uneditable Description', false));
    };

    this.deleteGroup = index => {
      this.groups.splice(index, 1);
    };
  };
}


export default BillingLimitsController;
