import controller from './restrictions.controller';
import template from './restrictions.tpl.html';

const RestrictionsComponent = {
  controller,
  template
};

export default RestrictionsComponent;
