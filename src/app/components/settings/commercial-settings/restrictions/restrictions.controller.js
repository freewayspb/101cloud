import inputs from './restrictions.inputs';
import modalController from './modal/modal.controller';
import modalTemplate from './modal/modal.tpl.html';

function RestrictionsController($uibModal) {
  this.$onInit = () => {
    this.inputs = inputs;

    class Restriction {
      constructor(countries) {
        this.id = new Date().getTime();
        this.countries = countries;
      }
    }

    this.restrictions = [];

    this.getRegions = countries => {
      const dirtyRegions = _.map(countries, country => country.Region);
      return _.uniqBy(dirtyRegions, 'Id');
    };

    this.editRestriction = result => {
      if (result.id) {
        const restrictionsIndex = _.findIndex(this.restrictions, restrictions => restrictions.id === result.id);
        this.restrictions[restrictionsIndex] = new Restriction(result.countries);
      } else {
        this.restrictions.push(new Restriction(result.countries));
      }
    };

    this.add = () => {
      const addNewRestriction = $uibModal.open({
        animation: true,
        template: modalTemplate,
        controller: modalController,
        controllerAs: '$ctrl',
        size: 'sm',
        resolve: {restriction: undefined}
      });

      addNewRestriction.result
        .then(this.editRestriction);
    };

    this.edit = restriction => {
      restriction.regions = this.getRegions(restriction.countries);
      const editRestriction = $uibModal.open({
        animation: true,
        template: modalTemplate,
        controller: modalController,
        controllerAs: '$ctrl',
        size: 'sm',
        resolve: {restriction}
      });

      editRestriction.result
        .then(this.editRestriction);
    };

    this.delete = index => {
      this.restrictions.splice(index, 1);
    };

    this.getCollectionString = entities => {
      const names = _.map(entities, entity => entity.Name);
      return names.join(', ');
    };
  };
}


export default RestrictionsController;
