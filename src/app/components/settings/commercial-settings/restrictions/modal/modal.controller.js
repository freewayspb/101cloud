import inputs from './modal.inputs';

function CloudTypeModalController($uibModalInstance, restriction, countryService) {
  this.$onInit = () => {
    this.inputs = inputs;
    this.restriction = restriction;

    countryService.countriesQ
      .then(allCountries => {
        this.inputs.countries.data = allCountries;
      });

    countryService.regionsQ
      .then(allRegions => {
        this.inputs.regions.data = allRegions;
      });

    if (restriction) {
      this.inputs.countries.selected = restriction.countries;
      this.inputs.regions.selected = restriction.regions;
    } else {
      this.inputs.countries.selected = undefined;
      this.inputs.regions.selected = undefined;
    }
  };

  this.confirm = () => {
    $uibModalInstance.close({
      countries: this.inputs.countries.selected,
      id: restriction && restriction.id
    });
  };
}

export default CloudTypeModalController;
