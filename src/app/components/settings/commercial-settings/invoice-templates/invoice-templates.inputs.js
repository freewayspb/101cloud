const psCsInvoiceTemplatesInputs = {
  bill: {
    title: 'Bill-from company name'
  },
  country: {
    title: 'Country'
  },
  currency: {
    title: 'Billing Currency'
  },
  language: {
    title: 'Invoice Language'
  },
  font: {
    title: 'Invoice Font'
  },
  address1: {
    title: 'Bill-from address line 1'
  },
  address2: {
    title: 'Bill-from address line 2'
  },
  city: {
    title: 'City'
  },
  state: {
    title: 'State/Province'
  },
  zip: {
    title: 'ZIP/Postal code'
  },
  firstName: {
    title: 'Bill-from contact first name'
  },
  secondName: {
    title: 'Bill-from contact second name'
  },
  phone: {
    title: 'Bill-from contact phone'
  },
  email: {
    title: 'Bill-from email'
  },
  bankDetails: {
    title: 'Bank Details'
  },
  addPrefix: {
    data: {
      title: 'Add specific invoice prefix'
    }
  },
  prefix: {
  },
  startWith: {
    data: {
      title: 'Start numbering at first every new period'
    }
  }
};

export default psCsInvoiceTemplatesInputs;