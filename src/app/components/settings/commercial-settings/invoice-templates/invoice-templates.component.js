import controller from './invoice-templates.controller';
import template from './invoice-templates.tpl.html';

const BillingLimitsComponent = {
  controller,
  template
};

export default BillingLimitsComponent;
