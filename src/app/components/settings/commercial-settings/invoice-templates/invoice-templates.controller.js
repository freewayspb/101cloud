import inputs from './invoice-templates.inputs';

function BillingLimitsController() {
  this.$onInit = () => {
    this.inputs = inputs;
  }
}

export default BillingLimitsController;
