class PartsetComsetSecurityService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'User',
        dataProperty: 'User',
        enableSorting: true
      },
      {
        name: 'User Type',
        dataProperty: 'Type',
        enableSorting: true
      },
      {
        name: 'Email Address',
        dataProperty: 'Email',
        enableSorting: true
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN PARTNER SETTINGS -> COMMERCIAL SETTINGS -> SECURITY ::::', error);
      });
  }
}

export default PartsetComsetSecurityService;
