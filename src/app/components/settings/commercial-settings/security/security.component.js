import controller from './security.controller';
import template from './security.tpl.html';

const BillingLimitsComponent = {
  controller,
  template
};

export default BillingLimitsComponent;
