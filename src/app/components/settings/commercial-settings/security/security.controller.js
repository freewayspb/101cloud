import _ from 'lodash';
function BillingLimitsController(settingsComsetSecurityService) {
  this.$onInit = () => {
    settingsComsetSecurityService.getItems('ps-cs-security.json')
      .then(result => {
        this.tables = [];
        _.each(result, table => {
          const tableOptions = {
            data: table.Data,
            headers: _.cloneDeep(settingsComsetSecurityService.headers),
            isToolbarHidden: true,
            hideHeader: true,
            displayName: table.Name
          };
          this.tables.push(tableOptions);
        });
    });
  };
}

export default BillingLimitsController;
