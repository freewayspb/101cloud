import controller from './ht-settings.controller';
import template from './ht-settings.tpl.html';

const HtComponent = {
  controller,
  template
};

export default HtComponent;
