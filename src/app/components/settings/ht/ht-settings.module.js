import angular from 'angular';

import component from './ht-settings.component';
import service from './ht-settings.service';

import AddHt from './add/ht-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.ht', {
      url: '/ht',
      template: '<ui-view />',
      redirectTo: 'app.settings.ht.list'
    })
    .state('app.settings.ht.list', {
      url: '/list',
      template: '<settings-ht></settings-ht>'
  });
};

const sHt = angular
  .module('app.settings.ht', [AddHt])
  .component('settingsHt', component)
  .service('settingsHtService', service)
  .config(config)
  .name;

export default sHt;
