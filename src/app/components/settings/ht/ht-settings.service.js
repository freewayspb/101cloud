class BillingHtService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'HW Template',
        dataProperty: 'HwTemplate',
        enableSorting: true
      },
      {
        name: 'Fabric',
        dataProperty: 'Fabric',
        enableSorting: true
      },
      {
        name: 'Cores',
        dataProperty: 'Cores',
        enableSorting: true
      },
      {
        name: 'RAM',
        dataProperty: 'Ram',
        enableSorting: true
      },
      {
        name: 'Data Disks',
        dataProperty: 'DataDisks',
        enableSorting: true
      },
      {
        name: 'Max IOPS',
        dataProperty: 'MaxIops',
        enableSorting: true
      },
      {
        name: 'Public',
        dataProperty: 'Public',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'LoadBalancing',
        dataProperty: 'Load Balancing',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'Support',
        dataProperty: 'Support',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'Delivery Time',
        dataProperty: 'DeliveryTime',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> HARDWARE TEMPLATE SETTINGS -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingHtService;
