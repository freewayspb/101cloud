function HtController(settingsHtService) {
  this.isLoading = true;

  settingsHtService.getItems('settings-ht.json')
    .then(result => {
      this.tableOptions = {
        data: result,
        headers: settingsHtService.headers,
        sref: 'app.settings.ht.add'
      };
      this.isLoading = false;
    });
}

export default HtController;
