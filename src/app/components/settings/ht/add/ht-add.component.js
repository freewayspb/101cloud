import controller from './ht-add.controller';
import template from './ht-add.tpl.html';

const AddHtComponent = {
  controller,
  template
};

export default AddHtComponent;
