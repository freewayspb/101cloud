import angular from 'angular';

import component from './ht-add.component';
import service from './ht-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.ht.add', {
      url: '/add',
      template: '<settings-add-ht></settings-add-ht>'
    });
};

const sAddHt = angular
  .module('app.settings.ht.add', [])
  .component('settingsAddHt', component)
  .service('settingsAddHtService', service)
  .config(config)
  .name;

export default sAddHt;
