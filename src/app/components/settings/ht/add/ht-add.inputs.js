const SettingsAddHtInputs = {
  name: {
    data: '',
    title: 'Hardware Template Name',
    placeholder: 'Enter User Name'
  },
  fabric: {
    data: '',
    title: 'Fabric',
    placeholder: 'Enter Fabric'
  },
  deliveryTime: {
    data: '',
    title: 'Delivery Time',
    placeholder: 'Enter Time'
  },
  deliveryTimeInterval: {
    data: '',
    title: ' ',
    placeholder: 'Enter Time'
  },
  isPublic: {
    data: {
      title: 'Public',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  availableForReselling: {
    data: {
      title: 'Available for reselling by other Partners',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  cores: {
    data: '',
    placeholder: 'Enter',
    title: 'Cores',
    options: {
      postIconClass: 'icon-icon-2'
    }
  },
  ram: {
    data: '',
    placeholder: 'Enter',
    title: 'Ram, GB',
    options: {
      postIconClass: 'icon-icon-3'
    }
  },
  dataDisks: {
    data: '',
    placeholder: 'Enter',
    title: 'Data Disks',
    options: {
      postIconClass: 'icon-icon-4'
    }
  },
  logicalSsd: {
    data: '',
    placeholder: 'Enter',
    title: 'Logical SSD, GB',
    options: {
      postIconClass: 'icon-icon-6'
    }
  },
  maxIops: {
    data: '',
    placeholder: 'Enter',
    title: 'Max IOPS',
    options: {
      postIconClass: 'icon-icon-5'
    }
  },
  loadBalancing: {
    data: {
      title: 'Load Balancing',
      valueTrue: true,
      valueFalse: false,
      model: false
    },
    options: {
      preIconClass: 'icon-icon-7'
    }
  },
  premiumDiskSupport: {
    data: {
      title: 'Premium Disk Support',
      valueTrue: true,
      valueFalse: false,
      model: false
    },
    options: {
      preIconClass: 'icon-info'
    }
  },
  regions: {
    data: [],
    title: 'Regions',
    placeholder: 'Select',
    options: {
      multiselect: true
    }
  },
  countries: {
    data: [],
    title: 'Countries',
    placeholder: 'Select',
    options: {
      multiselect: true
    }
  }
};

export default SettingsAddHtInputs;
