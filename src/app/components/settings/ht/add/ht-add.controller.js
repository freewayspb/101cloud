import inputs from './ht-add.inputs';

function AddHtController(countryService) {
  this.inputs = inputs;
  countryService.countriesQ
    .then(countries => {
      this.inputs.countries.data = countries;
    });

  countryService.regionsQ
    .then(regions => {
      this.inputs.regions.data = regions;
    });
}

export default AddHtController;
