import angular from 'angular';

import component from './settings.component';

import partners from './partners/partners.module';
import comset from './commercial-settings/comset.module';
import priceset from './price-settings/priceset.module';
import additionalServices from './additional-services/additional-services.module';
import cloudTypes from './cloud-types/cloud-types.module';
import fabricSettings from './fabric-settings/fabric-settings.module';
import htSettings from './ht/ht-settings.module';
import legalSettings from './legal-settings/legal-settings.module';
import riskManagement from './risk-management/risk-management.module';
import vdcSettings from './vdc-settings/vdc-settings.module';
import subscriptions from './subscriptions/subscriptions.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings', {
      url: '/settings',
      redirectTo: 'app.settings.partners',
      template: '<settings></settings>'
    });
};

const settings = angular
  .module('app.settings', [partners, comset, priceset, additionalServices, cloudTypes, fabricSettings, htSettings,
    legalSettings, riskManagement, vdcSettings, subscriptions])
  .component('settings', component)
  .config(config)
  .name;

export default settings;
