import controller from './fabric-add.controller';
import template from './fabric-add.tpl.html';

const AddFabricComponent = {
  controller,
  template
};

export default AddFabricComponent;
