import angular from 'angular';

import component from './fabric-add.component';
import service from './fabric-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.fabric.add', {
      url: '/add',
      template: '<settings-add-fabric></settings-add-fabric>'
    });
};

const sAddFabric = angular
  .module('app.settings.fabric.add', [])
  .component('settingsAddFabric', component)
  .service('settingsAddFabricService', service)
  .config(config)
  .name;

export default sAddFabric;
