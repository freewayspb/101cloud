class BillingAddFabricService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> ADD FABRIC -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingAddFabricService;
