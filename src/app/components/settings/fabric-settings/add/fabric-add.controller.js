import inputs from './fabric-add.inputs';

function AddFabricController() {
  this.inputs = inputs;
}

export default AddFabricController;
