const SettingsAddFabricInputs = {
  locationPrefix: {
    data: '',
    title: 'Location Prefix'
  },
  name: {
    data: '',
    title: 'Fabric Name',
    placeholder: 'Enter Fabric Name',
    options: {
      max: 15
    }
  },
  region: {
    data: [],
    title: 'Region',
    placeholder: 'Select Region'
  },
  country: {
    data: [],
    title: 'Country',
    placeholder: 'Select Country'
  },
  city: {
    data: '',
    title: 'City',
    placeholder: 'Enter City'
  },
  subscription: {
    data: [],
    title: 'Subscription',
    placeholder: 'Select Subscription'
  },
  technology: {
    data: [],
    title: 'Technology',
    placeholder: 'Select Technology'
  },
  vmmServerName: {
    data: '',
    title: 'VMM Server Name (FQDN)',
    placeholder: 'Enter'
  },
  ip4Address: {
    data: '',
    title: 'IPv4 Address',
    placeholder: 'Enter',
    options: {
      required: true,
      max: 15
    }
  },
  ip6Address: {
    data: '',
    title: 'IPv6 Address',
    placeholder: 'Enter'
  },
  login: {
    data: '',
    title: 'Login',
    placeholder: 'Enter',
    options: {
      max: 15
    }
  },
  password: {
    data: '',
    title: 'Password',
    placeholder: 'Enter Password',
    options: {
      isPasword: true,
      type: 'password'
    }
  },
  addTopLevel: {
    data: '',
    title: 'Active Directory Domain Top Level',
    placeholder: 'Enter',
    options: {
      required: true
    }
  },
  addRoot: {
    data: '',
    title: 'Active Directory Domain Root',
    placeholder: 'Enter'
  },
  addChild: {
    data: '',
    title: 'Active Directory Domain Child',
    placeholder: 'Enter'
  },
  vmmLibraryShare: {
    data: '',
    title: 'VMM Library Share',
    placeholder: '\\\\servernsme.domain.internal\\Sharename',
    options: {
      required: true
    }
  },
  networkSettingsManaging: {
    data: [{displayName: 'by VMM Administrator'}],
    selected: {displayName: 'by VMM Administrator'},
    title: 'Network Settings Managing'
  },
  isPublic: {
    data: {
      title: 'Public',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  publicName: {
    data: '',
    title: 'Public Name (FQDN)',
    placeholder: 'Enter',
    options: {
      required: true,
      max: 15
    }
  },
  internalName: {
    data: '',
    title: 'Internal Name',
    placeholder: 'Enter',
    options: {
      max: 15
    }
  },
  remoteDesktopGatewayServer: {
    data: '',
    title: 'Remote Desktop Gateway Server',
    placeholder: 'Enter',
    options: {
      max: 15
    }
  }
};

export default SettingsAddFabricInputs;
