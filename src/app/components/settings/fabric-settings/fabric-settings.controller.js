function FabricController(settingsFabricService) {
  this.isLoading = true;

  settingsFabricService.getItems('settings-fabric-settings.json')
    .then(result => {
      this.tableOptions = {
        data: result,
        headers: settingsFabricService.headers,
        sref: 'app.settings.fabric.add'
      };
      this.isLoading = false;
    });
}

export default FabricController;
