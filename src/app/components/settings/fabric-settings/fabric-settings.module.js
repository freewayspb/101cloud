import angular from 'angular';

import component from './fabric-settings.component';
import service from './fabric-settings.service';

import AddFabric from './add/fabric-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.fabric', {
      url: '/fabric-settings',
      template: '<ui-view />',
      redirectTo: 'app.settings.fabric.list'
    })
    .state('app.settings.fabric.list', {
      url: '/list',
      template: '<settings-fabric></settings-fabric>'
  });
};

const sFabric = angular
  .module('app.settings.fabric', [AddFabric])
  .component('settingsFabric', component)
  .service('settingsFabricService', service)
  .config(config)
  .name;

export default sFabric;
