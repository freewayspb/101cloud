class BillingFabricService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Fabric',
        dataProperty: 'Fabric',
        enableSorting: true
      },
      {
        name: 'Region',
        dataProperty: 'Region',
        enableSorting: true
      },
      {
        name: 'Country',
        dataProperty: 'Country',
        enableSorting: true
      },
      {
        name: 'LocalPrefix',
        dataProperty: 'LocalPrefix',
        enableSorting: true
      },
      {
        name: 'City',
        dataProperty: 'City',
        enableSorting: true
      },
      {
        name: 'Subscription',
        dataProperty: 'Subscription',
        enableSorting: true
      },
      {
        name: 'Technology',
        dataProperty: 'Technology',
        enableSorting: true
      },
      {
        name: 'Public',
        dataProperty: 'Public',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> FABRIC SETTINGS -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingFabricService;
