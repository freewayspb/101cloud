import controller from './fabric-settings.controller';
import template from './fabric-settings.tpl.html';

const FabricComponent = {
  controller,
  template
};

export default FabricComponent;
