const SettingsAddCloudSettingsInputs = {
  name: {
    data: '',
    title: 'Cloud Type Name',
    placeholder: 'Enter'
  },
  description: {
    data: '',
    title: 'description'
  },
  isPublic: {
    data: {
      title: 'Public',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  availableForReselling: {
    data: {
      title: 'Available for reselling by other Partners',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  deliveryTime: {
    data: '',
    title: 'Delivery Time',
    placeholder: 'Enter Time'
  },
  deliveryTimeInterval: {
    data: '',
    title: ' ',
    placeholder: 'Enter Time'
  },
  isLimitedUsers: {
    data: {
      title: 'Up To Users',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  firstBlock: {
    fileServices: {
      data: {
        title: 'File Services',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    printServices: {
      data: {
        title: 'Print Services',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    activeDirectory: {
      data: {
        title: 'Active Directory',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    adfsFederationServices: {
      data: {
        title: 'AD FS - Federation Services',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    enterprisePki: {
      data: {
        title: 'Enterprice PKI',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    softwareDevelopment: {
      data: {
        title: 'Software Development',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    mobileDeviceManagement: {
      data: {
        title: 'Mobile Device Management',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    sfbEnterpriseVoice: {
      data: {
        title: 'Skype for Business Enterprice Voice',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    corporateAndGuestWlan: {
      data: {
        title: 'Corporate & Guest WLAN',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    informationRightsManagement: {
      data: {
        title: 'IRM - Information Rights Management',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    directAccess: {
      data: {
        title: 'Direct Access',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    }
  },
  secondBlock: {
    office365: {
      data: {
        title: 'Office 365',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    exchange: {
      data: {
        title: 'Exchange',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    sharePoint: {
      data: {
        title: 'SharePoint',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    sfb: {
      data: {
        title: 'Skype for Business',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    microsoftAzure: {
      data: {
        title: 'Microsoft Azure',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    azureAd: {
      data: {
        title: 'Azure AD',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    azureKeyVault: {
      data: {
        title: 'Azure KeyVault',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    },
    aws: {
      data: {
        title: 'AWS',
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    }
  },
  regions: {
    data: [],
    title: 'Regions',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  },
  countries: {
    data: [],
    title: 'Countries',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  }
};

export default SettingsAddCloudSettingsInputs;
