import controller from './cloud-settings-add.controller';
import template from './cloud-settings-add.tpl.html';

const AddCloudSettingsComponent = {
  controller,
  template
};

export default AddCloudSettingsComponent;
