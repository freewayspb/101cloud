import inputs from './cloud-settings-add.inputs';

function AddCloudSettingsController(countryService, FileUploader) {
  this.addSoft = true;
  this.addIntegration = true;
  this.inputs = inputs;
  this.documentUploader = new FileUploader();

  countryService.countriesQ
    .then(countries => {
      this.inputs.countries.data = countries;
    });

  countryService.regionsQ
    .then(regions => {
      this.inputs.regions.data = regions;
    });

  this.addMoreSoft = () => {
    this.addSoft = !this.addSoft;
  };

  this.addMoreIntegration = () => {
    this.addIntegration = !this.addIntegration;
  };

  this.addMoreSoftPush = value => {
    if (this.inputs.firstBlock[value.data] === undefined && value.data.length) {
      this.inputs.firstBlock[value.data] = generateInput(value);
      this.addSoft = !this.addSoft;
    }
  };

  this.addMoreIntegrationPush = value => {
    if (this.inputs.secondBlock[value.data] === undefined && value.data.length) {
      this.inputs.secondBlock[value.data] = generateInput(value);
      this.addIntegration = !this.addIntegration;
    }
  };

  function generateInput(value) {
    return {
      data: {
        title: value.data,
        valueTrue: true,
        valueFalse: false,
        model: false
      }
    };
  }
}

export default AddCloudSettingsController;
