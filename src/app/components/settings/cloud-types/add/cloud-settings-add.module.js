import angular from 'angular';

import component from './cloud-settings-add.component';
import service from './cloud-settings-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.cloudTypes.add', {
      url: '/add',
      template: '<settings-add-cloud-settings></settings-add-cloud-settings>'
    });
};

const sAddCloudSettings = angular
  .module('app.settings.cloudTypes.add', [])
  .component('settingsAddCloudSettings', component)
  .service('settingsAddCloudSettingsService', service)
  .config(config)
  .name;

export default sAddCloudSettings;
