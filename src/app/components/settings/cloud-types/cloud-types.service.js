import _ from 'lodash';

class BillingCloudTypesService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Cloud Type',
        dataProperty: 'Type',
        enableSorting: true
      },
      {
        name: 'Public',
        dataProperty: 'Public',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'Capabilities',
        dataProperty: 'Capabilities',
        enableSorting: true
      },
      {
        name: 'Integrations',
        dataProperty: 'IntegrationList',
        enableSorting: true,
        specials: {
          type: 'ct-integration'
        }
      },
      {
        name: 'Delivery Time',
        dataProperty: 'DeliveryTime',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> CLOUD TYPE SETTINGS -> GETITEMS SERVICE ::::', error);
      });
  }

  setCapabilities(items) {
    return _.map(items, item => {
      item.Capabilities = _.reduce(item.BenefitsList, setItemCapabilities);
    });

    function setItemCapabilities(concatedString, item) {
      concatedString += `, ${item}`;
      return concatedString;
    }
  }
}

export default BillingCloudTypesService;
