import controller from './cloud-types.controller';
import template from './cloud-types.tpl.html';

const CloudTypesComponent = {
  controller,
  template
};

export default CloudTypesComponent;
