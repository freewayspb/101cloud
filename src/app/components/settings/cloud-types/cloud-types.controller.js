function CloudTypesController(settingsCloudTypesService) {
  this.isLoading = true;
  settingsCloudTypesService.getItems('settings-cloud-types.json')
    .then(result => {
      settingsCloudTypesService.setCapabilities(result);
      this.tableOptions = {
        data: result,
        headers: settingsCloudTypesService.headers,
        sref: 'app.settings.cloudTypes.add'
      };
      this.isLoading = false;
    });
}

export default CloudTypesController;
