import angular from 'angular';
import 'angular-img-cropper';

import component from './cloud-types.component';
import service from './cloud-types.service';

import AddCloudTypes from './add/cloud-settings-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.cloudTypes', {
      url: '/cloud-types',
      template: '<ui-view />',
      redirectTo: 'app.settings.cloudTypes.list'
    })
    .state('app.settings.cloudTypes.list', {
      url: '/list',
      template: '<settings-cloud-types></settings-cloud-types>'
  });
};

const sCloudTypes = angular
  .module('app.settings.cloudTypes', [AddCloudTypes, 'angular-img-cropper'])
  .component('settingsCloudTypes', component)
  .service('settingsCloudTypesService', service)
  .config(config)
  .name;

export default sCloudTypes;
