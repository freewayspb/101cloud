class AdditionalServicesService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Service Name',
        dataProperty: 'ServiceName',
        enableSorting: true
      },
      {

        name: 'Additional Name',
        dataProperty: 'AdditionalName',
        enableSorting: true
      },
      {
        name: 'Description',
        dataProperty: 'Description',
        enableSorting: true
      },
      {
        name: 'Categories',
        dataProperty: 'Categories',
        enableSorting: true
      },
      {
        name: 'Public',
        dataProperty: 'Public',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'Delivery Time',
        dataProperty: 'DeliveryTime',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> ADDITIONAL SERVICES -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default AdditionalServicesService;
