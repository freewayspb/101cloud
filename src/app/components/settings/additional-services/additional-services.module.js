import angular from 'angular';

import component from './additional-services.component';
import service from './additional-services.service';

import AddAdditionalServices from './add/additional-services-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.additionalServices', {
      url: '/additional-services',
      template: '<ui-view />',
      redirectTo: 'app.settings.additionalServices.list'
    })
    .state('app.settings.additionalServices.list', {
      url: '/list',
      template: '<settings-additional-services></settings-additional-services>'
  });
};

const sAdditionalServices = angular
  .module('app.settings.additionalServices', [AddAdditionalServices])
  .component('settingsAdditionalServices', component)
  .service('settingsAdditionalServicesService', service)
  .config(config)
  .name;

export default sAdditionalServices;
