function AdditionalServicesController(settingsAdditionalServicesService) {
  this.isLoading = true;
  settingsAdditionalServicesService.getItems('settings-additional-services.json')
    .then(result => {
      this.tableOptions = {
        data: result,
        headers: settingsAdditionalServicesService.headers,
        sref: 'app.settings.additionalServices.add'
      };
      this.isLoading = false;
    });
}

export default AdditionalServicesController;
