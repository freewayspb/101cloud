import controller from './additional-services-add.controller';
import template from './additional-services-add.tpl.html';

const AddAdditionalServicesComponent = {
  controller,
  template
};

export default AddAdditionalServicesComponent;
