import angular from 'angular';

import component from './additional-services-add.component';
import service from './additional-services-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.additionalServices.add', {
      url: '/add',
      template: '<settings-add-additional-services></settings-add-additional-services>'
    });
};

const sAddAdditionalServices = angular
  .module('app.settings.additionalServices.add', [])
  .component('settingsAddAdditionalServices', component)
  .service('settingsAddAdditionalServicesService', service)
  .config(config)
  .name;

export default sAddAdditionalServices;
