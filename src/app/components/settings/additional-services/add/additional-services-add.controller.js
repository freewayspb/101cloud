import inputs from './additional-services-add.inputs';

function AddAdditionalServicesController(countryService) {
  this.inputs = inputs;

  countryService.countriesQ
    .then(countries => {
      this.inputs.countries.data = countries;
    });

  countryService.regionsQ
    .then(regions => {
      this.inputs.regions.data = regions;
    });
}

export default AddAdditionalServicesController;
