class BillingAddAdditionalServicesService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> ADD VDC -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingAddAdditionalServicesService;
