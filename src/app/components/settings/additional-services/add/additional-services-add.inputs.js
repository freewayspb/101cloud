const SettingsAddAdditionalServicesInputs = {
  serviceName: {
    data: '',
    title: 'Service Name',
    placeholder: 'Enter'
  },
  additionalName: {
    data: '',
    title: 'Additional Name',
    placeholder: 'Enter'
  },
  description: {
    data: '',
    title: 'Description',
    placeholder: 'Enter'
  },
  deliveryTime: {
    data: '',
    title: 'Delivery Time',
    placeholder: 'Enter'
  },
  deliveryTimeInterval: {
    data: [
      {displayName: 'Day'},
      {displayName: 'Hour'},
      {displayName: 'Min'},
      {displayName: 'Sec'}
    ],
    title: ' ',
    selected: {displayName: 'Min'}
  },
  isPublic: {
    data: {
      title: 'Public',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  availableForReselling: {
    data: {
      title: 'Available for reselling by other Partners',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  categories: {
    data: '',
    title: 'Categories',
    placeholder: 'Enter'
  },
  regions: {
    data: [],
    title: 'Regions',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  },
  countries: {
    data: [],
    title: 'Countries',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  }
};

export default SettingsAddAdditionalServicesInputs;
