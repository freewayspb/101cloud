import controller from './additional-services.controller';
import template from './additional-services.tpl.html';

const AdditionalServicesComponent = {
  controller,
  template
};

export default AdditionalServicesComponent;
