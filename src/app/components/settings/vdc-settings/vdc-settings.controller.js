function VdcsController(settingsVdcsService) {
  this.isLoading = true;
  settingsVdcsService.getItems('settings-vdc.json')
    .then(result => {
      this.tableOptions = {
        data: result,
        headers: settingsVdcsService.headers,
        sref: 'app.settings.vdc.add'
      };
      this.isLoading = false;
    });
}

export default VdcsController;
