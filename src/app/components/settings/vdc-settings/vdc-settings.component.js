import controller from './vdc-settings.controller';
import template from './vdc-settings.tpl.html';

const VdcsComponent = {
  controller,
  template
};

export default VdcsComponent;
