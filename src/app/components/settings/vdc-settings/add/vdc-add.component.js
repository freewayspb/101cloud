import controller from './vdc-add.controller';
import template from './vdc-add.tpl.html';

const AddVdcComponent = {
  controller,
  template
};

export default AddVdcComponent;
