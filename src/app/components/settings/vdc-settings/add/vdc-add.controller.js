import inputs from './vdc-add.inputs';

function AddVdcController(countryService) {
  this.inputs = inputs;

  countryService.countriesQ
    .then(countries => {
      this.inputs.countries.data = countries;
    });

  countryService.regionsQ
    .then(regions => {
      this.inputs.regions.data = regions;
    });
}

export default AddVdcController;
