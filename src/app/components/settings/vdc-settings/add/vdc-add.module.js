import angular from 'angular';

import component from './vdc-add.component';
import service from './vdc-add.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.vdc.add', {
      url: '/add',
      template: '<settings-add-vdc></settings-add-vdc>'
    });
};

const sAddVdc = angular
  .module('app.settings.vdc.add', [])
  .component('settingsAddVdc', component)
  .service('settingsAddVdcService', service)
  .config(config)
  .name;

export default sAddVdc;
