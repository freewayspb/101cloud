const SettingsAddVdcInputs = {
  name: {
    data: '',
    title: 'Package Name',
    placeholder: 'Enter User Name'
  },
  fabric: {
    data: '',
    title: 'Fabric',
    placeholder: 'Enter Fabric'
  },
  deliveryTime: {
    data: '',
    title: 'Delivery Time',
    placeholder: 'Enter Time'
  },
  deliveryTimeInterval: {
    data: '',
    title: ' ',
    placeholder: 'Enter Time'
  },
  isPublic: {
    data: {
      title: 'Public',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  availableForReselling: {
    data: {
      title: 'Available for reselling by other Partners',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  upload: {
    data: '',
    title: 'Upload, Mbps',
    placeholder: 'Enter'
  },
  uploadLimit: {
    data: '',
    title: 'Upload Limit, GB',
    placeholder: 'Enter'
  },
  download: {
    data: '',
    title: 'Download, Mbps',
    placeholder: 'Enter'
  },
  downloadLimit: {
    data: '',
    title: 'DownloadLimit, GB',
    placeholder: 'Enter'
  },
  privateIp4: {
    data: '',
    title: 'Private IP v4',
    placeholder: 'Enter'
  },
  privateIp6: {
    data: '',
    title: 'Private IP v6',
    placeholder: 'Enter'
  },
  privateIps: {
    data: '',
    title: 'Private IPS',
    placeholder: 'Enter'
  },
  maxPerformance: {
    data: '',
    title: 'Max Performance, Mbps',
    placeholder: 'Enter'
  },
  antivirus: {
    data: {
      title: 'Antivirus',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  vpn: {
    data: {
      title: 'VPN',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  ids: {
    data: {
      title: 'IDS',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  iops: {
    data: {
      title: 'IOPS',
      valueTrue: true,
      valueFalse: false,
      model: false
    }
  },
  ram: {
    data: '',
    title: 'RAM, GB',
    placeholder: 'Enter',
    options: {
      postIconClass: ''
    }
  },
  storage: {
    data: '',
    title: 'Storage, GB',
    placeholder: 'Enter',
    options: {
      postIconClass: 'icon-icon-6'
    }
  },
  cpus: {
    data: '',
    title: 'CPUs',
    placeholder: 'Enter',
    options: {
      postIconClass: 'icon-icon-2'
    }
  },
  additionalDescription: {
    data: '',
    title: 'Additional Description',
    placeholder: 'Additional Description'
  },
  support: {
    data: '',
    title: 'Support',
    placeholder: 'Enter'
  },
  regions: {
    data: [],
    title: 'Regions',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  },
  countries: {
    data: [],
    title: 'Countries',
    placeholder: 'Enter',
    options: {
      multiselect: true
    }
  }
};

export default SettingsAddVdcInputs;
