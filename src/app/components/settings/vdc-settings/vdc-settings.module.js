import angular from 'angular';

import component from './vdc-settings.component';
import service from './vdc-settings.service';

import AddVdc from './add/vdc-add.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.vdc', {
      url: '/vdc',
      template: '<ui-view />',
      redirectTo: 'app.settings.vdc.list'
    })
    .state('app.settings.vdc.list', {
      url: '/list',
      template: '<settings-vdcs></settings-vdcs>'
  });
};

const sVdcs = angular
  .module('app.settings.vdc', [AddVdc])
  .component('settingsVdcs', component)
  .service('settingsVdcsService', service)
  .config(config)
  .name;

export default sVdcs;
