class BillingVdcsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Virtual DC',
        dataProperty: 'VirtualDc',
        enableSorting: true
      },
      {

        name: 'Fabric',
        dataProperty: 'Fabric',
        enableSorting: true
      },
      {
        name: 'Public',
        dataProperty: 'Public',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'Reselling By Others',
        dataProperty: 'ResellingByOthers',
        enableSorting: true,
        type: 'boolean',
        specials: {
          type: 'checkbox'
        }
      },
      {
        name: 'Delivery Time',
        dataProperty: 'DeliveryTime',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> VDC SETTINGS -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default BillingVdcsService;
