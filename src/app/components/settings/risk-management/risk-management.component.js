import controller from './risk-management.controller';
import template from './risk-management.tpl.html';

const RisksComponent = {
  controller,
  template
};

export default RisksComponent;
