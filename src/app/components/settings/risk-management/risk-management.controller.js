function RisksController(settingsRiskManagementService) {
  this.isLoading = true;
  this.$onInit = () => {
    settingsRiskManagementService.getItems('settings-risk-management.json')
      .then(result => {
        const resultWithPhoto = _.map(result, user => {
          const userPhoto = user.ImageBase64
            ? 'data:image/png;base64,' + user.ImageBase64
            : '/app/assets/img/profiles/no-photo.png';

          user.User = {
            Name: user.User,
            Photo: userPhoto
          };
          return user;
        });

        this.tableOptions = {
          tables: [
            {
              show: 'MY CUSTOMERS',
              data: resultWithPhoto,
              enableRowSelection: true,
              enableSelectAll: true,
              headers: settingsRiskManagementService.myCustomersHeaders
            }, {
              show: 'MY PARTNERS',
              data: resultWithPhoto,
              enableRowSelection: true,
              enableSelectAll: true,
              headers: settingsRiskManagementService.myPartnersHeaders
            }
          ],
          checkboxes: {
            checked: 'MY PARTNERS',
            class: 'table-checkbox-group',
            data: [
              {displayName: 'MY PARTNERS'},
              {displayName: 'MY CUSTOMERS'}
            ]
          },
          rowHeight: 36
        };
        this.isLoading = false;
      });
  };
}

export default RisksController;
