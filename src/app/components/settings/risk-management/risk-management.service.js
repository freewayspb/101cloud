class RiskManagementService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.myCustomersHeaders = [
      {
        name: 'Customer',
        dataProperty: 'User',
        enableSorting: true,
        specials: {
          type: 'user-photo',
          params: {
            photo: ''
          }
        }
      },
      {
        name: 'Group',
        dataProperty: 'Group',
        enableSorting: true,
        specials: {
          type: 'select',
          params: {
            data: [
              {displayName: 'Group A; D: 4k CHF, M: 20k CHF'},
              {displayName: 'Group B; D: 3k CHF, M: 21k CHF'},
              {displayName: 'Group C; D: 2k CHF, M: 22k CHF'}
            ],
            selected: {displayName: 'Group B; D: 3k CHF, M: 21k CHF'}
          }
        }
      },
      {
        name: 'General Discount',
        dataProperty: 'GeneralDiscount',
        enableSorting: true,
        specials: {
          type: 'general-discount'
        }
      },
      {
        name: 'Payment Terms',
        dataProperty: 'PaymentTerms',
        enableSorting: true,
        specials: {
          type: 'select',
          params: {
            data: [
              {displayName: 'Payment in advance', value: 'advance'},
              {displayName: 'Payment in 30 days', value: '30'},
              {displayName: 'Payment in 10 days', value: '10'},
              {displayName: 'Newcomers Proposal', value: 'newcomers'}
            ],
            selected: {displayName: 'Payment in 10 days', value: '10'}
          }
        }
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            archive: true,
            delete: true
          }
        }
      }
    ];
    this.myPartnersHeaders = [
      {
        name: 'Partner',
        dataProperty: 'User',
        enableSorting: true,
        specials: {
          type: 'user-photo',
          params: {
            photo: ''
          }
        }
      },
      {
        name: 'Group',
        dataProperty: 'Group',
        enableSorting: true,
        specials: {
          type: 'select',
          params: {
            data: [
              {displayName: 'Group A; D: 4k CHF, M: 20k CHF'},
              {displayName: 'Group B; D: 3k CHF, M: 21k CHF'},
              {displayName: 'Group C; D: 2k CHF, M: 22k CHF'}
            ],
            selected: {displayName: 'Group B; D: 3k CHF, M: 21k CHF'}
          }
        }
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            archive: true,
            delete: true
          }
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SETTINGS -> RISK MANAGEMENT -> GETITEMS SERVICE ::::', error);
      });
  }
}

export default RiskManagementService;
