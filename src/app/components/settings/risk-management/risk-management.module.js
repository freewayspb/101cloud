import angular from 'angular';

import component from './risk-management.component';
import service from './risk-management.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.riskManagement', {
      url: '/risk-management',
      template: '<ui-view />',
      redirectTo: 'app.settings.riskManagement.list'
    })
    .state('app.settings.riskManagement.list', {
      url: '/list',
      template: '<settings-risks></settings-risks>'
    });
};

const sRisks = angular
  .module('app.settings.riskManagement', [])
  .component('settingsRisks', component)
  .service('settingsRiskManagementService', service)
  .config(config)
  .name;

export default sRisks;
