import angular from 'angular';

import component from './partners.component';
import service from './partners.service';

import become from './become/become.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.partners', {
      url: '/partners',
      template: '<ui-view />',
      redirectTo: 'app.settings.partners.list'
    })
    .state('app.settings.partners.list', {
      url: '/list',
      template: '<settings-partners></settings-partners>'
  });
};

const bPartners = angular
  .module('app.settings.partners', [become])
  .component('settingsPartners', component)
  .service('settingsPartnersService', service)
  .config(config)
  .name;

export default bPartners;
