import controller from './partners.controller';
import template from './partners.tpl.html';

const PartnersComponent = {
  controller,
  template
};

export default PartnersComponent;
