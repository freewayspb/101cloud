function PartnersController(settingsPartnersService, $q, $state) {
  const vm = this;
  vm.isLoading = true;

  vm.$onInit = () => {
    const partnersPromise = settingsPartnersService.getItems('settings-partners-partners.json');
    const customersPromise = settingsPartnersService.getItems('settings-partners-customers.json');

    $q.all([partnersPromise, customersPromise])
      .then(response => {
        vm.isLoading = false;
        const partners = response[0];
        const customers = response[1];
        vm.tableOptions = {
          tables: [
            {
              show: 'MY PARTNERS',
              data: partners,
              enableRowSelection: true,
              enableSelectAll: true,
              headers: [
                {
                  name: 'Partner',
                  dataProperty: 'Partner',
                  enableSorting: true
                }, {
                  name: 'Type of Business',
                  dataProperty: 'Type',
                  enableSorting: true
                }, {
                  name: 'Languages',
                  dataProperty: 'Languages',
                  enableSorting: true
                }, {
                  name: 'Region',
                  dataProperty: 'Region',
                  enableSorting: true
                }, {
                  name: 'Local Time',
                  dataProperty: 'LocalTime',
                  enableSorting: true
                }, {
                  name: 'Currency',
                  dataProperty: 'Currency',
                  enableSorting: true
                },
                {
                  name: '',
                  specials: {
                    type: 'actions',
                    params: {
                      edit: true,
                      delete: true
                    }
                  }
                }
              ]
            },
            {
              show: 'MY CUSTOMERS',
              data: customers,
              enableRowSelection: true,
              enableSelectAll: true,
              headers: [
                {
                  name: 'Partner',
                  dataProperty: 'Partner',
                  enableSorting: true
                }, {
                  name: 'Type of Business',
                  dataProperty: 'Type',
                  enableSorting: true
                }, {
                  name: 'Languages',
                  dataProperty: 'Languages',
                  enableSorting: true
                }, {
                  name: 'Region',
                  dataProperty: 'Region',
                  enableSorting: true
                }, {
                  name: 'Local Time',
                  dataProperty: 'LocalTime',
                  enableSorting: true
                }, {
                  name: 'Currency',
                  dataProperty: 'Currency',
                  enableSorting: true
                },
                {
                  name: '',
                  specials: {
                    type: 'actions',
                    params: {
                      edit: true,
                      delete: true
                    }
                  }
                }
              ]
            }
          ],
          checkboxes: {
            checked: 'MY PARTNERS',
            class: 'table-checkbox-group',
            data: [
              {displayName: 'MY PARTNERS'},
              {displayName: 'MY CUSTOMERS'}
            ]
          },
          actions: [
            {
              displayName: 'BECOME A PARTNER',
              class: 'btn-primary',
              action: () => {
                $state.go('app.settings.partners.become');
              }
            }
          ]
        };
      });
  };
}

export default PartnersController;
