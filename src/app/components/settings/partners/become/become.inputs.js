const BecomePartnerInputs = {
  visibility: 'public',
  website: {
    title: 'Website'
  },
  cc: {
    title: 'Country Code',
    data: []
  },
  rc: {
    title: 'Region Code'
  },
  phone: {
    title: 'Phone Number'
  },
  timezone: {
    title: 'Local Time'
  },
  currency: {
    title: 'Currency',
    options: {
      multiselect: true
    }
  },
  primaryLang: {
    title: 'Primary Language'
  },
  secondaryLang: {
    title: 'Secondary Language',
    options: {
      multiselect: true
    }
  },
  shortProfile: {
    title: 'Short profile of Partner'
  },
  longProfile: {
    title: 'Full profile of Partner'
  },
  tcName: {
    title: 'Terms & Conditions Name'
  },
  tcDescription: {},
  termsConditions: {
    EN: '<p>EN Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p> <p>Please read <a href=\'#\'>Terms&Conditions of the Service Provider</a></p>',
    DE: '<p>DE Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p> <p>Please read <a href=\'#\'>Terms&Conditions of the Service Provider</a></p>'
  },
  confirm: {
    data: {
      valueTrue: true,
      valueFalse: false,
      model: false,
      title: 'I accept the Terms & Conditions'
    },
    options: {
      required: true
    }
  },
  language: {
    data: [{displayName: 'EN'}, {displayName: 'DE'}],
    selected: {displayName: 'EN'}
  }
};

export default BecomePartnerInputs;
