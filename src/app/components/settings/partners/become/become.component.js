import controller from './become.controller';
import template from './become.tpl.html';

const BecomePartnerComponent = {
  controller,
  template
};

export default BecomePartnerComponent;
