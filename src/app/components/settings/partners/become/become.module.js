import angular from 'angular';

import component from './become.component';
import service from './become.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.settings.partners.become', {
      url: '/become',
      template: '<settings-become-partner></settings-become-partner>'
    });
};

const becomePartner = angular
  .module('app.settings.partners.become', [])
  .component('settingsBecomePartner', component)
  .service('settingsBecomePartnerService', service)
  .config(config)
  .name;

export default becomePartner;
