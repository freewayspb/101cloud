import inputs from './become.inputs';

function becomePartnerController($state) {
  this.$onInit = () => {
    this.inputs = inputs;
    this.step = 'settings';
  };

  this.continue = () => {
    this.step = 'agreement';
  };

  this.apply = () => {
    $state.go('app.settings.partners');
  };
}

export default becomePartnerController;
