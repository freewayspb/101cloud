class settingsPartnersService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN PARTNERS&SETTINGS -> PARTNERS -> GETITEMS SERVICE ::::', error);
    });
  }
}

export default settingsPartnersService;
