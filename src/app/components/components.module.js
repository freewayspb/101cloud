import angular from 'angular';

import core from './core/core.module';
import dashboard from './dashboard/dashboard.module';

import depconf from './depconf/depconf.module';
import operations from './operations/operations.module';
import orgset from './orgset/orgset.module';
import billing from './billing/billing.module';
import softlic from './softlic/softlic.module';
import users from './users/users.module';
import acthis from './acthis/acthis.module';
import settings from './settings/settings.module';

const components = angular
  .module('app.components', [
    dashboard,
    core,
    depconf,
    operations,
    orgset,
    billing,
    softlic,
    users,
    acthis,
    settings
  ])
  .name;

export default components;
