import controller from './services.controller';
import template from './services.tpl.html';

const ServicesComponent = {
  controller,
  template
};

export default ServicesComponent;
