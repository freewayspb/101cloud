import angular from 'angular';

import select from '../../../../common/addservice/steps/select.tpl.html';
import configure from '../../../../common/addservice/steps/configure.tpl.html';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations.services.addservice', {
      url: '/addservice',
      template: '<add-service />',
      abstract: true
    })
    .state('app.operations.services.addservice.select', {
      url: '/select',
      template: select
    })
    .state('app.operations.services.addservice.configure', {
      url: '/configure',
      template: configure
    });
};

config.$inject = ['$stateProvider'];

const addservice = angular
  .module('app.operations.services.addservice', [])
  .config(config)
  .name;

export default addservice;
