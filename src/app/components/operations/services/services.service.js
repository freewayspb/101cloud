class OperationsServicesService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN OPERATIONS -> SERVICES SERVICE ::::', error);
    });
  }
}

export default OperationsServicesService;
