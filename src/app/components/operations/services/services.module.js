import angular from 'angular';

import ServicesComponent from './services.component';

import addservice from './addservice/addservice.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations.services', {
      url: '/services',
      template: '<ui-view />',
      redirectTo: 'app.operations.services.list'
    })
    .state('app.operations.services.list', {
      url: '/list',
      template: '<o-services />'
    });
};

config.$inject = ['$stateProvider'];

const services = angular
  .module('app.operations.services', [addservice])
  .component('oServices', ServicesComponent)
  .config(config)
  .name;

export default services;
