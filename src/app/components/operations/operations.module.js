import angular from 'angular';

import OperationsComponent from './operations.component';
import template from './operations.tpl.html';
import controller from './operations.controller';

import dashboard from './dashboard/dashboard.module';
import servers from './servers/servers.module';
import services from './services/services.module';
import datacenters from './datacenters/datacenters.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations', {
      url: '/operations',
      redirectTo: 'app.operations.dashboard',
      template: '<operations />'
    });
};

config.$inject = ['$stateProvider'];

const operations = angular
  .module('app.operations', [dashboard, servers, services, datacenters])
  .component('operations', OperationsComponent)
  .controller('operationsController', controller)
  .config(config)
  .name;

export default operations;
