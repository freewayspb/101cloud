import controller from './operations.controller';
import template from './operations.tpl.html';

const OperationsComponent = {
  controller,
  template
};

export default OperationsComponent;
