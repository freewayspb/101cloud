import angular from 'angular';

import Datacenters from './datacenters.component';

import AddDataCenter from './adddatacenter/adddatacentrer.module';

import DatacentersService from './datacenters.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations.datacenters', {
      url: '/datacenters',
      template: '<ui-view />',
      redirectTo: 'app.operations.datacenters.list'
    })
    .state('app.operations.datacenters.list', {
      url: '/list',
      template: '<o-datacenters />'
    });
};

config.$inject = ['$stateProvider'];

const datacenters = angular
  .module('app.operations.datacenters', [AddDataCenter])
  .component('oDatacenters', Datacenters)
  .service('operationsDatacentersService', DatacentersService)
  .config(config)
  .name;

export default datacenters;
