function DatacentersController(operationsDatacentersService) {
  this.isLoading = true;
  this.$onInit = () => {
    operationsDatacentersService.getItems('datacenters.json')
      .then(result => {
        this.isLoading = false;
        this.tableOptions = {
          data: result,
          headers: [
            {
              name: 'Name',
              dataProperty: 'name',
              enableSorting: true
            }, {
              name: 'Country',
              dataProperty: 'country',
              enableSorting: true
            }, {
              name: 'City',
              dataProperty: 'city',
              enableSorting: true
            }, {
              name: 'Infrastructure Provider',
              dataProperty: 'iProvider',
              enableSorting: true
            }, {
              name: 'Subscription',
              dataProperty: 'subscription',
              enableSorting: true
            }, {
              name: '',
              specials: {
                type: 'actions',
                params: {
                  context: false,
                  edit: true,
                  delete: true
                }
              }
            }
          ],
          sref: 'app.operations.datacenters.adddatacenter'
        };
      });
      this.actions = {
        context: false,
        edit: true,
        delete: true
      };
  };
}

export default DatacentersController;
