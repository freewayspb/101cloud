import _ from 'lodash';

function AddDatacenterController(addDataserviceService) {
  this.$onInit = () => {
    addDataserviceService.getItems('countries.json')
      .then(result => {
        this.countryList = {
          data: result,
          title: 'Country'
        };
      }, err => {
        console.error(err);
      });

    this.cityList ={
      data: [{ displayName: 'Liverpool' }, { displayName: 'Singapore' }, { displayName: 'Hanoi' }],
      title: 'City'
    };
  };
}

export default AddDatacenterController;
