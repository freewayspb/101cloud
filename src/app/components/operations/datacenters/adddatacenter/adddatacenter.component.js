import controller from './adddatacenter.controller';
import template from './adddatacenter.tpl.html';

const AddServerComponent = {
  controller,
  template
};

export default AddServerComponent;
