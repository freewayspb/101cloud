class AddServerService {

  /*@ngInject*/

  constructor($http, dataService) {
    this.$http = $http;
    this.dataService = dataService;
  }

  getItems(item) {
    return this.dataService.getItems(item)
      .then(result => result, error => {
        console.error(':::: ERROR IN OPERATIONS -> ADDDATASERVER SERVICE GETITEMS ::::', error);
      });
  }
}

export default AddServerService;
