import angular from 'angular';

import AddDatacenterComponent from './adddatacenter.component';
import AddDatacenterService from './adddatacenter.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations.datacenters.adddatacenter', {
      url: '/adddatacenter',
      template: '<o-adddatacenter />'
    });
};

config.$inject = ['$stateProvider'];

const addserver = angular
  .module('app.operations.datacenters.adddatacenter', [])
  .component('oAdddatacenter', AddDatacenterComponent)
  .service('addDataserviceService', AddDatacenterService)
  .config(config)
  .name;

export default addserver;
