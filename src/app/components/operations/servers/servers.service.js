class OperationsServersService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.tableHeaders = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true,
        specials: {
          type: 'iconed'
        }
      }, {
        name: 'Status',
        dataProperty: 'Status',
        enableSorting: false,
        specials: {
          type: 'status',
          params: {
            icons: true
          }
        },
        isCentralized: true
      }, {
        name: 'Power',
        dataProperty: 'Power',
        enableSorting: false,
        specials: {
          type: 'power'
        },
        isCentralized: true
      }, {
        name: 'Cloud',
        dataProperty: 'Cloud',
        enableSorting: true
      }, {
        name: 'Datacenter',
        dataProperty: 'Provider',
        enableSorting: true
      }, {
        name: 'CPU',
        dataProperty: 'Cpu',
        enableSorting: false,
        specials: {
          type: 'status',
          params: {
            icons: false
          }
        },
        isCentralized: true
      }, {
        name: 'RAM',
        dataProperty: 'Ram',
        enableSorting: false,
        specials: {
          type: 'status',
          params: {
            icons: false
          }
        },
        isCentralized: true
      }, {
        name: 'Disk',
        dataProperty: 'Disk',
        enableSorting: false,
        specials: {
          type: 'status',
          params: {
            icons: false
          }
        },
        isCentralized: true
      },{
        name: 'Net',
        dataProperty: 'net',
        enableSorting: false,
        specials: {
          type: 'status',
          params: {
            icons: false
          }
        },
        isCentralized: true
      },{
        name: 'Description',
        dataProperty: 'Description',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            context: false,
            edit: true,
            delete: true
          }
        }
      }
    ];
  }
  getListServers(tenantId) {
    return this.dataService.get(this.apiService.basePaths.tenants + '/' + tenantId + '/servers')
      .then(
        response => response,
        error => console.error(error)
      );
  }
}

export default OperationsServersService;
