import angular from 'angular';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations.servers.addserver', {
      url: '/addserver',
      template: '<add-server />'
    });
};

config.$inject = ['$stateProvider'];

const addserver = angular
  .module('app.operations.servers.addserver', ['ui.grid'])
  .config(config)
  .name;

export default addserver;
