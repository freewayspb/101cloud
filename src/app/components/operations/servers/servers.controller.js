function ServersController(operationsServersService, headerService) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    headerService.getTenantPromise().then(
      id => {
        operationsServersService.getListServers(id)
          .then(result => {
              vm.tableOptions = {
                data: result,
                headers: operationsServersService.tableHeaders,
                sref: 'app.operations.servers.addserver'
              };
              vm.isLoading = false;
            },
            error => {
              console.error(error);
              vm.isLoading = false;
            }
          );
      },
        error => {
          console.error(error);
          vm.isLoading = false;
        }
      );
  };
}

export default ServersController;
