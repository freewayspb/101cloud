import angular from 'angular';

import ServersComponent from './servers.component';

import addserver from './addserver/addserver.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.operations.servers', {
      url: '/servers',
      template: '<ui-view />',
      redirectTo: 'app.operations.servers.list'
    })
    .state('app.operations.servers.list', {
      url: '/list',
      template: '<o-servers />'
    });
};

config.$inject = ['$stateProvider'];

const servers = angular
  .module('app.operations.servers', [addserver])
  .component('oServers', ServersComponent)
  .config(config)
  .name;

export default servers;
