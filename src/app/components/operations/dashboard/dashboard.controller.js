import tableDetailedHeaders from './dashboard.table.detailed';

function DashboardController(operationsDashboardService) {
  const vm = this;

  vm.$onInit = () => {
    vm.getGraph('year')
      .then(result => {
        vm.dashboard = result;
      });

    vm.getGraph('year', 'licenses')
      .then(result => {
        vm.detailedDashboard = result;
        vm.detailedDashboard.options = {
          isDetails: true
        };
      });
    vm.clouds = {
      labels: ['Error', 'Power OFF', 'OK', 'Warnings'],
      data: [100, 300, 1000, 300],
      options: {},
      title: 'Clouds'
    };
    vm.servers = {
      labels: ['Error', 'Power OFF', 'OK', 'Warnings'],
      data: [150, 500, 1100, 500],
      options: {},
      title: 'Servers'
    };
    vm.services = {
      labels: ['Error', 'Power OFF', 'OK', 'Warnings'],
      data: [250, 700, 1100, 400],
      options: {},
      title: 'Services'
    };
    vm.datacenters = {
      labels: ['Error', 'Power OFF', 'OK', 'Warnings'],
      data: [350, 500, 1100, 500],
      options: {},
      title: 'Datacenters'
    };
    vm.subscriptions = {
      labels: ['Limit', 'Not used', 'OK', 'Expiry'],
      data: [450, 1500, 1100, 500],
      options: {},
      title: 'Subscriptions'
    };
  };

  vm.getGraph = (period, entity) => {
    entity = entity ? entity + '-' : '';
    return operationsDashboardService.getItems('billing-dashboard-' + entity + period + '.json')
      .then(result => result,
        error => {
          alertsService.setAlert({msg: 'Cannot set graph.', type: 'danger'});
          console.error(':::: ERROR OCCURED IN BILLING DASHBOARD CONTROLLER WHILE FETCHING GRAPHS ::::', error);
          throw error;
        });
  };

  vm.selectCallback = params => {
    let storage;
    if (params.params.entity) {
      storage = vm.detailedDashboard;
      vm.detailedDashboard = undefined;
      storage.entity = 'detailedDashboard';
    } else {
      storage = vm.dashboard;
      vm.dashboard = undefined;
      storage.entity = 'dashboard';
    }
    vm.getGraph(params.params.period, params.params.entity)
      .then(result => {
        if (params.params.entity) {
          vm.detailedDashboard = result || {};
          vm.detailedDashboard.options = {
            isDetails: true,
            period: params.params.period,
            entity: params.params.entity
          };
        } else {
          vm.dashboard = result;
          vm.dashboard.options = {
            period: params.params.period
          };
        }
      }, () => {
        vm[storage.entity] = storage;
      });
  };

  vm.tableDataSelect = params => {

  };

  vm.tableOptions = undefined;
  operationsDashboardService.getItems('billing-drill-down-licenses-year.json')
    .then(result => {
      vm.tableOptions = {
        tableOptions: {
          data: result,
          headers: tableDetailedHeaders,
          hideHeader: true,
          isToolbarHidden: true,
          rowHeight: 50
        },
        title: 'Drill Down by Services',
        options: {
          isDetails: true
        }
      };
      if (result.status - 200 > 100) {
        vm.tableOptions.error = result;
      }
    });
}

export default DashboardController;
