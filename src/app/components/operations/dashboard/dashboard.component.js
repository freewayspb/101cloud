import controller from './dashboard.controller';
import template from './dashboard.tpl.html';

const DashboardComponent = {
  controller,
  template
};

export default DashboardComponent;
