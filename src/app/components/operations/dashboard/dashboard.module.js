import angular from 'angular';

import DashboardComponent from './dashboard.component';
import service from './dashboard.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.operations.dashboard', {
      url: '/dashboard',
      template: '<o-dashboard />'
    });
};

const datacenters = angular
  .module('app.operations.dashboard', [])
  .component('oDashboard', DashboardComponent)
  .service('operationsDashboardService', service)
  .config(config)
  .name;

export default datacenters;
