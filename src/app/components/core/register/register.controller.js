function RegisterController($state, $auth) {
  this.register = function () {
    const user = {
      email: this.email,
      password: this.password
    };

    $auth.signup(user)
      .then(() => {
        $state.go('dashboard');
      })
      .catch(() => {
        window.alert('Error: Register failed');
      });
  };
}

export default RegisterController;
