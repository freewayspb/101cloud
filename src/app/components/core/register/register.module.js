import angular from 'angular';

import RegisterComponent from './register.component';

const config = $stateProvider => {
  $stateProvider
    .state('app.register', {
      url: '/register',
      template: '<register></register>'
    });
};

config.$inject = ['$stateProvider'];

const register = angular
  .module('app.register', [])
  .component('register', RegisterComponent)
  .config(config)
  .name;

export default register;
