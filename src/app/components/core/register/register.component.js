import controller from './register.controller';
import template from './register.tpl.html';

const RegisterComponent = {
  controller,
  template,
  bindings: {
    user: '='
  }
};

export default RegisterComponent;
