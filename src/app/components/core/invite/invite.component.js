import controller from './invite.controller';
import template from './invite.tpl.html';

const LoginComponent = {
  controller,
  template,
  bindings: {
    user: '='
  }
};

export default LoginComponent;
