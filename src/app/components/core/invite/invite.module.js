import angular from 'angular';

import inviteComponent from './invite.component';
import InviteService from './invite.service';

const config = $stateProvider => {
  $stateProvider
    .state('invite', {
      url: '/invite',
      template: '<invite></invite>'
    });
};

config.$inject = ['$stateProvider'];

const invite =  angular
  .module('app.invite', [])
  .component('invite', inviteComponent)
  .service('inviteService', InviteService)
  .config(config)
  .name;

export default invite;
