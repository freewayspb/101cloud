class InviteService {
  constructor($http, dataService, apiService) {
    this.$http = $http;
    this.dataService = dataService;
    this.apiService = apiService;
  }

  post(data) {
    return this.dataService.post(this.apiService.basePaths.invite, data)
      .then(result => {
          return result;
        },
        error => {
          console.error(':::: ERROR IN ADDCLOUD SERVICE POST ::::', error, param);
          throw error;
        });
  }
}

export default InviteService;
