function inviteController(inviteService, authService, $state) {
  /*@ngInject*/
  const vm = this;
  vm.showForm = true;
  vm.authService = authService;
  vm.inviteInputs = {
    email: {
      title: 'E-mail',
      data: '',
      placeholder: 'Please type your e-mail',
      options: {
        type: 'email',
        required: true
      }
    },
    termsConditions: {
      title: 'TERMS & CONDITIONS',
      data: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc ege
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc ege`,
      options: {
        rows: '10'
      }
    },
    termsConditionsSign: {
      data: {
        title: 'I accept the Terms & Conditions',
        model: false,
        valueTrue: true,
        valueFalse: false
      },
      options: {
        required: true
      }
    }
  };
  vm.$onInit = () => {
  };

  vm.goToMain = () => {
    $state.go('app.dashboard');
  };

  vm.onSubmit = () => {
    const inviteData = {
      Email: vm.inviteInputs.email.data,
      ReturnUrl: window.location.protocol + '//' + window.location.host,
      SendInvitationMessage: true
    };
    console.log(inviteData);
    inviteService.post(inviteData).then(
      response => {
        vm.inviteLink = response;
        vm.showForm = false;
        },
      error => console.error(error)
    );
  };


}

export default inviteController;
