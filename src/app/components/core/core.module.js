import angular from 'angular';
import login from './login/login.module';
import register from './register/register.module';
import invite from './invite/invite.module';

const core =  angular
  .module('app.core', [login, register, invite])
  .name;

export default core;
