import controller from './login.controller';
import template from './login.tpl.html';

const LoginComponent = {
  controller,
  template,
  bindings: {
    user: '='
  }
};

export default LoginComponent;
