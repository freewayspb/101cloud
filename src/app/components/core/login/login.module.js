import angular from 'angular';

import LoginComponent from './login.component';

const config = $stateProvider => {
  $stateProvider
    .state('login', {
      url: '/login',
      template: '<login></login>'
    });
};

config.$inject = ['$stateProvider'];

const login =  angular
  .module('app.login', [])
  .component('login', LoginComponent)
  .config(config)
  .name;

export default login;
