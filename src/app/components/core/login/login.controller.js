function LoginController(authService, $location, $scope, $state) {
  const vm = this;
  vm.authService = authService;
  vm.$onInit = () => {
    if(localStorage.getItem('global.beforeLoginPath') && localStorage.getItem('adal.idtoken')) {
      $location.path(localStorage.getItem('global.beforeLoginPath'));
      localStorage.removeItem('global.beforeLoginPath');
    }
  };
  vm.goToMain = () => {
    $state.go('app.dashboard');
  };


}

export default LoginController;
