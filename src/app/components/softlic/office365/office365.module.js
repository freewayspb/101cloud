import angular from 'angular';

import component from './office365.component';
import service from './office365.service';

import addoffice365 from './addoffice365/addoffice365.module';

const config = $stateProvider => {

  /* ngInject */

  $stateProvider
    .state('app.softlic.office365', {
      url: '/office365',
      template: '<ui-view />',
      redirectTo: 'app.softlic.office365.list'
    })
    .state('app.softlic.office365.list', {
      url: '/list',
      template: '<sl-office365 />'
    });
};

const office365 = angular
  .module('app.softlic.office365', [addoffice365])
  .component('slOffice365', component)
  .service('slOffice365Service', service)
  .config(config)
  .name;

export default office365;
