import controller from './office365.controller';
import template from './office365.tpl.html';

const Office365Component = {
  controller,
  template
};

export default Office365Component;
