import controller from './addoffice365.controller';
import template from './addoffice365.tpl.html';

const AddOffice365Component = {
  controller,
  template
};

export default AddOffice365Component;
