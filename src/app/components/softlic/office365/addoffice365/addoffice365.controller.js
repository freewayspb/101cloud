import _ from 'lodash';

function AddOffice365Controller($stateParams, slOffice365Service, notificationService, $state,
  alertsService, headerService, slAddOffice365Service) {
  const vm = this;
  vm.isLoading = true;
  vm.ProductId = $stateParams.ProductId;
  vm.$onInit = () => {
    if ($stateParams.ProductId) {
      headerService.getTenantPromise().then(
        tenantId => {
          vm.payBy = slAddOffice365Service.payBy;
          vm.autoAssign = slAddOffice365Service.autoAssign;
          const params = {
            licenseType: 2,
            currency: 'CHF',
            productId: vm.ProductId,
            tenantid: tenantId,
            ProductType: 1
          };
          slOffice365Service.getProducts({params: params}).then(
            response => {
              vm.item = response[0];
              vm.resellersList = _.uniqBy(vm.getResellersList(vm.item.Prices), 'Name');
              vm.licenseResellers = {
                title: 'Licenses reseller',
                data: vm.resellersList,
                selected: vm.resellersList[0]
              };
              vm.refreshOfferByResseller();
              notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: true});
              notificationService.broadcast('onInformationChange', {
                type: 'item', discount: 0, price: vm.item.price, qty: 1
              });
              vm.isLoading = false;
            },
            error => {
              console.error(error);
              vm.isLoading = false;
            }
          );
        }
      );
    }
  };

  vm.getResellersList = slAddOffice365Service.getResellersList;

  vm.refreshOfferByResseller = () => {
      vm.offers = {
        data: vm.item.Prices,
        active: vm.item.Prices[0],
        model: vm.item.Prices[0].value
      };
  };

  vm.makeOrder = model => {
    vm.isLoading = true;
    const data = {
      'OfferId': vm.offers.active.OfferId,
      'CustomerId': headerService.getTenantId(),
      'Quantity': model.qty,
      'AutoAssign': true,
      'ProductType': 1
    };
    slAddOffice365Service.makeOrder(data).then(
      response => {
        vm.isLoading = false;
        $state.go('app.softlic.office365.list');
        alertsService.setAlert({msg: 'You purchase was successfully placed', type: 'success', showTime: 6000});
        return response;
      }, error => {
        console.error(':::: ERROR OCCURRED IN ADD OFFICE365 MAKE ORDER ::::', error);
        alertsService.setAlert({msg: 'Something happen during office purchasing', type: 'danger',  showTime: 6000});
        vm.isLoading = false;
      }
    );
  };

  vm.refreshOffer = () => {
    notificationService.broadcast('onInformationChange', {
      type: 'item',
      name: vm.offers.active.OfferName,
      discount: 0,
      price: vm.offers.active.ErpPrice,
      qty: 1,
      makeOrder: vm.makeOrder
    });
  };

  vm.selectCallback = {
    onChange: vm.refreshOfferByResseller
  };

  vm.radioGroupCallback = {
    onClick: vm.refreshOffer
  };

  vm.$onDestroy = () => {
    notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: false});
  };
}

export default AddOffice365Controller;
