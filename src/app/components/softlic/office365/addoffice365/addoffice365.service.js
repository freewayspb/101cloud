class AddOffice365Service {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.payBy = {
      title: 'How do you want to pay?',
      placeholder: 'Select Interval',
      data: [{'displayName': 'Pay Montly'}]
    };
    this.autoAssign = {
      data: {
        'title': 'Automatically assign to all of your users with no licenses',
        'model': true,
        'valueTrue': true,
        'valueFalse': false
      }
    };
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> ADD OFFICE365 SERVICE ::::', error);
    });
  }

  getResellersList(data) {
    const resellersList = [];
    data = _.map(data, item => {
      item.Name = item.ResellerName;
      item.value = item.OfferId;
      item.title = item.OfferName + ' - CHF ' + item.ErpPrice;
      resellersList.push({Name: item.ResellerName});
      return item;
    });
    return data;
  };

  makeOrder(data) {
    return this.dataService.post(this.apiService.basePaths.makeOrder, data).then(
      response => response,
      error => error);
  }
}

export default AddOffice365Service;
