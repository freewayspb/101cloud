import angular from 'angular';

import component from './addoffice365.component';
import service from './addoffice365.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic.office365.addoffice365plan', {
      url: '/checkoutSpla/:ProductId',
      template: '<sl-add-office365/>',
      params: {
        resellerId: null
      }
    });
};

config.$inject = ['$stateProvider'];

const addoffice365 = angular
  .module('app.softlic.office365.addoffice365plan', [])
  .component('slAddOffice365', component)
  .service('slAddOffice365Service', service)
  .config(config)
  .name;

export default addoffice365;
