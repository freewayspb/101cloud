class Office365Service {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.inputs = {
      search: {
        placeholder: 'Search',
        options: {
          isSearch: true
        }
      },
      filter: {
        data: [
          {displayName: 'All'},
          {
            displayName: 'Name',
            value: 'ProductName'
          },
          {
            displayName: 'Description',
            value: 'Description'
          }],
        placeholder: 'Filter by',
        selected: {displayName: 'All'}
      }
    };
  }
  getProducts(param) {
    return this.dataService.get(this.apiService.basePaths.products, param).then(
      result => result,
      error => {
        console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> OFFICE 365 SERVICE ::::', error);
    });
  }
}

export default Office365Service;
