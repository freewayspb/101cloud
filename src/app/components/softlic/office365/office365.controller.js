import _ from 'lodash';

function Office365Controller(slOffice365Service, slAddOffice365Service, $state, headerService) {
  const vm = this;
  vm.isLoading = true;
  vm.inputs = slOffice365Service.inputs;
  vm.$onInit = () => {
    vm.category = [];
    headerService.getTenantPromise().then(
      tenantId => {
        const params = {
          licenseType: 2,
          currency: 'CHF',
          tenantid: tenantId,
          ProductType: 1
        };
        slOffice365Service.getProducts({params: params})
          .then(
            response => {
              _.each(response, item => {
                _.each(item.Prices, price => {
                  price.CategoryName = vm.checkOther(price.CategoryName);
                  vm.category = vm.checkCategory(vm.category, price.CategoryName);
                  item.category = vm.checkCategory(item.category, price.CategoryName);
                  item.sortParams = item.Icons.length;
                });
              });
              response = _.sortBy(response, 'sortParams').reverse();
              vm.data = vm.dataTemplate = response;
              vm.isLoading = false;
            },
            error => console.error('can not get products in office365 controller' + error)
          );
      },
      error => console.error(error)
    );
  };

  const shouldSaveObject = (item, search) => {
    let shouldSave = false;
    _.each(item, (value, key) => {
      if(key === 'ProductName' || key === 'Description') {
        value = value.toString().toLowerCase();
        search = search.toString().toLowerCase();
        if (_.includes(value, search) &&
          (key === vm.inputs.filter.selected.value || !vm.inputs.filter.selected.value)) {
          shouldSave = true;
        }
      }
    });
    return shouldSave;
  };

  vm.checkCategory = (categoryList, categoryName) => {
    if(!categoryList) {
      categoryList = [];
    }
    if (_.findIndex(categoryList, function(o) { return o === categoryName; }) < 0) {
      categoryList.push(categoryName);
    }
    return categoryList;
  };

  vm.checkOther = category => {
    if(category !== null) {
      return category;
    } else {
      return 'Other';
    }
  };

  vm.productFilter = () => {
    if(vm.inputs.search.data !== '') {
      vm.data = _.filter(vm.dataTemplate, item => shouldSaveObject(item, vm.inputs.search.data));
    } else {
      vm.data = vm.dataTemplate;
    }
  };
  vm.callbacks = {
    onChange: vm.productFilter,
    onSelect: vm.productFilter
  };

  vm.goToDetails = ProductId => {
    slAddOffice365Service.ProductId = ProductId;
    $state.go('app.softlic.office365.addoffice365plan', {ProductId: ProductId});
  };
}

export default Office365Controller;
