class AzureService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.headers = [
      {
        name: 'Reseller',
        dataProperty: 'Reseller',
        enableSorting: 'true'
      },
      {
        name: 'Product Name',
        dataProperty: 'name',
        enableSorting: 'true'
      },
      {
        name: 'Category',
        dataProperty: 'Category',
        enableSorting: 'true'
      },
      {
        name: 'Subcategory',
        dataProperty: 'Subcategory',
        enableSorting: 'true'
      },
      {
        name: 'Region',
        dataProperty: 'Region',
        enableSorting: 'true'
      },
      {
        name: 'Price',
        dataProperty: 'price',
        enableSorting: 'true'
      },
      {
        name: 'Unit',
        dataProperty: 'Unit',
        enableSorting: 'true',
        specials: {
          type: 'price'
        }
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> AZURE SERVICE ::::', error);
    });
  }
}

export default AzureService;
