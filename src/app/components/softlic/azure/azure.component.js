import controller from './azure.controller';
import template from './azure.tpl.html';

const AzureComponent = {
  controller,
  template
};

export default AzureComponent;
