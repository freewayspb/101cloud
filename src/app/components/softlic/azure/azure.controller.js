function AzureController(slAzureService, headerService, alertsService, $scope) {
  const vm = this;

  this.inputs = {
    search: {
      placeholder: 'Search',
      data: '',
      options: {
        isSearch: true
      }
    },
    filter: {
      placeholder: 'Filter by',
      data: [{displayName: 'All'}]
    }
  };

  this.$onInit = () => {
    headerService.getTenantPromise()
      .then(reloadProducts);
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    reloadProducts(tenant.Id);
  });


  function reloadProducts(tenantId) {
    if (vm.isLoading) {
      return;
    }

    //TODO: configure params to match request
    const params = {
      tenantId,
      currency: headerService.getCurrency().Name,
      productType: 13
    };

    vm.isLoading = true;
    slAzureService.getItems('sl-azure.json')
      .then(result => {
        vm.tableOptions = {
          data: result,
          headers: slAzureService.headers,
          tableGridToggler: true,
          gridCard: 'azure',
          dropdownOptions: {
            data: [{displayName: '- filter by -'}, {displayName: 'Most popular'}],
            placeholder: 'Filter By'
          },
          excludeDropdownLabel: true,
          priceListSref: 'app.pricelist({entity: \'azure\'})'
        };
        vm.isLoading = false;
      })
      .catch(error => {
        alertsService.setAlert('Cannot fetch products');
        console.error(`:::: CAN'T FETCH PRODUCTS IN AZURE CONTROLLER OR SOMETHING HAPPENED ON CLIENT SIDE ::::`, error);
        vm.isLoading = false;
      });
  }
}

export default AzureController;
