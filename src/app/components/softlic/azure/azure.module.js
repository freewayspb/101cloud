import angular from 'angular';

import componentA from './azure.component';
import service from './azure.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic.azure', {
      url: '/azure',
      template: '<ui-view />',
      redirectTo: 'app.softlic.azure.list'
    })
    .state('app.softlic.azure.list', {
      url: '/list',
      template: '<sl-azure />'
    });
};

config.$inject = ['$stateProvider'];

const azure = angular
  .module('app.softlic.azure', [])
  .component('slAzure', componentA)
  .service('slAzureService', service)
  .config(config)
  .name;

export default azure;
