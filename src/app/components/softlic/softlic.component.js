import controller from './softlic.controller';
import template from './softlic.tpl.html';

const SoftlicComponent = {
  controller,
  template
};

export default SoftlicComponent;
