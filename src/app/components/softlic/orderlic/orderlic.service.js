class OrderlicService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.api = apiService;
  }

  getLicenses(param) {
    return this.dataService.get(this.api.basePaths.licenses + param).then(
      response => response,
      error => console.error('error with licenses source')
    );
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> ORDER LICENSE SERVICE ::::', error);
      });
  }
}

export default OrderlicService;
