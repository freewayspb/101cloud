import angular from 'angular';

import component from './orderlic.component';
import service from './orderlic.service';

import licenseDetails from './license-details/license-details.module';
import adalAngular from 'modular-adal-angular';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.softlic.orderlic', {
      url: '/licenses',
      template: '<ui-view />',
      redirectTo: 'app.softlic.orderlic.list'
    })
    .state('app.softlic.orderlic.list', {
      url: '/list',
      template: '<sl-order-lic />',
      requireADLogin: true
    });
};

const orderlic = angular
  .module('app.softlic.orderlic', [licenseDetails, adalAngular])
  .component('slOrderLic', component)
  .service('slOrderlicService', service)
  .config(config)
  .name;

export default orderlic;
