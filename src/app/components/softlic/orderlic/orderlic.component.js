import controller from './orderlic.controller';
import template from './orderlic.tpl.html';

const OrderlicComponent = {
  controller,
  template
};

export default OrderlicComponent;
