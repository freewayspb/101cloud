function OrderlicController(slOrderlicService, headerService, $scope, $filter) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    headerService.getTenantPromise().then(res => {
      slOrderlicService.getLicenses('/' + res + '/')
        .then(response => {
          slOrderlicService.licenses = response;
          vm.tableOptions = {
            data: response,
            headers: [
              {
                name: 'LICENSE',
                dataProperty: 'Name',
                enableSorting: true
              }, {
                name: 'Interval',
                dataProperty: 'Inverval',
                enableSorting: true
              }, {
                name: 'Expiration',
                dataProperty: 'ExpirationDate',
                enableSorting: true,
                specials: {
                  type: 'date'
                }
              }, {
                name: 'Available',
                dataProperty: 'AvailableUnits',
                enableSorting: true
              }, {
                name: 'Assigned',
                dataProperty: 'ConsumedUnits',
                enableSorting: true
              }, {
                name: 'Status',
                dataProperty: 'Status',
                enableSorting: true,
                specials: {
                  type: 'status',
                  params: {
                    icons: false,
                    license: true
                  }
                }
              }, {
                name: 'Order More',
                dataProperty: 'Id',
                enableSorting: false,
                specials: {
                  type: 'actions',
                  params: {
                    moreLicense: true
                  }
                }
              },
              {
                name: '',
                specials: {
                  type: 'actions',
                  params: {
                    gear: true
                  }
                }
              }
            ],
            dropdownOptions: {
              data: [{displayName: 'All'}],
              placeholder: 'Filter by'
            }
          };
          vm.isLoading = false;
        },
        error => {
          console.error(error);
          vm.isLoading = false;
        });
    }, err => {
      console.error(err);
      vm.isLoading = false;
    });
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    vm.isLoading = true;
    slOrderlicService.getLicenses('/' + tenant.Id + '/').then(
      response => {
        vm.isLoading = false;
        slOrderlicService.licenses = response;
        vm.refreshData();
      },
      err => {
        console.error(err);
        vm.isLoading = false;
      }
    );
  });

  vm.refreshData = () => {
    if (vm.tableOptions) {
      vm.tableOptions.data = $filter('filter')(slOrderlicService.licenses, vm.searchText);
    }
  };
}

export default OrderlicController;
