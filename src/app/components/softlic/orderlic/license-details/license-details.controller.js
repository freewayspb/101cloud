function OrderlicController(slOrderlicService, slLicenseDetailsService, $stateParams, headerService) {
  const vm = this;
  vm.tableOptions = {};
  this.isLoading = true;
  this.$onInit = () => {
    headerService.getTenantPromise().then(res => {
      slOrderlicService.getLicenses('/' + res + '/' + $stateParams.id + '/')
        .then(result => {
          console.log(result);
          vm.license = result[0];
        });
    });
  };

  function getLicenseDetails() {
    vm.license = _.find(slOrderlicService.licenses, element => element.id === +$stateParams.id);
    vm.license.expiration = new Date(vm.license.expiration);
    vm.license.started = new Date(vm.license.started);
    vm.license.interval = getDateInterval();
    slOrderlicService.getItems(vm.license.detailsReq + '.json')
      .then(result => {
        vm.details = result;
        setNumbers();
        vm.tableOptions.headers = slLicenseDetailsService.headers;
        vm.tableOptions.data = result;
        vm.tableOptions.isToolbarHidden = true;
        vm.isLoading = false;
      });
  }

  function getDateInterval() {
    const msInterval = vm.license.expiration - vm.license.started;
    if (msInterval >= 2419200000 && msInterval <= 2678400000) {
      return 'per month';
    }
  }

  function setNumbers() {
    _.each(vm.details, (element, index) => {
      element.number = index + 1;
    });
  }
}

export default OrderlicController;
