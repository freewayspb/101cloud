import controller from './license-details.controller';
import template from './license-details.tpl.html';

const OrderlicComponent = {
  controller,
  template
};

export default OrderlicComponent;
