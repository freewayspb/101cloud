import angular from 'angular';

import component from './license-details.component';
import service from './license-details.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic.orderlic.details', {
      url: '/details/:id',
      template: '<sl-license-details />'
    });
};

config.$inject = ['$stateProvider'];

const licdetails = angular
  .module('app.softlic.orderlic.details', [])
  .component('slLicenseDetails', component)
  .service('slLicenseDetailsService', service)
  .config(config)
  .name;

export default licdetails;
