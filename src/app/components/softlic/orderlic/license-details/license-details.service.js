class LicenseDetailsService {

  /*@ngInject*/

  constructor() {
    this.headers = [
      {
        name: 'Number',
        dataProperty: 'number',
        enableSorting: false,
        width: 100,
        specials: {
          type: 'index'
        }
      }, {
        name: 'Stop',
        dataProperty: 'stop',
        enableSorting: true,
        specials: {
          type: 'status',
          params: {
            icons: true
          }
        }
      }, {
        name: 'Assignment',
        dataProperty: 'assignment',
        enableSorting: true
      }, {
        name: '',
        width: 50,
        specials: {
          type: 'actions',
          params: {
            editLicense: true
          }
        }
      }, {
        name: 'Status',
        dataProperty: 'status',
        enableSorting: true,
        specials: {
          type: 'status',
          params: {
            icons: true
          }
        }
      }, {
        name: 'End Date',
        dataProperty: 'endDate',
        enableSorting: true,
        specials: {
          type: 'date'
        }
      }
    ];
  }
}

export default LicenseDetailsService;
