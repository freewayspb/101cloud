import controller from './spla.controller';
import template from './spla.tpl.html';

const SplaComponent = {
  controller,
  template
};

export default SplaComponent;
