class CheckoutSplaService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.headers = [
      {
        name: 'Product',
        dataProperty: 'Name'
      },
      {
        name: 'Product ID',
        dataProperty: 'Article'
      },
      {
        name: 'Seller #',
        dataProperty: 'SellerNumber'
      },
      {
        name: 'Subscription Type',
        dataProperty: 'LicenseName'
      },
      {
        name: 'Quantity',
        dataProperty: 'qty'
      },
      {
        name: 'Price',
        dataProperty: 'Price'
      }
    ];
  }

}

export default CheckoutSplaService;
