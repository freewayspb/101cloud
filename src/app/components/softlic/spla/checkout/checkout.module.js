import angular from 'angular';

import component from './checkout.component';
import service from './checkout.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic.spla.checkout', {
      url: '/checkout',
      template: '<sl-checkout-spla/>'
    });
};

config.$inject = ['$stateProvider'];

const checkoutSpla = angular
  .module('app.softlic.spla.checkout', [])
  .component('slCheckoutSpla', component)
  .service('slCheckoutSplaService', service)
  .config(config)
  .name;

export default checkoutSpla;
