function CheckoutSplaController(slCheckoutSplaService, slSplaService, $state, alertsService) {
  this.$onInit = () => {
    if (!slSplaService.splas.length) {
      $state.go('app.softlic.spla');
    }

    this.splaData = {
      headers: slCheckoutSplaService.headers,
      data: slSplaService.splas
    };
  };

  this.checkoutCallbacks = {
    backCb: () => {
      $state.go('app.softlic.spla');
    },
    deployCb: () => {
      alertsService.setAlert({msg: 'Not Implemented', type: 'danger'});
    }
  };
}

export default CheckoutSplaController;
