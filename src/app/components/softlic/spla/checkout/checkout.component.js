import controller from './checkout.controller';
import template from './checkout.tpl.html';

const CheckoutSplaComponent = {
  controller,
  template
};

export default CheckoutSplaComponent;
