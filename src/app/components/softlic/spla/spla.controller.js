function SplaController(slSplaService, headerService, apiService, alertsService, $scope, $state, notificationService) {
  const vm = this;

  this.$onInit = () => {
    headerService.getTenantPromise()
      .then(reloadProducts);

    slSplaService.setInformationPanel();
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    reloadProducts(tenant.Id);
  });

  function reloadProducts(tenantId) {
    if (vm.isLoading) {
      return;
    }

    const params = {
      tenantId,
      currency: headerService.getCurrency().Name,
      productType: 13
    };

    vm.isLoading = true;
    slSplaService.get(apiService.spla, {params})
      .then(result => {
        const tableData = _.map(result, item => {
          item.qty = 0;
          return item;
        });
        vm.tableOptions = {
          enableRowSelection: true,
          data: tableData,
          headers: slSplaService.headers,
          tableGridToggler: true,
          gridCard: 'spla',
          dropdownOptions: {
            data: [ {displayName: '- filter by -'}, {displayName: 'Most popular'} ],
            placeholder: 'Filter By'
          },
          excludeDropdownLabel: true,
          priceListSref: 'app.pricelist({entity: \'spla\'})'
        };
        vm.isLoading = false;
      })
      .catch(error => {
        alertsService.setAlert('Cannot fetch products');
        console.error(':::: CANNOT FETCH PRODUCTS IN SPLA CONTROLLER OR SOMETHING HAPPENED ON CLIENT SIDE ::::', error);
        vm.isLoading = false;
      });
  }
}

export default SplaController;
