class SplaService {

  /*@ngInject*/

  constructor(dataService, notificationService, informationPanelService, $state) {
    this.splas = [];
    this.$state = $state;
    this.notificationService = notificationService;
    let firstClick = true;
    const onQtyChange = {
      qty: {
        onChange: (item, qty) => {
          const SplaInListIndex = _.findIndex(this.splas, spla => spla.RateCardId === item.RateCardId);

          if (!(SplaInListIndex + 1)) {
            this.splas.push(item);
          }
          if (!qty) {
            this.splas.splice(SplaInListIndex, 1);
          }
          if (firstClick) {
            informationPanelService.buttons = [
              {title: 'Add to Cart'},
              {title: 'Checkout', clickCb: this.checkout.bind(this)}
            ];
            firstClick = false;
          }
          this.setInformationPanel();
        }
      }
    };

    this.dataService = dataService;
    this.headers = [
      {
        name: 'Infrastructure Provider',
        dataProperty: 'TenantName',
        enableSorting: true
      },
      {
        name: 'Product Name',
        dataProperty: 'Name',
        enableSorting: true
      },
      {
        name: 'Subscription Type',
        dataProperty: 'LicenseName',
        enableSorting: true
      },
      {
        name: 'Product ID',
        dataProperty: 'Article',
        enableSorting: true
      },
      {
        name: 'Seller #',
        dataProperty: 'SellerNumber',
        enableSorting: true
      },
      {
        name: 'Price',
        dataProperty: 'Price',
        enableSorting: true,
        specials: {
          type: 'price'
        }
      },
      {
        name: '',
        dataProperty: 'Actions',
        specials: {
          type: 'actions',
          params: {
            qty: true
          },
          callbacks: onQtyChange
        }
      }
    ];
  }

  checkout(event, model) {
    _.each(this.splas, (spla, index) => {
      spla.qty = model.priceList[index].inputs.qty.data;
    });
    this.notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: false});
    this.$state.go('app.softlic.spla.checkout');
  }

  setInformationPanel() {
    this.notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: this.splas.length});
    this.notificationService.broadcast('onInformationChange', {
      type: 'price',
      priceList: _.map(this.splas, element => {
        return {
          name: element.Name,
          price: element.Price,
          currency: element.Currency,
          inputs: {
            qty: {
              data: element.qty,
              placeholder: 1,
              options: {
                classNameInput: 'information-panel-qty-no-arrows',
                type: 'number',
                min: 1
              }
            }
          }
        };
      }),
      delivery: '2 Days'
    });
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> Spla SERVICE ::::', error);
    });
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN SOFTLIC -> Spla GET SERVICE ::::', error);
    });
  }
}

export default SplaService;
