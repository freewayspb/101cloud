import angular from 'angular';

import component from './spla.component';
import service from './spla.service';

import checkout from './checkout/checkout.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic.spla', {
      url: '/spla',
      redirectTo: 'app.softlic.spla.list'
    })
    .state('app.softlic.spla.list', {
      url: '/list',
      template: '<sl-spla />'
    });
};

config.$inject = ['$stateProvider'];

const spla = angular
  .module('app.softlic.spla', [checkout])
  .component('slSpla', component)
  .service('slSplaService', service)
  .config(config)
  .name;

export default spla;
