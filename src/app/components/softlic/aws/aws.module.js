import angular from 'angular';

import component from './aws.component';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic.aws', {
      url: '/aws',
      template: '<sl-aws />'
    });
};

config.$inject = ['$stateProvider'];

const aws = angular
  .module('app.softlic.aws', [])
  .component('slAws', component)
  .config(config)
  .name;

export default aws;
