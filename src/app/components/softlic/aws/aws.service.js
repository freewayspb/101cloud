class AwsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN SOFTWARE&LICENSE -> AWS SERVICE ::::', error);
    });
  }
}

export default AwsService;
