import controller from './aws.controller';
import template from './aws.tpl.html';

const AwsComponent = {
  controller,
  template
};

export default AwsComponent;
