import angular from 'angular';

import component from './softlic.component';

import orderlic from './orderlic/orderlic.module';
import office365 from './office365/office365.module';
import azure from './azure/azure.module';
import spla from './spla/spla.module';
import aws from './aws/aws.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.softlic', {
      url: '/softlic',
      redirectTo: 'app.softlic.orderlic',
      template: '<softlic></softlic>'
    });
};

config.$inject = ['$stateProvider'];

const softlic = angular
  .module('app.softlic', [orderlic, office365, azure, spla, aws])
  .component('softlic', component)
  .config(config)
  .name;

export default softlic;
