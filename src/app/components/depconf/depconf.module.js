import angular from 'angular';

import DepconfComponent from './depconf.component';

import clouds from './clouds/clouds.module.js';
import datacenters from './datacenters/datacenters.module.js';
import locations from './locations/locations.module';
import servers from './servers/servers.module.js';
import services from './services/services.module.js';
import subscriptions from './subscriptions/subscriptions.module.js';


const config = $stateProvider => {
  $stateProvider
    .state('app.depconf', {
      url: '/dc',
      template: '<depconf />',
      redirectTo: 'app.depconf.clouds'
    });
};

config.$inject = ['$stateProvider'];

const depconf = angular
  .module('app.depconf', [
    clouds,
    datacenters,
    locations,
    servers,
    services,
    subscriptions])
  .component('depconf', DepconfComponent)
  .config(config)
  .name;

export default depconf;
