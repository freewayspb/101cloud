import controller from './servers.controller';
import template from './servers.tpl.html';

const ServersComponent = {
  controller,
  template
};

export default ServersComponent;
