import angular from 'angular';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.servers.addserver', {
      url: '/addserver',
      template: '<add-server />'
    });
};

config.$inject = ['$stateProvider'];

const addserver = angular
  .module('app.depconf.servers.addserver', [])
  .config(config)
  .name;

export default addserver;
