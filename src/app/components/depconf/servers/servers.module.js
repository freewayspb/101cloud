import angular from 'angular';

import component from './servers.component';
import service from './servers.service';

import addserver from './addserver/addserver.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.servers', {
      url: '/servers',
      template: '<ui-view />',
      redirectTo: 'app.depconf.servers.list'
    })
    .state('app.depconf.servers.list', {
      url: '/list',
      template: '<dc-servers />'
    });
};

config.$inject = ['$stateProvider'];

const servers = angular
  .module('app.depconf.servers', [addserver])
  .component('dcServers', component)
  .service('depconfServersService', service)
  .config(config)
  .name;

export default servers;
