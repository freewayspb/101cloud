class DepconfServersService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getListServers(tenantId) {
    return this.dataService.get(this.apiService.basePaths.tenants + '/' + tenantId + '/servers')
      .then(
        response => response,
        error => console.error(error)
      );
  }
}

export default DepconfServersService;
