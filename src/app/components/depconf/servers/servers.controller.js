function ServersController(depconfServersService, headerService, $scope, $filter) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    headerService.getTenantPromise().then(
      id => {
        depconfServersService.getListServers(id).then(
          response => {
            vm.serversList = response;
            vm.tableOptions = {
              data: vm.serversList,
              headers: [
                {
                  name: 'Name',
                  dataProperty: 'Name',
                  enableSorting: true
                },{
                  name: 'Description',
                  dataProperty: 'Description',
                  enableSorting: true
                }, {
                  name: 'Status',
                  dataProperty: 'Status',
                  enableSorting: false,
                  specials: {
                    type: 'power'
                  },
                  isCentralized: true
                }, {
                  name: 'Cloud',
                  dataProperty: 'Cloud',
                  enableSorting: true
                }, {
                  name: 'Datacenter',
                  dataProperty: 'datacenter',
                  enableSorting: true
                }, {
                  name: 'Infrastructure provider',
                  dataProperty: 'Provider',
                  enableSorting: true,
                  isCentralized: true
                }, {
                  name: '',
                  specials: {
                    type: 'actions',
                    params: { context: false, edit: true, delete: true}
                  }
                }
              ],
              sref: 'app.depconf.servers.addserver'
            };
            vm.isLoading = false;
          },
          error => {
            console.error(error);
            vm.isLoading = false;
          }
        );

      },
      error => console.error(error)
    );
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    vm.isLoading = true;
    depconfServersService.getListServers(tenant.Id).then(
      response => {
        vm.isLoading = false;
        vm.serversList = response;
        vm.refreshData();
      }
    );
  });

  vm.refreshData = () => {
    if (vm.tableOptions) {
      vm.tableOptions.data = $filter('filter')(vm.serversList, vm.searchText);
    }
  };
}

export default ServersController;
