import angular from 'angular';

import CloudsComponent from './clouds.component';
import CompareCloudsComponent from './comparecloud/comparecloud.component';

import CloudsTypeComponent from '../../../common/cards/cloud-type/cloud-type.component';

import addcloud from './addcloud/addcloud.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.clouds', {
      url: '/clouds',
      template: '<ui-view />',
      redirectTo: 'app.depconf.clouds.list'
    })
    .state('app.depconf.clouds.list', {
      url: '/list',
      template: '<clouds />'
    })
    .state('app.depconf.clouds.compare', {
      url: '/compare',
      template: '<compareclouds />'
    });
};

config.$inject = ['$stateProvider'];

const clouds = angular
  .module('app.depconf.clouds', ['ui.grid', addcloud])
  .component('clouds', CloudsComponent)
  .component('compareclouds', CompareCloudsComponent)
  .component('cloudtype', CloudsTypeComponent)
  .config(config)
  .name;

export default clouds;
