function CloudsController(cloudsService, $scope, headerService) {
  const vm = this;

  this.$onInit = () => {
    headerService.getTenantPromise()
      .then(reloadClouds);
  };

  vm.$onInit = () => {
    headerService.getTenantPromise().then(
      tenantId => reloadClouds(tenantId)
    );
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    reloadClouds(tenant.Id);
  });

  function reloadClouds(tenantId) {

    if (vm.isLoading) {
      return;
    }
    vm.isLoading = true;
    cloudsService.getClouds(tenantId).then(response => {
        vm.cloudsList = response;
        vm.tableOptions = {
          data: vm.cloudsList,
          headers: cloudsService.headers,
          sref: 'app.depconf.clouds.addcloud'
        };
        vm.isLoading = false;
      },
      error => {
        console.error(error);
        vm.isLoading = false;
      });
  }
}

export default CloudsController;
