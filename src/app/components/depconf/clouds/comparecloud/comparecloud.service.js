class CompareCloudService {

  /*@ngInject*/

  constructor($http, dataService) {
    this.$http = $http;
    this.dataService = dataService;
  }

  get(path) {
    return this.dataService.get(path)
      .then(result => result, error => {
        console.error(':::: ERROR IN ADDCLOUD SERVICE COMPARECLOUD ::::', error);
      });
  }
}

export default CompareCloudService;
