import controller from './comparecloud.controller';
import template from './comparecloud.tpl.html';

const CompareCloudComponent = {
  controller,
  template
};

export default CompareCloudComponent;
