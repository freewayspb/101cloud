import _ from 'lodash';

function CompareCloudController(compareCloudService, $location, apiService, addCloudService, $scope, alertsService) {

  this.$onInit = () => {
    this.compareList = addCloudService.compareList;
  };

  this.getIndexBoldStart = cloudTypeIndex => {
    if (cloudTypeIndex === 0) {
      return -1
    } else {
      return this.compareList[cloudTypeIndex - 1].BenefitsList.length;
    }
  };

  $scope.$on('onCurrencyChange', () => {
    alertsService.setAlert({msg: 'Changes will not take effect until you won\'t go this page again', type: 'warning'});
  });
}

export default CompareCloudController;
