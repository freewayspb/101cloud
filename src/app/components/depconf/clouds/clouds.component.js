import controller from './clouds.controller';
import template from './clouds.tpl.html';

const CloudsComponent = {
  controller,
  template
};

export default CloudsComponent;
