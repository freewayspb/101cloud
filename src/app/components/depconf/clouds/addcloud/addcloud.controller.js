import inputs from './addcloud.inputs.model';
import compileFullCloud from './addcloud.assigner';

function AddCloudController($state, addCloudService, alertsService, notificationService, datacenterService,
                            informationPanelService) {
  const vm = this;
  vm.isPostProcessing = false;

  vm.inputs = inputs;
  vm.state = 'steps';
  vm.progressbar = addCloudService.progressbar = {
    steps: addCloudService.steps
  };
  vm.lowerProgressBarType = 'pager';
  informationPanelService.buttons = [
    {title: 'Add to Cart'},
    {title: 'Checkout', isDisabled: true, clickCb: onConfirmClick }
  ];
  vm.$onInit = () => {
    vm.inputs.subnets = [];
  };
  vm.$state = $state;

  vm.callbacks = {
    onConfirmClick,
    onStepClick,
    onSaveClick
  };
  function onConfirmClick() {
    if (vm.state === 'steps') {
      vm.state = 'checkout';
      vm.checkoutData = {Cloud: inputs.Cloud, Subscriptions: inputs._Subscriptions};
      vm.lowerProgressBarType = '';
      notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: false});
    }
  }
  function onStepClick(step) {
    vm.state = 'steps';
    vm.lowerProgressBarType = 'pager';
    vm.datacenter = datacenterService.datacenter;
    notificationService.broadcast('onInformationToggle',
      {setOpen: false, setShown: (vm.state === 'steps') && (step.name !== 'cloud-type')});
  }
  function onSaveClick() {
    try {
      localStorage.lastSavedCloud = JSON.stringify(inputs);
      alertsService.setAlert({msg: 'Cloud successfully saved to local storage', type: 'success'});
    } catch (e) {
      alertsService.setAlert({msg: 'Cannot save cloud', type: 'danger'});
      console.error(':::: SOMETHING HAPPENED WHILE SAVING TO LOCAL STORAGE AT ADDCLOUD CONTROLLER ::::', e);
    }
  }

  vm.validate = params => {
    const nextStepIndex = _.findIndex(addCloudService.steps,
        step => step.name === addCloudService.progressbar.currentStep.name) + 1;
    vm.progressbar.currentStep.isValid = params.isValid;
    if (addCloudService.steps[nextStepIndex] && vm.progressbar.currentStep.isValid === true) {
      addCloudService.steps[nextStepIndex].isDisabled =
        !(params.isValid && nextStepIndex <= addCloudService.steps.length);
    }
    informationPanelService.buttons[1].isDisabled = Boolean(_.findIndex(addCloudService.steps, step =>
        step.isDisabled || step.isValid === false) + 1) || !this.progressbar.currentStep.isLast;
    notificationService.broadcast('onInformationToggle',
      {setOpen: !informationPanelService.buttons[1].isDisabled, setShown: true});
  };

  vm.checkoutCallbacks = {
    deployCb,
    backCb
  };

  function deployCb() {
    vm.isPostProcessing = true;
    try {
      const FullCloud = compileFullCloud();
      vm.showFullCloud = localStorage.showFullCloud;
      vm.FullCloud = localStorage.showFullCloud && FullCloud;
      addCloudService.post({data: FullCloud})
        .then(result => {
          vm.isPostProcessing = false;
          alertsService.setAlert({msg: 'Cloud will deploy in a few minutes', type: 'success'});
          return result;
        }, error => {
          console.error(':::: ERROR OCCURRED IN ADD CLOUD CONTROLLER DEPLOY CLOUD ::::', error);
          alertsService.setAlert({msg: `Cannot deploy cloud. ${error.data.Message}`, type: 'danger'});
          vm.isPostProcessing = false;
        });
    } catch (e) {
      alertsService.setAlert({msg: `Sorry, something happened on client side`, type: 'danger'});
      console.error(':::: CLIENT ERROR WHILE DEPLOYING FULL CLOUD ::::', e);
      vm.isPostProcessing = false;
    }
  }
  function backCb() {
    vm.state = 'steps';
    vm.lowerProgressBarType = 'pager';
  }
}

export default AddCloudController;
