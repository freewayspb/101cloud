import angular from 'angular';

// import screens

import component from './addcloud.component';
import service from './addcloud.service';

import CloudSettingsComponent from './steps/cloud-settings/iz-add-cloud-cloud-settings.component';
import AdditionalServicesComponent from './steps/additional-services/iz-add-cloud-additional-services.component';
import CloudTypeComponent from './steps/cloud-type/iz-add-cloud-cloud-type.component';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.clouds.addcloud', {
      url: '/addcloud',
      template: '<addcloud />'
    });
};

config.$inject = ['$stateProvider'];

const addcloud = angular
  .module('app.depconf.clouds.addcloud', ['ui.grid'])
  .component('addcloud', component)

  .component('izAddCloudAdditionalServices', AdditionalServicesComponent)
  .component('izAddCloudCloudSettings', CloudSettingsComponent)
  .component('izAddCloudCloudType', CloudTypeComponent)

  .service('addcloudService', service)

  .config(config)
  .name;

export default addcloud;
