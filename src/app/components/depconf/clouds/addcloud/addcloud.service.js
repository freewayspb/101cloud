class AddCloudService {

  /*@ngInject*/

  constructor($http, dataService, apiService) {
    this.$http = $http;
    this.dataService = dataService;
    this.apiService = apiService;
    this.steps = [
      {
        title: 'cloud type',
        name: 'cloud-type',
        isDisabled: false
      },
      {
        title: 'customer',
        name: 'customer',
        isDisabled: true
      },
      {
        title: 'datacenter',
        name: 'datacenter',
        isDisabled: true
      },
      {
        title: 'cloud settings',
        name: 'cloud-settings',
        isDisabled: true
      },
      {
        title: 'office',
        name: 'location',
        isDisabled: true
      },
      {
        title: 'additional services',
        name: 'additional-services',
        isDisabled: true
      },
      {
        title: 'subscription',
        name: 'subscription',
        isDisabled: true
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param)
      .then(result => result, error => {
        console.error(':::: ERROR IN ADDCLOUD SERVICE GETITEMS ::::', error);
        throw error;
      });
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params)
      .then(result => result, error => {
        console.error(':::: ERROR IN ADDCLOUD SERVICE GET ::::', error);
        throw error;
      });
  }

  post(param) {
    return this.dataService.post(this.apiService.FullCloud, param.data)
      .then(result => {
          console.warn(result);
          return result;
        },
        error => {
          console.error(':::: ERROR IN ADDCLOUD SERVICE POST ::::', error, param);
          throw error;
        });
  }
}

export default AddCloudService;
