export default {
  cloudtype: {
    env: {
      data: [
        {displayName: 'Production', id: 1},
        {displayName: 'Integration', id: 2},
        {displayName: 'Development', id: 3}
      ],
      title: 'Classification',
      selected: {displayName: 'Production', id: 1}
    },
    period: {
      data: [
        {displayName: '1 Year', period: 7, id: 1, suffix: 'per year'},
        {displayName: '3 Months', period: 2, id: 2, suffix: 'per quarter'},
        {displayName: '1 Month', period: 4, id: 3, suffix: 'per month'},
        {displayName: '1 Day', period: 5, id: 4, suffix: 'per day'}
      ],
      title: 'Subscription Type',
      selected: {displayName: '1 Month', period: 4, id: 3, suffix: 'per month'}
    }
  },
  cloudSettings: {
    cloudName: {
      title: 'Cloud Name',
      placeholder: 'Enter Cloud Name',
      data: '',
      options: {
        required: true
      }
    },
    generalAdministratorLogin: {
      data: '',
      title: 'Name',
      options: {
        required: true
      }
    },
    generalAdministratorPassword: {
      data: '',
      title: 'Password',
      options: {
        required: true,
        pattern: `(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}`,
        isPassword: true,
        type: 'password',
        popover: {
          msg: 'Password must contain at least one uppercase, one lowercase standard latin letter and a digit',
          trigger: 'mouseenter'
        }
      }
    },
    localAdministratorLogin: {
      data: '',
      title: 'Name',
      options: {
        required: true
      }
    },
    localAdministratorPassword: {
      data: '',
      title: 'Password',
      options: {
        required: true,
        pattern: `(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}`,
        isPassword: true,
        type: 'password',
        popover: {
          msg: 'Password must contain at least one uppercase, one lowercase standard latin letter and a digit',
          trigger: 'mouseenter'
        }
      }
    },
    azureKeyVault: {
      title: 'Azure Key Vault Name',
      placeholder: 'Enter Azure Key Vault',
      data: '',
      options: {
        maxLength: 15
      }
    },
    internalDomainName: {
      title: 'Internal Domain Name',
      placeholder: 'Enter Internal Domain Name',
      data: '',
      options: {
        required: true
      }
    },
    externalDomainName: {
      title: 'External Domain Name',
      placeholder: 'Enter External Domain Name',
      data: '',
      options: {
        required: true
      }
    },
    newSubnetRangeEnd: {
      title: 'New Subnet Range End',
      placeholder: 'Enter New Subnet Range End',
      data: ''
    },
    newSubnetRangeStart: {
      title: 'New Subnet Range Start',
      placeholder: 'Enter New Subnet Range Start',
      data: ''
    },
    subnetRangeEnd: {
      title: 'Subnet Range End',
      placeholder: 'Enter Subnet Range End',
      data: ''
    },
    subnetRangeStart: {
      title: 'Subnet Range Start',
      placeholder: 'Enter Subnet Range Start',
      data: ''
    },
    siteCode: {
      title: 'Site Code',
      placeholder: 'Enter Site Code',
      data: ''
    },
    operatingSystemVersion: {
      title: 'Operating System Version',
      data: [
        {displayName: 'Windows Server 2016 Standard Edition', id: 3},
        {displayName: 'Windows Server 2016 Datacenter Edition', id: 3},
        {displayName: 'Windows Server 2012 R2 Standard Edition', id: 3},
        {displayName: 'Windows Server 2012 R2 Datacenter Edition', id: 3}
      ],
      selected: {displayName: 'Windows Server 2016 Standard Edition', id: 3}
    },
    siteDescription: {
      title: 'Site Description',
      placeholder: 'Enter Site Description',
      data: ''
    },
    schedule: {
      title: 'From - To',
      data: [],
      display: ''
    },
    availability: {
      title: 'Patching',
      placeholder: 'Select timeslot',
      data: [
        {
          displayName: 'Every Day'
        }, {
          displayName: 'Every Week'
        }, {
          displayName: 'Every Month'
        }, {
          displayName: 'Never'
        }
      ],
      selected: {}
    },
    maintenanceWindow: {
      title: 'Maintenance Window',
      data: [
        { displayName: 'Window1' },
        { displayName: 'Window2' }
      ]
    },
    maintenanceType: {
      title: 'Maintenance Type',
      data: [
        { displayName: 'Save State' },
        { displayName: 'Shut Down' },
        { displayName: 'Pause' }
      ]
    },
    backupServer: {
      title: 'Backup Server',
      data: [
        {
          displayName: 'Yes'
        },
        {
          displayName: 'No'
        }
      ]
    },
    backupInterval: {
      title: 'Backup Interval',
      data: [
        { displayName: 'Hourly' },
        { displayName: 'Daily' },
        { displayName: 'Weekly' },
        { displayName: 'Monthly' }
      ]
    },
    dataRetentionTime: {
      title: 'Data Retention Time',
      data: [
        { displayName: '1 Week' },
        { displayName: '2 Weeks' },
        { displayName: '1 Month' },
        { displayName: '1 Year' }
      ]
    },
    monitoringService: {
      title: 'Monitoring Server',
      data: [
        { displayName: 'Yes' },
        { displayName: 'No' }
      ]
    },
    serviceManagementContracts: {
      title: 'Service Management Contracts',
      data: [
        { displayName: 'Support' },
        { displayName: 'Consolidated Billing' },
        { displayName: 'Operations' }
      ]
    }
  }
};
