import inputs from './addcloud.inputs.model';

export default function() {
  const Cloud = {
    Classification: {
      Id: inputs.cloudtype.env.selected.id || null,
      Name: inputs.cloudtype.env.selected.displayName || null
    },
    CloudType: {
      //TODO: Remove all properties except OfferId when back is ready
      Type: inputs.cloudtype.cloudType || null,
      Id: inputs.cloudtype.id,
      Description: inputs.cloudtype.description || null,
      OfferId: inputs.Cloud.OfferId
    },
    Description: inputs.cloudtype.description || null,
    SiteCode: inputs.cloudSettings.siteCode.data || '',
    InternalDomain: inputs.cloudSettings.internalDomainName.data || null,
    ExternalDomain: inputs.cloudSettings.externalDomainName.data || null,
    Status: 1,
    Power: true,
    Name: inputs.cloudSettings.cloudName.data || null,
    Id: 1,
    StatusMessage: 1
  };
  const Tenant = inputs.Tenant;
  const Fabric = inputs.DataCenter;
  const Offices = inputs.Offices;
  const Subscriptions = inputs.Subscriptions;
  return {
    Cloud,
    Tenant,
    Fabric,
    Offices,
    Subscriptions,
    SelectedOsTemplate: inputs.DataCenter.OsTemplates[0].Id,
    SelectedVirtualDataCenter: inputs._virtualDataCenter.OfferId || null,
    SelectedHardwareTemplate: 10
  };
}
