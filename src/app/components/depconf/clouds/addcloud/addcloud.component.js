import controller from './addcloud.controller';
import template from './addcloud.tpl.html';

const AddCloudComponent = {
  controller,
  template
};

export default AddCloudComponent;
