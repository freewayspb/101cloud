import inputs from '../../addcloud.inputs.model';

function IzAddCloudAdditionalServicesController(addCloudService, currencyService) {
  const vm = this;
  this.$onInit = () => {
    this.inputs = inputs;
    this.operations = [];

    addCloudService.getItems('operations.json').then(result => {
      this.operations = _.map(result, operationGroup => {
        operationGroup = _.map(operationGroup, operation => {
          operation.qty = 0;
          return operation;
        });
        validate();
        return operationGroup;
      }, err => {
        console.error(err);
      });
    });

    /**
     * @name getIntPrice
     * @desc Gets formatted price integer value as on design
     * @returns {string}
     * @param {number} price actual price
     */
    this.getIntPrice = price => currencyService.getPriceString(price)
      .substr(0, currencyService.getPriceString(price).length - 3);

    this.incrementOperationCounter = operation => {
      operation.qty++;
    };
    this.decrementOperationCounter = operation => {
      if (operation.qty > 0) {
        operation.qty--;
      }
    };

    this.getSubTotalAdditional = () => _.reduce(this.operations[0], (sum, val) => sum + val.price * val.qty, 0);
    this.getSubTotalManaged = () => _.reduce(this.operations[1], (sum, val) => sum + val.price * val.qty, 0);

    this.setMaintenanceOption = (value, key) => {
      this.maintenance[key].active = value.displayName;
    };
  };

  function validate() {
    vm.validate({isValid: true});
  }
}

export default IzAddCloudAdditionalServicesController;
