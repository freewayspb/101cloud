import controller from './iz-add-cloud-addittional-services.controller';
import template from './iz-add-cloud-addittional-services.tpl.html';

const IzAddCloudAdditionalServicesComponent = {
  bindings: {
    validate: '&'
  },
  controller,
  template
};

export default IzAddCloudAdditionalServicesComponent;
