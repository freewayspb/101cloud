import inputs from '../../addcloud.inputs.model';

function IzAddCloudCloudTypeController(addCloudService, $state, alertsService, notificationService,
                                       $scope, apiService, headerService) {

  const vm = this;

  function updateCloudTypes() {
    vm.isLoading = true;
    vm.cloudPlans = [];
    headerService.getTenantPromise()
      .then(tenantid => {
        const currency = JSON.parse(localStorage.Currency).Name.toUpperCase();
        const periodtype = inputs.cloudtype.period.selected.period;
        const params = {currency, periodtype, tenantid, producttype: 4, typeofbusiness: 2};

        addCloudService.get(apiService.basePaths.products, {params: params})
          .then(result => {
            result = _.sortBy(result, type => type.Price);
            _.each(result, type => {
              type.suffix = vm.inputs.cloudtype.period.selected.suffix;
            });
            vm.cloudPlans = addCloudService.cloudPlans = result;
            vm.isLoading = false;
            return result;
          });

        addCloudService.get(apiService.basePaths.products, {params: params})
          .then(result => {
            result = _.sortBy(result, type => type.Price);
            _.each(result, type => {
              type.suffix = vm.inputs.cloudtype.period.selected.suffix;
            });
            vm.cloudPlans = addCloudService.cloudPlans = result;
            vm.isLoading = false;
            return result;
          });

      });
  }

  this.$onInit = () => {
    this.inputs = inputs;
    updateCloudTypes();
  };

  this.getIndexBoldStart = cloudTypeIndex => {
    if (cloudTypeIndex === 0) {
      return -1;
    } else {
      return this.cloudPlans[cloudTypeIndex - 1].BenefitsList.length;
    }
  };

  this.setCloudType = (cloud, e) => {
    if (!e.target.classList.contains('is-suggestion') && !e.target.classList.contains('cloud-type__read-more') &&
      inputs.cloudtype.env.selected.displayName) {
      inputs.cloudtype.cloudType = cloud.Type;
      inputs.cloudtype.id = cloud.Id;
      inputs.cloudtype.description = cloud.Description;
      inputs.Cloud = cloud;
      inputs.Cloud.period = inputs.cloudtype.period.selected;
      inputs.Cloud.Name = '101Cloud Subscription';
      addCloudService.progressbar.steps[1].isDisabled = false;
      addCloudService.progressbar.currentStep = addCloudService.progressbar.steps[1];
      notificationService.broadcast('onInformationToggle', {setOpen: false, setShown: true});
      notificationService.broadcast('onInformationChange', {
        type: 'price',
        delivery: '2 Days',
        priceList: [{
          name: cloud.Type,
          price: cloud.Price,
          inputs: {
            qty: {
              data: 1,
              placeholder: 1,
              options: {
                classNameInput: 'information-panel-qty-no-arrows',
                type: 'number',
                min: 1
              }
            }
          }
        }],
        entity: {data: cloud}
      });
    } else if (!e.target.classList.contains('is-suggestion') && !inputs.cloudtype.env.selected.displayName) {
      alertsService.setAlert({msg: 'Please select Classification first', type: 'warning'});
    }
  };

  const getCompareList = () => _.filter(this.cloudPlans || [], elem => elem.suggestion === 1);

  this.compare = () => {
    addCloudService.compareList = getCompareList();
    $state.go('app.depconf.clouds.compare');
  };

  this.isCompareDisabled = () => getCompareList().length < 2;

  $scope.$on('onCurrencyChange', updateCloudTypes);
  this.periodCallbacks = {
    onChange: updateCloudTypes
  };
}

export default IzAddCloudCloudTypeController;
