import controller from './iz-add-cloud-cloud-type.controller';
import template from './iz-add-cloud-cloud-type.tpl.html';

const IzAddCloudCloudTypeComponent = {
  controller,
  template
};

export default IzAddCloudCloudTypeComponent;
