import controller from './iz-add-cloud-cloud-settings.controller';
import template from './iz-add-cloud-cloud-settings.tpl.html';

const IzAddCloudCloudSettingsComponent = {
  bindings: {
    validate: '&'
  },
  controller,
  template
};

export default IzAddCloudCloudSettingsComponent;
