import inputs from '../../addcloud.inputs.model';

function IzAddCloudCloudSettingsController(addCloudService, currencyService, notificationService, $scope, apiService) {
  const vm = this;
  this.$onInit = () => {
    this.inputs = inputs;

    addCloudService.get(apiService.basePaths.hardwareTemplates + inputs.DataCenter.Tenant.Id).then(result => {
      if (result && result.length === 0) {
        addCloudService.getItems('maintenance.json').then(fallbackResult => {
          this.hardwareTemplates = fallbackResult.hardwaresInformation;

          this.maintenance = fallbackResult;
          this.inputs.hardwaresInformation = 0;
          this.inputs.hardwareTemplates = this.maintenance.hardwaresInformation;
          validate();
        }, err => {
          console.error(err);
        });
      }
      this.hardwareTemplates = result;
      this.inputs.hardwaresInformation = 0;
      this.inputs.hardwareTemplates = this.hardwareTemplates.hardwaresInformation;
      validate();
    }, err => {
      addCloudService.getItems('maintenance.json').then(fallbackResult => {
        this.hardwareTemplates = fallbackResult.hardwaresInformation;

        this.maintenance = fallbackResult;
        this.inputs.hardwaresInformation = 0;
        this.inputs.hardwareTemplates = this.maintenance.hardwaresInformation;
        validate();
      }, err => {
        console.error(err);
      });
      console.error(err);
    });
  };

  this.getPrice = price => currencyService.getPriceString(price);

  this.onAddingItemComplete = (uid) => {
    addCloudService.virtualDataCenterInfoId = uid;
  };

  this.genericCallbacks = {
    onChange: validate
  };

  function validate() {
    vm.validate({isValid: $scope.cloudSettingsForm.$valid});
  }

  this.$onDestroy = () => {
    notificationService.broadcast('onInformationPriceListItemChange', {
      id: addCloudService.virtualDataCenterInfoId,
      name: this.hardwareTemplates[this.inputs.hardwaresInformation].Name,
      price: this.hardwareTemplates[this.inputs.hardwaresInformation].Price,
      inputs: {
        qty: {
          data: 1,
          placeholder: 1,
          options: {
            classNameInput: 'information-panel-qty-no-arrows',
            type: 'number',
            min: 1
          }
        }
      },
      cb: this.onAddingItemComplete
    });
  };
}

export default IzAddCloudCloudSettingsController;
