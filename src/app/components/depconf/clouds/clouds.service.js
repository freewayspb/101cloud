class CloudService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.headers = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true
      }, {
        name: 'Description',
        dataProperty: 'Description',
        enableSorting: true
      }, {
        name: 'Business Owner',
        dataProperty: 'BusinessOwner',
        enableSorting: true
      }, {
        name: 'Tech Owner',
        dataProperty: 'TechnicalOwner',
        enableSorting: true
      }, {
        name: 'Classification',
        dataProperty: 'Classification',
        enableSorting: true
      }, {
        name: 'Status',
        dataProperty: 'Status',
        enableSorting: false,
        specials: {
          type: 'status',
          params: {
            icons: true
          }
        },
        isCentralized: true
      }, {
        name: 'Power',
        dataProperty: 'Power',
        enableSorting: false,
        specials: {
          type: 'power'
        },
        isCentralized: true
      }, {
        name: '',
        specials: {
          type: 'actions',
          params: {
            context: true,
            edit: true,
            delete: true
          }
        }
      }
    ];
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN CLOUD SERVICE GETITEMS ::::', error);
    });
  }

  getClouds(tenantId) {
    return this.dataService.get(this.apiService.basePaths.tenants + '/' + tenantId + '/clouds')
      .then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN CLOUD SERVICE GET ::::', error);
      return error;
    });
  }
}

export default CloudService;
