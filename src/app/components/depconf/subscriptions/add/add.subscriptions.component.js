import template from './add.subscriptions.tpl.html';
import controller from './add.subscription.controller';

const AddSubscriptionsComponent = {
  bindings: {
    isStep: '='
  },
  template,
  controller
};

export default AddSubscriptionsComponent;
