const AddSubscriptionInputs = {
  country: {
    data: [],
    title: 'Country'
  },
  region: {
    data: [],
    title: 'Region'
  },
  serviceProvider: {
    data: [],
    title: 'Service Provider'
  },
  businessOwner: {
    data: [],
    title: 'Business Owner'
  },
  businessOwnerDeputy: {
    data: [],
    title: 'Business Owner Deputy'
  },
  technicalOwner: {
    data: [],
    title: 'Technical Owner'
  },
  technicalOwnerDeputy: {
    data: [],
    title: 'Technical Owner Deputy'
  }
};

export default AddSubscriptionInputs;
