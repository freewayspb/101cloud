import angular from 'angular';

import AddSubscriptionsComponent from './add.subscriptions.component';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.subscriptions.add', {
      url: '/add/',
      component: 'addSubscriptions'
    });
};

config.$inject = ['$stateProvider'];

const addSubscriptions = angular
  .module('app.depconf.subscriptions.add', [])
  .component('addSubscriptions', AddSubscriptionsComponent)
  .config(config)
  .name;

export default addSubscriptions;
