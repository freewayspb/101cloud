import _each from 'lodash/each';
function SubscriptionsController(subscriptionsService, apiService, headerService) {

  const vm = this;

  vm.isLoading = true;
  vm.$onInit = () => {
    headerService.getTenantPromise()
      .then(tenantId => subscriptionsService.get(apiService.subscriptions, {params: {tenantId}}))
      .then(result => {
        _each(result, item => {
          item.period = [item.StartDate, item.EndDate];
        });
        vm.isLoading = false;
        vm.tableOptions = {
          data: result,
          headers: [
            {
              name: 'Name',
              dataProperty: 'Name',
              enableSorting: true
            }, {
              name: 'subscriptions id',
              dataProperty: 'Id',
              enableSorting: true
            }, {
              name: 'role / access',
              dataProperty: 'Role.Role',
              enableSorting: true
            }, {
              name: 'Status',
              dataProperty: 'Status',
              enableSorting: true,
              isCentralized: true,
              cellClass: function(grid, row, col) {
                if (grid.getCellValue(row,col) === 'Inactive') {
                  return 'gray';
                } else if (grid.getCellValue(row,col) === 'Past due') {
                  return 'red';
                }
              }
            },
            {
              name: 'billing period',
              dataProperty: 'period',
              enableSorting: true,
              cellClass: 'period',
              specials: {
                type: 'period'
              }
            }, {
              name: '',
              specials: {
                type: 'actions',
                params: {
                  context: false,
                  edit: true,
                  delete: true
                }
              }
            }
          ],
          sref: 'app.depconf.subscriptions.add'
        };
      });
  };
}

export default SubscriptionsController;
