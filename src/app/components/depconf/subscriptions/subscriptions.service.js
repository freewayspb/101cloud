class SubscriptionsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.steps = [
      {
        title: 'general',
        name: 'general'
      }, {
        title: 'legal',
        name: 'legal'
      }, {
        title: 'checkout',
        name: 'checkout'
      }
    ];
  }

  getItems(param) {
    return this.dataService.getItems(param).then(
      result => result,
      error => {
        console.error(':::: ERROR OCCURRED IN SUBSCRIPTION -> SERVICES ::::', error);
      });
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN DEPCONF -> SUBSCRIPTION -> GET ITEMS ::::', error);
    });
  }
}

export default SubscriptionsService;
