import angular from 'angular';

import SubscriptionsComponent from './subscriptions.component';
import SubscriptionsService from './subscriptions.service';

import AddSubscriptions from './add/add.subscriptions.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.subscriptions', {
      url: '/subscriptions',
      template: '<ui-view></ui-view>',
      redirectTo: 'app.depconf.subscriptions.list'
    })
  .state('app.depconf.subscriptions.list', {
    url: '/list',
    template: '<subscriptions></subscriptions>'
  });
};

const subscriptions = angular
  .module('app.depconf.subscriptions', [
    AddSubscriptions
  ])
  .component('subscriptions', SubscriptionsComponent)
  .service('subscriptionsService', SubscriptionsService)
  .config(config)
  .name;

export default subscriptions;
