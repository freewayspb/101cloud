class DepconfDatacentersService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getListDataCenters(tenantId) {
    return this.dataService.get(this.apiService.basePaths.tenants + `/${tenantId}/Fabrics`)
      .then(
        response => response,
        error => console.error(error)
      );
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN DEPCONF -> DATACENTERS SERVICE ::::', error);
    });
  }
}

export default DepconfDatacentersService;
