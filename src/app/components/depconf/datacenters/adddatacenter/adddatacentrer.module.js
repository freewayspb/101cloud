import angular from 'angular';

import AddDatacenterComponent from '../../../../common/screens/adddatacenter/adddatacenter.component';


const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.datacenters.adddatacenter', {
      url: '/adddatacenter',
      template: '<add-dc-d-datacenter />'
    });
};

config.$inject = ['$stateProvider'];

const addserver = angular
  .module('app.depconf.datacenters.adddatacenter', [])
  .component('addDcDDatacenter', AddDatacenterComponent)
  .config(config)
  .name;

export default addserver;
