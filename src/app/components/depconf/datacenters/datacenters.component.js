import controller from './datacenters.controller';
import template from './datacenters.tpl.html';

const DatacentersComponent = {
  controller,
  template
};

export default DatacentersComponent;
