import angular from 'angular';

import Datacenters from './datacenters.component';
import AddDataCenter from './adddatacenter/adddatacentrer.module';
import DatacentersService from './datacenters.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.datacenters', {
      url: '/datacenters',
      template: '<ui-view />',
      redirectTo: 'app.depconf.datacenters.list'
    })
    .state('app.depconf.datacenters.list', {
      url: '/list',
      template: '<dc-datacenters />'
    });
};

config.$inject = ['$stateProvider'];

const datacenters = angular
  .module('app.depconf.datacenters', [AddDataCenter])
  .component('dcDatacenters', Datacenters)
  .service('depconfDatacentersService', DatacentersService)
  .config(config)
  .name;

export default datacenters;
