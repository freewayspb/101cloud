
function DataCentersController(depconfDatacentersService, headerService, $scope, $filter) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    headerService.getTenantPromise().then(
      id => {
        depconfDatacentersService.getListDataCenters(id).then(
          response => {
            vm.datacentersList = response;
            vm.tableOptions = {
              data: vm.datacentersList,
              headers: [
                {
                  name: 'Name',
                  dataProperty: 'Name',
                  enableSorting: true
                }, {
                  name: 'Country',
                  dataProperty: 'Addresses[0].Country.Name',
                  enableSorting: true
                }, {
                  name: 'City',
                  dataProperty: 'Addresses[0].City',
                  enableSorting: true
                }, {
                  name: 'Infrastructure Provider',
                  dataProperty: 'Provider',
                  enableSorting: true
                }, {
                  name: 'Subscription',
                  dataProperty: 'Subscription',
                  enableSorting: true
                }, {
                  name: '',
                  specials: {
                    type: 'actions',
                    params: {
                      context: false,
                      edit: true,
                      delete: true
                    }
                  }
                }
              ],
              sref: 'app.depconf.datacenters.adddatacenter'
            };
            vm.isLoading = false;
          }, error => console.error(error));
      },
    error => console.warn(error)
    );
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    vm.isLoading = true;
    depconfDatacentersService.getListDataCenters(tenant.Id).then(
      response => {
        vm.isLoading = false;
        vm.datacentersList = response;
        vm.refreshData();
      }
    );
  });

  vm.refreshData = () => {
    if (vm.tableOptions) {
      vm.tableOptions.data = $filter('filter')(vm.datacentersList, vm.searchText);
    }
  };
}

export default DataCentersController;
