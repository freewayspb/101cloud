import controller from './depconf.controller';
import template from './depconf.tpl.html';

const DepconfComponent = {
  controller,
  template
};

export default DepconfComponent;
