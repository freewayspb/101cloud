import controller from './locations.controller';
import template from './locations.tpl.html';

const LocationsComponent = {
  controller,
  template
};

export default LocationsComponent;
