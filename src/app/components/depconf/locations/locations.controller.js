function LocationsController($filter, locationsService, headerService, $scope, alertsService) {
  const vm = this;

  vm.$onInit = () => {
    headerService.getTenantPromise()
      .then(reloadSubscriptions)
      .catch(showError);
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    reloadSubscriptions(tenant.Id);
  });

  function reloadSubscriptions(tenantId) {
    if (vm.isLoading) {
      return;
    }

    vm.isLoading = true;

    locationsService.getListLocations(tenantId)
      .then(response => {
        vm.locations = response;
        vm.tableOptions = {
          data: response,
          headers: locationsService.headers,
          sref: 'app.depconf.locations.add'
        };
        vm.isLoading = false;
      });
  }

  function showError(error) {
    console.error(':::: AN ERRORN OCCURRED IN OFFICE CONTROLLER ::::', error);
    alertsService.setAlert('An error occurred in offices');
  }
}

export default LocationsController;
