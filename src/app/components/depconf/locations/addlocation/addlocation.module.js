import angular from 'angular';

import AddLocationComponent from './addlocation.component';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.locations.add', {
      url: '/add',
      template: '<add-dc-l-location />'
    });
};

config.$inject = ['$stateProvider'];

const addlocation = angular
  .module('app.depconf.locations.add', [])
  .component('addDcLLocation', AddLocationComponent)
  .config(config)
  .name;

export default addlocation;
