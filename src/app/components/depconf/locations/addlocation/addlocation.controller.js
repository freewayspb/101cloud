function addLocationController(addLocationService) {
  this.isFormValid = false;
  this.validate = params => {
    this.isFormValid = params.isValid;
  };
  this.addLocation = () => {
    this.showFullLocation = localStorage.showFullLocation;
    if (localStorage.showFullLocation) {
      this.fullLocation = addLocationService.getLocationPostModel();
    }
  };
}

export default addLocationController;
