import template from './addlocation.tpl.html';
import controller from './addlocation.controller';

const AddLocationComponent = {
  template,
  controller
};

export default AddLocationComponent;
