class LocationsService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.headers = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true
      }, {
        name: 'Country',
        dataProperty: 'Addresses[0].Country.Name',
        enableSorting: true
      }, {
        name: 'City',
        dataProperty: 'Addresses[0].City',
        enableSorting: true
      }, {
        name: 'Address',
        dataProperty: 'Addresses[0].Street',
        enableSorting: true
      }, {
        name: '',
        specials: {
          type: 'actions',
          params: {
            context: false,
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getListLocations(tenantId) {
    return this.dataService.get(this.apiService.basePaths.tenants + '/' + tenantId + '/Offices')
      .then(
        response => response,
        error => console.error(error)
      );
  }
}

export default LocationsService;
