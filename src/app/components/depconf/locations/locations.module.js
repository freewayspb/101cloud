import angular from 'angular';

import LocationsComponent from './locations.component';
import AddLocation from './addlocation/addlocation.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.locations', {
      url: '/locations',
      template: '<ui-view></ui-view>',
      redirectTo: 'app.depconf.locations.list'
    })
    .state('app.depconf.locations.list', {
      url: '/list',
      template: '<locations />'
    });
};

const locations = angular
  .module('app.depconf.locations', [AddLocation])
  .component('locations', LocationsComponent)
  .config(config)
  .name;

export default locations;
