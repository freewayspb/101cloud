import angular from 'angular';

import select from '../../../../common/addservice/steps/select.tpl.html';
import configure from '../../../../common/addservice/steps/configure.tpl.html';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.services.addservice', {
      url: '/addservice',
      template: '<add-service />',
      abstract: true
    })
    .state('app.depconf.services.addservice.select', {
      url: '/select',
      template: select
    })
    .state('app.depconf.services.addservice.configure', {
      url: '/configure',
      template: configure
    });
};

config.$inject = ['$stateProvider'];

const addservice = angular
  .module('app.depconf.services.addservice', [])
  .config(config)
  .name;

export default addservice;
