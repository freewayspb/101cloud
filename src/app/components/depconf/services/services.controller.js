function ServicesController() {
  this.isLoading = true;
  this.$onInit = () => {
    this.isLoading = false;
    this.tableOptions = {
      data: [],
      headers: [
        {
          name: 'Name',
          dataProperty: 'name',
          enableSorting: true
        }, {
          name: 'Description',
          dataProperty: 'description',
          enableSorting: true
        }, {
          name: 'Status',
          dataProperty: 'status',
          enableSorting: false,
          specials: {
            type: 'power'
          },
          isCentralized: true
        }, {
          name: 'Cloud',
          dataProperty: 'cloud',
          enableSorting: true
        }, {
          name: 'Datacenter',
          dataProperty: 'datacenter',
          enableSorting: true
        }, {
          name: 'Infrastructure Provider',
          dataProperty: 'iProvider',
          enableSorting: true
        }, {
          name: '',
          specials: {
            type: 'actions',
            params: {
              context: false,
              edit: true,
              delete: true
            }
          }
        }
      ],
      sref: 'app.depconf.services.addservice.select'
    };
  };
}

export default ServicesController;
