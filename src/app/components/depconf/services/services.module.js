import angular from 'angular';

import ServicesComponent from './services.component';
import ServicesService from './services.service';
import addservice from './addservice/addservice.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.depconf.services', {
      url: '/services',
      template: '<ui-view />',
      redirectTo: 'app.depconf.services.list'
    })
    .state('app.depconf.services.list', {
      url: '/list',
      template: '<dc-services />'
    });
};

config.$inject = ['$stateProvider'];

const services = angular
  .module('app.depconf.services', [addservice])
  .component('dcServices', ServicesComponent)
  .service('depconfServicesService', ServicesService)
  .config(config)
  .name;

export default services;
