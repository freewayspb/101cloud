import controller from './company.controller';
import template from './company.tpl.html';

const CompanyComponent = {
  controller,
  template
};

export default CompanyComponent;
