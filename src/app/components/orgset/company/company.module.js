import angular from 'angular';

import component from './company.component';
import service from './company.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.orgset.company', {
      url: '/company',
      template: '<os-company />'
    });
};

config.$inject = ['$stateProvider'];

const company = angular
  .module('app.orgset.company', [])
  .component('osCompany', component)
  .service('customerService', service)
  .config(config)
  .name;

export default company;
