class BillingCompanyService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> COMPANY GET ITEMS SERVICE ::::', error);
    });
  }

  get(endpoint, params) {
    return this.dataService.get(endpoint, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> COMPANY GET SERVICE ::::', error);
    });
  }

  put(endpoint, payload, params) {
    return this.dataService.put(endpoint, payload, params).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> COMPANY POST SERVICE ::::', error);
    });
  }
}

export default BillingCompanyService;
