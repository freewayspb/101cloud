function CompanyController(addCustomerService, customerService, apiService, headerService, $scope, alertsService) {
  const vm = this;
  this.isLoading = true;
  headerService.getTenantPromise()
    .then(tenant => {
      reloadCompany(tenant);
    });

  this.validate = params => {
    this.isTenantFormValid = params.isValid;
  };

  this.saveCompany = () => {
    this.Tenant = addCustomerService.getCompanyPutModel(vm.initData.CompanyId);
    this.showFullTenant = localStorage.isDevMode;
    customerService.put(apiService.companyInfo, this.Tenant)
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    reloadCompany(tenant);
  });

  function reloadCompany(tenant) {
    vm.isLoading = true;
    if (typeof tenant === 'object') {
      tenant = tenant.Id;
    }

    customerService.get(apiService.companyInfo + tenant)
      .then(result => {
        vm.initData = result;
        vm.isLoading = false;
      }, error => {
        alertsService.setAlert({msg: 'Something happen while loading company for current Tenant', type: 'danger'});
        console.error(':::: ERROR OCCURRED IN COMPANY CONTROLLER ::::', error);
      });
  }
}

export default CompanyController;
