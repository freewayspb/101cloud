import angular from 'angular';

import component from './orgset.component';

import company from './company/company.module';
import contracts from './contracts/contracts.module';

const config = $stateProvider => {
  $stateProvider
    .state('app.orgset', {
      url: '/orgset',
      redirectTo: 'app.orgset.company',
      template: '<orgset></orgset>'
    });
};

config.$inject = ['$stateProvider'];

const orgset = angular
  .module('app.orgset', [company, contracts])
  .component('orgset', component)
  .config(config)
  .name;

export default orgset;
