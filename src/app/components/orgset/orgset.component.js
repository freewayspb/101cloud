import controller from './orgset.controller';
import template from './orgset.tpl.html';

const OrgsetComponent = {
  controller,
  template
};

export default OrgsetComponent;
