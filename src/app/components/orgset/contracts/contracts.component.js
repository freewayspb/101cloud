import controller from './contracts.controller';
import template from './contracts.tpl.html';

const ContractsComponent = {
  controller,
  template
};

export default ContractsComponent;
