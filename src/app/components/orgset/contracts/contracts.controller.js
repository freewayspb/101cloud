function InvoicesController(billingInvoicesService) {
  this.isLoading = true;

  this.$onInit = () => {
    billingInvoicesService.getItems('billing-invoices.json')
      .then(result => {
        this.isLoading = false;
        this.tableOptions = {
          data: result,
          headers: [
            {
              name: 'Service',
              dataProperty: 'service',
              enableSorting: true
            }, {
              name: 'Service Provider',
              dataProperty: 'sProvider',
              enableSorting: true
            }, {
              name: 'Subscription',
              dataProperty: 'subscription',
              enableSorting: true
            }, {
              name: 'Invoice Number',
              dataProperty: 'invNumber',
              enableSorting: true
            }, {
              name: 'Creation Date',
              dataProperty: 'dateCreated',
              enableSorting: true,
              specials: {
                type: 'date'
              }
            }, {
              name: 'Billing Date',
              dataProperty: 'dateBilling',
              enableSorting: true,
              specials: {
                type: 'date'
              }
            }, {
              name: 'Amount',
              dataProperty: 'amount',
              enableSorting: true,
              specials: {
                type: 'price'
              }
            },
            {
              name: '',
              specials: {
                type: 'actions',
                params: {
                  context: false,
                  edit: false,
                  delete: false,
                  view: true,
                  download: true
                }
              }
            }
          ],
          dropdownLabel: 'Billing Date:',
          dropdownOptions: {
            data: [{displayName: 'All'}]
          }
        };
      });
  };
}

export default InvoicesController;
