class OrgsetContractsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN ORGSET -> COMPANY SERVICE ::::', error);
    });
  }
}

export default OrgsetContractsService;
