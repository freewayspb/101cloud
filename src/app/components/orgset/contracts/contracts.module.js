import angular from 'angular';

import component from './contracts.component';
import service from './contracts.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.orgset.contracts', {
      url: '/contracts',
      template: '<ui-view />',
      redirectTo: 'app.orgset.contracts.list'
    })
    .state('app.orgset.contracts.list', {
      url: '/list',
      template: '<os-contracts />'
  });
};

config.$inject = ['$stateProvider'];

const contracts = angular
  .module('app.orgset.contracts', [])
  .component('osContracts', component)
  .service('orgsetContractsService', service)
  .config(config)
  .name;

export default contracts;
