import angular from 'angular';

import addUserComponent from './adduser.component';
import addUserService from './adduser.service';

import IzAddUserGeneralComponent from './steps/general/iz-add-user-general.component';
import IzAddUserWorkInfoComponent from './steps/work-info/iz-add-user-work-info.component';

/*ngInject*/
const config = $stateProvider => {
  $stateProvider
    .state('app.users.all.add', {
      url: '/add',
      template: '<add-user />'
    });
};

const addUsers = angular
  .module('app.users.all.add', [])
  .component('addUser', addUserComponent)
  .component('izAddUserGeneral', IzAddUserGeneralComponent)
  .component('izAddUserWorkInfo', IzAddUserWorkInfoComponent)
  .service('addUserService', addUserService)
  .config(config)
  .name;

export default addUsers;
