function IzAddUserGeneralController(dataService) {
  const vm = this;
  vm.$onInit = () => {
    dataService.getItems('add-user.json').then(
      res => {
        vm.general = res.general;
      },
      error => console.error(error)
    );

  };
}

export default IzAddUserGeneralController;
