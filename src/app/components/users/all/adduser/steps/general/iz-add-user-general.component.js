import controller from './iz-add-user-general.controller';
import template from './iz-add-user-general.tpl.html';

const IzAddUserGeneralComponent = {
  bindings: {
    validate: '&'
  },
  controller,
  template
};

export default IzAddUserGeneralComponent;