const AddUserWorkInfoInputs = {
  department: {
    data: '',
    title: 'Department',
    placeholder: 'Enter Department'
  },
  jobPosition: {
    data: '',
    title: 'Job Position',
    placeholder: 'Enter Job Position'
  },
  street: {
    data: '',
    title: 'Street',
    placeholder: 'Enter Street'
  },
  number: {
    data: '',
    title: 'Numner',
    placeholder: 'Enter Numner'
  },
  zip: {
    data: '',
    title: 'Zip',
    placeholder: 'Enter Zip'
  },
  city: {
    data: '',
    title: 'City',
    placeholder: 'Enter City'
  },
  state: {
    data: '',
    title: 'State',
    placeholder: 'Enter State'
  },
  country: {
    data: [],
    title: 'Country',
    placeholder: 'Enter Country',
    options: {
      requierd: true
    }
  },
  phone1CountryCode: {
    data: [],
    title: 'Country Code',
    placeholder: 'Enter Code',
    options: {
      requierd: true
    }
  },
  phone1RegionCode: {
    data: '',
    title: 'Region Code',
    placeholder: 'Enter Code',
    options: {
      requierd: true
    }
  },
  phone1: {
    data: '',
    title: 'Phone',
    placeholder: 'Enter Phone',
    options: {
      requierd: true
    }
  },
  phone2CountryCode: {
    data: [],
    title: 'Country Code',
    placeholder: 'Enter Code',
    options: {
      requierd: true
    }
  },
  phone2RegionCode: {
    data: '',
    title: 'Region Code',
    placeholder: 'Enter Code',
    options: {
      requierd: true
    }
  },
  phone2: {
    data: '',
    title: 'Phone',
    placeholder: 'Enter Phone',
    options: {
      requierd: true
    }
  }
};

export default AddUserWorkInfoInputs;