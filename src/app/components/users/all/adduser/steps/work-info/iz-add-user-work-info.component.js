import controller from './iz-add-user-work-info.controller';
import template from './iz-add-user-work-info.tpl.html';

const IzAddUserGeneralComponent = {
  bindings: {
    validate: '&'
  },
  controller,
  template
};

export default IzAddUserGeneralComponent;