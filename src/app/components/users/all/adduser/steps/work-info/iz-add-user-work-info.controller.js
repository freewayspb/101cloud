import inputs from './iz-add-user-work-info.inputs';

function IzAddUserWorkInfoController() {
  const vm = this;
  this.inputs = inputs;

  vm.$onInit = () => {
  };
}

export default IzAddUserWorkInfoController;
