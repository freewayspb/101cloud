class AddUserService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.steps = [
      {
        title: 'general',
        name: 'general',
        isDisabled: false
      },
      {
        title: 'work info',
        name: 'work-info',
        isDisabled: false
      },
      // {
      //   title: 'licenses',
      //   name: 'licenses',
      //   isDisabled: false
      // }
    ];
  }

  getItems(item) {
    return this.dataService.getItems(item)
      .then(result => result, error => {
        console.error(':::: ERROR IN OPERATIONS -> ADDDATASERVER SERVICE GETITEMS ::::', error);
      });
  }
}

export default AddUserService;
