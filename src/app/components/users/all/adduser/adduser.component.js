import controller from './adduser.controller';
import template from './adduser.tpl.html';

const AddUserComponent = {
  controller,
  template
};

export default AddUserComponent;
