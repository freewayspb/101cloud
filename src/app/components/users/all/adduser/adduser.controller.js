function AddUserController(addUserService) {
  const vm = this;
  vm.isPostProcessing = false;
  vm.state = 'steps';
  vm.progressbar = addUserService.progressbar = {
    steps: addUserService.steps
  };
}

export default AddUserController;
