function AllUsersController(allUsersService, $scope, headerService) {
  const vm = this;

  this.$onInit = () => {
    headerService.getTenantPromise()
      .then(reloadUsers);
  };

  $scope.$on('onTenantChange', (event, tenant) => {
    reloadUsers(tenant.Id);
  });

  function reloadUsers(tenantId) {
    if (vm.isLoading) {
      return;
    }
    vm.isLoading = true;
    allUsersService.getUsers(tenantId)
      .then(users => {
          const usersWithPhoto = _.map(users, user => {
            const userPhoto = user.ImageBase64
              ? 'data:image/png;base64,' + user.ImageBase64
              : '/app/assets/img/profiles/no-photo.png';

            user.User = {
              Name: user.DisplayName,
              Photo: userPhoto
            };
            return user;
          });
          vm.tableOptions = {
            data: usersWithPhoto,
            headers: allUsersService.tableHeaders,
            dropdownOptions: {
              data: [
                {displayName: 'by role'},
                {displayName: 'by group'}
              ],
              placeholder: 'Filter by'
            },
            sref: 'app.users.all.add'
          };
          vm.isLoading = false;
        },
        error => {
          console.error(':::: ERROR OCCURRED IN ALL USERS CONTROLLER AT reloadUsers() ::::', error);
        });
  }
}

export default AllUsersController;
