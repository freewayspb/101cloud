import controller from './all.controller';
import template from './all.tpl.html';

const DashboardComponent = {
  controller,
  template
};

export default DashboardComponent;
