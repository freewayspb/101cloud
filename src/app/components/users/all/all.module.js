import angular from 'angular';

import component from './all.component';
import service from './all.service';

import addUsers from './adduser/adduser.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.users.all', {
      url: '/all',
      template: '<ui-view />',
      redirectTo: 'app.users.all.list'
    })
    .state('app.users.all.list', {
      url: '/list',
      template: '<all-users-list></all-users-list>'
    });
};

const allUsers = angular
  .module('app.users.all', [addUsers])
  .component('allUsersList', component)
  .service('allUsersService', service)
  .config(config)
  .name;

export default allUsers;
