class AllUsersService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.tableHeaders = [
      {
        name: 'Name',
        dataProperty: 'User',
        enableSorting: true,
        specials: {
          type: 'user-photo',
          params: {
            photo: ''
          }
        }
      }, {
        name: 'Login name',
        dataProperty: 'Mail',
        enableSorting: true,
        isCentralized: true
      }, {
        name: 'status',
        dataProperty: 'AccountEnabled',
        enableSorting: true,
        specials: {
          type: 'status',
          params: {
            userStatus: true
          }
        },
        isCentralized: false
      }, {
        name: '101 Cloud role',
        dataProperty: 'GroupNames',
        enableSorting: true,
        specials: {
          type: 'array'
        }
      }, {
        name: 'X',
        dataProperty: 'X',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            context: false,
            edit: true,
            delete: true
          }
        }
      }
    ];
  }

  getUsers(tenantId) {
    return this.dataService.get(this.apiService.basePaths.users + `/${tenantId}`)
      .then(
        response => response,
        error => console.error(error)
      );
  }
}

export default AllUsersService;
