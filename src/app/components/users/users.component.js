import controller from './users.controller';
import template from './users.tpl.html';

const UsersComponent = {
  controller,
  template
};

export default UsersComponent;
