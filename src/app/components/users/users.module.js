import angular from 'angular';

import component from './users.component';

import usersList from './all/all.module';
import groupList from './groups/groups.module';
import permissions from './permissions/permissions.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.users', {
      url: '/users',
      redirectTo: 'app.users.all.list',
      template: '<users></users>'
    });
};

const users = angular
  .module('app.users', [usersList, groupList, permissions])
  .component('users', component)
  .config(config)
  .name;

export default users;
