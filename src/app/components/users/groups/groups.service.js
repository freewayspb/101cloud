class AllGroupService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.tableHeaders = [
      {
        name: 'Name',
        dataProperty: 'Name',
        enableSorting: true
      }, {
        name: 'Description',
        dataProperty: 'Description',
        enableSorting: true,
        isCentralized: true
      }, {
        name: 'Group type',
        dataProperty: 'Grouptype',
        enableSorting: true,
        isCentralized: true
      }, {
        name: 'Membership type',
        dataProperty: 'Membershiptype',
        enableSorting: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            context: false,
            edit: true,
            delete: true
          }
        }
      }
    ];
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> DASHBOARD SERVICE ::::', error);
      throw error;
    });
  }
}

export default AllGroupService;
