import angular from 'angular';

import component from './groups.component';
import service from './groups.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.users.groups', {
      url: '/groups',
      template: '<ui-view />',
      redirectTo: 'app.users.groups.list'
    })
    .state('app.users.groups.list', {
      url: '/list',
      template: '<all-groups-list></all-groups-list>'
    });
};

const allGroups = angular
  .module('app.users.groups', [])
  .component('allGroupsList', component)
  .service('allGroupsService', service)
  .config(config)
  .name;

export default allGroups;
