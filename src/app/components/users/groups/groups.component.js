import controller from './groups.controller';
import template from './groups.tpl.html';

const DashboardComponent = {
  controller,
  template
};

export default DashboardComponent;
