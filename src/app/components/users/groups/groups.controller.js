function AllGroupsController(allGroupsService) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    allGroupsService.getItems('test_groups.json')
      .then(result => {
        vm.isLoading = false;
        vm.tableOptions = {
          data: result,
          headers: allGroupsService.tableHeaders,
          dropdownOptions: {
            data: [{displayName: 'by group type'},
              {displayName: 'by membership type'}],
              placeholder: 'Filter by'
          },
          sref: 'app.users.groups.add'
        };
      });

  };
}

export default AllGroupsController;
