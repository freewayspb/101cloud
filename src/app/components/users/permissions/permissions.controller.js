function PermissionsController(permissionsService) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    permissionsService.getItems('test_permissions.json')
      .then(result => {
        vm.isLoading = false;
        vm.tableOptions = {
          data: result,
          headers: permissionsService.tableHeaders,
          sref: 'app.users.permissions.add'
        };
      });

  };
}

export default PermissionsController;
