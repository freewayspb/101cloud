import controller from './permissions.controller';
import template from './permissions.tpl.html';

const DashboardComponent = {
  controller,
  template
};

export default DashboardComponent;
