import angular from 'angular';

import component from './permissions.component';
import service from './permissions.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.users.permissions', {
      url: '/permissions',
      template: '<ui-view />',
      redirectTo: 'app.users.permissions.list'
    })
    .state('app.users.permissions.list', {
      url: '/list',
      template: '<permissions-list></permissions-list>'
    });
};

const permissions = angular
  .module('app.users.permissions', [])
  .component('permissionsList', component)
  .service('permissionsService', service)
  .config(config)
  .name;

export default permissions;
