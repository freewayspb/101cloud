class permissionsService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
    this.tableHeaders = [
      {
        name: 'User role',
        dataProperty: 'Userrole',
        enableSorting: true
      }, {
        name: 'Description',
        dataProperty: 'Description',
        enableSorting: true,
        isCentralized: true
      }, {
        name: 'status',
        dataProperty: 'Status',
        enableSorting: true,
        specials: {
          type: 'switch'
        },
        isCentralized: true
      },
      {
        name: '',
        specials: {
          type: 'actions',
          params: {
            context: false,
            edit: true,
            delete: true
          }
        }
      }
    ];
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> DASHBOARD SERVICE ::::', error);
      throw error;
    });
  }
}

export default permissionsService;
