import angular from 'angular';
import ngCookies from 'angular-cookies';

import DashboardComponent from './dashboard.component';
import DashboardService from './dashboard.service';

const dashboard =  angular
  .module('dashboard', [ngCookies])
  .component('dashboard', DashboardComponent)
  .service('dashboardService', DashboardService)
  .name;

export default dashboard;
