function DashboardController(dashboardService) {
  /*ngInject*/
  const vm = this;
  vm.dashboard = {
    datacenters: [],
    map: {
      pushpins: dashboardService.pushpins
    }
  };
  vm.isLoading = true;
  vm.$onInit = () => {
    dashboardService.getMapObjects();
    vm.isLoading = false;
  };
}

export default DashboardController;
