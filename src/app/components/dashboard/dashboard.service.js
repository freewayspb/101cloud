import _each from 'lodash/each';
class DashboardService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
    this.pushpins = [];
  }

  getMapObjects() {
    this.dataService.get(this.apiService.basePaths.cloudDatacenters).then(
      items => {
        _each(items, item => {
          const pushpin = this.parsePinOptions(item);
          this.pushpins.push(pushpin);
        });
      },
      error => console.error(error)
    );
    this.dataService.get(this.apiService.basePaths.fullDatacenters).then(
      response => {
        _each(response, item => {
          const pushpin = this.parsePinOptions(item);
          this.pushpins.push(pushpin);
        });
      },
    error => console.error(error)
    );
  }

  parsePinOptions(data) {
    return {
      location: {
        latitude: (data.Address && data.Address.Latitude) || data.Addresses[0].Latitude,
        longitude: (data.Address && data.Address.Longitude) || data.Addresses[0].Longitude},
      options: {
        draggable: false,
        data: data.Name,
        width: null,
        height: null,
        htmlContent: this.selectIcon(data)
      }
    };
  }

  selectIcon(type) {
    const checkType = type.Hoster ? type.Hoster.Short : null;
    switch(checkType) {
      case 'Azure':
        return `<div class="pushpin-azure"></div>`;
      case 'AWS':
        return `<div class="pushpin-aws"></div>`;
      default:
        return `<div class="pushpin-other"></div>`;
    }
  }
}

export default DashboardService;

