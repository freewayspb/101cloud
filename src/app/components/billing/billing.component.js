import controller from './billing.controller';
import template from './billing.tpl.html';

const BillingComponent = {
  controller,
  template
};

export default BillingComponent;
