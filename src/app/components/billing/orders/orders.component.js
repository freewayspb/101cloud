import controller from './orders.controller';
import template from './orders.tpl.html';

const InvoicesComponent = {
  controller,
  template
};

export default InvoicesComponent;
