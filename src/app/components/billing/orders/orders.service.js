class BillingOrdersService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getOrders(tenantId) {
    const params = {
      params: {
        tenantId: tenantId
      }
    };
    return this.dataService.get(this.apiService.basePaths.orders, params)
      .then( response => response,
      error => console.error(':::: ERROR OCCURRED IN BILLING -> DASHBOARD SERVICE ::::' + error));
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> DASHBOARD SERVICE ::::', error);
    });
  }
}

export default BillingOrdersService;
