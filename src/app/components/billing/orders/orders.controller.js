import _ from 'lodash';
/* ngInject */
function OrdersController(billingOrdersService, headerService, $scope, $filter) {
  const vm = this;
  vm.isLoading = true;

  vm.$onInit = () => {
    headerService.getTenantPromise().then(tenantId => {
      billingOrdersService.getOrders(tenantId)
        .then(response => {
          vm.orders = _.sortBy(response, 'CreationDate').reverse();
          vm.tableOptions = {
            data: vm.orders,
            headers: [
              {
                name: 'Order#',
                dataProperty: 'OrderNumber',
                enableSorting: true
              },{
                name: 'Supplier Name',
                dataProperty: 'SupplierName',
                enableSorting: true
              }, {
                name: 'Product',
                dataProperty: 'ProductName',
                enableSorting: true
              }, {
                name: 'QTY',
                dataProperty: 'Quantity',
                enableSorting: true
              }, {
                name: 'Amount',
                dataProperty: 'Amount',
                enableSorting: true,
                specials: {
                  type: 'price'
                }
              }, {
                name: 'Creation Date',
                dataProperty: 'CreationDate',
                enableSorting: true,
                specials: {
                  type: 'date',
                  params: {
                    format: 'dd.MM.yyyy'
                  }
                }
              }, {
                name: 'Payment',
                dataProperty: 'PaymentByContract',
                enableSorting: true,
                specials: {
                  type: 'payment'
                }
              }, {
                name: 'Invoice Date',
                dataProperty: 'InvoiceDate',
                enableSorting: true,
                specials: {
                  type: 'date'
                }
              }, {
                name: 'Due Date',
                dataProperty: 'DueDate',
                enableSorting: true,
                specials: {
                  type: 'date'
                }
              }, {
                name: 'Payment status',
                dataProperty: 'Status',
                enableSorting: true,
                specials: {
                  type: 'status',
                  params: {
                    icons: true
                  }
                }
              }
            ],
            dropdownLabel: 'Order Date:',
            dropdownOptions: {
              data: [{displayName: 'All'}],
              selected: {displayName: 'All'}
            }
          };
          $scope.$on('onTenantChange', (event, tenant) => {
            vm.isLoading = true;
            billingOrdersService.getOrders(tenant.Id).then(
              orders => {
                vm.isLoading = false;
                vm.orders = orders;
                vm.refreshData();
              }
            );
          });
          vm.isLoading = false;
        });
    },
      error => {
        console.error(error);
        vm.isLoading = false;
      });
  };

  vm.refreshData = () => {
    if (vm.tableOptions) {
      vm.tableOptions.data = $filter('filter')(vm.orders, vm.searchText);
    }
  };
}

export default OrdersController;
