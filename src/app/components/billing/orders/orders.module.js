import angular from 'angular';

import component from './orders.component';
import service from './orders.service';

const config = $stateProvider => {
  $stateProvider
    .state('app.billing.orders', {
      url: '/orders',
      template: '<ui-view />',
      redirectTo: 'app.billing.orders.list'
    })
    .state('app.billing.orders.list', {
      url: '/list',
      template: '<orders-list />'
  });
};

config.$inject = ['$stateProvider'];

const invoices = angular
  .module('app.billing.orders', [])
  .component('ordersList', component)
  .service('billingOrdersService', service)
  .config(config)
  .name;

export default invoices;
