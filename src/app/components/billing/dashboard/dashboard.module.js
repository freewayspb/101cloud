import angular from 'angular';

import component from './dashboard.component';
import service from './dashboard.service';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.billing.dashboard', {
      url: '/dashboard',
      template: '<b-dashboard />'
    });
};

const dashboard = angular
  .module('app.billing.dashboard', [])
  .component('bDashboard', component)
  .service('billingDashboardService', service)
  .config(config)
  .name;

export default dashboard;
