import tableDetailedHeaders from './dashboard.table.detailed';

function DashboardController(billingDashboardService, alertsService) {
  const vm = this;
  // TODO make normal logic (try to replace primitive values, not reference)
  this.$onInit = () => {

    this.getGraph('year')
      .then(result => {
        this.dashboard = result;
      });

    this.getGraph('year', 'licenses')
      .then(result => {
        this.detailedDashboard = result;
        this.detailedDashboard.options = {
          isDetails: true
        };
      });

  };

  this.getGraph = (period, entity) => {
    entity = entity ? entity + '-' : '';
    return billingDashboardService.getItems('billing-dashboard-' + entity + period + '.json')
      .then(result => result,
        error => {
        alertsService.setAlert({msg: 'Cannot set graph.', type: 'danger'});
        console.error(':::: ERROR OCCURED IN BILLING DASHBOARD CONTROLLER WHILE FETCHING GRAPHS ::::', error);
        throw error;
      });
  };

  this.selectCallback = params => {
    let storage;
    if (params.params.entity) {
      storage = this.detailedDashboard;
      this.detailedDashboard = undefined;
      storage.entity = 'detailedDashboard';
    } else {
      storage = this.dashboard;
      this.dashboard = undefined;
      storage.entity = 'dashboard';
    }
    this.getGraph(params.params.period, params.params.entity)
      .then(result => {
        if (params.params.entity) {
          this.detailedDashboard = result || {};
          this.detailedDashboard.options = {
            isDetails: true,
            period: params.params.period,
            entity: params.params.entity
          };
        } else {
          this.dashboard = result;
          this.dashboard.options = {
            period: params.params.period
          };
        }
      }, () => {
        this[storage.entity] = storage;
      });
  };

  this.tableDataSelect = params => {

  };

  vm.tableOptions = undefined;
  billingDashboardService.getItems('billing-drill-down-licenses-year.json')
    .then(result => {
      vm.tableOptions = {
        tableOptions: {
          data: result,
          headers: tableDetailedHeaders,
          hideHeader: true,
          isToolbarHidden: true,
          rowHeight: 50
        },
        title: 'Drill Down by Services',
        options: {
          isDetails: true
        }
      };
      if (result.status - 200 > 100) {
        vm.tableOptions.error = result;
      }
    });
}

export default DashboardController;
