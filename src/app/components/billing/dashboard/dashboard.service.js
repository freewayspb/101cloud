class BillingDashboardService {

  /*@ngInject*/

  constructor(dataService) {
    this.dataService = dataService;
  }
  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> DASHBOARD SERVICE ::::', error);
      throw error;
    });
  }
}

export default BillingDashboardService;
