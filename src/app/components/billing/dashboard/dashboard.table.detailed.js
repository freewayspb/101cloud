const DashboardTableDetailedHeaders = [
  {
    name: 'License',
    dataProperty: 'Name',
    enableSorting: true,
    cellClass: 'billing-dashboard__table-data'
  }, {
    name: 'Level Consumption',
    dataProperty: 'LevelConsumption',
    enableSorting: true,
    cellClass: 'billing-dashboard__table-data',
    specials: {
      type: 'status',
      params: {
        icons: false
      }
    }
  }, {
    name: 'Saving Potential',
    dataProperty: 'SavingPotential',
    enableSorting: true,
    cellClass: 'billing-dashboard__table-data',
    specials: {
      type: 'price'
    }
  }, {
    name: 'Subscriptions',
    dataProperty: 'Subscriptions',
    enableSorting: true,
    cellClass: 'billing-dashboard__table-data',
    specials: {
      type: 'dashboard-subscription'
    }
  }, {
    name: '',
    dataProperty: 'Graph',
    enableSorting: false,
    specials: {
      type: 'dashboard-graph'
    }
  }, {
    name: 'Best',
    dataProperty: 'Best',
    enableSorting: true,
    cellClass: 'billing-dashboard__centralize',
    specials: {
      type: 'dashboard-extremum'
    }
  }, {
    name: 'Worst',
    dataProperty: 'Worst',
    enableSorting: true,
    cellClass: 'billing-dashboard__centralize',
    specials: {
      type: 'dashboard-extremum'
    }
  }
];

export default DashboardTableDetailedHeaders;