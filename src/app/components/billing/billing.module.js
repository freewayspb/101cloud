import angular from 'angular';

import component from './billing.component';

import dashboard from './dashboard/dashboard.module';
import invoices from './invoices/invoices.module';
import orders from './orders/orders.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.billing', {
      url: '/billing',
      redirectTo: 'app.billing.dashboard',
      template: '<billing></billing>'
    });
};

const billing = angular
  .module('app.billing', [dashboard, invoices, orders])
  .component('billing', component)
  .config(config)
  .name;

export default billing;
