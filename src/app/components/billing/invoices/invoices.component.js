import controller from './invoices.controller';
import template from './invoices.tpl.html';

const InvoicesComponent = {
  controller,
  template
};

export default InvoicesComponent;
