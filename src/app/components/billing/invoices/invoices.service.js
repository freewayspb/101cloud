class BillingInvoicesService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getInvoices(tenantId) {
    return this.dataService.get(this.apiService.basePaths.billingInvoices + '/' + tenantId)
      .then(response => response, error => console.error(error));
  }

  getItems(param) {
    return this.dataService.getItems(param).then(result => result, error => {
      console.error(':::: ERROR OCCURRED IN BILLING -> DASHBOARD SERVICE ::::', error);
    });
  }

  generateInvoices(tenantId) {
    return this.dataService.post(this.apiService.basePaths.billingInvoices+'/' + tenantId,
      {tenantId: tenantId}).then(
      response => response,
      error => console.error(error)
    );
  }
}

export default BillingInvoicesService;
