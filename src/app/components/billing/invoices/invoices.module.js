import angular from 'angular';

import component from './invoices.component';
import service from './invoices.service';

import viewInvoice from './view-invoice/view-invoice.module';

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.billing.invoices', {
      url: '/invoices',
      template: '<ui-view />',
      redirectTo: 'app.billing.invoices.list'
    })
    .state('app.billing.invoices.list', {
      url: '/list',
      template: '<b-invoices></b-invoices>'
  });
};

const bInvoices = angular
  .module('app.billing.invoices', [viewInvoice])
  .component('bInvoices', component)
  .service('billingInvoicesService', service)
  .config(config)
  .name;

export default bInvoices;
