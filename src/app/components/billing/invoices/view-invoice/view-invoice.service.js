class billingViewInvoicesService {

  /*@ngInject*/

  constructor(dataService, apiService) {
    this.dataService = dataService;
    this.apiService = apiService;
  }

  getInvoice(invoiceId) {
    return this.dataService.get(this.apiService.basePaths.billingInvoices + '/Invoice/' + invoiceId)
      .then(response => response, error => console.error(error));
  }
}

export default billingViewInvoicesService;
