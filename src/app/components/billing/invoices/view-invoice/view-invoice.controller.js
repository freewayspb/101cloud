function InvoicesController($stateParams, billingViewInvoicesService, $state, $scope) {
  const vm = this;
  vm.isLoading = true;
  this.$onInit = () => {
    vm.invoiceId = $stateParams.invoiceId;
    billingViewInvoicesService.getInvoice(vm.invoiceId).then(
      invoice => {
        vm.pdfLink = invoice.InvoiceFileUrl;
        vm.isLoading = false;
      },
      error => {
        console.error(error);
        vm.isLoading = false;
      }
    );
  };
  vm.goBack = () => {
    $state.go('app.billing.invoices');
  };
  vm.pdfOptions = {
    mouseZoom: false,
    mousePan:  true
  };
}

export default InvoicesController;
