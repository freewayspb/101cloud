import controller from './view-invoice.controller';
import template from './view-invoices.tpl.html';

const ViewInvoiceComponent = {
  controller,
  template
};

export default ViewInvoiceComponent;
