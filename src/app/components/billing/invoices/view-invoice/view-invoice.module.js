import angular from 'angular';

import component from './view-invoice.component';
import service from './view-invoice.service';
require('../../../../../../node_modules/angular-pdfjs/dist/angular-pdfjs.min');

const config = $stateProvider => {
  /*ngInject*/
  $stateProvider
    .state('app.billing.invoices.view-invoice', {
      url: '/viewinvoice/:invoiceId',
      template: '<b-view-invoice />'
    });
};

const viewInvoices = angular
  .module('app.billing.invoices.view-invoice', ['angular-pdfjs'])
  .component('bViewInvoice', component)
  .service('billingViewInvoicesService', service)
  .config(config)
  .name;

export default viewInvoices;
