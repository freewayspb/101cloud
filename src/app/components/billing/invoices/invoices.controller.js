import _ from 'lodash';
function InvoicesController(headerService, billingInvoicesService, $scope, $filter, moment) {
  const vm = this;
  vm.isLoading = true;
  vm.$onInit = () => {
    vm.incoming = [];
    vm.outgoing = [];
    headerService.getTenantPromise().then(tenantId => {
      billingInvoicesService.getInvoices(tenantId).then(response => {
          response = _.sortBy(response, 'CreationDate').reverse();
        _.each(response, item => {
          if(item.InvoiceType === 1) {
            vm.incoming.push(item);
          } else {
            vm.outgoing.push(item);
          }
        });
          vm.tableOptions = {
          tables: [{
            show: 'INCOMING',
            data: vm.incoming,
            headers: [
              {
                name: 'reseller',
                dataProperty: 'ResellerName',
                enableSorting: true
              }, {
                name: 'Service provider',
                dataProperty: 'ServiceTypeName',
                enableSorting: true
              }, {
                name: 'Subscription',
                dataProperty: 'Subscription',
                enableSorting: true
              }, {
                name: 'Invoice #',
                dataProperty: 'InvoiceNumber',
                enableSorting: true
              }, {
                name: 'Order #',
                dataProperty: 'Orders',
                enableSorting: true,
                specials: {
                  type: 'array'
                }
              }, {
                name: 'Creation Date',
                dataProperty: 'CreationDate',
                enableSorting: true,
                specials: {
                  type: 'date'
                }
              }, {
                name: 'Billing Date',
                dataProperty: 'BillingDate',
                enableSorting: true,
                specials: {
                  type: 'date'
                }
              }, {
                name: 'Amount',
                dataProperty: 'Amount',
                enableSorting: true,
                specials: {
                  type: 'price'
                }
              },
              {
                name: '',
                specials: {
                  type: 'actions',
                  params: {
                    context: false,
                    edit: false,
                    delete: false,
                    view: true,
                    download: true
                  }
                }
              }
            ]
          }, {
              show: 'OUTGOING',
              data: vm.outgoing,
              enableRowSelection: true,
              enableSelectAll: true,
              headers: [
                {
                  name: 'customer',
                  dataProperty: 'ResellerName',
                  enableSorting: true
                }, {
                  name: 'Service',
                  dataProperty: 'ServiceTypeName',
                  enableSorting: true
                }, {
                  name: 'Subscription',
                  dataProperty: 'Subscription',
                  enableSorting: true
                }, {
                  name: 'Invoice #',
                  dataProperty: 'InvoiceNumber',
                  enableSorting: true
                }, {
                  name: 'Order #',
                  dataProperty: 'Orders',
                  enableSorting: true,
                  specials: {
                    type: 'array'
                  }
                }, {
                  name: 'Creation Date',
                  dataProperty: 'CreationDate',
                  enableSorting: true,
                  specials: {
                    type: 'date'
                  }
                }, {
                  name: 'Billing Date',
                  dataProperty: 'BillingDate',
                  enableSorting: true,
                  specials: {
                    type: 'date'
                  }
                }, {
                  name: 'Amount',
                  dataProperty: 'Amount',
                  enableSorting: true,
                  specials: {
                    type: 'price'
                  }
                }, {
                  name: '',
                  specials: {
                    type: 'actions',
                    params: {
                      context: false,
                      edit: false,
                      delete: false,
                      view: true,
                      download: true
                    }
                  }
                }
              ]
            }],
          dropdownLabel: 'Billing Date:',
          dropdownOptions: {
            data: [
              {displayName: 'All'},
              {displayName: 'This month'},
              {displayName: 'Last month'},
              {displayName: 'Post 3 month'},
              {displayName: 'Post 6 month'},
              {displayName: 'Post year'}
            ],
            selected: {displayName: 'All'}
          },
          dropdownCallbacks: vm.dropdownCallbacks,
          checkboxes: {
            checked: 'INCOMING',
            class: 'table-checkbox-group',
            data: [
            {displayName: 'INCOMING'},
            {displayName: 'OUTGOING'}
          ]}
          // ,
          // actions: [
          //   {
          //     displayName: 'GENERATE',
          //     class: 'btn-success',
          //     action: () => {
          //       billingInvoicesService.generateInvoices(res);
          //     }
          //   }
          // ]
        };
          $scope.$on('onTenantChange', (event, tenant) => {
            vm.isLoading = true;
            billingInvoicesService.getInvoices(tenant.Id).then(
              response => {
                vm.incoming = [];
                vm.outgoing = [];
                _.each(response, item => {
                  if(item.InvoiceType === 1) {
                    vm.incoming.push(item);
                  } else {
                    vm.outgoing.push(item);
                  }
                });
                vm.refreshData();
                vm.isLoading = false;
              }
            );
          });
          vm.isLoading = false;
      },
        error => {
          console.error(error);
          vm.isLoading = false;
        });
    },
    error => {
      console.error(error);
      vm.isLoading = false;
    });
  };

  vm.refreshData = () => {
    if (vm.tableOptions) {
      _.each(vm.tableOptions.tables, table => {
        if(vm.incoming && table.show === 'INCOMING') {
          table.data = $filter('filter')(vm.incoming, vm.searchText);
        } else if (vm.outgoing && table.show === 'OUTGOING') {
          table.data = $filter('filter')(vm.outgoing, vm.searchText);
        }
      });
    }
  };
  vm.updateInvoices = () => {
    //console.log(moment().subtract(1, 'year'));
  };
  vm.dropdownCallbacks = {
    onChange: vm.updateInvoices
  };


}

export default InvoicesController;
