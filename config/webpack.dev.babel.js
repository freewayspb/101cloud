import path from 'path';
import webpack from 'webpack';
import webpackMerge from 'webpack-merge';
import common from './webpack.common.babel';

import UglifyJSPlugin from 'uglifyjs-webpack-plugin';
import OpenBrowserWebpackPlugin from 'open-browser-webpack-plugin';

const config = webpackMerge(common, {
  devtool: 'eval-source-map',
  output: {
    path: path.resolve('./dist'),
    publicPath: '/',
    filename: '[name].[hash].js',
    sourceMapFilename: '[name].[hash].map'
  },
  plugins: [
    new OpenBrowserWebpackPlugin({
      url: 'https://localhost:8080'
    }),
    new UglifyJSPlugin({
      compress: {warnings: false},
      output: {comments: false},
      mangle: {
        except: ['$super', '$', 'exports', 'require', 'import']
      },
      minimize: true,
      sourceMap: true
    })
  ],
  devServer: {
    historyApiFallback: true,
    stats: 'minimal'
  }
});

export default config;
