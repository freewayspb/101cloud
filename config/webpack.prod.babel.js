import path from 'path';
import webpack from 'webpack';
import webpackMerge from 'webpack-merge';
import common from './webpack.common.babel';

import CleanWebpackPlugin from 'clean-webpack-plugin';

const config = webpackMerge(common, {
  devtool: false,
  output: {
    path: path.resolve('./dist'),
    publicPath: '/',
    filename: '[name].[hash].bundle.js',
    sourceMapFilename: '[name].[hash].map'
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: process.cwd(),
      exclude: ['web.config']
    }),
    new webpack.DefinePlugin({
      'process.env':{
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      mangle: false
    })
  ]
});

export default config;
