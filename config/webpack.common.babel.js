import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const autoprefixer = require('autoprefixer');
const nodeEnv = process.env.NODE_ENV || 'development';
const isProduction = nodeEnv === 'production';
const buildPath = path.join(__dirname, './dist');
const imgPath = path.join(__dirname, './app/assets/img');
const sourcePath = path.join(__dirname, './dist');
const DashboardPlugin = require('webpack-dashboard/plugin');


const config = {
  entry: {
    'app': path.resolve('./src/app/app')
  },
  resolve: {
    extensions: ['.js', '.tpl.html'],
    modules: [
      'node_modules'
    ]
  },
  performance: {
    hints: false,
    maxEntrypointSize: 400000
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader'
      }]
    }, {
      test: /\.scss$/,
      loaders: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader']
    }, {
      test: /\.tpl.html/,
      use: [{
        loader: 'html-loader'
      }]
    }, {
      test: /\.json$/,
      use: 'file-loader?limit=5000000&name=app/assets/json/[name].[ext]'
    }, {
      test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
      use: 'file-loader?limit=5000000&name=app/assets/img/[name].[ext]'
    },{
      test: /.*\.(gif|png|jpe?g)$/i,
      loaders: [
        'file-loader',
        {
          loader: 'image-webpack-loader',
          query: {
            mozjpeg: {
              progressive: true,
            },
            gifsicle: {
              interlaced: false,
            },
            optipng: {
              optimizationLevel: 4,
            },
            pngquant: {
              quality: '75-90',
              speed: 3
            }
          }
        }
      ]
    }, {
      test: /\.config$/,
      use: 'file-loader?name=[name].[ext]'
    }]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].[hash].css',
      disable: false,
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      title: '101 Cloud'
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer()
        ]
      }
    }),
    new DashboardPlugin()
  ],
  node: {
    global: true,
    crypto: 'empty',
    process: true,
    module: false,
    clearImmediate: false,
    setImmediate: false
  }
};

export default config;
