# 101-cloud
101 Cloud admin panel

### Setup

These instructions will help you set up this project locally in order to modify it directly.

#### Installation

1. Install the following utilities:
    - [Git](http://git-scm.com/downloads): a free and open source distributed version control system.
        - Unfamiliar with Git? I recommend using [GitHub for Windows](https://windows.github.com/) or [Github for Mac](https://mac.github.com/).
    - [Node.js](http://nodejs.org/download/): a software platform that is used to build server-side applications.

2. Clone this repository to wherever you prefer to keep your projects:

        $ git clone git@bitbucket.org:izinformatikzentrumag/101-cloud-ux.git


3. Navigate to where you've installed 101-cloud-app:

        $ cd ~/.../101-cloud-ux

4. Install npm dependencies
        
        $ npm install

#### Running the App

- Run the app from your localhost:

        $ npm run server

- The development harness should automatically install all dependencies and open in your default browser. If not, visit [https://localhost:8080/](https://localhost:8080/) in your preferred browser.


Copyright 2016-2017 101 Cloud